 Xexpressao
"uBdSinc = SQL_DB
=Tts_criacampo("tts_qt","tabela","C","Queue de Transa��es","''",20,0,uBdSinc)
=Tts_criacampo("tts_qt","tipo","N","Queue de Transa��es","0",2,0,uBdSinc)
=Tts_criacampo("tts_qt","bddestino","C","Queue de Transa��es","''",60,0,uBdSinc)
=Tts_criacampo("tts_qt","stamp","C","Queue de Transa��es","''",25,0,uBdSinc)
=Tts_criacampo("tts_qt","tctlstamp","C","Queue de Transa��es","''",25,0,uBdSinc)
=Tts_criacampo("tts_qt","ins","L","Queue de Transa��es","''",20,0,uBdSinc)
=Tts_criacampo("tts_qt","del","L","Queue de Transa��es","''",20,0,uBdSinc)
=Tts_criacampo("tts_qt","upd","L","Queue de Transa��es","''",20,0,uBdSinc)
=Tts_criacampo("tts_qt","data","D","Queue de Transa��es","''",10,0,uBdSinc)
=Tts_criacampo("tts_qt","hora","C","Queue de Transa��es","''",10,0,uBdSinc)
=Tts_criacampo("tts_qt","email","L","Queue de Transa��es","''",10,0,uBdSinc)
=Tts_criacampo("tts_qt","id","Y","Queue de Transa��es","''",10,0,uBdSinc)

Select u_tctl
Go Top
Do While !Eof()
	uTabela=""
	Do Case
		Case u_tctl.tipo = 1 Or u_tctl.tipo = 2
			uTabela = "FT"
			uCondDoc = "FT.ndoc=" + Alltrim(Str(u_tctl.orindoc))
		Case u_tctl.tipo = 3
			uTabela = "BO"
			uCondDoc = "BO.ndos=" + Alltrim(Str(u_tctl.orindoc))
		Case u_tctl.tipo = 4
			uTabela = "PO"
			uCondDoc = "PO.process = 1 And PO.cm=" + Alltrim(Str(u_tctl.orindoc))
	Endcase

	uCondicao = " Where " + Alltrim(uTabela) + ".no=" + Alltrim(Str(u_tct.orino)) + " And " + Alltrim(uTabela) + ".estab = " + Alltrim(Str(u_tct.oriestab)) 
	uCondicao = uCondicao + " And " + uCondDoc
	uCondicao = uCondicao + Iif(!Empty(u_tctl.oriwhere)," And " + Alltrim(u_tctl.oriwhere),"")

	Tts_Cria_triggers_automaticos_sinc(u_tctl.u_tctlstamp,uTabela,Alltrim(Str(u_tctl.tipo)),uCondicao,u_tct.bdados)

	Select u_tctl
	Skip
Enddo

Msg("Acabei!!")


Procedure Tts_Cria_triggers_automaticos_sinc
	Lparameters uStamp,uTabela,uTipo,uCondicoesTr,uBaseDadosDestino

	uTabela = Alltrim(uTabela)
	uStamp = Strtran(Alltrim(uStamp)," ","")

	If Empty(uCondicoesTr)
		uCondicoesTr = ""
	Endif

	Tts_Cria_triggers_sinc(uTabela,uStamp,"QT_"+Strtran(uStamp,",","")+"_"+uTabela+"_Sinc_" +uBaseDadosDestino,uTipo,uBaseDadosDestino,uCondicoesTr)

Endproc

Procedure Tts_Cria_triggers_sinc
	Lparameters uTabela,uStamp,uNome,uTipo,uBDDestino,uCondicoesTr

	uTabela = Alltrim(Upper(uTabela))
	uTabelaDest = Alltrim(uTabela)
	uBDDestino = Alltrim(uBDDestino)

	uDeclara = ""

	uIDENTITY_INSERT_OFF = ""
	uIDENTITY_INSERT_ON = ""
	If Alltrim(Upper(uTabela))=="USQL" And !Like("BATCH*",Alltrim(Upper(uBDDestino)))
		uIDENTITY_INSERT_OFF = "SET IDENTITY_INSERT " + uBDDestino + "..USQL OFF"
		uIDENTITY_INSERT_ON = "SET IDENTITY_INSERT  " + uBDDestino + "..USQL ON"
	Endif

	uScpInsMemo = ""
	uScpUpdMemo = ""

	uTabelaAux = uTabela + "."

	TEXT TO uSql TEXTMERGE NOSHOW
		IF EXISTS (SELECT name FROM sysobjects
		      WHERE name = 'Tts_INS_<<uNome>>' AND type = 'TR')
		   DROP TRIGGER Tts_INS_<<uNome>>
	ENDTEXT

	If u_sqlexec(uSql)
	Endif

	uCondicoesTrAux = uCondicoesTr
	uCondicoesDelAux = uCondicoesTr	

	If !Empty(uCondicoesTrAux)
		uCondicoesIns = uCondicoesTrAux
	Else
		uCondicoesIns = ""
	Endif

	If !Empty(uCondicoesDelAux)			
		uCondicoesDel = uCondicoesDelAux 
	Else
		uCondicoesDel = ""
	Endif

	TEXT TO uSqlI TEXTMERGE noshow
		Create TRIGGER Tts_INS_<<uNome>>
		on <<uTabela>>
		for insert
		as
		BEGIN
	ENDTEXT

	TEXT TO uSqlM TEXTMERGE noshow

		INSERT INTO tts_qt (tabela,bddestino,stamp,tctlstamp,ins,data,hora,tipo )
		Select '<<uTabela>>' as tabela,'<<uBDDestino>>' as bddestino
			,inserted.<<uTabela>>Stamp as stamp,'<<uStamp>>' as tctlstamp
			,1,getdate(),right(convert(char(20),getdate(),113),8),<<uTipo>> 
			From inserted
			<<IIF(!EMPTY(uCondicoesIns),ALLTRIM(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesIns))," "+uTabelaAux," inserted."),"."+uTabelaAux,".inserted."),"("+uTabelaAux,"(inserted.")),"")>>
	ENDTEXT

*!*			MESSAGEBOX(uCondicoesIns)
*!*			MESSAGEBOX(uTabelaAux)
*!*			MESSAGEBOX(IIF(!EMPTY(uCondicoesIns)," And " + ALLTRIM(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesIns))," "+uTabelaAux," inserted."),"."+uTabelaAux,".inserted."),"("+uTabelaAux,"(inserted.")),""))

	TEXT TO uSql TEXTMERGE noshow

		<<uSqlI>>

		<<Iif(Alltrim(Upper(uTabela)) == "USQL" Or Alltrim(Upper(uTabela)) == "OUN",uIDENTITY_INSERT_ON,"")>>

		<<uSqlM>>

		<<Iif(Alltrim(Upper(uTabela)) == "USQL" Or Alltrim(Upper(uTabela)) == "OUN",uIDENTITY_INSERT_OFF,"")>>

		End
	ENDTEXT
*!*	_Cliptext=uSql
*!*	MESSAGEBOX(uSql)
	If !u_sqlexec(uSql)
		Msg("N�o consegui criar o trigger Tts_INS_" + uNome)
	Endif

	uCondicoesTrAux = uCondicoesTr
	uCondicoesDelAux = uCondicoesTr

	If !Empty(uCondicoesTrAux)
		uCondicoesIns = uCondicoesTrAux
	Else
		uCondicoesIns = ""
	Endif

	If !Empty(uCondicoesDelAux)			
		uCondicoesDel = uCondicoesDelAux 
	Else
		uCondicoesDel = ""
	Endif

	TEXT TO uSql TEXTMERGE NOSHOW
		IF EXISTS (SELECT name FROM sysobjects
		      WHERE name = 'Tts_DEl_<<uNome>>' AND type = 'TR')
		   DROP TRIGGER Tts_DEl_<<uNome>>
	ENDTEXT

	If u_sqlexec(uSql)
	Endif

	TEXT TO uSqlI TEXTMERGE noshow
		Create TRIGGER Tts_DEL_<<uNome>>
		on <<uTabela>>
		for delete
		as
		BEGIN
	ENDTEXT

	TEXT TO uSqlM TEXTMERGE noshow

		INSERT INTO tts_qt (tabela,bddestino,stamp,tctlstamp,del,data,hora,tipo )
		Select '<<uTabela>>' as tabela,'<<uBDDestino>>' as bddestino
			,deleted.<<uTabela>>Stamp as stamp,'<<uStamp>>' as tctlstamp
			,1,getdate(),right(convert(char(20),getdate(),113),8),<<uTipo>> 
			From deleted
			<<IIF(!EMPTY(uCondicoesDel),ALLTRIM(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesDel))," "+uTabelaAux," deleted."),"."+uTabelaAux,".deleted."),"("+uTabelaAux,"(deleted.")),"")>>
	ENDTEXT

*!*		MESSAGEBOX(uCondicoesDel)
*!*		MESSAGEBOX(uTabelaAux)
*!*		MESSAGEBOX(IIF(!EMPTY(uCondicoesDel),IIF(uEOutTab And !EMPTY(uTabelaPrincipal) And !EMPTY(uLigTabelaPrinc)," And " + ALLTRIM(uCondicoesDel)," And " + ALLTRIM(STRTRAN(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesDel))," "+uTabelaAux," deleted."),"#TTS#"+uTabelaAux," deleted."),"."+uTabelaAux,".deleted."),"("+uTabelaAux,"(deleted."))),""))

	TEXT TO uSql TEXTMERGE noshow
		<<uSqlI>>
		<<uSqlM>>
		End
	ENDTEXT
*!*	_Cliptext=uSql
*!*	MESSAGEBOX(uSql)
	If !u_sqlexec(uSql)
		Msg("N�o consegui criar o trigger Tts_DEL_" + uNome)
	Endif

	TEXT TO uSql TEXTMERGE NOSHOW
		IF EXISTS (SELECT name FROM sysobjects
		      WHERE name = 'Tts_UPD_<<uNome>>' AND type = 'TR')
		   DROP TRIGGER Tts_UPD_<<uNome>>
	ENDTEXT

	If u_sqlexec(uSql)
	Endif

	TEXT TO uSqlI TEXTMERGE noshow
		Create TRIGGER Tts_UPD_<<uNome>>
		on <<uTabela>>
		for update
		as
		BEGIN

	ENDTEXT

	TEXT TO uSqlM TEXTMERGE noshow

		INSERT INTO tts_qt (tabela,bddestino,stamp,tctlstamp,upd,data,hora,tipo )
		Select '<<uTabela>>' as tabela,'<<uBDDestino>>' as bddestino
			,inserted.<<uTabela>>Stamp as stamp,'<<uStamp>>' as tctlstamp
			,1,getdate(),right(convert(char(20),getdate(),113),8),<<uTipo>> 
			From inserted
			<<IIF(!EMPTY(uCondicoesIns),ALLTRIM(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesIns))," "+uTabelaAux," inserted."),"."+uTabelaAux,".inserted."),"("+uTabelaAux,"(inserted.")),"")>>

	ENDTEXT

*!*		MESSAGEBOX(uCondicoesIns)
*!*		MESSAGEBOX(uTabelaAux)
*!*		MESSAGEBOX(IIF(!EMPTY(uCondicoesIns)," And " + ALLTRIM(STRTRAN(STRTRAN(STRTRAN(STRTRAN(ALLTRIM(UPPER(uCondicoesIns))," "+uTabelaAux," inserted."),"#TTS#"+uTabelaAux," inserted."),"."+uTabelaAux,".inserted."),"("+uTabelaAux,"(inserted.")),""))


	TEXT TO uSql TEXTMERGE noshow

		<<uSqlI>>

		<<Iif(Alltrim(Upper(uTabela)) == "USQL" Or Alltrim(Upper(uTabela)) == "OUN",uIDENTITY_INSERT_ON,"")>>

		<<uSqlM>>

		<<Iif(Alltrim(Upper(uTabela)) == "USQL" Or Alltrim(Upper(uTabela)) == "OUN",uIDENTITY_INSERT_OFF,"")>>

		End
	ENDTEXT

*!*		_Cliptext=uSql
*!*		MESSAGEBOX(uSql)