USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbostil]    Script Date: 30/03/2023 15:00:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STIL_Delete]
		@c1 char(25),
		@c2 varchar(18),
		@c3 varchar(60),
		@c4 datetime,
		@c5 numeric(14,3),
		@c6 char(25),
		@c7 char(25),
		@c8 numeric(5,0),
		@c9 varchar(30),
		@c10 varchar(25),
		@c11 varchar(25),
		@c12 bit,
		@c13 bit,
		@c14 numeric(6,0),
		@c15 varchar(20),
		@c16 varchar(20),
		@c17 numeric(18,5),
		@c18 numeric(19,6),
		@c19 varchar(30),
		@c20 datetime,
		@c21 varchar(8),
		@c22 varchar(30),
		@c23 datetime,
		@c24 varchar(8),
		@c25 bit,
		@c26 varchar(25),
		@c27 varchar(30),
		@c28 varchar(25),
		@c29 varchar(55),
		@c30 numeric(18,5),
		@c31 numeric(19,6),
		@c32 numeric(10,0),
		@c33 varchar(4),
		@c34 bit,
		@c35 bit,
		@c36 numeric(3,0),
		@c37 varchar(50)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[stil] 
	where [stilstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[stilstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stil]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from stil where stilstamp=@c1