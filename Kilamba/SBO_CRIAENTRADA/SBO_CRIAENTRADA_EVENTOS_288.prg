** Criado por: Vasco Rocha
** Data da cria��o: 10.09.2015
** Cria Entrada de Material a partir do monitor
PUBLIC p_monitor, p_mano, p_nrforn

SELECT pdarm
IF NOT EMPTY(pdarm.no)
	*chama dossier
	DO CASE
	CASE pdarm.ndos = p_dieencforn
		doread("BO","SBO")
		*dossier com o tipo de encomenda a fornecedor
		sbo.newndos(p_dieentrmat)
		*introduzir o novo dossier
		sbo.dointroduzir

		DO dbfusecl
		TEXT TO m.msel NOSHOW TEXTMERGE
		Select flstamp
		from fl (nolock)
		where no = <<astr(pdarm.no)>> and estab = <<pdarm.estab>>
		ENDTEXT
		IF NOT u_sqlexec(m.msel, 'myflstamp')
			msg('Erro ao consultar fornecedor')
			RETURN .F.
		ENDIF
		SELECT myflstamp
		v_flstamp = myflstamp.flstamp
		fecha("myflstamp")
		u_requery("FL")
	CASE pdarm.ndos = 938
		doread("BO","SBO")
		*dossier com o tipo de encomenda a fornecedor
		public p_PDAtrf
		p_PDAtrf = 1   
		sbo.newndos(p_dietrfarm)
		*introduzir o novo dossier
		sbo.dointroduzir
		release p_PDAtrf
		SELECT bi  
		DELETE 
		SELECT  bi2 
		DELETE 
		DO dbfusecl
		TEXT TO m.msel NOSHOW TEXTMERGE
		Select clstamp
		from cl (nolock)
		where no = <<astr(pdarm.no)>> and estab = <<pdarm.estab>>
		ENDTEXT
		IF NOT u_sqlexec(m.msel, 'myclstamp')
			msg('Erro ao consultar o cliente')
			RETURN .F.
		ENDIF
		SELECT myclstamp
		v_clstamp = myclstamp.clstamp
		fecha("myclstamp")
		u_requery("CL")

	ENDCASE



	SELECT pdarm
	SELECT bo
	REPLACE bo.NOME WITH pdarm.NOME
	REPLACE bo.no WITH pdarm.no
	REPLACE bo.estab WITH pdarm.estab
	replace bo.pastamp WITH pdarm.bostamp
	KEYBOARD"{TAB}"
	DO boclact
	sbo.REFRESH

	p_monitor = 1
	p_mano = pdarm.boano
	p_nrforn = pdarm.obrano
	DO CASE
	CASE pdarm.ndos = p_dieencforn
		SBO.Painelfundo.Page1.Obj1.CLICK()
	CASE pdarm.ndos = 938	
			TEXT TO msel NOSHOW TEXTMERGE
				select boo.nmdos, boo.obrano, boo.bistamp,boo.ref, boo.design,boo.edebito, boo.desconto,boo.desc2,  boo.qtt, boo.qtt2, boo.pendente , boo.ar2mazem 
				from
					(select bo.nmdos, bo.obrano, ref,design , bistamp, bi.edebito ,bi.desconto,  bi.desc2
							,qtt, qtt2 ,qtt-qtt2 pendente, bi.lordem, ar2mazem 
					from 	bo (nolock)
					inner 	join  bi (nolock) on bo.bostamp = bi.bostamp
					where 	bo.ndos in(938)  and  bo.bostamp = '<<pdarm.bostamp>>' and ref <> '' and bo.fechada = 0
							and  bi.fechada = 0
							and  qtt-qtt2 > 0 ) boo
				order by lordem
			ENDTEXT
			IF NOT u_sqlexec(msel,"c_bitemp")
				msg('Erro ao consultar dados da Transfer�ncia.')
				RETURN .f.
			ENDIF 
			*fecha("c_botemp")
			SELECT c_bitemp
			IF RECCOUNT("c_bitemp") = 0
				msg("A encomenda n�o existe ou est� fechada")
				fecha("c_bitemp")
				RETURN
			ENDIF
			**SELECT  c_bitemp
			**mostrameisto("c_bitemp")
			select c_bitemp
			goto top 
			SELECT bi
			goto bottom
			LOCATE FOR bi.design = RTRIM(c_bitemp.nmdos) + ' n� ' +  astr(c_bitemp.obrano)
			IF NOT FOUND()
				SELECT bi
				DO Boine2in
				REPLACE bi.DESIGN  WITH  RTRIM(c_bitemp.nmdos) + ' n� ' +  astr(c_bitemp.obrano)
			ENDIF 
			SELECT  c_bitemp
			SCAN FOR  c_bitemp.pendente  > 0
				SELECT bi
				GOTO BOTTOM 
				LOCATE FOR UPPER(ALLTRIM(bi.ref)) == UPPER(ALLTRIM(c_bitemp.ref)) AND ALLTRIM(bi.oobistamp) = ALLTRIM(c_bitemp.bistamp)
				IF NOT FOUND()
					SELECT bi
					DO Boine2in
					SELECT bi
					REPLACE bi.ref WITH  c_bitemp.ref
					DO BOACTREF WITH '',.T.,'OKPRECOS','BI'
					REPLACE bi.obistamp WITH  c_bitemp.bistamp
					REPLACE bi.oobistamp WITH  c_bitemp.bistamp
					REPLACE bi.qtt WITH  c_bitemp.pendente 
					REPLACE bi.armazem WITH  c_bitemp.ar2mazem
					REPLACE bi.ar2mazem WITH  p_armazem
					REPLACE bi.edebito WITH  c_bitemp.edebito
					REPLACE bi.desconto WITH  c_bitemp.desconto
					REPLACE bi.desc2 WITH  c_bitemp.desc2
					*REPLACE bi.series WITH  c_bitemp.u_pdarmstamp
					DO u_bottdeb WITH 'BI',.F.
				ENDIF
			ENDSCAN
			sbo.refresh()	
	ENDCASE	
	
ENDIF
