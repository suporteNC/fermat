USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbobi]    Script Date: 02/03/2023 13:08:47 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_BIPAL_Insert]
    @c1 char(25),
    @c2 varchar(24),
    @c3 numeric(10,0),
    @c4 char(18),
    @c5 varchar(60),
    @c6 numeric(14,4),
    @c7 numeric(14,4),
    @c8 numeric(18,5),
    @c9 numeric(18,5),
    @c10 numeric(14,3),
    @c11 numeric(5,2),
    @c12 numeric(1,0),
    @c13 numeric(5,0),
    @c14 numeric(1,0),
    @c15 numeric(10,0),
    @c16 numeric(18,5),
    @c17 varchar(50),
    @c18 numeric(7,0),
    @c19 numeric(10,0),
    @c20 numeric(10,0),
    @c21 datetime,
    @c22 varchar(24),
    @c23 numeric(3,0),
    @c24 numeric(3,0),
    @c25 varchar(20),
    @c26 numeric(2,0),
    @c27 datetime,
    @c28 datetime,
    @c29 datetime,
    @c30 varchar(40),
    @c31 varchar(20),
    @c32 datetime,
    @c33 datetime,
    @c34 numeric(4,0),
    @c35 varchar(20),
    @c36 varchar(20),
    @c37 varchar(20),
    @c38 varchar(40),
    @c39 varchar(40),
    @c40 varchar(40),
    @c41 numeric(4,0),
    @c42 varchar(20),
    @c43 varchar(20),
    @c44 numeric(19,6),
    @c45 numeric(2,0),
    @c46 bit,
    @c47 datetime,
    @c48 datetime,
    @c49 varchar(20),
    @c50 bit,
    @c51 bit,
    @c52 bit,
    @c53 bit,
    @c54 numeric(3,0),
    @c55 bit,
    @c56 numeric(5,0),
    @c57 bit,
    @c58 bit,
    @c59 varchar(25),
    @c60 numeric(10,0),
    @c61 bit,
    @c62 bit,
    @c63 varchar(43),
    @c64 varchar(55),
    @c65 varchar(45),
    @c66 char(55),
    @c67 bit,
    @c68 bit,
    @c69 varchar(18),
    @c70 varchar(30),
    @c71 bit,
    @c72 char(25),
    @c73 char(25),
    @c74 varchar(25),
    @c75 varchar(20),
    @c76 varchar(20),
    @c77 numeric(9,3),
    @c78 numeric(13,3),
    @c79 numeric(13,3),
    @c80 numeric(13,3),
    @c81 numeric(13,3),
    @c82 numeric(13,3),
    @c83 varchar(50),
    @c84 bit,
    @c85 bit,
    @c86 numeric(14,4),
    @c87 numeric(19,6),
    @c88 numeric(12,2),
    @c89 varchar(20),
    @c90 numeric(6,0),
    @c91 bit,
    @c92 char(25),
    @c93 char(25),
    @c94 varchar(20),
    @c95 varchar(20),
    @c96 varchar(35),
    @c97 varchar(20),
    @c98 varchar(120),
    @c99 varchar(30),
    @c100 bit,
    @c101 bit,
    @c102 varchar(4),
    @c103 varchar(4),
    @c104 char(25),
    @c105 char(25),
    @c106 bit,
    @c107 bit,
    @c108 varchar(18),
    @c109 bit,
    @c110 numeric(18,5),
    @c111 numeric(19,6),
    @c112 numeric(18,5),
    @c113 numeric(19,6),
    @c114 numeric(15,2),
    @c115 numeric(15,2),
    @c116 numeric(16,3),
    @c117 numeric(14,4),
    @c118 numeric(18,5),
    @c119 numeric(19,6),
    @c120 varchar(13),
    @c121 varchar(25),
    @c122 numeric(1,0),
    @c123 numeric(19,6),
    @c124 numeric(19,6),
    @c125 numeric(19,6),
    @c126 numeric(18,5),
    @c127 numeric(19,6),
    @c128 numeric(10,0),
    @c129 numeric(10,0),
    @c130 varchar(40),
    @c131 bit,
    @c132 varchar(15),
    @c133 varchar(10),
    @c134 numeric(6,2),
    @c135 numeric(5,2),
    @c136 numeric(5,2),
    @c137 numeric(5,2),
    @c138 numeric(5,2),
    @c139 numeric(5,2),
    @c140 text,
    @c141 text,
    @c142 varchar(20),
    @c143 varchar(20),
    @c144 numeric(19,5),
    @c145 bit,
    @c146 char(25),
    @c147 varchar(20),
    @c148 varchar(20),
    @c149 varchar(20),
    @c150 varchar(20),
    @c151 varchar(20),
    @c152 varchar(18),
    @c153 numeric(6,2),
    @c154 bit,
    @c155 numeric(18,5),
    @c156 numeric(19,6),
    @c157 numeric(13,3),
    @c158 numeric(11,3),
    @c159 bit,
    @c160 numeric(7,3),
    @c161 varchar(4),
    @c162 numeric(14,3),
    @c163 numeric(14,3),
    @c164 varchar(20),
    @c165 text,
    @c166 bit,
    @c167 bit,
    @c168 bit,
    @c169 numeric(18,5),
    @c170 numeric(19,6),
    @c171 numeric(18,5),
    @c172 numeric(19,6),
    @c173 char(30),
    @c174 bit,
    @c175 bit,
    @c176 bit,
    @c177 datetime,
    @c178 varchar(10),
    @c179 bit,
    @c180 varchar(55),
    @c181 numeric(10,0),
    @c182 numeric(3,0),
    @c183 bit,
    @c184 char(25),
    @c185 char(25),
    @c186 bit,
    @c187 numeric(18,5),
    @c188 numeric(19,6),
    @c189 numeric(18,5),
    @c190 numeric(19,6),
    @c191 numeric(18,5),
    @c192 numeric(19,6),
    @c193 numeric(18,5),
    @c194 numeric(19,6),
    @c195 bit,
    @c196 numeric(18,5),
    @c197 numeric(19,6),
    @c198 bit,
    @c199 numeric(1,0),
    @c200 numeric(18,5),
    @c201 numeric(19,6),
    @c202 char(25),
    @c203 varchar(30),
    @c204 datetime,
    @c205 varchar(8),
    @c206 varchar(30),
    @c207 datetime,
    @c208 varchar(8),
    @c209 bit,
    @c210 numeric(16,8),
    @c211 numeric(16,3),
    @c212 numeric(5,0),
    @c213 varchar(40),
    @c214 numeric(14,3),
    @c215 numeric(14,3),
    @c216 numeric(15,3),
    @c217 datetime,
    @c218 varchar(20),
    @c219 text,
    @c220 numeric(16,2),
    @c221 varchar(15)
as
begin  
	insert into [PL].[bi] (
		[bistamp],
		[nmdos],
		[obrano],
		[ref],
		[design],
		[qtt],
		[qtt2],
		[pu],
		[debito],
		[prorc],
		[iva],
		[tabiva],
		[armazem],
		[stipo],
		[no],
		[pcusto],
		[serie],
		[nomquina],
		[nopat],
		[fno],
		[fdata],
		[nmdoc],
		[ndoc],
		[ndos],
		[forref],
		[txiva],
		[rdata],
		[dedata],
		[atedata],
		[lobs],
		[obranome],
		[datafinal],
		[dataopen],
		[tecnico],
		[maquina],
		[marca],
		[litem],
		[lobs2],
		[litem2],
		[lobs3],
		[vendedor],
		[vendnm],
		[tabela1],
		[vumoeda],
		[ldossier],
		[fechada],
		[dataobra],
		[datafecho],
		[zona],
		[resfor],
		[rescli],
		[resrec],
		[iprint],
		[estab],
		[resusr],
		[ar2mazem],
		[composto],
		[compostoori],
		[lrecno],
		[lordem],
		[fmarcada],
		[producao],
		[local],
		[morada],
		[codpost],
		[nome],
		[tabfor],
		[descli],
		[reff],
		[lote],
		[ivaincl],
		[cor],
		[tam],
		[segmento],
		[bofref],
		[bifref],
		[grau],
		[partes],
		[partes2],
		[altura],
		[largura],
		[espessura],
		[biserie],
		[infref],
		[lifref],
		[uni2qtt],
		[epcusto],
		[ttmoeda],
		[adoc],
		[cpoc],
		[stns],
		[obistamp],
		[oobistamp],
		[usr1],
		[usr2],
		[usr3],
		[usr4],
		[usr5],
		[usr6],
		[usalote],
		[texteis],
		[unidade],
		[unidad2],
		[oftstamp],
		[ofostamp],
		[promo],
		[epromo],
		[familia],
		[noserie],
		[slvu],
		[eslvu],
		[sltt],
		[esltt],
		[slvumoeda],
		[slttmoeda],
		[ncmassa],
		[ncunsup],
		[ncvest],
		[encvest],
		[nccod],
		[ncinteg],
		[classif],
		[epu],
		[edebito],
		[eprorc],
		[ttdeb],
		[ettdeb],
		[binum1],
		[binum2],
		[codigo],
		[sattotal],
		[classifc],
		[posic],
		[desconto],
		[desc2],
		[desc3],
		[desc4],
		[desc5],
		[desc6],
		[series],
		[series2],
		[ccusto],
		[ncusto],
		[num1],
		[fechabo],
		[oobostamp],
		[ltab1],
		[ltab2],
		[ltab3],
		[ltab4],
		[ltab5],
		[fami],
		[pctfami],
		[adjudicada],
		[tieca],
		[etieca],
		[mtieca],
		[volume],
		[iecasug],
		[iecagrad],
		[iecacodisen],
		[peso],
		[pbruto],
		[codfiscal],
		[dgeral],
		[temoci],
		[temomi],
		[temsubemp],
		[encargo],
		[eencargo],
		[custoind],
		[ecustoind],
		[tiposemp],
		[pvok],
		[mntencargos],
		[boclose],
		[dtclose],
		[quarto],
		[emconf],
		[efornecedor],
		[efornec],
		[efornestab],
		[cativo],
		[optstamp],
		[oristamp],
		[temeco],
		[ecoval],
		[eecoval],
		[tecoval],
		[etecoval],
		[ecoval2],
		[eecoval2],
		[tecoval2],
		[etecoval2],
		[econotcalc],
		[debitoori],
		[edebitoori],
		[trocaequi],
		[tpromo],
		[valdesc],
		[evaldesc],
		[bostamp],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[u_convers],
		[u_qtdfor],
		[u_estab],
		[u_locais],
		[u_qttenc],
		[u_qttpda],
		[u_qttpick],
		[u_ttssinc],
		[u_vendedor],
		[u_obs],
		[u_nciqttc],
		[u_serv]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57,
		@c58,
		@c59,
		@c60,
		@c61,
		@c62,
		@c63,
		@c64,
		@c65,
		@c66,
		@c67,
		@c68,
		@c69,
		@c70,
		@c71,
		@c72,
		@c73,
		@c74,
		@c75,
		@c76,
		@c77,
		@c78,
		@c79,
		@c80,
		@c81,
		@c82,
		@c83,
		@c84,
		@c85,
		@c86,
		@c87,
		@c88,
		@c89,
		@c90,
		@c91,
		@c92,
		@c93,
		@c94,
		@c95,
		@c96,
		@c97,
		@c98,
		@c99,
		@c100,
		@c101,
		@c102,
		@c103,
		@c104,
		@c105,
		@c106,
		@c107,
		@c108,
		@c109,
		@c110,
		@c111,
		@c112,
		@c113,
		@c114,
		@c115,
		@c116,
		@c117,
		@c118,
		@c119,
		@c120,
		@c121,
		@c122,
		@c123,
		@c124,
		@c125,
		@c126,
		@c127,
		@c128,
		@c129,
		@c130,
		@c131,
		@c132,
		@c133,
		@c134,
		@c135,
		@c136,
		@c137,
		@c138,
		@c139,
		@c140,
		@c141,
		@c142,
		@c143,
		@c144,
		@c145,
		@c146,
		@c147,
		@c148,
		@c149,
		@c150,
		@c151,
		@c152,
		@c153,
		@c154,
		@c155,
		@c156,
		@c157,
		@c158,
		@c159,
		@c160,
		@c161,
		@c162,
		@c163,
		@c164,
		@c165,
		@c166,
		@c167,
		@c168,
		@c169,
		@c170,
		@c171,
		@c172,
		@c173,
		@c174,
		@c175,
		@c176,
		@c177,
		@c178,
		@c179,
		@c180,
		@c181,
		@c182,
		@c183,
		@c184,
		@c185,
		@c186,
		@c187,
		@c188,
		@c189,
		@c190,
		@c191,
		@c192,
		@c193,
		@c194,
		@c195,
		@c196,
		@c197,
		@c198,
		@c199,
		@c200,
		@c201,
		@c202,
		@c203,
		@c204,
		@c205,
		@c206,
		@c207,
		@c208,
		@c209,
		@c210,
		@c211,
		@c212,
		@c213,
		@c214,
		@c215,
		@c216,
		@c217,
		@c218,
		@c219,
		@c220,
		@c221	)
				
		EXEC dbo.NC_Sinc_Insert 'dbo','PL','bi',@c1, 'bistamp' 
end  
