 Xexpressao
"
if type("P_pesq_st") <> 'U'
 	If P_pesq_st = 'STIC'
 	       ctipoimp = 'STIC' 
	       sstic.showsave()
	Else
	      ctipoimp = 'SP' 
	       ssp.showsave()
	 Endif
else  
 	ctipoimp = 'ST'
endif  

CREATE CURSOR c_erros(codigo c(25))

if  ctipoimp <> 'SP' and ctipoimp <> 'STIC' 
SELECT  stlist
SCAN
	DELETE
ENDSCAN
endif  

CREATE CURSOR c_bc (codigo c(18))
lnLines = ALINES(laLines,c_memo.variavel, 1 )
i = 1
DO WHILE i <= ALEN(laLines)
	SELECT c_bc
	APPEND BLANK
	REPLACE c_bc.codigo  WITH  laLines[i]
	i =  i + 1
ENDDO

SELECT  c_bc

SELECT  c_bc
SCAN
	TEXT TO msel NOSHOW TEXTMERGE
		DECLARE @cvar varchar(18) , @ref varchar(18) ,  @ststamp varchar(25), @design  varchar(65)
		set   @cvar = '<<c_bc.codigo>>'
		SET @ref = ''
		SET @ststamp = ''
		SELECT  @ref = ref , @ststamp = ststamp ,  @design = design  FROM st (nolock) WHERE ref = @cvar AND  inactivo = 0
		IF @ref = ''
		BEGIN
			SELECT  @ref = ref , @ststamp = ststamp,  @design = design   FROM st (nolock) WHERE codigo = @cvar AND  inactivo = 0
			IF @ref = ''
			BEGIN
				SELECT  @ref = ref , @ststamp = ststamp   FROM bc (nolock) WHERE codigo = @cvar
					if @ref <> ''
					begin
					 	select @design = design  from st (nolock) where ref = @ref
					end
			END
		END
		SELECT  @ref ref , @ststamp ststamp, @design design
	ENDTEXT
	IF NOT  u_sqlexec(msel,"c_temp")
		msg(msel)
		RETURN
	ENDIF

	if ctipoimp <> 'SP' and ctipoimp <> 'STIC' 
		**** FICHA DE ARTIGOS ***************
		IF  NOT EMPTY(c_temp.ref)
			SELECT stlist
			GOTO TOP
			LOCATE FOR ALLTRIM(stlist.ref) ==  ALLTRIM(c_temp.ref)
			IF  NOT FOUND()
				SELECT stlist
				APPEND BLANK
				REPLACE stlist.ststamp  WITH  c_temp.ststamp
				REPLACE stlist.ref  WITH  c_temp.ref
				REPLACE stlist.DESIGN  WITH  c_temp.DESIGN
			ENDIF
		ELSE
			SELECT c_erros
			APPEND BLANK
			REPLACE  c_erros.codigo  WITH c_bc.codigo
		ENDIF
	else  
		If ctipoimp = 'SP'
			**** PROMO��ES ***************
			IF  NOT EMPTY(c_temp.ref)
				SELECT u_spst
				GOTO TOP
				LOCATE FOR ALLTRIM(u_spst.ref) ==  ALLTRIM(c_temp.ref)
				IF  NOT FOUND()
					SELECT u_spst
					APPEND BLANK
					REPLACE u_spst.u_spststamp WITH  u_stamp()
					REPLACE u_spst.spstamp WITH  sp.spstamp
					REPLACE u_spst.ref  WITH  c_temp.ref
					REPLACE u_spst.DESIGN  WITH  c_temp.DESIGN
				ENDIF
			ELSE
				SELECT c_erros
				APPEND BLANK
				REPLACE  c_erros.codigo  WITH c_bc.codigo
			ENDIF 
		Else
			**** INVENT�RIO ***************
			IF  NOT EMPTY(c_temp.ref)

				*** Vamos desdobrar as linhas pelas localiza��es
				TEXT TO msel NOSHOW TEXTMERGE
					Select distinct a.ref,a.design,a.local,a.unidade,a.cpoc
					from (
					Select ref,design,local as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					union all
					Select ref,design,u_local2 as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					union all
					Select ref,design,u_local3 as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					union all
					Select ref,design,u_local4 as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					union all
					Select ref,design,u_local5 as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					union all
					Select ref,design,u_local6 as local,unidade,cpoc from st(nolock) where st.ref = '<<c_temp.ref>>'
					) a					
					where a.local<>''
				ENDTEXT
				IF NOT  u_sqlexec(msel,"uCurLocais")
					msg(msel)
					RETURN
				ENDIF
				Select uCurLocais
				If reccount("uCurLocais") > 0
					*** tem localiza��es
					Goto top
					Scan				
						SELECT stil
						APPEND BLANK
						REPLACE stil.stilstamp	WITH  u_stamp()
						REPLACE stil.sticstamp  WITH  stic.sticstamp
						REPLACE stil.ref  		WITH  c_temp.ref
						REPLACE stil.DESIGN  	WITH  c_temp.DESIGN
						REPLACE stil.local  		WITH  uCurLocais.local
						REPLACE stil.unidade	WITH  uCurLocais.unidade
						REPLACE stil.cpoc		WITH  uCurLocais.cpoc
						REPLACE stil.armazem	WITH  2001
						REPLACE stil.ccusto	WITH  uCurLocais.local
					Endscan
				Else
					*** N�o tem localiza��o
					SELECT stil
					APPEND BLANK
					REPLACE stil.stilstamp	WITH  u_stamp()
					REPLACE stil.sticstamp  WITH  stic.sticstamp
					REPLACE stil.ref  		WITH  c_temp.ref
					REPLACE stil.DESIGN  	WITH  c_temp.DESIGN
					REPLACE stil.armazem	WITH  2001

					fecha("tempst")
					if get_stref(c_temp.ref,"unidade,cpoc")
						Select stil
						REPLACE stil.unidade	WITH  tempst.unidade
						REPLACE stil.cpoc		WITH  tempst.cpoc
					Endif

				Endif
				
			ELSE
				SELECT c_erros
				APPEND BLANK
				REPLACE  c_erros.codigo  WITH c_bc.codigo
			ENDIF 
		endif  
		endif
ENDSCAN

SELECT c_erros
IF RECCOUNT("c_erros") > 0
	msg("C�digos por importar")
	mostrameisto("c_erros")
ENDIF

fecha("c_erros")
fecha("c_bc")

if ctipoimp <> 'SP' and ctipoimp <> 'STIC'
SELECT  stlist
GOTO TOP
endif  

If ctipoimp = 'STIC'
	sstic.refrescar
Endif

msg("Artigos Importados")
"
