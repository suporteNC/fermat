 Xexpressao
"if empty(m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value)
   m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value=astr(year(date()))
endif
if empty(m.ObjRecebido.Janela.pageframe1.udcpage6.grelha.conta.texto1.value)
  m.ObjRecebido.Janela.pageframe1.udcpage6.grelha.conta.texto1.value=1
endif
if empty(m.ObjRecebido.Janela.pageframe1.udcpage6.combo_grau.value)
   m.ObjRecebido.Janela.pageframe1.udcpage6.combo_grau.value="1"
endif
if empty(m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value)
   m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value=""
endif

myvar=""
if m.ObjRecebido.Janela.pageframe1.udcpage6.chkbox_somov.value=.T.
  myvar="having sum(case when pcsa.mes between 1 and 15 then (pcsa.edeb+pcsa.ecre) else 0 end) >0"
else
   myvar=""
endif

Text to mselsnc textmerge noshow

select distinct pcsa.conta, pc.descricao,
isnull(sum(case when pcsa.mes=0 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as zer,
isnull(sum(case when pcsa.mes=1 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jan,
isnull(sum(case when pcsa.mes=2 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as fev,
isnull(sum(case when pcsa.mes=3 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Mar,
isnull(sum(case when pcsa.mes=4 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Abr,
isnull(sum(case when pcsa.mes=5 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Mai,
isnull(sum(case when pcsa.mes=6 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jun,
isnull(sum(case when pcsa.mes=7 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jul,
isnull(sum(case when pcsa.mes=8 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Ago,
isnull(sum(case when pcsa.mes=9 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'Set',
isnull(sum(case when pcsa.mes=10 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'Out',
isnull(sum(case when pcsa.mes=11 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Nov,
isnull(sum(case when pcsa.mes=12 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Dez,
isnull(sum(case when pcsa.mes=13 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'tre',
isnull(sum(case when pcsa.mes=14 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'qua',
isnull(sum(case when pcsa.mes=15 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'qui',
isnull(sum(case when pcsa.mes between 0 and 15 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Totallin
from pcsa(nolock)
inner join pc(nolock) on pc.conta=pcsa.conta and  pc.ano='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value>>' 
where pcsa.ano='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value>>' and 
len(pcsa.conta)<='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_grau.value>>' and
(''='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value>>' or pcsa.conta like '<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value>>%')
group by pcsa.conta, pc.descricao
<<myvar>>
union all
select top 1 'T O T A L' as conta,'G E R A L' as descricao,
isnull(sum(case when pcsa.mes=0 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as zer,
isnull(sum(case when pcsa.mes=1 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jan,
isnull(sum(case when pcsa.mes=2 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Fev,
isnull(sum(case when pcsa.mes=3 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Mar,
isnull(sum(case when pcsa.mes=4 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Abr,
isnull(sum(case when pcsa.mes=5 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Mai,
isnull(sum(case when pcsa.mes=6 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jun,
isnull(sum(case when pcsa.mes=7 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Jul,
isnull(sum(case when pcsa.mes=8 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Ago,
isnull(sum(case when pcsa.mes=9 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'Set',
isnull(sum(case when pcsa.mes=10 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'Out',
isnull(sum(case when pcsa.mes=11 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Nov,
isnull(sum(case when pcsa.mes=12 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Dez,
isnull(sum(case when pcsa.mes=13 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'tre',
isnull(sum(case when pcsa.mes=14 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'qua',
isnull(sum(case when pcsa.mes=15 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as 'qui',
isnull(sum(case when pcsa.mes between 0 and 15 then (pcsa.edeb-pcsa.ecre) else 0 end),0) as Totallin
from pcsa(nolock)
inner join pc(nolock) on pc.conta=pcsa.conta and pc.ano='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value>>'
where pc.integracao=1 and len(pcsa.conta)=1 and pcsa.ano='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_ano.value>>' and 
len(pcsa.conta)<='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_grau.value>>' and
(''='<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value>>' or pcsa.conta like '<<m.ObjRecebido.Janela.pageframe1.udcpage6.combo_classe.value>>%')
order by pcsa.conta, pc.descricao
Endtext

if u_sqlexec(mselsnc,"cursor_snc")
   select cursor_snc
endif"
