** Criado por: Am�ndio Pacheco
** Data cria��o: 05.03.2014
select bo
if bi.ndos <> 24 && Localiza��es
	return .f.
endif
LOCAL localstamp, msel, lini, lfim
localstamp = 'ADM14030354527,537000001 ' && Dossier de localiza��es n� 1
m.msel = ""
lini = ""
lfim = ""
Create Cursor xVars (no N(5), tipo c(1), Nome c(40), Pict c(100), lOrdem N(10), nValor N(18,2), cValor c(40), lValor L, dValor D, tBval M) 
* prencher os dados de cada variavel a pedir 
Select xVars 
Append Blank 
Replace xVars.no With 1
Replace xVars.tipo With "C" 
Replace xVars.Nome With "Local. Inicial" 
Replace xVars.Pict With "" 
Replace xVars.lOrdem With 1
Replace xVars.cValor With ""
Select xVars 
Append Blank 
Replace xVars.no With 2
Replace xVars.tipo With "C" 
Replace xVars.Nome With "Local. Final" 
Replace xVars.Pict With "" 
Replace xVars.lOrdem With 3
Replace xVars.cValor With ""
m.Escolheu = .F. 
m.mCaption = "Filtro a aplicar" 
docomando("do form usqlvar with 'xvars', m.mCaption") 
If Not m.Escolheu 
	mensagem("Opera��o cancelada!","DIRECTA") 
	Return .F. 
Else 
	Select xVars 
	Locate 
	lini = xVars.cValor
	Select xVars 
	Skip 
	lfim = xVars.cValor
Endif 
TEXT TO m.Msel TEXTMERGE NOSHOW PRETEXT 7
	select	bi.bostamp, bi.bistamp, bi.litem, bi.litem2, bi.lobs3, bi.design,
			cast(0 as bit) as treechkbox 
	from	bo (nolock) 
	inner	join ts (nolock) on ts.ndos = bo.ndos
	inner	join bi (nolock) on bo.bostamp = bi.bostamp
	where	ts.ndos = 24 and bo.bostamp = '<<localstamp>>'
ENDTEXT
do case
	case not empty(lini) and empty(lfim)
		m.msel = m.msel + "	and bi.design > '" + alltrim(lini) + "' "
	case not empty(lini) and not empty(lfim)
		m.msel = m.msel + "	and bi.design between '" + alltrim(lini) + "' and '" + alltrim(lfim) + "' "
endcase
m.msel = m.msel + "order	by bi.litem, bi.design"
if not u_sqlexec(m.Msel,"c_tmplpda") 
	msg("Erro ao gerar a listagem!")
	return .f.
endif
if reccount("c_tmplpda") = 0
	msg("N�o existem localiza��es!")
	return .f.
endif
Declare list_tit(3), list_cam(3), list_tam(3), list_node(3), list_ali(3), list_image(3), list_chkbox(3), list_edit(3) 
list_tit(1)="Corredor"
list_cam(1)="c_tmplpda.litem"
list_node(1)="c_tmplpda.litem" 
list_tam(1)=8*30
list_ali(1)=3 
list_image(1)=0 
list_edit(1)="" 
list_chkbox(1)=.T.
list_tit(2)="Prateleira"
list_cam(2)="c_tmplpda.litem2"
list_node(2)="c_tmplpda.litem2" 
list_tam(2)=8*30
list_ali(2)=3 
list_image(2)=0 
list_edit(2)="" 
list_chkbox(2)=.T.
list_tit(3)="Localiza��o" 
list_cam(3)="c_tmplpda.design" 
list_node(3)="c_tmplpda.design" 
list_tam(3)=8*30 
list_ali(3)=3 
list_image(3)=0 
list_edit(3)="" 
list_chkbox(3)=.T. 
m.escolheu = .f. 
m.m_subtitulo = "Marca��o de Localiza��es para Impress�o de Etiquetas"
m.treechkbox = "c_tmplpda.treechkbox"
* Executa a Treelist, o �ltimo par�metro a True activa o bot�o de op��es diversas
docomando("do form treelist with 'Marca��o de Localiza��es para Impress�o de Etiquetas', 'c_tmplpda', 'c_tmplpda',1,1,'','','','','',.F.,.F.,.T.") 
if m.escolheu 
	if not u_sqlexec("update bi set marcada = 0 where bostamp = '" + alltrim(c_tmplpda.bostamp) + "'")
		msg("Erro ao actualizar tabela!")
		return .f.
	endif
	SELECT c_tmplpda
	SCAN FOR treechkbox
		if not u_sqlexec("update bi set marcada = 1 where bistamp = '" + alltrim(c_tmplpda.bistamp) + "'")
			msg("Erro ao actualizar tabela!")
			return .f.
		endif		
	ENDSCAN
	SBO.Etiqs.click()
else
	msg("Opera��o cancelada!")
endif

