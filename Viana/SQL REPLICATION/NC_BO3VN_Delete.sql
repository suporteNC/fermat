USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbobo3]    Script Date: 02/03/2023 13:08:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_BO3VN_Delete]
		@c1 char(25),
		@c2 varchar(2),
		@c3 varchar(55),
		@c4 varchar(50),
		@c5 varchar(30),
		@c6 datetime,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 bit,
		@c12 varchar(60),
		@c13 varchar(20),
		@c14 datetime,
		@c15 varchar(12),
		@c16 varchar(30),
		@c17 bit,
		@c18 bit,
		@c19 varchar(5),
		@c20 varchar(3),
		@c21 varchar(3),
		@c22 varchar(3),
		@c23 numeric(19,6),
		@c24 numeric(18,5),
		@c25 bit,
		@c26 varchar(40),
		@c27 bit,
		@c28 numeric(10,6),
		@c29 numeric(10,6),
		@c30 numeric(10,6),
		@c31 numeric(10,6),
		@c32 varchar(3),
		@c33 varchar(60),
		@c34 varchar(30),
		@c35 datetime,
		@c36 varchar(8),
		@c37 numeric(10,0),
		@c38 datetime,
		@c39 varchar(100),
		@c40 numeric(1,0),
		@c41 varchar(20),
		@c42 numeric(1,0),
		@c43 varchar(60),
		@c44 varchar(15),
		@c45 varchar(2),
		@c46 varchar(254)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [VN].[bo3] 
	where [bo3stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[bo3stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[bo3]', @param2=@primarykey_text, @param3=13234
		End
end  

delete from bo3 where bo3stamp=@c1