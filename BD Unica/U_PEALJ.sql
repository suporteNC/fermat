USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[APspPEA_CarregaTabelau_pea2]    Script Date: 29/12/2022 17:05:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
--exec [APspPEA_CarregaTabelau_pea2]  
  
create  Procedure [dbo].[PEA_Lojas] @Estab int, @Loja varchar(3)
as  
  
declare @temruturas bit  = 1 -- 1 calcula com ruturas - 0 calcula sem ruturas     
                                                    
                                                   
--=======================================================================================================================                                                    
--Limpar tabela u_pea                                                    
--=======================================================================================================================                                                    
--delete u_pea where ref = '71964'          
print 'apaga Registo tabela PEA'        
delete  from  u_pealj where pestab=@Estab        
             
print 'Inserir registos com os artigos permanentes e com planograma'                                       
 insert into u_pealj ( u_pealjstamp, ref, design, PESTAB
       , PCUSTO , mfornec, mfornec2, pliquido         
       , eoq        
       , fornec  
          , fornecedor        
       , forref  
         , familia   
         , faminome   
         , CTPART   
         , TPART   
         , CSUBFAM     
         , SUBFAM    
         , local   
         , local2   
         , local3   
         , local4   
         , local5     
         , local6    
         , stousrdata   
         , stkseg    
   , puniteur      
      )        
 select distinct st.ststamp, st.ref, st.design, @Estab        
  , pcusto = st.epcusto  
  , Desc1 =  st.mfornec           
  , Desc2 =  st.mfornec2           
  , PCustoLiq = round(st.epcusto *  (1-(st.mfornec/100))  *  (1-(st.mfornec2/100)),2)  
  , st.eoq        
  , Fornecedor =  fornec         
  , Nome = fornecedor          
  , st.forref  
  , st.familia   
  , st.faminome   
  , ST.U_CTPART   
  , ST.U_TPART   
  , ST.U_CSUBFAM     
  , ST.U_SUBFAM  
  , st.local    
  , ST.U_LOCAL2     
  , ST.U_LOCAL3  
  , ST.U_LOCAL4  
  , ST.U_LOCAL5  
  , ST.U_LOCAL6   
  , st.ousrdata  
  , stmin     
  , isnull((select top 1  vumoeda  from bo (nolock) inner join bi (nolock) on bo.bostamp =  bi.bostamp  where  bo.ndos = 955  and  bi.ref = st.ref order by bo.dataobra  desc),0)         
  from  st(nolock)  
  inner join stobs on stobs.ref = st.ref           
  inner join fl (nolock) on st.fornec = fl.no   --and (fl.u_fmodego = 1 or  st.fornec = 3 )     
  where  stobs.u_pea = 1   and  st.inactivo  = 0  and  st.stns = 0 and st.fobloq = 0   --and  st.ref = '122214'  
 -- and  ST.U_CTPART = 'A'  
 -- and st.ref not in ( select ref  from st (nolock) where fornec = '804'and usr1 = '01') -- N�o utiliza artigos da SUKI se��o "01"        
   
  
--****************************************************************************************************************************                                               
                        
Declare @trimestre as int                                             
Declare @mes as int                                                    
Declare @ano as int                                                    
Declare @datainicial as datetime                                                    
Declare @datafinal as datetime                                                    
Declare @dataauxiliar as smalldatetime                                                    
Declare @acondicionamento as int                                                    
Declare @ref as char(18)                                                    
Declare @x1 as numeric(19, 6)                                                    
Declare @x2 as numeric(19, 6)                                                    
Declare @y1 as numeric(19, 6)                                                    
Declare @y2 as numeric(19, 6)     
Declare @stmin as numeric(19, 6)        
Declare @stmax as numeric(19, 6)                                                 
Declare @mescurso as numeric(19, 6)                                                   
Declare @qttfor as numeric(19, 6)   
Declare @qttcli as numeric(19, 6)                                                    
Declare @c as numeric(19, 6)                                                    
Declare @vpac as numeric(19, 6)                                         
Declare @pac as int                            
--Declare @estab as float                                
declare @estab_modelo as int                               
Declare @fornec as float                                                    
Declare @fornestab as float                                                    
Declare @prazoentrega numeric(5,0)       
Declare @PENTREGA_st  numeric(5,0)                                                  
Declare @cadencia as numeric(19, 6)                                                    
Declare @f as numeric(19, 6)                                                    
Declare @ss as numeric(19, 6)                                                  
Declare @qttenc as numeric(19, 6)                                                    
Declare @Novo as bit                                                   
Declare @y1_count as numeric(19, 6)                                                    
Declare @y1_med as numeric(19, 6)                        
Declare @x1_count as numeric(19, 6)                                                    
Declare @x1_med as numeric(19, 6)                                                    
Declare @x2_count as numeric(19, 6)                       
Declare @x2_med as numeric(19, 6)     
  
Declare @datainicial_x1 smalldatetime                                              
Declare @datafinal_x1 smalldatetime   
Declare @datainicial_y1 smalldatetime                                                    
Declare @datafinal_y1 smalldatetime   
Declare @datainicial_x2 smalldatetime                                                  
Declare @datafinal_x2 smalldatetime                                               
                                                    
Declare @x1_p as numeric(19, 6)                                                    
Declare @x2_p as numeric(19, 6)                                                    
Declare @y1_p as numeric(19, 6)                                                    
Declare @act_p as numeric(19, 6)                                               
                                                    
Declare @epcusto as numeric(19, 6)                          
Declare @cancelada as numeric (1,0)                         
                        
--variavel para o calculo de rupturas                                                    
declare @var_contador int                                                     
declare @stock_total numeric(10,3)                                                    
declare @movimentos_dia numeric(10,3)                                                    
declare @stock_auxiliar numeric(10,3)                                                    
declare @stock_actual numeric(10,3)                                                    
declare @rupturaX1 int                                                    
declare @rupturaX2 int                                                    
declare @rupturaY1 int                                                    
declare @rupturames int                                              
declare @contador numeric(10,0)                                                    
set @contador = 1   
  
declare @user_PEA_pazoentrega_PT numeric(5)  
declare @user_PEA_diasperiodo numeric(5)  
declare @PEA_max_racio_trimestre numeric(5)  
declare @PEA_pazofornec_vazio numeric(5)  
declare @armazem numeric(5)  
declare @novo_meses numeric(5)  
  
declare  @meses_stmin numeric(5)  
  
set  @armazem = 1001  
--set  @estab = 2   
  
declare  @stousrdata  smalldatetime    
declare  @stkseg  numeric(15,3)  
  
--  prazo de entrega de protutal    
 set  @user_PEA_pazoentrega_PT = 0   
 select @user_PEA_pazoentrega_PT = valor   from para1 (nolock) where descricao like  'user_PEA_pazoentrega_PT'  
  
--Dia do periodo  de analise    
 set  @user_PEA_diasperiodo = 0   
 select @user_PEA_diasperiodo = valor   from para1 (nolock) where descricao like  'user_PEA_diasperiodo'  
  
  -- Max de racio de trimestre      
 set  @PEA_max_racio_trimestre = 0   
 select @PEA_max_racio_trimestre = valor   from para1 (nolock) where descricao like  'user_PEA_max_racio_trimestre'  
  
   -- Prazo de entrega fornecedor vazio     
 set  @PEA_pazofornec_vazio  = 0   
 select @PEA_pazofornec_vazio  = valor   from para1 (nolock) where descricao like  'user_PEA_pazofornec_vazio '  
  
                                                     
  
--=======================================================================================================================                                                    
--Criar cursor com os artigos permanentes e com planograma                                                    
--=======================================================================================================================                                                    
Declare Cur Cursor Local Forward_Only For                                                 
  select  st.ref, u_pealj.pcusto , st.PENTREGA, stousrdata,  stkseg                         
  from  u_pealj  (nolock)  
  inner join st (nolock) on st.ref =  u_pealj.ref                        
--  where ref = '138922'                        
  order  by st.ref                        
Open Cur                                                    
Fetch Next From Cur Into @ref, @epcusto, @PENTREGA_st, @stousrdata  , @stkseg                                                  
While @@Fetch_Status = 0                                                    
Begin                          
 print '*************************************************'    
 print 'REFERENCIA -> ' + @ref                                               
                 
 set @y1 = 0                                                    
 set @y2 = 0                                                    
 set @x1 = 0                                                    
 set @x2 = 0                                                    
 set @c = 0                                       
 set @f = 0                              
 set @x1_count = 0                                
 set @x1_p = 0                                              
 set @y1_count = 0                                
 set @y1_p = 0                            
 set @x2_count = 0                                
 set @x2_p = 0                            
                        
 set @qttfor = 0    
  set @qttcli = 0   
    
    
                               
--=======================================================================================================================                        
-- STMIN -  consumo m�dio dos ultimos 2 anos ou desede que o artigo existe caso seja mais novo                                                  
--=======================================================================================================================                                                    
                        
  ----print ''                                                    
--  print 'STMIN'                                                    
 select @datainicial = getdate() - (365 * 2 )                                 
 select @datafinal = getdate()    
  
  
 if  @stousrdata > @datainicial  
 set  @datainicial = @stousrdata  
                                              
  
set @meses_stmin = datediff(mm,  @datainicial, @datafinal)   
  
  
                        
 select @stmin  = isnull(sum(qtt),0)                        
 from (                        
  select  case when ( isnull(sum(case when ft.tipodoc=3 then -fi.qtt else fi.qtt end),0) ) < 0             
    then 0 else ( isnull(sum(case when ft.tipodoc=3 then -fi.qtt else fi.qtt end),0) ) end as qtt                                                   
  from   fi (nolock)            
 inner join ft (nolock) on ft.ftstamp=fi.ftstamp  and  ft.anulado = 0                   
  where  ref = @ref             
 and ft.tipodoc<>5 and ft.ndoc in (select ndoc from td where nmdoc like @Loja)
 and convert(varchar(10), fdata, 121) between convert(varchar(10), @datainicial, 121) and convert(varchar(10), @datafinal, 121)                                                    
  group by ref                                                 
 ) as vw_pn    
   
   
 set  @stmin = @stmin /  @meses_stmin                     
                                               
                          
                             
--=======================================================================================================================                        
--X1 Vendas do trimestre anterior de 365 dias atr�s do dia presente                                                    
--=======================================================================================================================                                                    
                        
  ----print ''                                                    
--  print 'X1'                                                    
 select @datainicial = getdate() - 365 - @user_PEA_diasperiodo                                   
 select @datafinal = getdate() - 365    
   
 set  @datainicial_x1  =  @datainicial  
 set  @datafinal_x1 =  @datafinal  
   
 -- valida se o artigo � novo    
 set @novo = case when  @datainicial  <   @stousrdata then 1 else  0  end                                                
  
set @novo_meses = datediff(mm,  @stousrdata, getdate())   
  
  
                        
 select @x1 = isnull(sum(qtt),0)                        
 from (                        
  select  case when ( isnull(sum(case when ft.tipodoc=3 then -fi.qtt else fi.qtt end),0) ) < 0             
    then 0 else ( isnull(sum(case when ft.tipodoc=3 then -fi.qtt else fi.qtt end),0) ) end as qtt                                                   
  from   fi (nolock)            
 inner join ft (nolock) on ft.ftstamp=fi.ftstamp  and  ft.anulado = 0                   
  where  ref = @ref
  and ft.tipodoc<>5             
 and convert(varchar(10), fdata, 121) between convert(varchar(10), @datainicial, 121) and convert(varchar(10), @datafinal, 121)                                                    
  group by ref                                                 
 ) as vw_pn                        
               
                                                                 
-- Rutura   
if @x1<>0 and  @temruturas = 1     
begin    
 set @var_contador = 1                                                    
 set @stock_total = 0                                                    
 set @stock_auxiliar = 0                                                    
 set @movimentos_dia = 0                                                    
 set @stock_actual = 0                                                    
 set @rupturaX1 = 0    
  
 select @stock_auxiliar = isnull(sum(qtt),0)                        
 from (                        
    select  isnull(sum(case when cm < 50 then qtt else -qtt end), 0)as qtt               
    from SL(nolock)    
    inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                                                  
     and  convert(varchar(10), datalc, 121) <= convert(varchar(10), @datafinal, 121)                            
  ) as sl                 
                        
     ----print 'Stock Actual na data '  + convert(varchar(10), @datafinal, 121)                                                    
     ----print @stock_auxiliar                                                    
                                                        
     set @stock_actual = @stock_auxiliar                                                    
                                                     
     WHILE @var_contador <= @user_PEA_diasperiodo --dia de um trimestre                                                    
     BEGIN                            
              
     select @movimentos_dia = isnull(sum(qtt),0)                        
     from (                        
    select isnull(sum(case when cm < 50 then qtt else -qtt end),0) qtt                        
     from  SL(nolock)     
     inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                      
     and convert(char(10),datalc,121) = convert(char(10),@datafinal-@var_contador,121)                                       
   ) as sl                        
                        
    if @stock_auxiliar <= 0                                 
     set @rupturaX1 = @rupturaX1 + 1                                  
                                                               
     set @stock_auxiliar = @stock_auxiliar - @movimentos_dia--controlo diario de stock(vai andando atras nas datas)                                                     
                                                        
     set @var_contador = @var_contador + 1                                                    
    END       
   
     -- Fim de criar tabela com os �ltimos 120 dias-- C�lculo do n�mero dias com stock                                                                          
     set @x1 = case when (@user_PEA_diasperiodo-@rupturax1) = 0 then (@user_PEA_diasperiodo*(@x1))/1 else (@user_PEA_diasperiodo*(@x1))/(@user_PEA_diasperiodo-@rupturax1) end    
end    
-- FIM  Rutura   
                                                
print '@x1 -> ' + ltrim(rtrim(str(@x1)))    
--=======================================================================================================================                                                    
  --Y1 Vendas do mesmo trimestre 365 dias atr�s do dia presente                                                    
--=======================================================================================================================                                                    
  ----print ''                                                    
  ----print 'y1'                                                    
                                                      
  select @datainicial = getdate() - 365                                        
  ----print convert(varchar(10), @datainicial, 121) + ' Data Inicial'                                                    
  select @datafinal = getdate() - 365 + @user_PEA_diasperiodo                                                   
  ----print convert(varchar(10), @datafinal, 121) + ' Data Final'  
      
   set  @datainicial_y1  =  @datainicial  
   set  @datafinal_y1 =  @datafinal                                                   
                        
 select @y1 = isnull(sum(qtt),0)                        
 from (                        
  select  case when ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) < 0 then 0 else ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) end as qtt                                                   
  from  fi (nolock)            
 inner join ft (nolock) on ft.ftstamp=fi.ftstamp  and  ft.anulado = 0                   
  where  ref = @ref
  and ft.tipodoc<>5                                
   and convert(varchar(10), fdata, 121) between convert(varchar(10), @datainicial, 121) and convert(varchar(10), @datafinal, 121)                                                    
  group by ref                          
                 
 ) as vw_pn                        
   
  
-- Rutura   
if @y1<>0  and  @temruturas = 1    
begin    
 set @var_contador = 1                                                    
 set @stock_total = 0                                                    
 set @stock_auxiliar = 0                                                    
 set @movimentos_dia = 0                                                    
 set @stock_actual = 0                                                    
 set @rupturaY1 = 0     
  
 select @stock_auxiliar = isnull(sum(qtt),0)                        
 from (                        
    select  isnull(sum(case when cm < 50 then qtt else -qtt end), 0)as qtt               
    from SL(nolock)    
    inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                                                  
     and  convert(varchar(10), datalc, 121) <= convert(varchar(10), @datafinal, 121)                            
  ) as sl                 
                        
     ----print 'Stock Actual na data '  + convert(varchar(10), @datafinal, 121)                                                    
     ----print @stock_auxiliar                                                    
                                                        
     set @stock_actual = @stock_auxiliar                                                    
                                                     
     WHILE @var_contador <= @user_PEA_diasperiodo --dia de um trimestre                                                    
     BEGIN                            
              
     select @movimentos_dia = isnull(sum(qtt),0)                        
     from (                        
    select isnull(sum(case when cm < 50 then qtt else -qtt end),0) qtt                        
     from  SL(nolock)     
     inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                      
     and convert(char(10),datalc,121) = convert(char(10),@datafinal-@var_contador,121)                                       
   ) as sl                        
                        
    if @stock_auxiliar <= 0                                 
     set @rupturay1 = @rupturay1 + 1                                  
                                                               
     set @stock_auxiliar = @stock_auxiliar - @movimentos_dia--controlo diario de stock(vai andando atras nas datas)                                                     
                                                        
     set @var_contador = @var_contador + 1                                                    
    END       
   
     -- Fim de criar tabela com os �ltimos 120 dias-- C�lculo do n�mero dias com stock                          
                                                        
     set @y1 = case when (@user_PEA_diasperiodo-@rupturay1) = 0 then (@user_PEA_diasperiodo*(@y1))/1 else (@user_PEA_diasperiodo*(@y1))/(@user_PEA_diasperiodo-@rupturay1) end                                                     
end    
-- FIM  Rutura   
            
   
print '@Y1 -> '+ ltrim(rtrim(str(@Y1)))                            
--=======================================================================================================================                                                      
  --X2 Vendas do trimestre anterior ao dia presente                                                    
--=======================================================================================================================                                                    
  ----print ''                                                    
  ----print 'X2'   
                                  
  select @datainicial = getdate() - @user_PEA_diasperiodo                                                                                                   
  select @datafinal = getdate()                                                                                                     
     
   set  @datainicial_x2  =  @datainicial  
 set  @datafinal_x2 =  @datafinal                     
  
  
 select @x2 = isnull(sum(qtt),0)                        
 from (                        
  select  case when ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) < 0 then 0 else ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) end as qtt                                                   
  from  fi (nolock)            
 inner join ft (nolock) on ft.ftstamp=fi.ftstamp   and  ft.anulado = 0                   
  where  ref = @ref
  and ft.tipodoc<>5                                            
   and convert(varchar(10), fdata, 121) between convert(varchar(10), @datainicial, 121) and convert(varchar(10), @datafinal, 121)                                                    
  group by ref                                          
 ) as vw_pn     
   
   
-- Rutura   
if @x2<>0  and  @temruturas = 1   
begin    
 set @var_contador = 1                                                    
 set @stock_total = 0                                                    
 set @stock_auxiliar = 0                                                    
 set @movimentos_dia = 0                                                    
 set @stock_actual = 0                                                    
 set @rupturax2 = 0     
  
 select @stock_auxiliar = isnull(sum(qtt),0)                        
 from (                        
    select  isnull(sum(case when cm < 50 then qtt else -qtt end), 0)as qtt               
    from SL(nolock)    
    inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                                                  
     and  convert(varchar(10), datalc, 121) <= convert(varchar(10), @datafinal, 121)                            
  ) as sl                 
                        
     ----print 'Stock Actual na data '  + convert(varchar(10), @datafinal, 121)                                                    
     ----print @stock_auxiliar                                                    
                                                        
     set @stock_actual = @stock_auxiliar                                                    
                                                     
     WHILE @var_contador <= @user_PEA_diasperiodo --dia de um trimestre                                                    
     BEGIN                            
              
     select @movimentos_dia = isnull(sum(qtt),0)                        
     from (                        
    select isnull(sum(case when cm < 50 then qtt else -qtt end),0) qtt                        
     from  SL(nolock)     
     inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1      
    where  ref = @ref                      
     and convert(char(10),datalc,121) = convert(char(10),@datafinal-@var_contador,121)                                       
   ) as sl                        
                        
    if @stock_auxiliar <= 0                                 
     set @rupturax2 = @rupturax2 + 1                                  
                                                               
     set @stock_auxiliar = @stock_auxiliar - @movimentos_dia--controlo diario de stock(vai andando atras nas datas)                                                     
                                                        
     set @var_contador = @var_contador + 1                                                    
    END       
   
     -- Fim de criar tabela com os �ltimos 120 dias-- C�lculo do n�mero dias com stock                          
                                                        
     set @x2 = case when (@user_PEA_diasperiodo-@rupturax2) = 0 then (@user_PEA_diasperiodo*(@x2))/1 else (@user_PEA_diasperiodo*(@x2))/(@user_PEA_diasperiodo-@rupturax2) end                                                     
end    
-- FIM  Rutura   
   
  
print '@x2 -> ' + ltrim(rtrim(str(@x2)))                          
--=======================================================================================================================                                                      
  -- Vendas mes em curso                                                    
--=======================================================================================================================   
-- n�o � utilizado                                                 
select  @mescurso = case when ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) < 0 then 0 else ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) end                                                    
from  fi (nolock)            
 inner join ft (nolock) on ft.ftstamp=fi.ftstamp and  ft.anulado = 0                                                  
where  ref = @ref and 0 = 1                                                                           
 and fdata between getdate()-30 and getdate()                                                   
--=======================================================================================================================                                                      
  -- Stock Actual                                                    
--=======================================================================================================================                                                    
select  @stock_actual = isnull(sum(case when cm < 50 then qtt else -qtt end), 0)                                                    
from  sl (nolock)  
inner  join sz(nolock) on  sl.armazem =  sz.no and  sz.u_peasede = 1                                                        
where  ref = @ref   
  -- and armazem = @armazem                                                     
 and  convert(varchar(10), datalc, 121) <= convert(varchar(10), getdate(), 121)                                                    
                           
                                                
--=======================================================================================================================                                                      
  --Encomendas em curso                                                    
--=======================================================================================================================                                                    
select  @qttfor = isnull(sum(bi.qtt-bi.qtt2), 0)       
FROM bi  (nolock)                                                     
 inner join bo(nolock) on bo.bostamp = bi.bostamp  
inner join ts (nolock) on ts.ndos =  bo.ndos  and  ts.resfor = 1   
 inner join bo2 on bo2.bo2stamp =  bo.bostamp  and  bo2.anulado = 0       
where  bi.ref=@ref and bo.fechada = 0    
and ts.u_estab = @estab   
and  bo.ndos in (936,203,2)   
  
select  @qttcli = isnull(sum(bi.qtt-bi.qtt2), 0)       
FROM bi  (nolock)                                                     
 inner join bo(nolock) on bo.bostamp = bi.bostamp  
inner join ts (nolock) on ts.ndos =  bo.ndos  and  ts.rescli = 1   
 inner join bo2 on bo2.bo2stamp =  bo.bostamp  and  bo2.anulado = 0       
where  bi.ref=@ref and bo.fechada = 0    
and ts.u_estab = @estab       
  
  
                                                 
--=======================================================================================================================                                                      
  --Periodo a Cobrir                                                    
--=======================================================================================================================                                                    
select @fornec = fornec, @fornestab = fornestab                         
from st(nolock)   
where ref = @ref                        
               
if @PENTREGA_st <> 0   
begin    
       set @prazoentrega = @PENTREGA_st  
end   
else    
begin                           
       select @prazoentrega = PENTREGA from fl(nolock) where no = @fornec and estab = @fornestab   
 end   
  
if @prazoentrega =0   
 set  @prazoentrega = @PEA_pazofornec_vazio   
                        
 print '@prazoentrega -> ' + ltrim(rtrim(str(@prazoentrega)))    
                                                                                                                                                                                 
                      
set  @pac = @prazoentrega + @user_PEA_pazoentrega_PT  
  
print 'PAC ' + str(@pac)  
  
--=======================================================================================================================                            
  --Coeficiente de Evolu��o de Vendas(C)                                                    
--=======================================================================================================================                                                    
 if @x1 <> 0                                                    
 BEGIN                                                    
   if @x2 <> 0                                                     
    select @c =  ( isnull(@x2 / @x1, 0) ) --/ 3                                                                                                    
 END                                                    
 else                                                    
 BEGIN                                                    
 --print 'o x1 � zero, logo n�o se pode calcular o C'                                                    
  set @c = 1--/3                        
 END  
  
if @c > @PEA_max_racio_trimestre    
       set  @c = @PEA_max_racio_trimestre                                                 
                        
print 'c -> ' + ltrim(rtrim(str(@c)))    
--=======================================================================================================================                                                    
  --Vendas Previstas para o Periodo a Cobrir(VPAC)                                                    
--=======================================================================================================================                                                    
   
   
 --select @vpac = ( isnull(( case when @y1 = 0 then 1 else @y1 end ) * @c * @pac, 0) ) / @user_PEA_diasperiodo   
                                           
 if   @y1 <> 0   
begin   
       set @vpac = (isnull(@y1 * @c * @pac,0)) / @user_PEA_diasperiodo  
end    
 else  
begin      
       set @vpac =   0 -- JR 20200407  
end                                          
 print 'Vendas Previstas -> ' + ltrim(rtrim(str(@vpac)))    
                                                    
if  (@c = 0  or  @y1 = 0) and  @vpac = 0   
begin    
  set @vpac = case when  @x1 = 0  and  @x2  <> 0 and @y1 <> 0   
       then  (((@x2+@y1)/2) *@pac)/ @user_PEA_diasperiodo  
       else case when  @x1 <> 0  and  @x2  = 0 and @y1 <> 0   
          then   (((@x1+@y1)/2) *@pac)/ @user_PEA_diasperiodo    
          else case when  @x1 <> 0  and  @x2  <> 0 and @y1 = 0   
            then  (((@x1+@x2)/2) *@pac)/ @user_PEA_diasperiodo  
            else  0   
            end    
          end     
       end  
  
end    
   
  
  
--se for artigo  novo                            
  
if  @novo = 1    
begin   
        select @vpac = isnull(sum(qtt),0)                        
        from (                        
         select  case when ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) < 0 then 0 else ( isnull(sum((case when ft.tipodoc=3 then -fi.qtt else fi.qtt end)),0) ) end as qtt                                                   
         from  fi (nolock)            
        inner join ft (nolock) on ft.ftstamp=fi.ftstamp   and  ft.anulado = 0                   
         where  ref = @ref                                                    
          and convert(varchar(10), fdata, 121) between convert(varchar(10), @stousrdata, 121) and convert(varchar(10), getdate(), 121)                                                    
         group by ref                                          
        ) as vw_pn   
          
        set  @vpac =  @vpac / datediff(mm,@stousrdata , getdate())  
       set  @vpac =  @vpac * (@pac/30)  
end   
                                                      
--=======================================================================================================================                                                    
  --Quantidade a encomendar          
--=======================================================================================================================                                                    
 select @acondicionamento = eoq from u_pealj(nolock) where ref = @ref                                                                        
                                                 
 set @qttenc = case when  @vpac<= @stock_actual + @qttfor - @qttcli  then 0 else  @vpac-(@stock_actual + @qttfor - @qttcli) end                                                    
                        
 set @qttenc = Ceiling (@qttenc)                                                    
   
 --=======================================================================================================================                                                    
  --Stock minimo de seguran�a (SS)                                                    
--=======================================================================================================================   
  
   if  @stkseg >= @qttenc and  @stkseg <> 0   
   begin   
             set @qttenc = @stkseg  
   end   
  
--=====================================================================================================  
-- quando o artigo � novo  e n�o tem encomendas a fornecedor   
--=====================================================================================================  
  
-- JR 2020-04-07 destivado a pedido do Ricardo quando � novo n�o encomenda   
if @qttenc = 0  and  (@stock_actual + @qttfor - @qttcli) = 0 and @novo_meses <= 6 and  0 = 1   /* 6 Meses */   
begin    
       set @qttenc = 1   
end    
  
--=======================================================================================================================                                                    
  --Acondicionamento    
--=======================================================================================================================   
  
                                                
                                                                         
 if @acondicionamento = 0                                                     
  set @acondicionamento = 1                                                    
 if @acondicionamento <> 1                                                     
 begin                                                     
  declare @fact numeric(15,5)                                                      
   set @fact =  ceiling(@qttenc /@acondicionamento)                                                    
  if @fact < 1                        
   begin                        
    set @fact = 1                        
   end                        
                        
    if convert(int, @qttenc) % @acondicionamento <> 0                    
    BEGIN                                                    
        set @qttenc = @acondicionamento * @fact           
      END                                                    
     ELSE                      
     BEGIN                                                    
       set @qttenc = @qttenc                                                                                                  
     END                                                    
 end                          
 print '@qttenc -> ' + ltrim(rtrim(str(@qttenc)))    
--=======================================================================================================================                                                    
  --Actualizar os dados do artigo na tabela u_pealj                                                    
--=======================================================================================================================                                                    
                        
  --print 'Vai ser actualizada a tabela u_pea '                                                    
       print 'X1 '  + str(@x1 )       
  update  u_pealj                                                        
  set  x1 =@x1,                                            
    y1 =@y1,            
       y2 = @y2,                                  
    x2 = @X2,                                                                                  
    c = @c,                                                                                    
    stkseg =  @stkseg,                                 
    pac =  @pac ,         
    vpac =  @vpac ,                                            
    qttfor =  @qttfor,   
       qttcli =  @qttcli,                                                                                                                              
    stock =  @stock_actual,                                            
    qttenc = @qttenc,                                            
    Novo = @novo ,  
  stmin =  @stmin ,   
  x1dataini  =  @datainicial_x1,  
 x1datafim =  @datafinal_x1,  
  y1dataini  =  @datainicial_y1,  
 y1datafim =  @datafinal_y1,  
  x2dataini  =  @datainicial_x2,  
 x2datafim =  @datafinal_x2  
   ,  per1 =  case when  @x1 = 0  and  @x2  <> 0 and @y1 <> 0 then  1   
       else case when  @x1 <> 0  and  @x2  = 0 and @y1 <> 0   
          then  1    
          else case when  @x1 <> 0  and  @x2  <> 0 and @y1 = 0   
            then  1   
            else  0   
            end    
          end     
       end   
  ,  per2 =  case when  @x1 <> 0  and  @x2 = 0 and @y1 = 0 then  1   
       else case when  @x1 =  0  and  @x2  <> 0 and @y1 = 0   
          then  1    
          else case when  @x1 = 0  and  @x2  = 0 and @y1 <> 0   
            then  1   
            else  0   
            end    
          end     
       end                                                  
  where  ref = @ref                                    
                                                 
  print '==========================================================='                                                        
  print 'Actualizado o artigo: ' + @ref                                                   
  print 'registo: ' + convert(char(10),@contador)                                                        
  print '==========================================================='                                                        
  set @contador = @contador + 1                                                    
                        
  Fetch Next From Cur Into @ref,@epcusto, @PENTREGA_st , @stousrdata  , @stkseg                                                     
                       
End                                              
Close Cur                                                    
Deallocate Cur                       
                                                     
                                                    
                        
--DELETE from u_pea where NOT (qttenc <>0 or (novo <> ''and novo <> ';C') or (novo= ';C' and stock<2) or (qttfor > 0 and (stock = 0 or stock = 1)) )                       
                  
  
  