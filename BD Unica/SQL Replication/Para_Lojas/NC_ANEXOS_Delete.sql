USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dboanexos]    Script Date: 07/09/2023 14:00:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_ANEXOS_Delete]
		@c1 char(25),
		@c2 varchar(80),
		@c3 char(80),
		@c4 varchar(80),
		@c5 varchar(80),
		@c6 char(25),
		@c7 varchar(80),
		@c8 text,
		@c9 image,
		@c10 text,
		@c11 varchar(150),
		@c12 varchar(30),
		@c13 numeric(12,0),
		@c14 numeric(1,0),
		@c15 varchar(30),
		@c16 text,
		@c17 varchar(80),
		@c18 numeric(3,0),
		@c19 numeric(3,0),
		@c20 varchar(30),
		@c21 datetime,
		@c22 varchar(8),
		@c23 varchar(30),
		@c24 datetime,
		@c25 varchar(8),
		@c26 varchar(80),
		@c27 bit,
		@c28 bit,
		@c29 varchar(80),
		@c30 numeric(3,0),
		@c31 varchar(80),
		@c32 varchar(30),
		@c33 datetime,
		@c34 varchar(8),
		@c35 varchar(30),
		@c36 datetime,
		@c37 varchar(8),
		@c38 bit,
		@c39 bit,
		@c40 varchar(80),
		@c41 bit,
		@c42 bit,
		@c43 numeric(4,0),
		@c44 varchar(55),
		@c45 numeric(6,0),
		@c46 varchar(55),
		@c47 numeric(1,0),
		@c48 text,
		@c49 datetime,
		@c50 numeric(1,0),
		@c51 varchar(25),
		@c52 text,
		@c53 numeric(1,0),
		@c54 numeric(2,0),
		@c55 bit,
		@c56 varchar(30),
		@c57 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[anexos] 
	where [anexosstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[anexosstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[anexos]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from anexos where anexosstamp=@c1