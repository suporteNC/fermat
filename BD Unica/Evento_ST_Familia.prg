*************************************
*** Preenche Familia e Tipo de artigo
*** Criado por Rui Vale
*** Criado em  30/11/2017
*************************************

If !Empty(st.u_csubfam)
	**If Empty(st.familia)
		TEXT TO uSql TEXTMERGE noshow
			Select ref,nome
			From stfami (nolock)
			Where ref = '<<LEFT(ALLTRIM(st.u_csubfam),3)>>'
		ENDTEXT

		If u_sqlexec(uSql,"uCurStFami") And Reccount("uCurStFami")>0
			Select st
			Replace st.familia With uCurStFami.ref
			Replace st.faminome With uCurStFami.Nome
		Else
			Select st
			Replace st.familia With ""
			Replace st.faminome With ""		
		Endif
	**Endif
	**If Empty(st.u_ctpart)
		TEXT TO uSql TEXTMERGE noshow
			Select ctpart,tpart
			From u_sttpart (nolock)
			Where ctpart = '<<LEFT(ALLTRIM(st.u_csubfam),1)>>'
		ENDTEXT

		If u_sqlexec(uSql,"uCurTpArt") And Reccount("uCurTpArt")>0
			Select st
			Replace st.u_ctpart With uCurTpArt.ctpart
			Replace st.u_tpart With uCurTpArt.tpart
		Else
			Select st
			Replace st.u_ctpart With ""
			Replace st.u_tpart With ""
		Endif
	**Endif
Endif
