USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_strel]    Script Date: 07/02/2023 19:21:58 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_UATT_Delete]
		@c1 varchar(25),
		@c2 text,
		@c3 text,
		@c4 varchar(10),
		@c5 varchar(25),
		@c6 date,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 date,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 bit,
		@c13 text
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [dbo].[u_attachment] 
	where [u_attachmentstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_attachmentstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[u_attachment]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_attachment where u_attachmentstamp=@c1