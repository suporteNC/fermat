USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_sttpart]    Script Date: 24/01/2023 18:01:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_STTPART_Update]
		@c1 char(25),
		@c2 varchar(10),
		@c3 varchar(30),
		@c4 varchar(30),
		@c5 datetime,
		@c6 varchar(8),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 bit,
		@c11 datetime,
		@c12 char(25),
		@c13 varchar(10),
		@c14 varchar(30),
		@c15 varchar(30),
		@c16 datetime,
		@c17 varchar(8),
		@c18 varchar(30),
		@c19 datetime,
		@c20 varchar(8),
		@c21 bit,
		@c22 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c12 = @c1)
begin 
update [PG].[u_sttpart] set
		[u_sttpartstamp] = @c12,
		[ctpart] = @c13,
		[tpart] = @c14,
		[ousrinis] = @c15,
		[ousrdata] = @c16,
		[ousrhora] = @c17,
		[usrinis] = @c18,
		[usrdata] = @c19,
		[usrhora] = @c20,
		[marcada] = @c21,
		[u_ttssinc] = @c22
	where [u_sttpartstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_sttpartstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_sttpart]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[u_sttpart] set
		[ctpart] = @c13,
		[tpart] = @c14,
		[ousrinis] = @c15,
		[ousrdata] = @c16,
		[ousrhora] = @c17,
		[usrinis] = @c18,
		[usrdata] = @c19,
		[usrhora] = @c20,
		[marcada] = @c21,
		[u_ttssinc] = @c22
	where [u_sttpartstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_sttpartstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_sttpart]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','u_sttpart',@c1, 'u_sttpartstamp', ''