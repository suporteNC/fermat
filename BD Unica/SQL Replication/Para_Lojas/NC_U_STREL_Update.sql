USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_strel]    Script Date: 07/02/2023 19:23:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_STREL_Update]
		@c1 char(25),
		@c2 char(25),
		@c3 varchar(18),
		@c4 varchar(60),
		@c5 varchar(30),
		@c6 datetime,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 bit,
		@c12 char(25),
		@c13 char(25),
		@c14 varchar(18),
		@c15 varchar(60),
		@c16 varchar(30),
		@c17 datetime,
		@c18 varchar(8),
		@c19 varchar(30),
		@c20 datetime,
		@c21 varchar(8),
		@c22 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c12 = @c1)
begin 
update [PG].[u_strel] set
		[u_strelstamp] = @c12,
		[ststamp] = @c13,
		[ref] = @c14,
		[design] = @c15,
		[ousrinis] = @c16,
		[ousrdata] = @c17,
		[ousrhora] = @c18,
		[usrinis] = @c19,
		[usrdata] = @c20,
		[usrhora] = @c21,
		[marcada] = @c22
	where [u_strelstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_strelstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_strel]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[u_strel] set
		[ststamp] = @c13,
		[ref] = @c14,
		[design] = @c15,
		[ousrinis] = @c16,
		[ousrdata] = @c17,
		[ousrhora] = @c18,
		[usrinis] = @c19,
		[usrdata] = @c20,
		[usrhora] = @c21,
		[marcada] = @c22
	where [u_strelstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_strelstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_strel]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','u_strel',@c1, 'u_strelstamp', ''