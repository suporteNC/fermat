
** Criado por TOTALSOFT | Paulo Ricardo Martins
** Data 03/08/2017
** Lan�ar desconto em cart�o
**************************************************

Select FPOSC
If FPOSC.ETOTAL <= 0
	MSG('Para aplicar desconto de cart�o tem de ter valores no documento')
	Return
Endif

MSG('Para Descontar valor em cart�o por favor confirme Cart�o de Cliente')
M_CARTAOCL = GETNOME('Passe Cart�o de Cliente!','','','',1,.T.)

If Empty(M_CARTAOCL)
	MSG('Ac��o cancelada pelo Utilizador')
	fecha('CR_CLTMP')
	Return
Endif

TEXT TO MSEL TEXTMERGE NOSHOW
	SELECT CL.*
	FROM CL (NOLOCK)
	INNER JOIN CL2 (NOLOCK) on cl2.cl2stamp = cl.clstamp
	WHERE CL2.u_NCARTAO = '<<ALLTRIM(M_CARTAOCL)>>'
ENDTEXT

If !U_sqlexec(MSEL,'CR_CLTMP')
	MSG('ERRO ENCONTRADO')
	MSG(MSEL)
	Return
Endif

Select CR_CLTMP
If Reccount()=0
	MSG('Cart�o de Cliente Invalido')
	Return
Endif

Select CL
v_clstamp = CR_CLTMP.CLSTAMP
U_requery('CL')

Select FPOSC
Replace FPOSC.no With CL.no
Replace FPOSC.estab With CL.estab

FTCLACT("MAIN","FPOSC","FPOSL")
stouchpos.posmetodos1.clivd = CL.clivd

stouchpos.Zonafposcinfo.Painel.Tobj1.Lblinfo.Caption = Alltrim(CL.Nome) + ' | ID '+Alltrim(m.m_chnome)

Select FPOSC

TEXT TO MSEL TEXTMERGE NOSHOW
	SELECT sum(debito-credito) as PONTOS
	FROM u_CTM (NOLOCK)
	WHERE u_CTM.NCARTAO = '<<ALLTRIM(M_CARTAOCL)>>'
	GROUP BY NCARTAO
ENDTEXT

If !U_sqlexec(MSEL,'CR_CARTAOMOV')
	MSG('ERRO ENCONTRADO')
	MSG(MSEL)
	Return
Endif

If Type("P_PONTOSVAL")="U"
	Public P_PONTOSVAL
	P_PONTOSVAL = 0.00
Endif

Select CR_CARTAOMOV
M_VAL = CR_CARTAOMOV.PONTOS*P_PONTOSVAL
M_VAL = GETNOME('PLAFFOND EM CART�O: '+Alltrim(Str(M_VAL,14,2))+Chr(13)+'Qual o valor a Descontar em Cart�o',M_VAL)

If M_VAL > 0

	If !PERGUNTA('CONFIRMA TROCA DE PONTOS DE CART�O CLIENTE')
		MSG('Cancelado pelo utilizador')
		Return
	Endif


** Cria Movimento no CC
****************************************************

	TEXT TO MCAMPOS TEXTMERGE NOSHOW
			[ccstamp]
           ,[datalc]
           ,[dataven]
           ,[cmdesc]
           ,[nrdoc]
           ,[deb]
           ,[cred]
           ,[edeb]
           ,[ecred]
           ,[nome]
           ,[moeda]
           ,[ultdoc]
           ,[no]
           ,[cm]
           ,[cr]
           ,[docref]
           ,[debf]
           ,[edebf]
           ,[credf]
           ,[ecredf]
           ,[fref]
           ,[ccusto]
           ,[ncusto]
           ,[origem]
           ,[debm]
           ,[credm]
           ,[debfm]
           ,[credfm]
           ,[zona]
           ,[recibado]
           ,[vendedor]
           ,[vendnm]
           ,[crdesc]
           ,[incobra]
           ,[ftndoc]
           ,[ftfno]
           ,[obs]
           ,[obscob]
           ,[fmarcada]
           ,[segmento]
           ,[recino]
           ,[recian]
           ,[odstamp]
           ,[escvtmp]
           ,[vtmp]
           ,[evtmp]
           ,[cobranca]
           ,[ftstamp]
           ,[restamp]
           ,[cambiofixo]
           ,[edifcambio]
           ,[difcambio]
           ,[tipo]
           ,[pais]
           ,[estab]
           ,[ivav1]
           ,[ivav2]
           ,[ivav3]
           ,[ivav4]
           ,[ivav5]
           ,[ivav6]
           ,[ivav7]
           ,[ivav8]
           ,[ivav9]
           ,[eivav1]
           ,[eivav2]
           ,[eivav3]
           ,[eivav4]
           ,[eivav5]
           ,[eivav6]
           ,[eivav7]
           ,[eivav8]
           ,[eivav9]
           ,[ftccstamp]
           ,[rdstamp]
           ,[cobrador]
           ,[rota]
           ,[clbanco]
           ,[clcheque]
           ,[valch]
           ,[evalch]
           ,[valre]
           ,[evalre]
           ,[mvalre]
           ,[intid]
           ,[lestamp]
           ,[lrstamp]
           ,[lmstamp]
           ,[tpstamp]
           ,[tpdesc]
           ,[chstamp]
           ,[covezes]
           ,[virs]
           ,[evirs]
           ,[virsreg]
           ,[evirsreg]
           ,[irsdif]
           ,[eirsdif]
           ,[difccont]
           ,[edifccont]
           ,[faccstamp]
           ,[faclstamp]
           ,[occstamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[marcada]
           ,[dispcbb]
           ,[cbbno]
           ,[ivatx1]
           ,[ivatx2]
           ,[ivatx3]
           ,[ivatx4]
           ,[ivatx5]
           ,[ivatx6]
           ,[ivatx7]
           ,[ivatx8]
           ,[ivatx9]
           ,[reexgiva]
           ,[formapag]
           ,[situacao]
           ,[cobradovunicre]
           ,[facstamp]
           ,[recindoc]
           ,[ccndoc]
           ,[ccnmdoc]
           ,[cobradovpaypal]
           ,[datasup12]
           ,[cecope]
           ,[operext]
           ,[rerevstamp]
	ENDTEXT

	M_CCSTAMP = U_STAMP()

	TEXT TO MVALORES TEXTMERGE NOSHOW
			'<<M_CCSTAMP>>'
           ,getdate()
           ,getdate()
           ,'Vale em Cart�o'
           ,(select count(*)+1 from CC where cm=122 and year(datalc)=year(getdate()))
           ,0
           ,<<ALLTRIM(STRTRAN(STR(M_VAL,14,2),',','.'))>>
           ,0
           ,<<ALLTRIM(STRTRAN(STR(M_VAL,14,2),',','.'))>>
           ,'<<FPOSC.NOME>>'
           ,'<<FPOSC.MOEDA>>'
           ,''
           ,<<FPOSC.NO>>
           ,122
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,''
           ,''
           ,'U_CTM'
           ,0
           ,0
           ,0
           ,0
           ,''
           ,0
           ,<<FPOSC.VENDEDOR>>
           ,'<<FPOSC.VENDNM>>'
           ,''
           ,0
           ,0
           ,0
           ,''
           ,''
           ,0
           ,''
           ,0
           ,0
           ,''
           ,0
           ,0
           ,0
           ,''
           ,''
           ,''
           ,0
           ,0
           ,0
           ,''
           ,<<FPOSC.PAIS>>
           ,<<FPOSC.ESTAB>>
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,''
           ,''
           ,'<<m.m_chinis>>'
           ,convert(char(10),getdate(),104)
           ,convert(char(10),getdate(),108)
           ,'<<m.m_chinis>>'
           ,convert(char(10),getdate(),104)
           ,convert(char(10),getdate(),108)
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,''
           ,0
           ,''
           ,0
           ,0
           ,''
           ,0
           ,'19000101'
           ,''
           ,0
           ,''
	ENDTEXT


	TEXT TO MCOMANDO TEXTMERGE NOSHOW
	INSERT INTO CC (<<MCAMPOS>>)
	SELECT <<MVALORES>>
	ENDTEXT

	If !U_sqlexec(MCOMANDO)
		MSG('Erro Encontrado')
		MSG(MCOMANDO)
		Return
	ENDIF
	
** FIM - Cria Movimento no CC
**********************************************************************************


** Cria Movimento no U_CTM
****************************************************

	TEXT TO MCAMPOS TEXTMERGE NOSHOW
			[u_ctmstamp]
           ,[data]
           ,[cmdesc]
           ,[nrdoc]
           ,[debito]
           ,[credito]
           ,[loja]
           ,[no]
           ,[nome]
           ,[saldo]
           ,[ftstamp]
           ,[tipo]
           ,[ncartao]
           ,[oristamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[marcada]
	ENDTEXT

	TEXT TO MVALORES TEXTMERGE NOSHOW
			'<<U_STAMP()>>'
           ,GETDATE()
           ,'Vale em Cart�o'
           ,(select count(*) from CC where cm=122 and year(datalc)=year(getdate()))
           ,<<ALLTRIM(STRTRAN(STR(M_VAL,14,2),',','.'))>>
           ,0
           ,1
           ,'<<FPOSC.NO>>'
           ,'<<FPOSC.NOME>>'
           ,0
           ,''
           ,''
           ,'<<FPOSFT2.u_NCARTAO>>'
           ,''
           ,'<<m.m_chinis>>'
           ,convert(char(10),getdate(),104)
           ,convert(char(10),getdate(),108)
           ,'<<m.m_chinis>>'
           ,convert(char(10),getdate(),104)
           ,convert(char(10),getdate(),108)
           ,0
	ENDTEXT


	TEXT TO MCOMANDO TEXTMERGE NOSHOW
	INSERT INTO U_CTM (<<MCAMPOS>>)
	SELECT <<MVALORES>>
	ENDTEXT

	If !U_sqlexec(MCOMANDO)
		MSG('Erro Encontrado')
		MSG(MCOMANDO)
		Return
	Endif
** FIM - Cria Movimento no U_CTM
**********************************************************************************

** Regulariza FTRD
********************************************************

	Select FPOSC
	M_TOTALDOC = FPOSC.ETOTAL

	TEXT TO MSEL TEXTMERGE NOSHOW
	SELECT
		convert (bit,0)Marcada
		, left(replace(newid(),'-',''),24)fposrdstamp
		, CC.nrdoc, CC.datalc
		, CC.moeda, CC.no
		, cc.cmdesc, cc.cm
		,(ecred-ecredf) as evemissao
		,(ecred-ecredf) as evreg
		, CC.ccstamp
		, CC.nome, 0 as eivav1, 0 as eivav2, 0 eivav3, 0 eivav4, 0 eivav5, 0 eivav6
		, CC.ecred
	FROM CC (nolock)
	WHERE CC.CCSTAMP = '<<M_CCSTAMP>>'
		and CC.ECRED <> CC.ECREDF
	ENDTEXT

	If !U_sqlexec(MSEL,'CR_FTRDTMP')
		MSG('ERRO ENCONTRADO')
		MSG(MSEL)
		Return
	Endif

	Select CR_FTRDTMP
	Go Top

	Select fposrd
	Append Blank
	Replace fposrd.fposrdstamp With CR_FTRDTMP.fposrdstamp
	Replace fposrd.cdesc With CR_FTRDTMP.cmdesc
	Replace fposrd.nrdoc With CR_FTRDTMP.nrdoc
	Replace fposrd.datalc With CR_FTRDTMP.datalc
	Replace fposrd.ftstamp With FPOSC.fposcstamp
	Replace fposrd.ccstamp With CR_FTRDTMP.ccstamp
	Replace fposrd.evemissao With CR_FTRDTMP.evemissao
	Replace fposrd.vemissao With CR_FTRDTMP.evemissao
	Replace fposrd.evreg With Iif(CR_FTRDTMP.evreg <= M_TOTALDOC, CR_FTRDTMP.evreg, M_TOTALDOC)
	Replace fposrd.vreg With Iif(CR_FTRDTMP.evreg <= M_TOTALDOC, CR_FTRDTMP.evreg, M_TOTALDOC)
	Replace fposrd.eivav1 With 0
	Replace fposrd.eivav2 With 0
	Replace fposrd.eivav3 With 0
	Replace fposrd.eivav4 With 0
	Replace fposrd.eivav5 With 0
	Replace fposrd.eivav6 With 0
	Replace fposrd.ivav1 With fposrd.eivav1
	Replace fposrd.ivav2 With fposrd.eivav2
	Replace fposrd.ivav3 With fposrd.eivav3
	Replace fposrd.ivav4 With fposrd.eivav4
	Replace fposrd.ivav5 With fposrd.eivav5
	Replace fposrd.ivav6 With fposrd.eivav6
	Replace fposrd.moeda With CR_FTRDTMP.moeda
	Replace fposrd.rdstamp With ''
	Replace fposrd.Multi With .F.

	M_TOTALDOC = M_TOTALDOC - fposrd.evreg

	fecha("CR_FTRDTMP")

	M_VALRD = 0
	Select fposrd
	Go Top
	Scan
		M_VALRD = M_VALRD + fposrd.evreg
	Endscan

	If M_VALRD > 0
		Select FPOSC
		Replace FPOSC.ERDTOTAL With M_VALRD
		Replace FPOSC.RDTOTAL With M_VALRD
	Endif
**	FIM - Regulariza FTRD
**********************************************************************************
Endif

stouchpos.REFRESCAR()
Select FPOSL
Goto Top



