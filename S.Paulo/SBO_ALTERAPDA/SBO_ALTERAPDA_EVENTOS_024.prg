

select bo
if empty(bo.bostamp)
	msg('N�o Existe nenhum Dossier Seleccionado!.')
	return
endif
*****************************************************************************************************
**		Cria um Array com as Origens Poss�veis do ficheiro (Tabela de Utilizador)
*****************************************************************************************************
local n_mycount
TEXT TO m.msel NOSHOW TEXTMERGE
	select	campo  
	from	dytable (nolock) 
	where	entityname='a_tab2001'
	union	all
	select	''  
	order by 1
ENDTEXT
if u_sqlexec(m.msel, [origemtemp])
	if reccount([origemtemp]) > 0
		declare a_myorigemarray(reccount([origemtemp]))
		select origemtemp
		m.n_mycount = 1
		scan
			a_myorigemarray(m.n_mycount) = origemtemp.campo
			m.n_mycount = m.n_mycount + 1
		endscan
		fecha([marcatemp])
	else
		mensagem("N�o existem registos.","DIRECTA")
		return
	endif 
else
	mensagem("ERRO SQL - array","DIRECTA")
	return 
endif

local var_pda
var_pda = ''
var_pda = getnome('Qual o PDA da Encomenda',"","Seleccione o PDA.","",1,.f.,"a_myorigemarray")
if empty(var_pda)
	mensagem('Aten��o:'+chr(13)+'N�o Preencheu o PDA!.','directa')
endif
var_data = date()
var_hora = time()
var_inis = m.m_chinis

TEXT TO m.msel NOSHOW TEXTMERGE
	update bo set bo.tabela2 = '<<var_pda>>'
			, bo.usrhora = '<<var_hora>>', bo.usrdata = '<<dtos(var_data)>>', bo.usrinis = '<<var_inis>>'
	where bo.bostamp = '<<bo.bostamp>>'
ENDTEXT
u_sqlexec(m.msel)
sbo.doactualizar()
