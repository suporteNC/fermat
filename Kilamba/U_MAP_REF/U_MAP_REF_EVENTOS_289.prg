** J�lio Ricardo
** 2020-11-22
**
** Verificar se tem infla��o para o ano corrente
**
If u_sqlexec("select * from u_inflacao where ano=year(getdate())","xinf") And Reccount("xinf")>0   and xinf.taxa>0

	cvar = ''
	cfam = ''
	csubfam = ''
	******* Definir dados do array com lojas para transferir
	TEXT TO sqle1 NOSHOW TEXTMERGE
				select  distinct  U_TPART campo  from st (nolock) where  inactivo = 0 order by  1
	ENDTEXT

	If Not u_sqlexec(sqle1, "c_temparr")
		msg(sqle1)
		Return
	Endif
	If Reccount("c_temparr")>0
		Declare var_array(Reccount("c_temparr"))
		Select c_temparr
		var_contador = 1
		Scan
			var_array(var_contador) = c_temparr.campo
			var_contador = var_contador + 1
		Endscan
		fecha("c_temparr")
	Else
		mensagem("N�o h� registos!!!","DIRECTA")
	Endif


	cvar = getnome('Introduza o tipo de artigo',"","","",1,.F.,"var_array")
	**
	** Procurar as familias do tipo de artigo
	**
	If !Empty(cvar)
		TEXT TO sqle1 NOSHOW TEXTMERGE
				select  distinct stfami.nome campo from st (nolock) inner join stfami (nolock) on stfami.ref=familia where u_tpart='CANALIZACAO' and  inactivo = 0 order by  1
		ENDTEXT

		If Not u_sqlexec(sqle1, "c_temparr")
			msg(sqle1)
			Return
		Endif
		If Reccount("c_temparr")>0
			Declare var_array(Reccount("c_temparr"))
			Select c_temparr
			var_contador = 1
			Scan
				var_array(var_contador) = c_temparr.campo
				var_contador = var_contador + 1
			Endscan
			fecha("c_temparr")
		Else
			mensagem("N�o h� registos!!!","DIRECTA")
		Endif
		cfam=getnome('Introduza a familia',"","Para o tipo de artigo: "+Alltrim(cvar),"",1,.F.,"var_array")

		If !Empty(cfam)
			u_sqlexec("select ref from stfami (nolock) where stfami.nome=?cfam","xcurfam")
			cfamnome=cfam
			cfam=xcurfam.ref

			TEXT TO sqle1 NOSHOW TEXTMERGE
				select  distinct u_subfam campo from st (nolock) where u_tpart='CANALIZACAO' and familia='<<cfam>>' and inactivo = 0 order by  1
			ENDTEXT

			If Not u_sqlexec(sqle1, "c_temparr")
				msg(sqle1)
				Return
			Endif
			If Reccount("c_temparr")>0
				Declare var_array(Reccount("c_temparr"))
				Select c_temparr
				var_contador = 1
				Scan
					var_array(var_contador) = c_temparr.campo
					var_contador = var_contador + 1
				Endscan
				fecha("c_temparr")
			Else
				mensagem("N�o h� registos!!!","DIRECTA")
			Endif


			csubfam=getnome('Introduza a sub-familia',"","Para o tipo de artigo: "+Alltrim(cvar)+Chr(13)+"e para a familia: "+Alltrim(cfamnome),"",1,.F.,"var_array")


		Endif

	Endif

	******* Fim de Definir dados do array


	TEXT TO msel NOSHOW TEXTMERGE
	exec sp_precos_euros_1 '<<cvar>>','<<cfam>>','<<csubfam>>'
	ENDTEXT
	If Not u_sqlexec(msel,"c_temp")
		msg(msel)
	Endif
	ntotal = Reccount("c_temp")

	msg("Ser�o introduzidos " +  astr(ntotal) + " registos.")
	regua(0,ntotal,"A processar as refer�ncias")
	Select c_temp

	Scan
		regua[1,recno(),"Processando a refer�ncia "+ c_temp.design]
		Select  u_mapl
		Append Blank
		Replace u_mapl.u_maplstamp  With  u_stamp(Recno())
		Replace u_mapl.u_mapstamp  With  u_map.u_mapstamp
		Replace u_mapl.ststamp  With  c_temp.ststamp
		Replace u_mapl.ref  With  c_temp.ref
		Replace u_mapl.Design  With  c_temp.Design
		Replace u_mapl.tpart  With  c_temp.tpart
		Replace u_mapl.ctpart  With  c_temp.ctpart
		Replace u_mapl.pcusto  With  c_temp.pcusto
		Replace u_mapl.pcustoeur  With  c_temp.pcustoeur
		Replace u_mapl.Marg  With  c_temp.Marg
		Replace u_mapl.margsug  With  c_temp.margsug
		Replace u_mapl.pvsiva  With  c_temp.pvsiva
		Replace u_mapl.pvsivaeur  With  c_temp.pvsivaeur
		Replace u_mapl.Pv  With  c_temp.Pv
		Replace u_mapl.pveur  With  c_temp.pveur
		Replace u_mapl.pcustoact  With c_temp.pcustoact
		Replace u_mapl.pcustoacteur  With c_temp.pcustoacteur
		Replace u_mapl.margact  With  c_temp.margact
		Replace u_mapl.actmargsug  With  c_temp.actmargsug
		Replace u_mapl.actmargreal  With  c_temp.actmargreal
		Replace u_mapl.actpfixo  With  c_temp.actpfixo
		Replace u_mapl.pvact  With  c_temp.pvact
		Replace u_mapl.pvacteur  With  c_temp.pvacteur
		Replace u_mapl.stock  With  c_temp.stock
		Replace u_mapl.stockeur  With  c_temp.stockeur
		Replace u_mapl.dataact  With  c_temp.dataact
		Replace u_mapl.cambio  With  c_temp.cambio
		Replace u_mapl.entrada  With  c_temp.entrada
		Replace u_mapl.dtentrada  With  c_temp.dtentrada
		Replace u_mapl.iva With  c_temp.iva


		If SU_MAP.pageframe1.page1.Cambhj.Value<>0
			Replace u_mapl.pvhj With  (c_temp.pcustoeur/SU_MAP.pageframe1.page1.Cambhj.Value)/(1/c_temp.cambio)
		Else
			Replace u_mapl.pvhj With  0
		Endif





	Endscan
	regua(2)

	nstockeur = 0
	Select u_mapl
	Scan
		nstockeur =  nstockeur  + u_mapl.stockeur
	Endscan

	Select  u_mapl
	Goto Top
	Append Blank
	Replace u_mapl.u_maplstamp  With  u_stamp(Recno())
	Replace u_mapl.u_mapstamp  With  u_map.u_mapstamp
	Replace u_mapl.Design   With 'Total'
	Replace u_mapl.stockeur With  nstockeur

	Select  u_mapl
	Goto Top
	SU_MAP.Refresh()
Else
	msg("Tem que definir a infla��o para o ano corrente. N�o pode continuar!")
Endif
