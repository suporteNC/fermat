*** Alterado por Paulo Ricardo Martins
*** Data 21/10/2020
*** POS - TERMINAR E PAGAR



*** motivo de Isen��o 
****************************************************
select  fposft2
**replace  fposft2.CODMOTISEIMP with  'M00'
replace  fposft2.MOTISEIMP with 'Regime Transit�rio'


*** Calcula pontos da venda 
****************************************************
IF stouchpos.posmetodos1.clivd = .f.
*** PONTOS DA VENDA

IF TYPE("p_valpontos") <> "U"  && p_VALPONTOS - Carregado no EVENTO INIT do FORM STOUCHPOS

	select FPOSC
	SELECT  fposft2
	IF !EMPTY(FPOSFT2.u_ncartao)

		REPLACE fposft2.u_pontos WITH round(IIF(fposc.tipodoc <> 3,fposc.etotal,-fposc.etotal)/val(p_valpontos),0)
  	
	ENDIF
ENDIF


*** PONTOS A ACUMULAR EM CART�O
************************************************

IF TYPE("p_LIMITEPTS") <> "U"  && p_LIMITEPTS - Carregado no EVENTO INIT do FORM STOUCHPOS

	select FPOSC
	SELECT  fposft2
	IF !EMPTY(FPOSFT2.u_ncartao)
		SELECT FPOSC
		TEXT TO MSQL TEXTMERGE NOSHOW
		SELECT u_PONTOS
		FROM CL (NOLOCK)
		WHERE CL.NO = <<ASTR(FPOSC.NO)>>
		ENDTEXT
		IF !U_SQLEXEC(MSQL,'CR_PONTOS')
			MSG('Erro Encontrado')
			MSG(MSQL)
			RETURN
		ENDIF
		select CR_PONTOS
		MPONTOS	= CR_PONTOS.U_PONTOS
		select FPOSFT2

		MPONTOS = MPONTOS + fposft2.u_pontos		

		REPLACE fposft2.u_PTSACUM WITH ((MPONTOS/VAL(p_LIMITEPTS))-INT((MPONTOS/VAL(p_LIMITEPTS))))*VAL(p_LIMITEPTS)
		
		IF TYPE("p_pontosval") <> "U" 
			REPLACE fposft2.u_VALACUM WITH INT((MPONTOS/VAL(p_LIMITEPTS)))*VAL(p_pontosval)		  
		ENDIF
		
	ENDIF
ENDIF

ENDIF





return