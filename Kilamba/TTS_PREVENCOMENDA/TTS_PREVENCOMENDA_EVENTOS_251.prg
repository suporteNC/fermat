**********************************************************
*** Evento Auxiliar para Previs�es de Encomendas
***
*** Criado em : 06/07/2017
*** Criado por: Rui Vale
**********************************************************
Select c_stk
Select fornec From c_stk Where stkenc>0 Group By fornecedor Into Cursor uCurFonec Readwrite
Select ref,Design,epcult,fornec,stkenc From c_stk Where stkenc>0 Into Cursor uCurLinArt Readwrite
Create Cursor uCurPrevPromo (no N(10),ref C(18),qtt N(16,3),Data D,bistamp C(25),ok L)
TEXT TO uSql TEXTMERGE noshow
	Select no,ref,design,edebito,qtt,ettdeb,cast(0 as numeric(16,3)) as qttmin,cast(1 as bit) as ok,bistamp
	From bi (nolock)
	Where 1=2
ENDTEXT
If u_sqlexec(uSql,"uCurPrevEncBi")
	Select uCurLinArt
	Go Top
	Do While !Eof()
		TEXT TO uSql TEXTMERGE noshow
			Select ref,design,u_qttminfl,epcpond,epcult
			From st (nolock)
			Where ref = '<<Astr(uCurLinArt.ref)>>'
		ENDTEXT
		If u_sqlexec(uSql,"uCurTmpSt") And Reccount("uCurTmpSt")>0
			Select uCurPrevEncBi
			Append Blank
			Replace uCurPrevEncBi.no With uCurLinArt.fornec
			Replace uCurPrevEncBi.ref With uCurLinArt.ref
			Replace uCurPrevEncBi.Design With uCurTmpSt.Design
			Replace uCurPrevEncBi.qtt With uCurLinArt.stkrep
			Replace uCurPrevEncBi.qttmin With uCurTmpSt.u_qttminfl
			Replace uCurPrevEncBi.edebito With uCurTmpSt.epcult
			Replace uCurPrevEncBi.ettdeb With uCurTmpSt.epcult * uCurLinArt.stkenc
			Replace uCurPrevEncBi.bistamp With u_stamp()
			If uCurLinArt.stkrep < uCurTmpSt.u_qttminfl
				Replace uCurPrevEncBi.ok With .F.
			Else
				Replace uCurPrevEncBi.ok With .T.
			Endif
		Endif
		TEXT TO uSql TEXTMERGE noshow
			Select st.fornec no,bi.ref,qtt-qtt2 as qtt,rdata as data,bistamp
			From bi (nolock)
			Inner join bo (nolock) on bo.bostamp = bi.bostamp and bo.ndos = 41
			Inner join st (nolock) on st.ref = bi.ref
			Where bi.ref = '<<Astr(uCurLinArt.ref)>>'
			And bi.emconf = 1
			And qtt-qtt2 > 0
		ENDTEXT
		If u_sqlexec(uSql,"uCurTmpBiPromo") And Reccount("uCurTmpBiPromo")>0
			Select uCurTmpBiPromo
			Go Top
			Do While !Eof()
				Select uCurPrevPromo
				Append Blank
				Replace uCurPrevPromo.no With uCurTmpBiPromo.no
				Replace uCurPrevPromo.ref With uCurTmpBiPromo.ref
				Replace uCurPrevPromo.qtt With uCurTmpBiPromo.qtt
				Replace uCurPrevPromo.Data With uCurTmpBiPromo.Data
				Replace uCurPrevPromo.bistamp With uCurTmpBiPromo.bistamp
				Replace uCurPrevPromo.ok With .F.
				Select uCurTmpBiPromo
				Skip
			Enddo
		Endif
		Select uCurLinArt
		Skip
	Enddo
Else
	Msg("Erro ao criar previs�o!!!")
	Return
Endif
TEXT TO uSql TEXTMERGE noshow
	Select no,nome,etotal,cast(1 as bit) as ok
		,cast(0 as numeric(16,2)) as valormin
	From bo (nolock)
	Where 1=2
ENDTEXT
If u_sqlexec(uSql,"uCurPrevEncBo")
	Select uCurFonec
	Go Top
	Do While !Eof()
		If Used("uCurSum")
			Fecha("uCurSum")
		Endif
		Select Sum(ettdeb) As ettdeb From uCurPrevEncBi Where uCurPrevEncBi.no = uCurFonec.fornec Into Cursor uCurSum Readwrite
		If Reccount("uCurSum")>0
			uTotal = uCurSum.ettdeb
		Else
			uTotal = 0
		Endif
		TEXT TO uSql TEXTMERGE noshow
			Select no,nome,u_evminenc
			From fl (nolock)
			Where no = <<Astr(uCurFonec.fornec)>>
		ENDTEXT
		If u_sqlexec(uSql,"uCurTmpFl") And Reccount("uCurTmpFl")>0
			Select uCurPrevEncBo
			Append Blank
			Replace uCurPrevEncBo.no With uCurFonec.fornec
			Replace uCurPrevEncBo.Nome With uCurTmpFl.Nome
			Replace uCurPrevEncBo.valormin With uCurTmpFl.u_evminenc
			Replace uCurPrevEncBo.etotal With uTotal
			If uTotal < uCurTmpFl.u_evminenc
				Replace uCurPrevEncBo.ok With .F.
			Else
				Replace uCurPrevEncBo.ok With .T.
			Endif
		Else
			Select uCurPrevEncBo
			Append Blank
			Replace uCurPrevEncBo.no With uCurFonec.fornec
			Replace uCurPrevEncBo.Nome With "<Sem Fornecedor>"
			Replace uCurPrevEncBo.valormin With 0
			Replace uCurPrevEncBo.etotal With uTotal
		Endif
		Select uCurFonec
		Skip
	Enddo
Else
	Msg("Erro ao criar previs�o!!!")
	Return
Endif
Select uCurPrevEncBo
Go Top
Select uCurPrevEncBi
Set Filter To uCurPrevEncBi.no = uCurPrevEncBo.no In uCurPrevEncBi
Select uCurPrevEncBi
Go Top
Select uCurPrevPromo
Set Filter To uCurPrevPromo.no = uCurPrevEncBo.no And uCurPrevPromo.ref = uCurPrevEncBi.ref In uCurPrevPromo
docomando("do genpdup with 'ADM17041054406,773405537'")
PDU_3B30RPHPD.actualizarsopag.nossobutton1.Click