USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_stsubfam]    Script Date: 24/01/2023 18:08:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_STSUBFAM_Insert]
    @c1 char(25),
    @c2 varchar(10),
    @c3 varchar(30),
    @c4 varchar(30),
    @c5 datetime,
    @c6 varchar(8),
    @c7 varchar(30),
    @c8 datetime,
    @c9 varchar(8),
    @c10 bit,
    @c11 numeric(16,2),
    @c12 bit,
    @c13 datetime
as
begin  
	insert into [PG].[u_stsubfam] (
		[u_stsubfamstamp],
		[csubfam],
		[subfam],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[margem],
		[pea],
		[u_ttssinc]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','u_stsubfam',@c1, 'u_stsubfamstamp'

end  
