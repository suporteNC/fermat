*!* Criado por Nelson Sim�es

**var_ano = Getnome('Qual o ano do invent�rio a criar',YEAR(DATE()))
var_ano = YEAR(DATE())

IF var_ano = 0
	msg('Aten��o:' + chr(13) + 'N�o Preencheu o Ano de Abertura do Invent�rio!')
	RETURN 
ENDIF 

m.mSel = ""
TEXT TO m.msel NOSHOW TEXTMERGE
	select	 '<<var_ano>>' + '.' + replicate('0',5 - len(isnull(max(right(stic.descricao,5)),0)  + 1)) + rtrim(convert(char(5),isnull(max(right(stic.descricao,5)),0)  + 1)) proximo
	
	from	stic (nolock)
	where	isnumeric(right(stic.descricao,5)) = 1
			and LEFT(stic.descricao,4) = '<<var_ano>>'

ENDTEXT

IF NOT u_sqlexec(m.mSel,"tempstic")
	Msg("Erro de Sql" + m.Sel)
	RETURN 
ELSE
	SELECT tempstic
	var_proximo = tempstic.proximo
	**var_proximo = alltrim(var_proximo) + '.' + m.m_chnome + '.'
	replace stic.u_sticquem with m.m_chnome
	fecha("tempstic")

	RETURN var_proximo
ENDIF