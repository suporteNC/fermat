USE [Mondego_testes]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbostobs]    Script Date: 24/01/2023 11:28:55 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_STOBSPG_Delete]
		@c1 char(25),
		@c2 varchar(18),
		@c3 text,
		@c4 bit,
		@c5 bit,
		@c6 numeric(1,0),
		@c7 bit,
		@c8 numeric(20,0),
		@c9 bit,
		@c10 bit,
		@c11 numeric(20,0),
		@c12 bit,
		@c13 numeric(20,0),
		@c14 bit,
		@c15 varchar(50),
		@c16 numeric(2,0),
		@c17 bit,
		@c18 bit,
		@c19 numeric(20,0),
		@c20 varchar(60),
		@c21 bit,
		@c22 varchar(60),
		@c23 varchar(3),
		@c24 varchar(6),
		@c25 varchar(30),
		@c26 char(25),
		@c27 numeric(5,2),
		@c28 bit,
		@c29 bit,
		@c30 varchar(1),
		@c31 varchar(120),
		@c32 varchar(120),
		@c33 varchar(15),
		@c34 varchar(200),
		@c35 varchar(6),
		@c36 varchar(250),
		@c37 varchar(3),
		@c38 varchar(3),
		@c39 varchar(250),
		@c40 varchar(3),
		@c41 varchar(100),
		@c42 bit,
		@c43 varchar(10),
		@c44 varchar(150),
		@c45 bit,
		@c46 varchar(5),
		@c47 varchar(115),
		@c48 varchar(25),
		@c49 datetime,
		@c50 datetime,
		@c51 bit,
		@c52 bit,
		@c53 datetime,
		@c54 varchar(30),
		@c55 datetime,
		@c56 varchar(8),
		@c57 varchar(30),
		@c58 datetime,
		@c59 varchar(8),
		@c60 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[stobs] 
	where [ref] = @c2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ref] = ' + convert(nvarchar(100),@c2,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stobs]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from stobs where stobsstamp=@c1