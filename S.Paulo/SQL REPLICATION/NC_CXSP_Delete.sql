USE [SPAULO]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbocx]    Script Date: 27/01/2023 13:10:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_CXSP_Delete]
		@c1 char(25),
		@c2 varchar(20),
		@c3 varchar(20),
		@c4 numeric(3,0),
		@c5 numeric(10,0),
		@c6 numeric(4,0),
		@c7 varchar(30),
		@c8 numeric(6,0),
		@c9 datetime,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 numeric(6,0),
		@c13 datetime,
		@c14 varchar(8),
		@c15 bit,
		@c16 text,
		@c17 varchar(30),
		@c18 datetime,
		@c19 varchar(8),
		@c20 varchar(30),
		@c21 datetime,
		@c22 varchar(8),
		@c23 bit,
		@c24 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [SP].[cx] 
	where [site] = @c2
  and [cxno] = @c5
  and [cxano] = @c6
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[site] = ' + convert(nvarchar(100),@c2,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxno] = ' + convert(nvarchar(100),@c5,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxano] = ' + convert(nvarchar(100),@c6,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[SP].[cx]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from cx where cxstamp=@c1