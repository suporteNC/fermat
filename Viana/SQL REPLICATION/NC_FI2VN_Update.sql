USE [VIANA]
GO

/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbofi2]    Script Date: 19/01/2023 19:39:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[NC_FI2VN_Update]
		@c1 char(25),
		@c2 bit,
		@c3 varchar(60),
		@c4 datetime,
		@c5 varchar(50),
		@c6 varchar(50),
		@c7 char(25),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 datetime,
		@c13 varchar(8),
		@c14 bit,
		@c15 datetime,
		@c16 bit,
		@c17 char(25),
		@c18 varchar(10),
		@c19 varchar(5),
		@c20 varchar(115),
		@c21 varchar(10),
		@c22 varchar(115),
		@c23 numeric(6,2),
		@c24 numeric(6,2),
		@c25 numeric(5,2),
		@c26 varchar(20),
		@c27 numeric(19,6),
		@c28 numeric(18,5),
		@c29 bit,
		@c30 numeric(19,6),
		@c31 numeric(18,5),
		@c32 numeric(5,0),
		@c33 bit,
		@c34 char(25),
		@c35 bit,
		@c36 varchar(60),
		@c37 datetime,
		@c38 varchar(50),
		@c39 varchar(50),
		@c40 char(25),
		@c41 varchar(30),
		@c42 datetime,
		@c43 varchar(8),
		@c44 varchar(30),
		@c45 datetime,
		@c46 varchar(8),
		@c47 bit,
		@c48 datetime,
		@c49 bit,
		@c50 char(25),
		@c51 varchar(10),
		@c52 varchar(5),
		@c53 varchar(115),
		@c54 varchar(10),
		@c55 varchar(115),
		@c56 numeric(6,2),
		@c57 numeric(6,2),
		@c58 numeric(5,2),
		@c59 varchar(20),
		@c60 numeric(19,6),
		@c61 numeric(18,5),
		@c62 bit,
		@c63 numeric(19,6),
		@c64 numeric(18,5),
		@c65 numeric(5,0),
		@c66 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c34 = @c1)
begin 
update [VN].[fi2] set
		[fi2stamp] = @c34,
		[prestsrv] = @c35,
		[originatingon] = @c36,
		[orderdate] = @c37,
		[refretif] = @c38,
		[motretif] = @c39,
		[ftstamp] = @c40,
		[ousrinis] = @c41,
		[ousrdata] = @c42,
		[ousrhora] = @c43,
		[usrinis] = @c44,
		[usrdata] = @c45,
		[usrhora] = @c46,
		[marcada] = @c47,
		[u_ttssinc] = @c48,
		[noupdqtt2ori] = @c49,
		[oristamp] = @c50,
		[origem] = @c51,
		[nomecmb] = @c52,
		[designcmb] = @c53,
		[codisp] = @c54,
		[designisp] = @c55,
		[pctengfssl] = @c56,
		[pctengrnv] = @c57,
		[co2emiss] = @c58,
		[unico2mdd] = @c59,
		[esbrcstincbio] = @c60,
		[sbrcstincbio] = @c61,
		[ispoveruni2] = @c62,
		[evalorisp] = @c63,
		[valorisp] = @c64,
		[u_estab] = @c65,
		[fechabosatisteito] = @c66
	where [fi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[fi2]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [VN].[fi2] set
		[prestsrv] = @c35,
		[originatingon] = @c36,
		[orderdate] = @c37,
		[refretif] = @c38,
		[motretif] = @c39,
		[ftstamp] = @c40,
		[ousrinis] = @c41,
		[ousrdata] = @c42,
		[ousrhora] = @c43,
		[usrinis] = @c44,
		[usrdata] = @c45,
		[usrhora] = @c46,
		[marcada] = @c47,
		[u_ttssinc] = @c48,
		[noupdqtt2ori] = @c49,
		[oristamp] = @c50,
		[origem] = @c51,
		[nomecmb] = @c52,
		[designcmb] = @c53,
		[codisp] = @c54,
		[designisp] = @c55,
		[pctengfssl] = @c56,
		[pctengrnv] = @c57,
		[co2emiss] = @c58,
		[unico2mdd] = @c59,
		[esbrcstincbio] = @c60,
		[sbrcstincbio] = @c61,
		[ispoveruni2] = @c62,
		[evalorisp] = @c63,
		[valorisp] = @c64,
		[u_estab] = @c65,
		[fechabosatisteito] = @c66
	where [fi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[fi2]', @param2=@primarykey_text, @param3=13233
		End
end 
end 

EXEC dbo.NC_Sinc_Update 'dbo','VN','fi2',@c1, 'fi2stamp', ''


GO


