** Criado por: Vasco Rocha

LOCAL stLocal, m.cGondola, m.cLocal, m.cTipo
stLocal = bi.u_locais
select bo

if bo.ndos = 82 && m.cLocaliza��es

	select bi
	m.cGondola= upper(alltrim(bi.lobs3))
	m.cLocal = upper(alltrim(bi.litem))
	m.cTipo = upper(alltrim(bi.litem2))
	
	replace bi.lobs with ""

	*!* validar m.cm.cGondola 
	if not empty(m.cGondola)
		if len(m.cGondola) <> 3 or not inlist(left(m.cGondola, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(substr(m.cGondola, 2, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(right(ALLTRIM(m.cGondola), 1), "0","1","2","3","4","5","6","7","8","9") or m.cGondola = "000"
			replace bi.lobs with "<Gondola inv�lida [001 - 999]>"
		endif
	else
		replace bi.lobs with "<Gondola n�o preenchida>"
	ENDIF
	
	*!* validar m.cLocal 
	if not empty(m.cLocal)
		if len(m.cLocal) <> 3 or not inlist(left(m.cLocal, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(substr(m.cLocal, 2, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(right(ALLTRIM(m.cLocal), 1), "0","1","2","3","4","5","6","7","8","9") or m.cLocal = "000"
			replace bi.lobs with "<Local inv�lido [001 - 999]>"
		endif
	else
		replace bi.lobs with "<Local n�o preenchido>"
	ENDIF
	
	*!* validar m.cTipo
	if not empty(m.cTipo)
		if len(m.cTipo) <> 2 or not inlist(left(m.cTipo, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(substr(m.cTipo, 2, 1), "0","1","2","3","4","5","6","7","8","9") or not inlist(right(ALLTRIM(m.cTipo), 1), "0","1","2","3","4","5","6","7","8","9") or m.cTipo = "00"
			replace bi.lobs with "<Tipo inv�lido [01 - 99]>"
		endif
	else
		replace bi.lobs with "<Tipo n�o preenchida>"
	endif		

	replace bi.lobs3 with m.cGondola
	replace bi.litem with m.cLocal
	replace bi.litem2 with m.cTipo
	stLocal = m.cGondola + m.cLocal + m.cTipo
	replace bi.codigo with stLocal
	replace bi.qtt with 1

endif

return stLocal