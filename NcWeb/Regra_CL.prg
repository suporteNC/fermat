Select cl2
**
** Se for revendedor
**
If cl2.u_revend=.T.
	** Verficar utilizador
	If Empty(u_user)
		msg("Tem que definir um utilizador para o NcWeb!")
		Return .F.
	Endif
	** Verficar Primeiro Nome
	If Empty(u_firstn)
		msg("Tem que definir o primeiro nome para o NcWeb!")
		Return .F.
	Endif
	** Verficar Ultimo Nome
	If Empty(u_lastn)
		msg("Tem que definir o ultimo nome para o NcWeb!")
		Return .F.
	Endif
	** Verficar Email
	Select cl
	If Empty(cl.email)
		msg("Tem que definir o email do cliente!")
		Return .F.
	Endif
	Select cl2
	** Verficar password
	If Empty(u_pass)
		msg("Tem que definir uma password para o Utilizador NcWeb!")
		Return .F.
	Endif
	** Verficar Comprimento password
	If Len(Alltrim(u_pass))<4
		msg("Tem que definir uma password com mais que 4 digitos!")
		Return .F.
	Endif
	** Verificar se tem nr na password
	pstr=Len(u_pass)
	If Len(u_pass)=Len(Chrtran(u_pass,"1234567890",""))
		msg("Tem que ter numeros na password!")
		Return .F.
	Endif
Endif
