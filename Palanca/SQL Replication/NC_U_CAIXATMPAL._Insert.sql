USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_caixatm]    Script Date: 27/01/2023 13:32:06 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXATMPAL._Insert]
    @c1 char(25),
    @c2 numeric(10,0),
    @c3 varchar(10),
    @c4 numeric(15,2),
    @c5 varchar(30),
    @c6 varchar(30),
    @c7 datetime,
    @c8 varchar(8),
    @c9 varchar(30),
    @c10 datetime,
    @c11 varchar(8),
    @c12 bit
as
begin  
	insert into [PL].[u_caixatm] (
		[u_caixatmstamp],
		[ordem],
		[tipo],
		[valor],
		[descricao],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12	) 
end  
		EXEC dbo.NC_Sinc_Insert 'dbo','PL','u_caixatm',@c1, 'u_caixatmstamp'