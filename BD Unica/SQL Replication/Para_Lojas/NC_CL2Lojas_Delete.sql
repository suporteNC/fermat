USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbocl2]    Script Date: 31/01/2023 16:45:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_CL2Lojas_Delete]
		@c1 char(25),
		@c2 varchar(2),
		@c3 varchar(55),
		@c4 bit,
		@c5 bit,
		@c6 bit,
		@c7 varchar(55),
		@c8 numeric(10,0),
		@c9 numeric(3,0),
		@c10 varchar(10),
		@c11 char(25),
		@c12 varchar(10),
		@c13 char(25),
		@c14 varchar(10),
		@c15 char(25),
		@c16 varchar(10),
		@c17 char(25),
		@c18 bit,
		@c19 varchar(5),
		@c20 bit,
		@c21 varchar(6),
		@c22 varchar(20),
		@c23 varchar(50),
		@c24 datetime,
		@c25 varchar(8),
		@c26 bit,
		@c27 bit,
		@c28 bit,
		@c29 numeric(10,6),
		@c30 numeric(10,6),
		@c31 varchar(6),
		@c32 bit,
		@c33 varchar(40),
		@c34 datetime,
		@c35 bit,
		@c36 varchar(4),
		@c37 varchar(11),
		@c38 varchar(15),
		@c39 varchar(3),
		@c40 varchar(3),
		@c41 varchar(250),
		@c42 varchar(4),
		@c43 varchar(40),
		@c44 varchar(2),
		@c45 varchar(55),
		@c46 bit,
		@c47 numeric(1,0),
		@c48 numeric(10,0),
		@c49 numeric(5,2),
		@c50 bit,
		@c51 varchar(254),
		@c52 bit,
		@c53 varchar(20),
		@c54 varchar(40),
		@c55 varchar(15),
		@c56 numeric(10,0),
		@c57 numeric(4,0),
		@c58 datetime,
		@c59 varchar(15),
		@c60 varchar(40),
		@c61 varchar(40),
		@c62 varchar(55),
		@c63 bit,
		@c64 bit,
		@c65 numeric(6,0),
		@c66 varchar(15),
		@c67 bit,
		@c68 bit,
		@c69 bit,
		@c70 varchar(10),
		@c71 varchar(10),
		@c72 numeric(6,0),
		@c73 varchar(3),
		@c74 varchar(30),
		@c75 varchar(30),
		@c76 varchar(40),
		@c77 text,
		@c78 numeric(10,2),
		@c79 bit,
		@c80 numeric(16,2),
		@c81 numeric(8,0),
		@c82 datetime,
		@c83 varchar(30),
		@c84 datetime,
		@c85 varchar(8),
		@c86 varchar(30),
		@c87 datetime,
		@c88 varchar(8),
		@c89 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[cl2] 
	where [cl2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[cl2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[cl2]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from cl2 where cl2stamp=@c1