*************************************
*** Importa��o de artigos para Dossier Pre�os de Fornecedor
*** Criado em 06/07/2017
*** Criado por Rui Vale
*************************************
If !SBO.adding And !SBO.editing
	Msg("O ecr� tem de estar em modo de edi��o!!")
	Return
Endif
Msg("Aten��o, a estrutura do ficheiro tem de ser REF,DESIGN,QTT,UNIDADE,CONVERSAO,QTT2,UNIDADE2,QTD MULTIPLA,PRECO,DESC1,DESC2,ARMAZEM,REFFOR,PRAZO,CODIGO BARRAS !!")
Create Cursor uCurErros ( Erro C(254) )
Create Cursor uDadosSt (Ref C(18), Design C(60),Qtt N(16,3),Unidade C(4),u_convers N(16,6),uni2qtt N(16,6),Unidad2 C(4),binum1 N(10),Preco N(16,3),Desc1 N(10,2),Desc2 N(10,2),Armazem N(10),FORREF C(18),binum2 N(10), codigo c(18))
** Vou pedir o ficheiro para importar
uNomeFicheiroZip = Getfile("","Ficheiro a abrir","Abrir",2,"Abrir")
If Empty(uNomeFicheiroZip)
	Msg("Cancelado pelo utilizador!!!")
	Return
Endif
uFolhas = ""
uExtFile = Upper(Justext(uNomeFicheiroZip))
If !Alltrim(Upper(uExtFile))=="XLS" And !Alltrim(Upper(uExtFile))=="XLSX"
	Msg("Tipo de ficheiro inv�lido!!!")
	Return
Endif
** Se � um XLSX, transformo em XLS
If uExtFile = "XLSX"
	u_nficheiro = Justpath(uNomeFicheiroZip)+"\"+Strtran(Justfname(uNomeFicheiroZip),".XLSX","_XLS.XLS")
	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)
	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next
	oExcel.Visible = .T.
	oExcel.DisplayAlerts = .F.
	oExcel.ActiveWorkbook.SaveAs(u_nficheiro, 39)
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()
	uNomeFicheiroZip = u_nficheiro
Else
	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)
	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()
Endif
**************
Create Cursor xVars ( No N(5), tipo C(1), Nome C(40), Pict C(100), lOrdem N(10), nValor N(18,5), cValor C(250), lValor l, dValor d, tbval M )
Select xVars
Append Blank
Replace xVars.No With 1
Replace xVars.tipo With "T"
Replace xVars.Nome With "Folha"
Replace xVars.Pict With ""
Replace xVars.lOrdem With 1
Replace xVars.tbval With uFolhas
m.mCaption = "Qual a Folha a importar"
m.escolheu=.F.
docomando("do form usqlvar with 'xvars',m.mCaption")
If ! m.escolheu
	Msg("Cancelado pelo utilizador")
	Return
Else
	Select xVars
	Locate For No=1
	uNomeFolha = xVars.cValor
Endif
uFechar = .F.
uNomeFicheiroZip = ["] + uNomeFicheiroZip + ["]
uNomeFolha = ["] + Alltrim(uNomeFolha) + ["]
Try
	Select uDadosSt
	Append From &uNomeFicheiroZip Type Xl5 Sheet &uNomeFolha
Catch
	Msg("Os dados a importar n�o est�o de acordo com o esperado!!!")
	uFechar = .T.
Endtry
If uFechar
	Return
Endif
Select uDadosSt
scan FOR  NOT EMPTY(uDadosSt.codigo)
	TEXT TO msel NOSHOW TEXTMERGE
		DECLARE @nr numeric(5), @ref  varchar(30)
		SET @ref = '' 
		SET  @nr = 0 
		SELECT @nr = COUNT(*) , @ref = MAX(ref)  FROM st (nolock) WHERE ref <> '<<uDadosSt.ref>>' AND  codigo  = '<<uDadosSt.codigo>>'
		IF @nr = 0 
			SELECT @nr = COUNT(*) ,@ref = MAX(ref) FROM bc (nolock) WHERE ref <> '<<uDadosSt.ref>>' AND  codigo  = '<<uDadosSt.codigo>>'	
			SELECT @nr nr , @ref ref   
	ENDTEXT
	IF  NOT  u_sqlexec(msel,"c_Temp")
		msg(msel)
		RETURN  
	ENDIF  
	IF c_Temp.nr > 0 
		Select uCurErros
		Append Blank
		Replace uCurErros.Erro With "O codigo de barras " + Alltrim(uDadosSt.codigo) + " j� existe no artigo "+c_Temp.ref+"!!"
	ENDIF  
	fecha("c_temp")
endscan 
Select uDadosSt
Go Top
Skip
Do While !Eof()
	TEXT TO uSql TEXTMERGE noshow
		SELECT *
		From st (nolock)
		Where ref = '<<uDadosSt.ref>>'
	ENDTEXT
	If u_Sqlexec(uSql,"uCurArtigos") And Reccount("uCurArtigos")>0
		Do Boine2in
		Select bi
		Replace bi.Ref With uDadosSt.Ref
		Do BOACTREF With '',.T.,'OKPRECOS','bi'
		Select bi
		Replace bi.Qtt With uDadosSt.Qtt
		Replace bi.u_codigo With uDadosSt.codigo
		Replace bi.uni2Qtt With uDadosSt.uni2Qtt
		If Alltrim(Upper(bo.moeda))=="EURO"
			Replace bi.debito With uDadosSt.Preco * 100
			Replace bi.edebito With uDadosSt.Preco
		Else
			Replace bi.vumoeda With uDadosSt.Preco
		Endif		
		If !Empty(uDadosSt.Armazem)
			Replace bi.Armazem With uDadosSt.Armazem
		Endif		
		If !Empty(uDadosSt.Unidade )
			Replace bi.unidade With uDadosSt.Unidade 
		Endif		
		If !Empty(uDadosSt.Unidad2)
			Replace bi.unidad2 With uDadosSt.Unidad2 
		Endif		
		If !Empty(uDadosSt.binum1)
			Replace bi.binum1 With uDadosSt.binum1 
		Endif		
		If !Empty(uDadosSt.binum2)
			Replace bi.binum2 With uDadosSt.binum2 
		Endif		
		If !Empty(uDadosSt.u_convers)
			Replace bi.u_convers With uDadosSt.u_convers 
		Endif		
		If !Empty(uDadosSt.forref)
			Replace bi.forref With uDadosSt.forref 
		Endif		
		If !Empty(uDadosSt.Desc1)
			Replace bi.desconto With uDadosSt.Desc1
		Endif		
		If !Empty(uDadosSt.Desc2)
			Replace bi.Desc2 With uDadosSt.Desc2
		Endif		
		Do u_bottdeb With 'bi'
	Else
		Select uCurErros
		Append Blank
		Replace uCurErros.Erro With "O Artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + " n�o existe!!"
	Endif
	Select uDadosSt
	Skip
Enddo
If Reccount("uCurErros")>0
	mostrameisto("uCurErros")
Endif
