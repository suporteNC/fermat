USE [SPaulo]
GO
/****** Object:  StoredProcedure [dbo].[sp_pda_QttPdarmRef]    Script Date: 29/11/2022 14:15:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_pda_QttPdarmRef]
	@ref as varchar(18),
	@boano as numeric(4),
	@obrano numeric(10)
AS
BEGIN

	declare @ip_address varchar(255);

	select @ip_address = CAST(CONNECTIONPROPERTY('client_net_address') as varchar(200))

if (charindex('192.168.29.',@ip_address)>0)
        /* PG */
        begin
        update u_pdarm
        set marcada=1, qtt=0
        where ref= @ref and boano= @boano and obrano= @obrano and marcada=0 and qtt<>0 and ndos in (205,5)
        end

if (charindex('192.168.26.',@ip_address)>0)
        /* SPAULO */
        begin
        update u_pdarm
        set marcada=1, qtt=0
        where ref= @ref and boano= @boano and obrano= @obrano and marcada=0 and qtt<>0 and ndos in (938,5)
        end


if (charindex('192.168.28.',@ip_address)>0)
        /* PALANCA */
        begin
        update u_pdarm
        set marcada=1, qtt=0
        where ref= @ref and boano= @boano and obrano= @obrano and marcada=0 and qtt<>0 and ndos in (938,205)
        end



	SELECT 	sum(qtt) qtdpdarm
	from	u_pdarm rm (nolock)
	WHERE	rm.ref = @ref and rm.boano = @boano and rm.obrano = @obrano

        update u_pdarm
        set marcada=0
        where ref= @ref and boano= @boano and obrano= @obrano and marcada=1 and qtt=0 and ndos in (938,5,205)


	RETURN 0

END
