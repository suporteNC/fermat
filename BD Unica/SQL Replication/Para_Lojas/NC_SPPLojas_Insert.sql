USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbospp]    Script Date: 09/02/2023 18:16:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_SPPLojas_Insert]
    @c1 char(25),
    @c2 varchar(100),
    @c3 text,
    @c4 text,
    @c5 text,
    @c6 bit,
    @c7 text,
    @c8 bit,
    @c9 bit,
    @c10 varchar(30),
    @c11 datetime,
    @c12 varchar(8),
    @c13 varchar(30),
    @c14 datetime,
    @c15 varchar(8),
    @c16 bit
as
begin  
	insert into [PG].[spp] (
		[sppstamp],
		[resumo],
		[obs],
		[descricao],
		[critexpr],
		[criteprg],
		[porspexpr],
		[porspeprg],
		[inactivo],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','spp',@c1, 'sppstamp'

end  
