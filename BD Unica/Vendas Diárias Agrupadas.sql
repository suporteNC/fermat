
if #1#<>'' and #2#<>''
begin
select 
	u_data.data as 'Data'
	,coalesce (convert(varchar,datepart(wk,u_data.data)),convert(varchar,datepart(wk,Data_Fat))) Sem
	,coalesce(
			(case 
				when datepart(dw,u_data.data) = 1 then 'Dom'
				when datepart(dw,u_data.data) = 2 then 'Seg'
				when datepart(dw,u_data.data) = 3 then 'Ter'
				when datepart(dw,u_data.data) = 4 then 'Qua'
				when datepart(dw,u_data.data) = 5 then 'Qui'
				when datepart(dw,u_data.data) = 6 then 'Sex'
				when datepart(dw,u_data.data) = 7 then 'Sáb'
			end),(case 
				when datepart(dw,Data_Fat) = 1 then 'Dom'
				when datepart(dw,Data_Fat) = 2 then 'Seg'
				when datepart(dw,Data_Fat) = 3 then 'Ter'
				when datepart(dw,Data_Fat) = 4 then 'Qua'
				when datepart(dw,Data_Fat) = 5 then 'Qui'
				when datepart(dw,Data_Fat) = 6 then 'Sex'
				when datepart(dw,Data_Fat) = 7 then 'Sáb'
			end)) Dia_Sem
	,isnull(zz.Total_fat,0) Total_faturas
	,isnull(zz.Nr_fat,0) Nr_faturas
	,isnull(zz.Total_cre,0) Total_creditos
	,isnull(zz.Nr_cre,0) Nr_creditos
	,isnull(zz.Total_vd,0) Total_vd
	,isnull(zz.Nr_vd,0) Nr_vd
	,isnull(zz.Total_dev,0) Total_devolucoes_POS
	,isnull(zz.Nr_dev,0) Nr_devolucoes_POS
	,isnull(zz.Total_fat+zz.Total_vd+zz.total_dev+zz.total_cre,0) Total_docs
	,isnull(zz.Nr_fat+zz.Nr_vd+zz.Nr_dev+zz.Nr_cre,0) Nr_docs
	
from u_data
left join
		(select
			convert(date,coalesce(fat.data_fat,vd.data_vd,dev.data_dev,dateadd(yy,+1,vd_1.data_vd_1),dateadd(yy,+1,dev_1.data_dev_1),dateadd(yy,+1,fat_1.data_fat_1)),102)  Data_Fat
			,isnull(sum(Fat.Total_fat),0) Total_fat
			,isnull(sum(Fat.Nr_fat),0) Nr_fat
			,isnull(sum(vd.Total_vd),0) Total_vd
			,isnull((sum(vd.Nr_vd)),0) Nr_vd
			,isnull(sum(dev.Total_dev),0) Total_dev
			,isnull((sum(dev.Nr_dev)),0) Nr_dev
			,isnull(sum(cre.Total_cre),0) Total_cre
			,isnull((sum(cre.Nr_cre)),0) Nr_cre
			,convert(date,coalesce(dateadd(yy,-1,fat.Data_Fat),dateadd(yy,-1,vd.Data_vd),dateadd(yy,-1,dev.Data_dev),fat_1.data_fat_1,vd_1.data_vd_1,dev_1.data_dev_1),102) Data_fat_1
			,isnull(sum(Total_fat_1),0) Total_fat_1
			,isnull(sum(Nr_fat_1),0) Nr_fat_1
			,isnull(sum(Total_vd_1),0) Total_vd_1
			,isnull(sum(Nr_vd_1),0) Nr_vd_1
			,isnull(sum(Total_dev_1),0) Total_dev_1
			,isnull(sum(Nr_dev_1),0) Nr_dev_1
		
			from
			(select 
				fdata Data_Fat
				,convert (numeric(14,3),sum(etotal)) as Total_fat
				,count(no) Nr_fat
			from ft (nolock)
			where ndoc =1 and anulado=0 and ftano =#1#
			group by	fdata
			) Fat
			full outer join 
			(select 
				fdata Data_Fat_1
				,convert (numeric(14,3),sum(etotal)) as Total_fat_1
				,count(no) Nr_fat_1
			from ft (nolock)
			where ndoc =1 and anulado=0 and ftano=#1#-1
			group by	fdata
			) Fat_1 on Fat.Data_Fat=Fat_1.Data_Fat_1
			full outer join 
	
			(select 
				fdata Data_vd
				,convert (numeric(14,3),sum(etotal)) as Total_vd
				,count(no) Nr_vd
			from ft (nolock)
			where ndoc =3 and anulado=0 and ftano =#1#
			group by	fdata
			) vd on Fat.Data_Fat=vd.Data_vd and Fat_1.Data_Fat_1=vd.Data_vd
			full outer join 
			(select 
				fdata Data_vd_1
				,convert (numeric(14,3),sum(etotal)) as Total_vd_1
				,count(no) Nr_vd_1
			from ft (nolock)
			where ndoc =3 and anulado=0 and ftano=#1#-1
			group by	fdata
			) vd_1 on Fat.Data_Fat=vd_1.Data_vd_1 and vd.Data_vd=vd_1.Data_vd_1 and fat_1.Data_fat_1=vd_1.Data_vd_1
			full outer join 
			(select 
				fdata Data_dev
				,convert (numeric(14,3),sum(etotal)) as Total_dev
				,count(no) Nr_dev
			from ft (nolock)
			where ndoc in (40) and anulado=0 and ftano =#1#
			group by	fdata
			) dev on Fat.Data_fat=dev.Data_dev


		full outer join 
			(select 
				fdata Data_cre
				,convert (numeric(14,3),sum(etotal)) as Total_cre
				,count(no) Nr_cre
			from ft (nolock)
			where ndoc in (4) and anulado=0 and ftano =#1#
			group by	fdata
			) cre on Fat.Data_fat=cre.Data_cre



			full outer join 
			(select 
				fdata Data_dev_1
				,convert (numeric(14,3),sum(etotal)) as Total_dev_1
				,count(no) Nr_dev_1
			from ft (nolock)
			where ndoc in (40,4) and anulado=0 and ftano=#1#-1
			group by	fdata
			) dev_1 on Fat.Data_Fat=dev_1.Data_dev_1
			group by convert(date,coalesce(fat.data_fat,vd.data_vd,dev.data_dev,dateadd(yy,+1,vd_1.data_vd_1),dateadd(yy,+1,dev_1.data_dev_1),dateadd(yy,+1,fat_1.data_fat_1)),102)
					,convert(date,coalesce(dateadd(yy,-1,fat.Data_Fat),dateadd(yy,-1,vd.Data_vd),dateadd(yy,-1,dev.Data_dev),fat_1.data_fat_1,vd_1.data_vd_1,dev_1.data_dev_1),102)
		) zz on zz.Data_Fat= u_data.data and zz.Data_Fat_1= dateadd(yy,-1,u_data.data)
where year(u_data.data)= #1# and month(u_data.data) = #2#
order by u_data.data
end

