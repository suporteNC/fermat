local lprg 
lprg = ""
lnLine=ALINES(laLine,_ClipText,chr(13))
if left(alltrim(laLine[1]), 6) ="export"
	lprg = RIGHT(_ClipText,LEN(_ClipText)-AT(CHR(13),_ClipText)- 1) && removes export line (the first)
	exportTarget = jsonParse(strtran(laLine[1], "export", ""))
	navega("TC", iif(u_sqlexec("select tcstamp from tc where ecran = '"+alltrim(exportTarget.form)+"'", "crstmp") and reccount("crstmp")>0, crstmp.tcstamp, ""))
	select tc 
	select ti
	go top 
	locate for alltrim(ti.teclas) = alltrim(exportTarget.event)
	if found()
		if !pergunta("Confirmas que queres alterar a tecla " +alltrim(tc.ecran)+ "  " +alltrim(ti.teclas), 1, "", .t.)
			return
		endif
		select ti
		replace ti.retorno 		with lprg
		replace ti.descricao 	with exportTarget.description
		u_tabupdate(.t., .t., "TI")
		Tablerevert(.t.,"TI")
		stc.showsave()
		stc.Gravar.Nossobutton1.click()   
		docomando("doread('"+iif(left(alltrim(exportTarget.form), 1)='S', substr(alltrim(exportTarget.form), 2, len(alltrim(exportTarget.form))), alltrim(exportTarget.form)) + "')")	
		docomando("keyboard"+chr(34)+"{"+alltrim(exportTarget.event)+"}"+chr(34))
	else 
		msg("Nopes! A tecla n�o existe")
	endif
else 
	if !EOF("TI")
		select tc
		select ti
		if !pergunta("Confirmas que queres alterar a tecla " +alltrim(tc.ecran)+ "  " +alltrim(ti.teclas), 1, "", .t.)
			return
		endif
		select ti
		replace ti.retorno with _ClipText
		xstamp = ti.tistamp
		stc.showsave()
		stc.Gravar.Nossobutton1.click()   
		docomando("doread('"+iif(left(alltrim(tc.ecran), 1)='S', substr(alltrim(tc.ecran), 2, len(alltrim(tc.ecran))), alltrim(tc.ecran)) + "')")	
		select ti 
		locate for ti.tistamp = xstamp
		docomando("keyboard"+chr(34)+"{"+alltrim(ti.teclas)+"}"+chr(34))
	endif
endif 
PROCEDURE jsonParse
PARAMETERS pcJSON, poTarget
	LOCAL lcScript
	LOCAL sc AS scriptcontrol
	IF PCOUNT() = 1
		poTarget = NEWOBJECT("Empty")
	ENDIF
	sc = CREATEOBJECT("scriptcontrol")
	sc.Language="JScript"
	TEXT TO lcScript TEXTMERGE NOSHOW
		function parseJSON() {
		var json = << pcJSON >>;
		var str = "";
		for (var name in json) {
			var realType = RealTypeOf(json[name]);
			if ( realType == "array" ) {
			str += "ADDPROPERTY(poTarget,'"+name+"(1)',.f.)\r\n";
			for (var i = 0;i < json[name].length;i++) {
				var arrayElement = "poTarget."+name+"["+(i+1)+"]"
				str += "DIMENSION "+arrayElement+"\r\n";
				var value = json[name][i];
				var realType = RealTypeOf(value);
				if ( realType == "object") {
				str += arrayElement + " =  jsonParse(\""+toJson(value)+"\")\r\n";
				} else if ( realType == "string" ) {
				str += arrayElement + ' = "'+value+'"'+"\r\n";
				} else if ( realType == "number" ) {
				str += arrayElement + " = "+value+"\r\n";
				} else if ( realType == "date" ) {
				str += arrayElement + " = {"+value+"}\r\n";
				} else if ( realType == "boolean" ) {
				str += arrayElement + " = "+((value)?".t.":".f.")+"\r\n";
				}
			}
			} else if ( realType == "string" ) {
			str += "ADDPROPERTY(poTarget,'"+name+"',["+json[name]+"])\r\n";
			} else if ( realType == "number" ) {
			str += "ADDPROPERTY(poTarget,'"+name+"',"+json[name]+")\r\n";
			} else if ( realType == "date" ) {
			str += "ADDPROPERTY(poTarget,'"+name+"',{"+json[name]+"})\r\n";
			} else if (realType == "boolean" ) {
			str += "ADDPROPERTY(poTarget,'"+name+"',"+((json[name])?".t.":".f.")+")\r\n";
			} else if ( realType == "object") {
			str += "ADDPROPERTY(poTarget,'"+name+"',jsonParse(\""+toJson(json[name])+"\"))\r\n";
			}
		}
		return str;
		}
		function RealTypeOf(v) {
			if (typeof(v) == "object") {
				if (v == null) return "null";
				if (v.constructor == (new Array).constructor) return "array";
				if (v.constructor == (new Date).constructor) return "date";
				if (v.constructor == (new RegExp).constructor) return "regex";
				return "object";
			}
			return typeof(v);
			}
			function toJson(obj) {
			switch (typeof obj) {
				case 'object':
				if (obj) {
					var list = [];
					if (obj instanceof Array) {
						for (var i=0;i < obj.length;i++) {
								list.push(toJson(obj[i]));
						}
						return '[' + list.join(',') + ']';
					} else {
						for (var prop in obj) {
								list.push(prop + ':' + toJson(obj[prop]));
						}
						return '{' + list.join(',') + '}';
					}
				} else {
					return 'null';
				}
				case 'string':
				return "'" + obj.replace(/(['])/g, '\\$1') + "'";
				case 'number':
				case 'boolean':
				return new String(obj);
			}
			}
	ENDTEXT
	sc.AddCode(lcScript)
	lcScript = sc.Eval("parseJSON();")
	EXECSCRIPT(lcScript)
	sc = null
	RELEASE sc
	RETURN poTarget
ENDPROC