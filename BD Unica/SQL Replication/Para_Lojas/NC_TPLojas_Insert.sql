USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbotp]    Script Date: 31/01/2023 16:40:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_TPLojas_Insert]
    @c1 char(25),
    @c2 numeric(1,0),
    @c3 varchar(55),
    @c4 numeric(3,0),
    @c5 numeric(3,0),
    @c6 numeric(3,0),
    @c7 numeric(6,2),
    @c8 numeric(6,2),
    @c9 numeric(6,2),
    @c10 numeric(6,2),
    @c11 numeric(3,0),
    @c12 numeric(1,0),
    @c13 varchar(20),
    @c14 varchar(30),
    @c15 datetime,
    @c16 varchar(8),
    @c17 varchar(30),
    @c18 datetime,
    @c19 varchar(8),
    @c20 bit,
    @c21 bit,
    @c22 bit,
    @c23 varchar(20),
    @c24 varchar(3),
    @c25 varchar(4),
    @c26 bit
as
begin  
	insert into [PG].[tp] (
		[tpstamp],
		[tipo],
		[descricao],
		[dias1f],
		[dias2f],
		[dias3f],
		[dpct1],
		[dpct2],
		[dpct3],
		[dpct4],
		[dias],
		[vencimento],
		[diames],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[cheque],
		[cobrancasdd],
		[mesesnaopag],
		[formapag],
		[paymenttermsid],
		[inativo]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','tp',@c1, 'tpstamp'

end  
