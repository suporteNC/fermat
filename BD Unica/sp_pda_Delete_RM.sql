USE [FERMATPG]
GO
/****** Object:  StoredProcedure [dbo].[sp_pda_Delete_RM]    Script Date: 11/29/2022 6:49:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_pda_Delete_RM]
	@bistamp char(25)
AS
BEGIN

	declare @ip_address varchar(255);

	select @ip_address = CAST(CONNECTIONPROPERTY('client_net_address') as varchar(200))

if (charindex('192.168.29.',@ip_address)>0)
        /* PG */
        begin
	DECLARE @retcode int

	BEGIN TRAN

	DELETE 	bi2 
	WHERE	bi2stamp = @bistamp

	if @@error <> 0 GOTO FAILURE

	DELETE 	bi 
	WHERE	bistamp = @bistamp

	if @@error <> 0 GOTO FAILURE

	COMMIT

	RETURN 0

FAILURE:
	IF (@@trancount > 0)
	BEGIN
		DECLARE @lerror int
		SELECT @lerror = @@error
		ROLLBACK TRAN 
		--RAISERROR (@lerror,16,-1)
		RETURN 1
	END
	ELSE
		RETURN 1
end
ELSE
/* SPaulo e Palanca */
BEGIN
	DECLARE @retcode int

	BEGIN TRAN

	DELETE 	u_pdarm 
	WHERE	u_pdarmstamp = @bistamp

	if @@error <> 0 GOTO FAILURE

	COMMIT

	RETURN 0

FAILURE:
	IF (@@trancount > 0)
	BEGIN
		DECLARE @lerror int
		SELECT @lerror = @@error
		ROLLBACK TRAN 
		--RAISERROR (@lerror,16,-1)
		RETURN 1
	END
	ELSE
		RETURN 1


END
END
