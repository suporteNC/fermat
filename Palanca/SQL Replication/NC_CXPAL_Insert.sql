USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbocx]    Script Date: 27/01/2023 13:29:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_CXPAL_Insert]
    @c1 char(25),
    @c2 varchar(20),
    @c3 varchar(20),
    @c4 numeric(3,0),
    @c5 numeric(10,0),
    @c6 numeric(4,0),
    @c7 varchar(30),
    @c8 numeric(6,0),
    @c9 datetime,
    @c10 varchar(8),
    @c11 varchar(30),
    @c12 numeric(6,0),
    @c13 datetime,
    @c14 varchar(8),
    @c15 bit,
    @c16 text,
    @c17 varchar(30),
    @c18 datetime,
    @c19 varchar(8),
    @c20 varchar(30),
    @c21 datetime,
    @c22 varchar(8),
    @c23 bit,
    @c24 bit
as
begin  
	insert into [PL].[cx] (
		[cxstamp],
		[site],
		[pnome],
		[pno],
		[cxno],
		[cxano],
		[ausername],
		[auserno],
		[dabrir],
		[habrir],
		[fusername],
		[fuserno],
		[dfechar],
		[hfechar],
		[fechada],
		[causa],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[exportado]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24	) 
end  
		EXEC dbo.NC_Sinc_Insert 'dbo','PL','cx',@c1, 'cxstamp'