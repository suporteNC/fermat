USE NCANGOLA
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF
GO
CREATE TRIGGER tr_ft_update ON ft WITH ENCRYPTION FOR UPDATE  AS 
SET NOCOUNT ON

DECLARE @Cinfo VARBINARY(128)
SELECT @Cinfo = Context_Info()
IF @Cinfo = 0x55555
RETURN

if update(nprotri) and (select count(*) from inserted where inserted.nprotri = 0) > 0 and (select count(*) from deleted where deleted.nprotri = 1) > 0
BEGIN
    IF (select count(*) from inserted inner join ft2 on ft2.ft2stamp = inserted.ftstamp inner join td on td.ndoc=inserted.ndoc and td.regrd = 0
    inner join cl on cl.no=(case when td.lancacli=0 then inserted.no else ft2.c2no end) And cl.estab=(case when td.lancacli=0 then inserted.estab else ft2.c2estab end)
    where (inserted.tipodoc=1 or inserted.tipodoc=3) )>0
    UPDATE CL SET cl.acmfact=cl.acmfact+inserted.ttiliq,cl.eacmfact=cl.eacmfact+inserted.ettiliq from INSERTED
    inner join ft2 on ft2.ft2stamp = inserted.ftstamp inner join td on td.ndoc =inserted.ndoc and td.regrd = 0
    where cl.no=(case when td.lancacli=0 then inserted.no else ft2.c2no end) and inserted.anulado = 0 and (inserted.tipodoc=1 or inserted.tipodoc=3)
    if (select count(*) from inserted where inserted.multi=0 and inserted.cheque=1)>0
    INSERT INTO CH (chstamp,ftstamp,
    usrdata,usrhora,usrinis,
    ousrdata,ousrhora,ousrinis,
    data,dataem,datareg,no,nome,estab,origem,valor,evalor,valorm,moeda,
    clbanco,clcheque,fref,tptit,contado,ollocal)
    select inserted.ftstamp, inserted.ftstamp,
    inserted.usrdata, inserted.usrhora, inserted.usrinis,
    inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
    inserted.chdata, inserted.ousrdata, inserted.ousrdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,
    case when td.lancacli=0 then inserted.nome else ft2.c2nome end,
    case when td.lancacli=0 then inserted.estab else ft2.c2estab end,
    'FT',
    case when td.tipodoc=3 then 0 else inserted.chtotal end,case when td.tipodoc=3 then 0 else inserted.echtotal end,
    case when td.tipodoc=3 then 0 else inserted.chtmoeda end, inserted.moeda,
    inserted.clbanco, inserted.clcheque, inserted.fref, inserted.tptit,
    case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end else 0 end,
    case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end else SPACE(50) end
    from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
    where inserted.multi=0 and inserted.cheque=1

    if (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=1 and inserted.multi=0)>0
    insert into CC
    (tpstamp, tpdesc, ccstamp, ftstamp, usrdata, usrhora, usrinis, ousrdata, ousrhora, ousrinis
    , datalc, no, nome, cm, cmdesc, origem, cobranca, cambiofixo
    , deb, edeb, cred, ecred, debm, credm, credf, ecredf, credfm
    , ivav1, ivav2,ivav3,ivav4,ivav5,ivav6,ivav7,ivav8,ivav9,eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9
    , ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, reexgiva
    , datasup12, formapag, situacao, clbanco, clcheque, moeda, nrdoc,dataven, fref, intid, cobrador, rota, ccusto, ncusto, zona
    , segmento, tipo, pais, estab, vendedor, vendnm, eivacativado, ivacativado, mivacativado
    , valch, evalch, virs, evirs  )
    select inserted.tpstamp, inserted.tpdesc, inserted.ftstamp, inserted.ftstamp
    , inserted.usrdata, inserted.usrhora, inserted.usrinis, inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
    , inserted.fdata
    , case when td.lancacli=0 then inserted.no else ft2.c2no end
    , case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmcc,td.cmccn,'FT'
    , inserted.cobranca, inserted.cambiofixo
    , case when td.tipodoc=3 then 0 else inserted.total end
    , case when td.tipodoc=3 then 0 else inserted.etotal end

    , case when td.tipodoc=3 then inserted.total*-1 else 0 end
    , case when td.tipodoc=3 then inserted.etotal*-1 else 0 end

    , case when td.tipodoc=3 then 0 else inserted.totalmoeda end
    , case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end

    , case when inserted.debreg >= case when td.tipodoc=3 then inserted.total*-1 else 0 end then case when td.tipodoc=3 then inserted.total*-1 else 0 end else inserted.debreg end
    , case when inserted.edebreg >= case when td.tipodoc=3 then inserted.etotal*-1 else 0 end then case when td.tipodoc=3 then inserted.etotal*-1 else 0 end else inserted.edebreg end
    , case when inserted.debregm >= case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end then case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end else inserted.debregm end
    , inserted.ivav1, inserted.ivav2, inserted.ivav3, inserted.ivav4, inserted.ivav5, inserted.ivav6, inserted.ivav7, inserted.ivav8, inserted.ivav9
    , inserted.eivav1, inserted.eivav2, inserted.eivav3, inserted.eivav4, inserted.eivav5, inserted.eivav6, inserted.eivav7, inserted.eivav8, inserted.eivav9
    , inserted.ivatx1, inserted.ivatx2, inserted.ivatx3, inserted.ivatx4, inserted.ivatx5, inserted.ivatx6, inserted.ivatx7, inserted.ivatx8, inserted.ivatx9
    , ft2.reexgiva , dateadd(Month,13, inserted.fdata-DAY(inserted.fdata)), ft2.formapag, '1'
    , inserted.clbanco, inserted.clcheque
    , inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto
    , inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo
    , case when td.lancacli=0 then inserted.pais else ft2.c2pais end
    , case when td.lancacli=0 then inserted.estab else ft2.c2estab end
    , inserted.vendedor, inserted.vendnm
    , isnull(ft3.eivacativado, 0), isnull(ft3.ivacativado, 0), isnull(ft3.mivacativado, 0)
    , case when inserted.cheque=1 and td.tipodoc<>3 then inserted.chtotal else 0 end
    , case when inserted.cheque=1 and td.tipodoc<>3 then inserted.echtotal else 0 end
    , inserted.virs, inserted.evirs
    from inserted
    inner join td on td.ndoc = inserted.ndoc
    inner join ft2 on ft2.ft2stamp = inserted.ftstamp
    inner join ft3 on ft3.ft3stamp = inserted.ftstamp
    where td.lancacc=1 and inserted.multi=0

    update CC
    set debf =case when td.tipodoc=3 then 0 else inserted.total end
    , edebf =case when td.tipodoc=3 then 0 else inserted.etotal end
    , debfm =case when td.tipodoc=3 then 0 else inserted.totalmoeda end
    , credf =case when td.tipodoc<>3 then 0 else inserted.total*-1 end
    , ecredf =case when td.tipodoc<>3 then 0 else inserted.etotal*-1 end
    , credfm =case when td.tipodoc<>3 then 0 else inserted.totalmoeda*-1 end
    , eivacativado=  isnull(ft3.eivacativado, 0)
    , ivacativado=  isnull(ft3.ivacativado, 0)
    , mivacativado= isnull(ft3.mivacativado, 0)
    , eivacatreg = isnull(ft3.eivacativado, 0)
    , ivacatreg = isnull(ft3.ivacativado, 0)
    , mivacatreg = isnull(ft3.mivacativado, 0)
    , virsreg =inserted.virs
    , evirsreg =inserted.evirs

    from inserted
    inner join td on td.ndoc=inserted.ndoc
    inner join cc on cc.ftstamp=inserted.ftstamp
    inner join ft3 on ft3.ft3stamp = inserted.ftstamp
    where td.lancacc=1 and td.ccautoreg=1
    IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=1 and inserted.multi=0)>0
    INSERT INTO CC (tpstamp,tpdesc,ccstamp,ftstamp,
    usrdata,usrhora,usrinis, ousrdata,ousrhora,ousrinis,
    datalc,no,nome,cm,cmdesc,origem,cobranca,cambiofixo,
    cred,ecred,credm,credf,ecredf,credfm,deb,edeb,debm,debf,edebf,debfm,
    ivav1,ivav2,ivav3,ivav4,ivav5,ivav6,ivav7,ivav8,ivav9,
    eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9,
    ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, reexgiva,
    datasup12, formapag,situacao, clbanco,clcheque
    , moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,segmento,tipo,pais,estab,vendedor,vendnm
    , eivacativado, ivacativado, mivacativado
    , eivacatreg, ivacatreg, mivacatreg
    , valch,evalch)
    select inserted.tpstamp, inserted.tpdesc,substring('R'+inserted.ftstamp,1,25), inserted.ftstamp,
    inserted.usrdata, inserted.usrhora, inserted.usrinis,
    inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
    inserted.fdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmccautoreg,td.cmccautoregn,'FT',
    inserted.cobranca, inserted.cambiofixo,
    case when td.tipodoc=3 then 0 else inserted.total-inserted.rdtotal end,case when td.tipodoc=3 then 0 else inserted.etotal-inserted.erdtotal end,case when td.tipodoc=3 then 0 else inserted.totalmoeda-inserted.rdtotalm end,
    case when td.tipodoc=3 then 0 else inserted.total-inserted.rdtotal end,case when td.tipodoc=3 then 0 else inserted.etotal-inserted.erdtotal end,case when td.tipodoc=3 then 0 else inserted.totalmoeda-inserted.rdtotalm end,
    case when td.tipodoc<>3 then 0 else (inserted.total-inserted.rdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.etotal-inserted.erdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.totalmoeda-inserted.rdtotalm)*-1 end,
    case when td.tipodoc<>3 then 0 else (inserted.total-inserted.rdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.etotal-inserted.erdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.totalmoeda-inserted.rdtotalm)*-1 end,
    inserted.ivav1, inserted.ivav2, inserted.ivav3, inserted.ivav4, inserted.ivav5, inserted.ivav6, inserted.ivav7, inserted.ivav8, inserted.ivav9,
    inserted.eivav1, inserted.eivav2, inserted.eivav3, inserted.eivav4, inserted.eivav5, inserted.eivav6, inserted.eivav7, inserted.eivav8, inserted.eivav9,
    inserted.ivatx1, inserted.ivatx2, inserted.ivatx3, inserted.ivatx4, inserted.ivatx5, inserted.ivatx6, inserted.ivatx7, inserted.ivatx8, inserted.ivatx9,
    ft2.reexgiva,
    dateadd(Month,13, inserted.fdata-DAY(inserted.fdata)),
    ft2.formapag,'1', inserted.clbanco, inserted.clcheque,
    inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto, inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo,
    case when td.lancacli=0 then inserted.pais else ft2.c2pais end,case when td.lancacli=0 then inserted.estab else ft2.c2estab end, inserted.vendedor, inserted.vendnm
    , case when td.tipodoc<>3 then isnull(ft3.eivacativado*-1, 0) else isnull(ft3.eivacativado, 0) end
    , case when td.tipodoc<>3 then isnull(ft3.ivacativado*-1, 0) else isnull(ft3.ivacativado, 0) end
    , case when td.tipodoc<>3 then isnull(ft3.mivacativado*-1, 0) else isnull(ft3.mivacativado, 0) end
    , isnull(ft3.eivacativado, 0), isnull(ft3.ivacativado, 0), isnull(ft3.mivacativado, 0)
    , case when inserted.cheque=1 and td.tipodoc<>3 then inserted.chtotal else 0 end
    , case when inserted.cheque=1 and td.tipodoc<>3 then inserted.echtotal else 0 end
    from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
    inner join ft3 on ft3.ft3stamp = inserted.ftstamp
    where td.lancacc=1 and td.ccautoreg=1 and inserted.multi=0
    IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancaol=1)>0
    INSERT INTO OL (olstamp,ftstamp,
    usrdata,usrhora,usrinis,
    ousrdata,ousrhora,ousrinis,origem,
    documento,cheque,
    data,dvalor,entr,eentr,entrm,descricao,olcodigo,grupo,sgrupo,fref,intid,ccusto,ncusto,
    site,pnome,pno,cxstamp,cxusername,ssstamp,ssusername,operext,
    ollocal,contado,local,moeda,
    acerto, eacerto, macerto, vdinheiro, evdinheiro, mvdinheiro,
    chtotal, echtotal, chtmoeda,
    modop1, epaga1, paga1, mpaga1, modop2, epaga2, paga2, mpaga2,
    modop3, epaga3, paga3, mpaga3, modop4, epaga4, paga4, mpaga4,
    modop5, epaga5, paga5, mpaga5, modop6, epaga6, paga6, mpaga6 )
    select inserted.ftstamp, inserted.ftstamp,
    inserted.usrdata, inserted.usrhora, inserted.usrinis,
    inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,'FT',
    inserted.nmdoc,convert(char(15), inserted.fno),
    inserted.fdata, inserted.fdata
    , inserted.total-inserted.rdtotal-inserted.virs
    , inserted.etotal-inserted.erdtotal-inserted.evirs
    , inserted.valorm2,
    case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.olcodigo,oc.olgrupo,oc.olsgrupo,
    inserted.fref, inserted.intid, inserted.ccusto, inserted.ncusto,
    inserted.site, inserted.pnome, inserted.pno, inserted.cxstamp, inserted.cxusername,
    inserted.ssstamp, inserted.ssusername,isnull(ft2.operext, 0),
    case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end,
    case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end,
    case when td.altollocal = 1 then ft2.vdlocal else td.vdlocal end,
    case when td.altollocal = 1 then ft2.olmoeda else td.olmoeda end,
    ft2.acerto, ft2.eacerto, ft2.macerto,
    case when ft2.parcial<>0 then ft2.parcial else case when ft2.eparcial=0 then ft2.vdinheiro else 0 end end,
    case when ft2.eparcial<>0 then ft2.eparcial else case when ft2.parcial=0 then ft2.evdinheiro else 0 end end,
    ft2.mvdinheiro,
    inserted.chtotal, inserted.echtotal, inserted.chtmoeda,
    ft2.modop1, ft2.epaga1, ft2.paga1, ft2.mpaga1, ft2.modop2, ft2.epaga2, ft2.paga2, ft2.mpaga2,
    ft2.modop3, ft2.epaga3, ft2.paga3, ft2.mpaga3, ft2.modop4, ft2.epaga4, ft2.paga4, ft2.mpaga4,
    ft2.modop5, ft2.epaga5, ft2.paga5, ft2.mpaga5, ft2.modop6, ft2.epaga6, ft2.paga6, ft2.mpaga6
    from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
    inner join oc on oc.olcodigo=td.olcodigo where td.lancaol=1
    IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=0 and inserted.rdtotal<>0 and inserted.tipodoc=1)>0
    INSERT INTO CC (ccstamp,ftstamp,
    usrdata,usrhora,usrinis,
    ousrdata,ousrhora,ousrinis,
    datalc,no,nome,cm,cmdesc,origem,cobranca,cambiofixo,deb,edeb,
    debm,
    formapag,situacao,
    moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,segmento,tipo,pais,estab,vendedor,vendnm
    )
    select inserted.ftstamp, inserted.ftstamp,
    inserted.usrdata, inserted.usrhora, inserted.usrinis,
    inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
    inserted.fdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmcc,td.cmccn,'FT',
    inserted.cobranca, inserted.cambiofixo, inserted.rdtotal, inserted.erdtotal,
    inserted.rdtotalm,
    ft2.formapag,'1',
    inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto, inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo,
    case when td.lancacli=0 then inserted.pais else ft2.c2pais end,case when td.lancacli=0 then inserted.estab else ft2.c2estab end, inserted.vendedor, inserted.vendnm
    from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
    where td.lancacc=0 and inserted.multi=0 and inserted.rdtotal<>0 and inserted.tipodoc=1
    if (select count(*) from inserted where inserted.lrstamp<>'')>0
    UPDATE LR SET lr.edebi=lr.edebi+inserted.total,
    lr.eedebi=lr.eedebi+inserted.etotal from inserted where lr.lrstamp=inserted.lrstamp
    if (select count(*) from inserted where inserted.lpstamp<>'')>0
    UPDATE LP SET lp.edebi=lp.edebi+inserted.total,
    lp.eedebi=lp.eedebi+inserted.etotal from inserted where lp.lpstamp=inserted.lpstamp
    if (select count(*) from inserted inner join ft2 ON ft2.ft2stamp = inserted.ftstamp where ft2.csupstamp<>'')>0
    UPDATE CSUP SET csup.ftstamp=inserted.ftstamp,csup.csupnmdoc=inserted.nmdoc, csup.csupadoc=inserted.fno, csup.csupdata=inserted.fdata
    from inserted inner join ft2 ON ft2.ft2stamp = inserted.ftstamp  where csup.csupstamp=ft2.csupstamp IF (select count(*) from inserted where inserted.anulado = 1) > 0
    UPDATE ft set ft.anulado = inserted.anulado from ft inner join inserted on ft.ftstamp = inserted.ftstamp where inserted.anulado = 1
END
if (select count(*) from inserted where inserted.nprotri = 0) > 0 and (select count(*) from deleted where deleted.nprotri = 0) > 0
BEGIN
IF update(ndoc) or update(no) OR UPDATE(tipodoc) OR UPDATE(ttiliq) OR UPDATE(ettiliq) OR UPDATE(anulado)
BEGIN

IF (select count(*) from deleted inner join ft2 on ft2.ft2stamp = deleted.ftstamp inner join td on td.ndoc=deleted.ndoc
inner join cl on cl.no=(case when td.lancacli=0 then deleted.no else ft2.c2no end) And cl.estab=(case when td.lancacli=0 then deleted.estab else ft2.c2estab end)
where (deleted.tipodoc=1 or deleted.tipodoc=3) )>0
UPDATE CL SET cl.acmfact=cl.acmfact-deleted.ttiliq,cl.eacmfact=cl.eacmfact-deleted.ettiliq from DELETED
inner join ft2 on ft2.ft2stamp = deleted.ftstamp inner join inserted on inserted.ftstamp = ft2.ft2stamp inner join td on td.ndoc=deleted.ndoc and td.regrd = 0
where cl.no=(case when td.lancacli=0 then deleted.no else ft2.c2no end) and deleted.anulado =0 and inserted.anulado = 0 and (deleted.tipodoc=1 or deleted.tipodoc=3)
IF (select count(*) from inserted inner join ft2 on ft2.ft2stamp = inserted.ftstamp inner join td on td.ndoc=inserted.ndoc and td.regrd = 0
inner join cl on cl.no=(case when td.lancacli=0 then inserted.no else ft2.c2no end) And cl.estab=(case when td.lancacli=0 then inserted.estab else ft2.c2estab end)
where (inserted.tipodoc=1 or inserted.tipodoc=3) )>0
UPDATE CL SET cl.acmfact=cl.acmfact+inserted.ttiliq,cl.eacmfact=cl.eacmfact+inserted.ettiliq from INSERTED
inner join ft2 on ft2.ft2stamp = inserted.ftstamp inner join td on td.ndoc=inserted.ndoc and td.regrd = 0
where inserted.anulado=0 and cl.no=(case when td.lancacli=0 then inserted.no else ft2.c2no end) and (inserted.tipodoc=1 or inserted.tipodoc=3)
END

DELETE from ftcc from inserted where inserted.multi = 0 and ftcc.ftstamp=inserted.ftstamp
IF UPDATE(chdata) OR UPDATE(no) OR UPDATE(nome) OR UPDATE(estab) OR UPDATE(chtotal) OR UPDATE(echtotal) OR UPDATE(chtmoeda)
OR UPDATE(moeda) OR UPDATE(clbanco) OR UPDATE(clcheque) OR UPDATE(fref) OR UPDATE(tptit) OR UPDATE(ndoc) OR UPDATE(multi) OR UPDATE(cheque)
 BEGIN
DELETE from CH from deleted
where ch.ftstamp = deleted.ftstamp and deleted.multi=0 and deleted.cheque=1 and deleted.ftstamp not in (select inserted.ftstamp from inserted where inserted.multi=0 and inserted.cheque=1)
 END
DELETE from CC from deleted
where cc.ftstamp = deleted.ftstamp and deleted.multi=0 and deleted.ftstamp not in (select inserted.ftstamp from inserted where inserted.multi=0)
DELETE from CC from deleted
where cc.ccstamp = substring('R'+deleted.ftstamp,1,25) and deleted.multi=0
IF UPDATE(chdata) OR UPDATE(no) OR UPDATE(nome) OR UPDATE(estab) OR UPDATE(chtotal) OR UPDATE(echtotal) OR UPDATE(chtmoeda)
OR UPDATE(moeda) OR UPDATE(clbanco) OR UPDATE(clcheque) OR UPDATE(fref) OR UPDATE(tptit) OR UPDATE(ndoc) OR UPDATE(multi) OR UPDATE(cheque)
 BEGIN
INSERT INTO CH (chstamp,ftstamp,
usrdata,usrhora,usrinis,
ousrdata,ousrhora,ousrinis,
data,dataem,datareg,no,nome,estab,origem,valor,evalor,valorm,moeda,
clbanco,clcheque,fref,tptit,contado,ollocal)
select inserted.ftstamp, inserted.ftstamp,
inserted.usrdata, inserted.usrhora, inserted.usrinis,
inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
inserted.chdata, inserted.ousrdata, inserted.ousrdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,
case when td.lancacli=0 then inserted.nome else ft2.c2nome end,
case when td.lancacli=0 then inserted.estab else ft2.c2estab end,
'FT',
case when td.tipodoc=3 then 0 else inserted.chtotal end,case when td.tipodoc=3 then 0 else inserted.echtotal end,
case when td.tipodoc=3 then 0 else inserted.chtmoeda end, inserted.moeda,
inserted.clbanco, inserted.clcheque, inserted.fref, inserted.tptit,
case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end else 0 end,
case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end else SPACE(50) end
from inserted inner join td on inserted.ndoc=td.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
where inserted.multi=0 and inserted.cheque=1
and inserted.ftstamp not in (select ch.ftstamp from ch where ch.ftstamp = inserted.ftstamp)
IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=1 and inserted.multi=0)>0
INSERT INTO CC (tpstamp,tpdesc,ccstamp,ftstamp,
usrdata,usrhora,usrinis,
ousrdata,ousrhora,ousrinis,
datalc,no,nome,cm,cmdesc,origem,cobranca,cambiofixo,deb,edeb,cred,ecred,
debm,credm,
credf,ecredf,credfm,
ivav1,ivav2,ivav3,ivav4,ivav5,ivav6,ivav7,ivav8,ivav9,
eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9,
ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, reexgiva,
datasup12, formapag,situacao,
clbanco,clcheque,
moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,segmento,tipo,pais,estab,vendedor,vendnm
,eivacativado, ivacativado, mivacativado
, valch,evalch,virs,evirs)
select inserted.tpstamp, inserted.tpdesc, inserted.ftstamp, inserted.ftstamp,
inserted.usrdata, inserted.usrhora, inserted.usrinis,
inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
inserted.fdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmcc,td.cmccn,'FT',
inserted.cobranca, inserted.cambiofixo,case when td.tipodoc=3 then 0 else inserted.total end,case when td.tipodoc=3 then 0 else inserted.etotal end,
case when td.tipodoc=3 then inserted.total*-1 else 0 end,case when td.tipodoc=3 then inserted.etotal*-1 else 0 end,
case when td.tipodoc=3 then 0 else inserted.totalmoeda end,case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end,
case when inserted.debreg>=case when td.tipodoc=3 then inserted.total*-1 else 0 end then case when td.tipodoc=3 then inserted.total*-1 else 0 end else inserted.debreg end,
case when inserted.edebreg>=case when td.tipodoc=3 then inserted.etotal*-1 else 0 end then case when td.tipodoc=3 then inserted.etotal*-1 else 0 end else inserted.edebreg end,
case when inserted.debregm>=case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end then case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end else inserted.debregm end,
inserted.ivav1, inserted.ivav2, inserted.ivav3, inserted.ivav4, inserted.ivav5, inserted.ivav6, inserted.ivav7, inserted.ivav8, inserted.ivav9,
inserted.eivav1, inserted.eivav2, inserted.eivav3, inserted.eivav4, inserted.eivav5, inserted.eivav6, inserted.eivav7, inserted.eivav8, inserted.eivav9,
inserted.ivatx1, inserted.ivatx2, inserted.ivatx3, inserted.ivatx4, inserted.ivatx5, inserted.ivatx6, inserted.ivatx7, inserted.ivatx8, inserted.ivatx9,
ft2.reexgiva,
dateadd(Month,13, inserted.fdata-DAY(inserted.fdata)),
isnull(ft2.formapag,'1'),'1',
inserted.clbanco, inserted.clcheque,
inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto, inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo,
case when td.lancacli=0 then inserted.pais else ft2.c2pais end,case when td.lancacli=0 then inserted.estab else ft2.c2estab end, inserted.vendedor, inserted.vendnm
, isnull(ft3.eivacativado, 0), isnull(ft3.ivacativado, 0)
,isnull(ft3.mivacativado, 0)
, case when inserted.cheque=1 and td.tipodoc<>3 then inserted.chtotal else 0 end
, case when inserted.cheque=1 and td.tipodoc<>3 then inserted.echtotal else 0 end
, inserted.virs, inserted.evirs
 from inserted inner join td on inserted.ndoc=td.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
 inner join ft3 on ft3.ft3stamp = inserted.ftstamp
 where td.lancacc=1 and inserted.multi = 0
and inserted.ftstamp not in (select cc.ccstamp from cc where cc.ccstamp = inserted.ftstamp)
 END
IF UPDATE(total) OR UPDATE(etotal) OR UPDATE(totalmoeda) OR UPDATE(virs) OR UPDATE(evirs) OR UPDATE(ndoc)
UPDATE CC set  debf=case when td.tipodoc=3 then 0 else inserted.total end, edebf=case when td.tipodoc=3 then 0 else inserted.etotal end, debfm=case when td.tipodoc=3 then 0 else inserted.totalmoeda end
, credf=case when td.tipodoc<>3 then 0 else inserted.total*-1 end
, ecredf=case when td.tipodoc<>3 then 0 else inserted.etotal*-1 end
, credfm=case when td.tipodoc<>3 then 0 else inserted.totalmoeda*-1 end
, eivacatreg = isnull(ft3.eivacativado, 0)
, ivacatreg = isnull(ft3.ivacativado, 0)
, mivacatreg = isnull(ft3.mivacativado, 0)
, virsreg=inserted.virs
, evirsreg=inserted.evirs
 from inserted inner join td on td.ndoc=inserted.ndoc inner join cc on cc.ftstamp=inserted.ftstamp
 inner join ft3 on ft3.ft3stamp = inserted.ftstamp
 where td.lancacc=1 and td.ccautoreg=1
INSERT INTO CC (tpstamp,tpdesc,ccstamp,ftstamp,
usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis,
datalc,no,nome,cm,cmdesc,origem,cobranca,cambiofixo,
cred,ecred,credm,credf,ecredf,credfm,deb,edeb,debm,debf,edebf,debfm,
ivav1,ivav2,ivav3,ivav4,ivav5,ivav6,ivav7,ivav8,ivav9,
eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9,
ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, reexgiva,
datasup12, formapag,situacao,
clbanco,clcheque,
moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,segmento,tipo,pais,estab,vendedor,vendnm
, valch,evalch)
select inserted.tpstamp, inserted.tpdesc,substring('R'+inserted.ftstamp,1,25), inserted.ftstamp,
inserted.usrdata, inserted.usrhora, inserted.usrinis,
inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
inserted.fdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmccautoreg,td.cmccautoregn,'FT',
inserted.cobranca, inserted.cambiofixo,
case when td.tipodoc=3 then 0 else inserted.total-inserted.rdtotal end,case when td.tipodoc=3 then 0 else inserted.etotal-inserted.erdtotal end,case when td.tipodoc=3 then 0 else inserted.totalmoeda-inserted.rdtotalm end,
case when td.tipodoc=3 then 0 else inserted.total-inserted.rdtotal end,case when td.tipodoc=3 then 0 else inserted.etotal-inserted.erdtotal end,case when td.tipodoc=3 then 0 else inserted.totalmoeda-inserted.rdtotalm end,
case when td.tipodoc<>3 then 0 else (inserted.total-inserted.rdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.etotal-inserted.erdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.totalmoeda-inserted.rdtotalm)*-1 end,
case when td.tipodoc<>3 then 0 else (inserted.total-inserted.rdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.etotal-inserted.erdtotal)*-1 end,case when td.tipodoc<>3 then 0 else (inserted.totalmoeda-inserted.rdtotalm)*-1 end,
inserted.ivav1, inserted.ivav2, inserted.ivav3, inserted.ivav4, inserted.ivav5, inserted.ivav6, inserted.ivav7, inserted.ivav8, inserted.ivav9,
inserted.eivav1, inserted.eivav2, inserted.eivav3, inserted.eivav4, inserted.eivav5, inserted.eivav6, inserted.eivav7, inserted.eivav8, inserted.eivav9,
inserted.ivatx1, inserted.ivatx2, inserted.ivatx3, inserted.ivatx4, inserted.ivatx5, inserted.ivatx6, inserted.ivatx7, inserted.ivatx8, inserted.ivatx9,
ft2.reexgiva,
dateadd(Month,13, inserted.fdata-DAY(inserted.fdata)),
isnull(ft2.formapag,'1'), '1'
, inserted.clbanco, inserted.clcheque
, inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto, inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo
, case when td.lancacli=0 then inserted.pais else ft2.c2pais end, case when td.lancacli=0 then inserted.estab else ft2.c2no end, inserted.vendedor, inserted.vendnm
, case when inserted.cheque=1 and td.tipodoc<>3 then inserted.chtotal else 0 end
, case when inserted.cheque=1 and td.tipodoc<>3 then inserted.echtotal else 0 end
 from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
 where td.lancacc=1 and td.ccautoreg=1 and inserted.multi=0
IF UPDATE(chdata) OR UPDATE(no) OR UPDATE(nome) OR UPDATE(estab) OR UPDATE(chtotal) OR UPDATE(echtotal) OR UPDATE(chtmoeda)
OR UPDATE(moeda) OR UPDATE(clbanco) OR UPDATE(clcheque) OR UPDATE(fref) OR UPDATE(tptit) OR UPDATE(ndoc) OR UPDATE(multi) OR UPDATE(cheque)
 BEGIN
UPDATE CH
set ch.usrdata=inserted.usrdata,ch.usrhora=inserted.usrhora,ch.usrinis=inserted.usrinis,
ch.data=inserted.chdata,ch.no=case when td.lancacli=0 then inserted.no else ft2.c2no end,
ch.nome=case when td.lancacli=0 then inserted.nome else ft2.c2nome end,
ch.estab=case when td.lancacli=0 then inserted.estab else ft2.c2estab end,
ch.origem='FT',
ch.valor=case when td.tipodoc=3 then 0 else inserted.chtotal end,ch.evalor=case when td.tipodoc=3 then 0 else inserted.echtotal end,
ch.valorm=case when td.tipodoc=3 then 0 else inserted.chtmoeda end,ch.moeda=inserted.moeda,
ch.clbanco=inserted.clbanco,ch.clcheque=inserted.clcheque,ch.fref=inserted.fref,ch.tptit=inserted.tptit
, ch.contado = case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end else 0 end,
ch.ollocal = case when td.lancaol = 1 then case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end else SPACE(50) end
from inserted inner join td on inserted.ndoc=td.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
where inserted.ftstamp=ch.ftstamp and inserted.multi=0 and inserted.cheque=1 and inserted.ftstamp IN (select deleted.ftstamp from deleted)
DELETE FROM CH FROM inserted where inserted.cheque=0 and inserted.ftstamp in (select ch.chstamp from ch where ch.chstamp=inserted.ftstamp) and inserted.ftstamp=ch.chstamp
 END
IF UPDATE(usrdata) OR UPDATE(usrhora) OR UPDATE(usrinis) OR UPDATE(tpdesc) OR UPDATE(fdata) OR UPDATE(no) OR UPDATE(nome) OR UPDATE(cobranca) OR UPDATE(cambiofixo)
OR UPDATE(moeda) OR UPDATE(fno) OR UPDATE(pdata) OR UPDATE(fref) OR UPDATE(intid) OR UPDATE(cobrador) OR UPDATE(rota) OR UPDATE(ccusto) OR UPDATE(ncusto)
OR UPDATE(zona) OR UPDATE(segmento) OR UPDATE(tipo) OR UPDATE(pais) OR UPDATE(estab) OR UPDATE(vendedor) OR UPDATE(vendnm)
OR UPDATE(ivav1) OR UPDATE(ivav2) OR UPDATE(ivav3) OR UPDATE(ivav4) OR UPDATE(ivav5) OR UPDATE(ivav6) OR UPDATE(ivav7) OR UPDATE(ivav8) OR UPDATE(ivav9)
OR UPDATE(eivav1) OR UPDATE(eivav2) OR UPDATE(eivav3) OR UPDATE(eivav4) OR UPDATE(eivav5) OR UPDATE(eivav6) OR UPDATE(eivav7) OR UPDATE(eivav8) OR UPDATE(eivav9)
OR UPDATE(ivatx1) OR UPDATE(ivatx2) OR UPDATE(ivatx3) OR UPDATE(ivatx4) OR UPDATE(ivatx5) OR UPDATE(ivatx6) OR UPDATE(ivatx7) OR UPDATE(ivatx8) OR UPDATE(ivatx9)
OR UPDATE(total) OR UPDATE(etotal) OR UPDATE(totalmoeda) OR UPDATE(clbanco) OR UPDATE(clcheque) OR UPDATE(debreg) OR UPDATE(debregm) OR UPDATE(cheque) OR UPDATE(virs)
OR UPDATE(evirs) OR UPDATE(ndoc) OR UPDATE(multi) OR UPDATE(echtotal) OR UPDATE(chtotal)
 BEGIN
UPDATE CC
set
cc.credf=(case when deleted.debreg>=cc.credf then 0 else cc.credf-deleted.debreg end),
cc.ecredf=(case when deleted.edebreg>=cc.ecredf then 0 else cc.ecredf-deleted.edebreg end),
cc.credfm=(case when deleted.debregm>=cc.credfm then 0 else cc.credfm-deleted.debregm end),
cc.valch=cc.valch-case when deleted.cheque=1 and td.tipodoc<>3 then deleted.chtotal else 0 end,
cc.evalch=cc.evalch-case when deleted.cheque=1 and td.tipodoc<>3 then deleted.echtotal else 0 end
from deleted inner join td on deleted.ndoc=td.ndoc where cc.ccstamp<>substring('R'+deleted.ftstamp,1,25) and deleted.ftstamp=cc.ftstamp and td.lancacc=1 and deleted.multi=0
UPDATE CC
set cc.usrdata=inserted.usrdata,cc.usrhora=inserted.usrhora,cc.usrinis=inserted.usrinis,
cc.tpstamp=inserted.tpstamp,cc.tpdesc=inserted.tpdesc,
cc.datalc=inserted.fdata,cc.no=case when td.lancacli=0 then inserted.no else ft2.c2no end,cc.nome=case when td.lancacli=0 then inserted.nome else ft2.c2nome end,cc.cm=td.cmcc,cc.cmdesc=td.cmccn,cc.cobranca=inserted.cobranca,
cc.cambiofixo=inserted.cambiofixo,
cc.moeda=inserted.moeda,cc.nrdoc=inserted.fno,cc.dataven=inserted.pdata,cc.fref=inserted.fref,cc.intid=inserted.intid,cc.cobrador=inserted.cobrador,cc.rota=inserted.rota,cc.ccusto=inserted.ccusto,cc.ncusto=inserted.ncusto,cc.zona=inserted.zona,
cc.segmento=inserted.segmento,cc.tipo=inserted.tipo,cc.pais=case when td.lancacli=0 then inserted.pais else ft2.c2pais end,cc.estab=case when td.lancacli=0 then inserted.estab else ft2.c2estab end,
cc.vendedor=inserted.vendedor,cc.vendnm=inserted.vendnm,
cc.ivav1=inserted.ivav1,cc.ivav2=inserted.ivav2,cc.ivav3=inserted.ivav3,cc.ivav4=inserted.ivav4,cc.ivav5=inserted.ivav5,cc.ivav6=inserted.ivav6,cc.ivav7=inserted.ivav7,cc.ivav8=inserted.ivav8,cc.ivav9=inserted.ivav9,
cc.eivav1=inserted.eivav1,cc.eivav2=inserted.eivav2,cc.eivav3=inserted.eivav3,cc.eivav4=inserted.eivav4,cc.eivav5=inserted.eivav5,cc.eivav6=inserted.eivav6,cc.eivav7=inserted.eivav7,cc.eivav8=inserted.eivav8,cc.eivav9=inserted.eivav9,
cc.ivatx1=inserted.ivatx1,cc.ivatx2=inserted.ivatx2,cc.ivatx3=inserted.ivatx3,cc.ivatx4=inserted.ivatx4,cc.ivatx5=inserted.ivatx5,cc.ivatx6=inserted.ivatx6,cc.ivatx7=inserted.ivatx7,cc.ivatx8=inserted.ivatx8,cc.ivatx9=inserted.ivatx9,
cc.reexgiva=td.reexgiva,
cc.datasup12 =dateadd(Month,13, inserted.fdata-DAY(inserted.fdata)),
cc.formapag=ft2.formapag,
cc.deb=case when td.tipodoc=3 then 0 else inserted.total end,cc.edeb=case when td.tipodoc=3 then 0 else inserted.etotal end,
cc.cred=case when td.tipodoc=3 then inserted.total*-1 else 0 end,cc.ecred=case when td.tipodoc=3 then inserted.etotal*-1 else 0 end,
cc.debm=case when td.tipodoc=3 then 0 else inserted.totalmoeda end,
cc.credm=case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end,
cc.clbanco =inserted.clbanco,cc.clcheque=inserted.clcheque,
cc.credf =(case when cc.credf+inserted.debreg>=(case when td.tipodoc=3 then inserted.total*-1 else 0 end) then (case when td.tipodoc=3 then inserted.total*-1 else 0 end) else cc.credf+inserted.debreg end),
cc.ecredf =(case when cc.ecredf+inserted.edebreg>=(case when td.tipodoc=3 then inserted.etotal*-1 else 0 end) then (case when td.tipodoc=3 then inserted.etotal*-1 else 0 end) else cc.ecredf+inserted.edebreg end),
cc.credfm =(case when cc.credfm+inserted.debregm>=(case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end) then (case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end) else cc.credfm+inserted.debregm end),
cc.valch =cc.valch+case when inserted.cheque=1 and td.tipodoc<>3 then inserted.chtotal else 0 end,
cc.evalch =cc.evalch+case when inserted.cheque=1 and td.tipodoc<>3 then inserted.echtotal else 0 end,
cc.eivacativado = isnull(ft3.eivacativado, 0),
cc.ivacativado = isnull(ft3.ivacativado, 0),
cc.mivacativado = isnull(ft3.mivacativado, 0),
cc.virs=inserted.virs,cc.evirs=inserted.evirs
 from inserted inner join td on inserted.ndoc=td.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
 inner join ft3 on ft3.ft3stamp = inserted.ftstamp
 where cc.ccstamp<>substring('R'+inserted.ftstamp,1,25) and inserted.ftstamp=cc.ftstamp and td.lancacc=1 and inserted.multi=0 and
inserted.ftstamp IN (select deleted.ftstamp from deleted) and cc.ccstamp <> substring('E'+inserted.ftstamp,1,25)
 END
IF UPDATE(usrdata) OR UPDATE(usrhora) OR UPDATE(usrinis) OR UPDATE(ousrdata) OR UPDATE(ousrhora) OR UPDATE(ousrinis)
OR UPDATE(nmdoc) OR UPDATE(fno) OR UPDATE(fdata) OR UPDATE(total) OR UPDATE(rdtotal) OR UPDATE(virs) OR UPDATE(etotal) OR UPDATE(erdtotal)
OR UPDATE(evirs) OR UPDATE(valorm2) OR UPDATE(nome) OR UPDATE(fref) OR UPDATE(intid) OR UPDATE(ccusto) OR UPDATE(ncusto) OR UPDATE(site)
OR UPDATE(pnome) OR UPDATE(pno) OR UPDATE(cxstamp) OR UPDATE(ssstamp) OR UPDATE(ssusername) OR UPDATE(chtotal) OR UPDATE(echtotal) OR UPDATE(chtmoeda) OR UPDATE(ndoc)
UPDATE OL SET
ol.olstamp=inserted.ftstamp,ol.ftstamp=inserted.ftstamp,
ol.usrdata=inserted.usrdata,ol.usrhora=inserted.usrhora,ol.usrinis=inserted.usrinis,
ol.ousrdata=inserted.ousrdata,ol.ousrhora=inserted.ousrhora,ol.ousrinis=inserted.ousrinis,ol.origem='FT',
ol.documento=inserted.nmdoc,ol.cheque=convert(char(15), inserted.fno),
ol.data=inserted.fdata,ol.dvalor=inserted.fdata
,ol.entr=inserted.total-inserted.rdtotal-inserted.virs
,ol.eentr=inserted.etotal-inserted.erdtotal-inserted.evirs
,ol.entrm=inserted.valorm2,ol.descricao=case when td.lancacli=0 then inserted.nome else ft2.c2nome end,ol.olcodigo=td.olcodigo,ol.grupo=oc.olgrupo,ol.sgrupo=oc.olsgrupo,
ol.fref=inserted.fref,ol.intid=inserted.intid,ol.ccusto=inserted.ccusto,ol.ncusto=inserted.ncusto,
ol.ollocal = case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end,
ol.contado = case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end,
ol.local = case when td.altollocal = 1 then ft2.vdlocal else td.vdlocal end,
ol.moeda = case when td.altollocal = 1 then ft2.olmoeda else td.olmoeda end,
ol.site=inserted.site,ol.pnome=inserted.pnome,ol.pno=inserted.pno,ol.cxstamp=inserted.cxstamp,
ol.ssstamp=inserted.ssstamp,ol.ssusername=inserted.ssusername,ol.operext=isnull(ft2.operext, 0),
 ol.acerto=ft2.acerto, ol.eacerto=ft2.eacerto, ol.macerto=ft2.macerto,
 ol.vdinheiro=case when ft2.parcial<>0 then ft2.parcial else case when ft2.eparcial=0 then ft2.vdinheiro else 0 end end,
 ol.evdinheiro=case when ft2.eparcial<>0 then ft2.eparcial else case when ft2.parcial=0 then ft2.evdinheiro else 0 end end,
 ol.mvdinheiro=ft2.mvdinheiro,
 ol.chtotal=inserted.chtotal, ol.echtotal=inserted.echtotal, ol.chtmoeda=inserted.chtmoeda,
 ol.modop1=ft2.modop1, ol.epaga1=ft2.epaga1, ol.paga1=ft2.paga1, ol.mpaga1=ft2.mpaga1,
 ol.modop2=ft2.modop2, ol.epaga2=ft2.epaga2, ol.paga2=ft2.paga2, ol.mpaga2=ft2.mpaga2,
 ol.modop3=ft2.modop3, ol.epaga3=ft2.epaga3, ol.paga3=ft2.paga3, ol.mpaga3=ft2.mpaga3,
 ol.modop4=ft2.modop4, ol.epaga4=ft2.epaga4, ol.paga4=ft2.paga4, ol.mpaga4=ft2.mpaga4,
 ol.modop5=ft2.modop5, ol.epaga5=ft2.epaga5, ol.paga5=ft2.paga5, ol.mpaga5=ft2.mpaga5,
 ol.modop6=ft2.modop6, ol.epaga6=ft2.epaga6, ol.paga6=ft2.paga6, ol.mpaga6=ft2.mpaga6
 from inserted inner join td on td.ndoc=inserted.ndoc inner join oc on oc.olcodigo=td.olcodigo
 inner join ft2 on ft2.ft2stamp = inserted.ftstamp where td.lancaol=1 and ol.ftstamp=inserted.ftstamp
IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancaol=1)>0
INSERT INTO OL (olstamp,ftstamp,
usrdata,usrhora,usrinis,
ousrdata,ousrhora,ousrinis,origem,
documento,cheque,
data,dvalor,entr,eentr,entrm,descricao,olcodigo,grupo,sgrupo,fref,intid,ccusto,ncusto,
site,pnome,pno,cxstamp,cxusername,ssstamp,ssusername,operext,
ollocal,contado,local,moeda,
 acerto, eacerto, macerto, vdinheiro, evdinheiro, mvdinheiro,
 chtotal, echtotal, chtmoeda,
 modop1, epaga1, paga1, mpaga1, modop2, epaga2, paga2, mpaga2,
 modop3, epaga3, paga3, mpaga3, modop4, epaga4, paga4, mpaga4,
 modop5, epaga5, paga5, mpaga5, modop6, epaga6, paga6, mpaga6 )
select inserted.ftstamp, inserted.ftstamp,
inserted.usrdata, inserted.usrhora, inserted.usrinis,
inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,'FT',
inserted.nmdoc,convert(char(15), inserted.fno), inserted.fdata, inserted.fdata
, inserted.total-inserted.rdtotal-inserted.virs
, inserted.etotal-inserted.erdtotal-inserted.evirs
, inserted.valorm2,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.olcodigo,oc.olgrupo,oc.olsgrupo,
inserted.fref, inserted.intid, inserted.ccusto, inserted.ncusto,
inserted.site, inserted.pnome, inserted.pno, inserted.cxstamp, inserted.cxusername,
inserted.ssstamp, inserted.ssusername,isnull(ft2.operext, 0),
case when td.altollocal = 1 then ft2.vdollocal else td.vdollocal end,
case when td.altollocal = 1 then ft2.vdcontado else td.vdcontado end,
case when td.altollocal = 1 then ft2.vdlocal else td.vdlocal end,
case when td.altollocal = 1 then ft2.olmoeda else td.olmoeda end,
ft2.acerto, ft2.eacerto, ft2.macerto,
case when ft2.parcial<>0 then ft2.parcial else case when ft2.eparcial=0 then ft2.vdinheiro else 0 end end,
case when ft2.eparcial<>0 then ft2.eparcial else case when ft2.parcial=0 then ft2.evdinheiro else 0 end end,
ft2.mvdinheiro,
inserted.chtotal, inserted.echtotal, inserted.chtmoeda,
ft2.modop1, ft2.epaga1, ft2.paga1, ft2.mpaga1, ft2.modop2, ft2.epaga2, ft2.paga2, ft2.mpaga2,
ft2.modop3, ft2.epaga3, ft2.paga3, ft2.mpaga3, ft2.modop4, ft2.epaga4, ft2.paga4, ft2.mpaga4,
ft2.modop5, ft2.epaga5, ft2.paga5, ft2.mpaga5, ft2.modop6, ft2.epaga6, ft2.paga6, ft2.mpaga6
from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
inner join oc on oc.olcodigo=td.olcodigo where td.lancaol=1
and inserted.ftstamp not in (select ol.olstamp from ol where ol.olstamp = inserted.ftstamp)
if (select count(*) from deleted where deleted.lrstamp<>'')>0
UPDATE LR SET lr.edebi=lr.edebi-deleted.total,
lr.eedebi=lr.eedebi-deleted.etotal from deleted where lr.lrstamp=deleted.lrstamp
if (select count(*) from inserted where inserted.lrstamp<>'')>0
UPDATE LR SET lr.edebi=lr.edebi+inserted.total,
lr.eedebi=lr.eedebi+inserted.etotal from inserted where lr.lrstamp=inserted.lrstamp and inserted.anulado =0
if (select count(*) from deleted where deleted.lpstamp<>'')>0
UPDATE LP SET lp.edebi=lp.edebi-deleted.total,
lp.eedebi=lp.eedebi-deleted.etotal from deleted where lp.lpstamp=deleted.lpstamp
if (select count(*) from inserted where inserted.lpstamp<>'')>0
UPDATE LP SET lp.edebi=lp.edebi+inserted.total,
lp.eedebi=lp.eedebi+inserted.etotal from inserted where lp.lpstamp=inserted.lpstamp and inserted.anulado =0
IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=0 and inserted.rdtotal<>0 and inserted.tipodoc=1)=0
and (select count(*) from deleted inner join td on td.ndoc=deleted.ndoc where td.lancacc=0 and deleted.rdtotal<>0 and deleted.tipodoc=1)>0
DELETE from CC from deleted
where cc.ftstamp = deleted.ftstamp and deleted.multi=0
IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=0 and inserted.rdtotal<>0 and inserted.tipodoc=1)>0
UPDATE CC  set cc.usrdata=inserted.usrdata,cc.usrhora=inserted.usrhora,cc.usrinis=inserted.usrinis,
cc.datalc=inserted.fdata,cc.no=case when td.lancacli=0 then inserted.no else ft2.c2no end,cc.nome=case when td.lancacli=0 then inserted.nome else ft2.c2nome end,cc.cm=td.cmcc,cc.cmdesc=td.cmccn,
cc.cobranca=inserted.cobranca,cc.cambiofixo=inserted.cambiofixo,cc.deb=inserted.rdtotal,cc.edeb=inserted.erdtotal,
cc.debm=inserted.rdtotalm,
cc.moeda=inserted.moeda,cc.nrdoc=inserted.fno,cc.dataven=inserted.pdata,cc.fref=inserted.fref,cc.intid=inserted.intid,
cc.cobrador=inserted.cobrador,cc.rota=inserted.rota,cc.ccusto=inserted.ccusto,cc.ncusto=inserted.ncusto,cc.zona=inserted.zona,
cc.segmento=inserted.segmento,cc.tipo=inserted.tipo,cc.pais=case when td.lancacli=0 then inserted.pais else ft2.c2pais end,cc.estab=case when td.lancacli=0 then inserted.estab else ft2.c2estab end,
cc.vendedor=inserted.vendedor,cc.vendnm=inserted.vendnm
from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp where td.lancacc=0 and inserted.multi=0 and inserted.rdtotal<>0 and inserted.tipodoc=1 and
inserted.ftstamp IN (select deleted.ftstamp from deleted) and inserted.ftstamp=cc.ftstamp
IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=0 and inserted.rdtotal<>0 and inserted.tipodoc=1)>0
INSERT INTO CC (ccstamp,ftstamp,
usrdata,usrhora,usrinis,
ousrdata,ousrhora,ousrinis,
datalc,no,nome,cm,cmdesc,origem,cobranca,cambiofixo,deb,edeb, debm,
moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,segmento,tipo,pais,estab,vendedor,vendnm
)
select inserted.ftstamp, inserted.ftstamp,
inserted.usrdata, inserted.usrhora, inserted.usrinis,
inserted.ousrdata, inserted.ousrhora, inserted.ousrinis,
inserted.fdata,case when td.lancacli=0 then inserted.no else ft2.c2no end,case when td.lancacli=0 then inserted.nome else ft2.c2nome end,td.cmcc,td.cmccn,'FT',
inserted.cobranca, inserted.cambiofixo, inserted.rdtotal, inserted.erdtotal,
inserted.rdtotalm,
inserted.moeda, inserted.fno, inserted.pdata, inserted.fref, inserted.intid, inserted.cobrador, inserted.rota, inserted.ccusto, inserted.ncusto, inserted.zona, inserted.segmento, inserted.tipo,
case when td.lancacli=0 then inserted.pais else ft2.c2pais end,case when td.lancacli=0 then inserted.estab else ft2.c2estab end, inserted.vendedor, inserted.vendnm
 from inserted inner join td on td.ndoc=inserted.ndoc inner join ft2 on ft2.ft2stamp = inserted.ftstamp
 where td.lancacc=0 and inserted.multi=0 and inserted.rdtotal<>0 and inserted.tipodoc=1 and
 not exists (select ccstamp from cc (nolock) where cc.ftstamp=inserted.ftstamp)
END
IF update(anulado) or (select COUNT(*) from inserted where inserted.anulado = 1) > 0
BEGIN DELETE FROM SL FROM inserted INNER JOIN fi (NOLOCK) ON inserted.ftstamp = fi.ftstamp WHERE inserted.anulado =1 and sl.fistamp=fi.fistamp AND fi.fistamp<>''
DELETE FROM SV FROM inserted INNER JOIN fi (NOLOCK) ON inserted.ftstamp = fi.ftstamp WHERE inserted.anulado =1 and sv.fistamp=fi.fistamp and fi.fistamp<>''

update BI
set bi.fno = 0
, bi.adoc = ''
, bi.nmdoc = ''
, bi.oftstamp = ''
, bi.ndoc = 0
, bi.qtt2 = bi.qtt2 -(select sum(fi.qtt *(case when fi.tipodoc=3 then -1 else 1 end))
from inserted
inner join fi (nolock) on inserted.ftstamp = fi.ftstamp
where inserted.anulado =1 and fi.bistamp <> '' and fi.bistamp = bi.bistamp)
from bi
inner join fi (nolock) on fi.bistamp =bi.bistamp
inner join inserted on inserted.ftstamp =fi.ftstamp
inner join deleted on deleted.ftstamp =fi.ftstamp
where inserted.anulado =1
and deleted.anulado = 0 and fi.bistamp<>''

UPDATE BI
set bi.partes2 = bi.partes2 -(select SUM(fi.partes)
from fi
inner join inserted on inserted.ftstamp = fi.ftstamp
where inserted.anulado = 1 and fi.bistamp <> '' and fi.bistamp = bi.bistamp)
from bi
inner join fi (nolock) on fi.bistamp = bi.bistamp
inner join inserted on inserted.ftstamp = fi.ftstamp
inner join ts (nolock) on ts.ndos = bi.ndos
inner join deleted on deleted.ftstamp=fi.ftstamp
where inserted.anulado = 1
and deleted.anulado = 0 and fi.bistamp<>'' and (ts.usam1 = 1 or ts.usam2 = 1 or ts.usam3 = 1)

UPDATE FTREG
set ftreg.vciva =ftreg.vciva - (select round(sum(case when fi.ivaincl=1 then ((fi.tiliquido)*-1)*(1-ftreg.fin/100)
else -((fi.tiliquido-round((fi.tiliquido*ftreg.fin/100), 2))*(1+fi.iva/100)) end), 2)
from fi
inner join inserted on inserted.ftstamp = fi.ftstamp
inner join deleted on deleted.ftstamp=fi.ftstamp
inner join ftreg on fi.ftregstamp=ftreg.ftregstamp
where inserted.anulado =1 and deleted.anulado =0)

, ftreg.vmoeda =ftreg.vmoeda - (select round(sum(case when fi.ivaincl=1 then ((fi.tmoeda)*-1)*(1-ftreg.fin/100)
else -((fi.tmoeda-round((fi.tmoeda*ftreg.fin/100), 2))*(1+fi.iva/100)) end), 2)
from fi
inner join inserted on inserted.ftstamp = fi.ftstamp
inner join deleted on deleted.ftstamp=fi.ftstamp
inner join ftreg on fi.ftregstamp=ftreg.ftregstamp
where inserted.anulado =1 and deleted.anulado =0)

, ftreg.evciva =ftreg.evciva - (select round(sum(case when fi.ivaincl=1 then ((fi.etiliquido)*-1)*(1-ftreg.fin/100)
else -((fi.etiliquido-round((fi.etiliquido*ftreg.fin/100), 2))*(1+fi.iva/100)) end), 2)
from fi
inner join inserted on inserted.ftstamp = fi.ftstamp
inner join deleted on deleted.ftstamp=fi.ftstamp
inner join ftreg on fi.ftregstamp=ftreg.ftregstamp where inserted.anulado =1 and deleted.anulado =0)
, ftreg.ultdoc=''
, ftreg.fin = inserted.fin
from ftreg
inner join fi (nolock) on ftreg.ftregstamp=fi.ftregstamp
inner join inserted on inserted.ftstamp=fi.ftstamp
inner join deleted on deleted.ftstamp=fi.ftstamp
where inserted.anulado = 1
and deleted.anulado= 0 and fi.tipodoc=3 and fi.ftregstamp<>space(25)

DELETE FROM PN FROM inserted inner join fi (nolock) on inserted.ftstamp=fi.ftstamp where inserted.anulado =1 and pn.fistamp=fi.fistamp

IF (select count(*) from inserted inner join td on td.ndoc=inserted.ndoc where td.lancacc=1)>0
UPDATE CC
SET deb=0, edeb=0, cred=0, ecred=0, debm=0, credm=0
, debf =0, edebf =0, debfm = 0, credf=0, ecredf=0, credfm=0
, ivav1=0, ivav2=0, ivav3=0, ivav4=0, ivav5=0, ivav6=0, ivav7=0, ivav8=0, ivav9=0
, eivav1=0, eivav2=0, eivav3=0,eivav4=0, eivav5=0, eivav6=0, eivav7=0, eivav8=0, eivav9=0
, ivatx1=0, ivatx2=0, ivatx3=0, ivatx4=0, ivatx5=0, ivatx6=0, ivatx7=0, ivatx8=0, ivatx9=0
, valch=0, evalch=0, virs=0,evirs=0
, eivacativado = 0, ivacativado = 0, mivacativado = 0
from inserted
inner join cc (nolock) on inserted.ftstamp = cc.ccstamp
where inserted.anulado = 1

DELETE from CC
from ftrd (nolock)
inner join inserted on ftrd.ftstamp =inserted.ftstamp
where cc.ccstamp ='C'+ftrd.ftrdstamp or cc.ccstamp='D'+ftrd.ftrdstamp and inserted.anulado=1
UPDATE CC SET
cc.difcambio = cc.difcambio - ISNULL((select SUM(ftrd.difcambio) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
										inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0 and ftrd.ccstamp <> '' and ftrd.ccstamp = cc.ccstamp), 0),
cc.edifcambio = cc.edifcambio - ISNULL((select SUM(ftrd.edifcambio) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                                         inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0 and ftrd.ccstamp <> '' and ftrd.ccstamp = cc.ccstamp), 0),
cc.credf=cc.credf - (select SUM(ftrd.vreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                      inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0 and ftrd.ccstamp <> '' and ftrd.ccstamp = cc.ccstamp),
cc.ecredf=cc.ecredf - (select SUM(ftrd.evreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                        inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0  and ftrd.ccstamp <> '' and ftrd.ccstamp = cc.ccstamp),
cc.credfm=cc.credfm - (select SUM(ftrd.vmreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                        inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0  and ftrd.ccstamp <> '' and ftrd.ccstamp = cc.ccstamp)
from cc inner join ftrd (nolock) on ftrd.ccstamp=cc.ccstamp inner join inserted on ftrd.ftstamp=inserted.ftstamp
inner join deleted on deleted.ftstamp=ftrd.ftstamp
where inserted.anulado = 1 and deleted.anulado =0 and ftrd.ccstamp <> ''
update cc set
cc.debf=cc.debf - (select SUM(ftrd.vreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                    inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0  and ftrd.ccstamp <> '' and ftrd.ftstamp = cc.ftstamp and ftrd.multi = 0),
cc.edebf=cc.edebf - (select SUM(ftrd.evreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                      inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0 and ftrd.ccstamp <> '' and ftrd.ftstamp = cc.ftstamp and ftrd.multi = 0),
cc.debfm=cc.debfm - (select SUM(ftrd.vmreg) from ftrd (nolock) inner join inserted on ftrd.ftstamp = inserted.ftstamp
                      inner join deleted on deleted.ftstamp=ftrd.ftstamp where inserted.anulado=1 and deleted.anulado =0 and ftrd.ccstamp <> '' and ftrd.ftstamp = cc.ftstamp and ftrd.multi = 0)
from cc inner join ft on ft.ftstamp = cc.ftstamp inner join ftrd on ftrd.ftstamp=cc.ftstamp inner join inserted on ftrd.ftstamp= inserted.ftstamp
inner join td on td.ndoc = ft.ndoc
inner join deleted on deleted.ftstamp=ftrd.ftstamp
where inserted.anulado = 1 and deleted.anulado =0 and cc.ccstamp<>'R'+ftrd.ftstamp and ft.multi = 0 and ftrd.multi = 0 and ftrd.ccstamp <> '' and td.ccautoreg = 0
update rd set rd.integrado=
(case isnull((select top 1 ftrd.ftrdstamp from ftrd ftrdori where ftrdori.rdstamp=ftrd.rdstamp),'') when '' then 0 else 1 end)
from ftrd inner join inserted on ftrd.ftstamp=inserted.ftstamp where inserted.anulado = 1 and rd.rdstamp=ftrd.rdstamp

UPDATE CC
set deb = 0, edeb = 0, debf = 0, edebf = 0, debfm = 0, debm = 0
,cred = 0, ecred = 0, credf = 0, ecredf = 0, credfm = 0, credm = 0
,ivav1 = 0, ivav2 = 0, ivav3 = 0, ivav4 = 0, ivav5 = 0, ivav6 = 0, ivav7 = 0, ivav8 = 0, ivav9 = 0
,eivav1 = 0, eivav2 = 0, eivav3 = 0, eivav4 = 0, eivav5 = 0, eivav6 = 0, eivav7 = 0, eivav8 = 0, eivav9 = 0
,ivatx1 = 0, ivatx2 = 0, ivatx3 = 0, ivatx4 = 0, ivatx5 = 0, ivatx6 = 0, ivatx7 = 0, ivatx8 = 0, ivatx9 = 0
, valch = 0, evalch = 0, virs = 0, evirs = 0
, virsreg = 0, evirsreg = 0
, eivacativado = 0, ivacativado = 0, eivacatreg = 0, ivacatreg = 0, mivacativado = 0, mivacatreg = 0
from inserted
inner join td on td.ndoc = inserted.ndoc
inner join cc on cc.ccstamp = 'R' + inserted.ftstamp
where td.lancacc = 1 and td.ccautoreg = 1 and inserted.anulado = 1

DELETE from OL 
from inserted
where ol.ftstamp = inserted.ftstamp and inserted.anulado = 1
UPDATE FT SET ft.facturada = 0, ft.nmdocft = '', ft.fnoft =0 
  FROM inserted 
 inner join deleted on deleted.ftstamp=inserted.ftstamp 
 INNER JOIN fi ON inserted.ftstamp=fi.ftstamp 
 INNER JOIN ft ON ft.fno=fi.fnoft AND fi.ndocft=ft.ndoc and fi.ftanoft = ft.ftano
 WHERE inserted.anulado=1 and deleted.anulado=0 
 UPDATE FI SET fi.ndocft = 0, fi.fnoft =0 
 FROM inserted 
 inner join deleted on deleted.ftstamp=inserted.ftstamp 
 INNER JOIN fi ON inserted.ftstamp=fi.ftstamp 
 WHERE inserted.anulado=1 and deleted.anulado=0 
DELETE FROM cc FROM inserted 
 INNER JOIN ftcc ON ftcc.ftstamp=inserted.ftstamp
 WHERE cc.ftstamp = inserted.ftstamp AND inserted.multi=1 AND inserted.anulado =1
UPDATE cl SET  cl.acmfact=cl.acmfact-inserted.ttiliq,cl.eacmfact=cl.eacmfact-inserted.ettiliq from inserted 
inner join ft2 on ft2.ft2stamp = inserted.ftstamp inner join td on td.ndoc=inserted.ndoc and td.regrd = 0 
inner join deleted on inserted.ftstamp = deleted.ftstamp 
where cl.no=(case when td.lancacli=0 then deleted.no else ft2.c2no end) and deleted.anulado = 0 and (deleted.tipodoc=1 or deleted.tipodoc=3) 
END

if update(nprotri) and (select count(*) from inserted where inserted.nprotri = 1) > 0 and (select count(*) from deleted where deleted.nprotri = 0) > 0 
BEGIN 
IF (select count(*) from deleted inner join td on td.ndoc=deleted.ndoc and td.lancacli=0 and td.regrd = 0
inner join cl on cl.no=deleted.no And cl.estab=deleted.estab
where (deleted.tipodoc=1 or deleted.tipodoc=3) )>0
UPDATE CL SET cl.acmfact=cl.acmfact-deleted.ttiliq, cl.eacmfact=cl.eacmfact-deleted.ettiliq from DELETED
 inner join td on td.ndoc=deleted.ndoc and td.lancacli=0 and td.regrd = 0
 where cl.no=deleted.no and deleted.anulado =0 and (deleted.tipodoc=1 or deleted.tipodoc=3)
DELETE from CH from deleted where ch.ftstamp=deleted.ftstamp
DELETE from CC from deleted where cc.ftstamp=deleted.ftstamp
DELETE from OL from deleted where ol.ftstamp=deleted.ftstamp
DELETE from ftcc from deleted where ftcc.ftstamp=deleted.ftstamp
if (select count(*) from deleted where deleted.lrstamp<>'')>0
UPDATE LR SET lr.edebi=lr.edebi-deleted.total,
lr.eedebi=lr.eedebi-deleted.etotal from deleted where lr.lrstamp=deleted.lrstamp
if (select count(*) from deleted where deleted.lpstamp<>'')>0
UPDATE LP SET lp.edebi=lp.edebi-deleted.total,
lp.eedebi=lp.eedebi-deleted.etotal from deleted where lp.lpstamp=deleted.lpstamp
delete from ftt from ftt inner join deleted on ftt.ftstamp =deleted.ftstamp
END
GO
