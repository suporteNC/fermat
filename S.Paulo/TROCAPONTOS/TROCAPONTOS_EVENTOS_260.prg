** Criado por: Paulo Ricardo Martins  em 25/09/2017
** Converte os pontos em Valor
******************************************************************


** Carrega a equivalencia de PONTOS
**************************************************************************************

IF TYPE("p_valpontos")='U'    && (X Akz / 1 Pts)
	public p_valpontos 
	p_valpontos = 0
endif


IF TYPE("p_pontosval")='U'    && (Limite minimo pontos / X Akz)
	public p_pontosval 
	p_pontosval = 0
endif


IF TYPE("p_limitepts")='U'    && (Limite minimo para troca de pontos)
	public p_limitepts 
	p_limitepts = 0
endif


** p_VALPONTOS 
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_valpontos'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_valpontos = cr_para1.valor
endif

fecha("CR_PARA1")



** p_PONTOSVAL
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_pontosval'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_pontosval = cr_para1.valor
endif

fecha("CR_PARA1")


** p_LIMITEPTS
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_LMINPONTOS'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_limitepts = cr_para1.valor
endif

fecha("CR_PARA1")

**************************************************************************

TEXT TO MSQL TEXTMERGE NOSHOW
SELECT CL.NOME, CL.NO, CL.ESTAB, CL.U_PONTOS, CL2.u_ncartao
FROM CL (nolock)
INNER JOIN CL2 (NOLOCK) ON CL2.CL2STAMP = CL.CLSTAMP
ORDER BY CL.NO
ENDTEXT

IF !U_SQLEXEC(MSQl,'CR_CLTMP')
	MSG('ERRO ENCONTRADO')
	MSG(MSQL)
	RETURN
ENDIF

select CR_CLTMP
go top
SCAN
	
	MCLPTS = CR_CLTMP.u_PONTOS

	MPONTOS = ((MCLPTS/VAL(p_LIMITEPTS))-INT((MCLPTS/VAL(p_LIMITEPTS))))*VAL(p_LIMITEPTS)
	
	MVAL = INT((MCLPTS/VAL(p_LIMITEPTS)))*VAL(p_pontosval)	
	MPTS = INT((MCLPTS/VAL(p_LIMITEPTS)))*VAL(p_LIMITEPTS)

	IF MVAL > 0
	
		U_SQLEXEC('BEGIN TRANSACTION')
	
		TEXT TO MSQL TEXTMERGE NOSHOW
		INSERT INTO CC (CCSTAMP, CRED, ECRED, DATALC, CMDESC, CM , MOEDA, NOME, NO, ESTAB, ORIGEM)
			VALUES ('<<U_STAMP()>>', <<STRTRAN(STR(MVAL,14,2),',','.')>>, <<STRTRAN(STR(MVAL,14,2),',','.')>>, GETDATE(), 'Vale em Cart�o', 122, 'AKZ', '<<CR_CLTMP.NOME>>',<<ASTR(CR_CLTMP.NO)>>,<<ASTR(CR_CLTMP.ESTAB)>>,'CC')	
		ENDTEXT
		IF !U_SQLEXEC(MSQL)
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			U_SQLEXEC('ROLLBACK')
			RETURN
		ENDIF
		
		
		TEXT TO MSQL TEXTMERGE NOSHOW
		insert into u_ctm (u_ctmstamp ,data ,cmdesc ,nrdoc ,debito ,credito ,loja  ,no ,nome ,saldo ,ftstamp ,tipo ,ncartao ) 
             select left(replace(newid(),'-',''),24), GETDATE(), 'Vale em Cart�o', (SELECT isnull(MAX(nrdoc),0)+1 FROM u_CTM (nolock) WHERE cmdesc = 'Vale em Cart�o')  
             , 0 as debito 
             , <<STRTRAN(STR(MPTS,14,2),',','.')>> as credito                   
             ,0 , <<ASTR(Cr_CLTMP.no)>>, '<<CR_CLTMP.nome>>',0, '','','<<CR_CLTMP.u_ncartao>>'  
		ENDTEXT
		IF !U_SQLEXEC(MSQL)
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			U_SQLEXEC('ROLLBACK')
			RETURN
		ENDIF			
			
			
		TEXT TO MSQL TEXTMERGE NOSHOW
		UPDATE CL
		SET U_PONTOS = <<STRTRAN(STR(MPONTOS,14,2),',','.')>>
		WHERE CL.NO = <<ASTR(CR_CLTMP.NO)>>
		ENDTEXT
		IF !U_SQLEXEC(MSQL)
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			U_SQLEXEC('ROLLBACK')
			RETURN
		ENDIF			
		
		U_SQLEXEC('COMMIT TRANSACTION')
				
	ENDIF 

ENDSCAN


