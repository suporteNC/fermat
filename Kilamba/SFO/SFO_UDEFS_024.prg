*******************************************
** Guarda estabelecimento na FO
**
** Criado por Rui Vale
** Criado em  28/09/2017
*******************************************

Select fo

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From e1(nolock)
	Where estab=0
ENDTEXT

If u_sqlexec(uSql,"uCurE1") And Reccount("uCurE1")>0
	Select uCurE1
	Replace fo.u_estab With uCurE1.u_estab

	fecha("uCurE1")
	Return fo.u_estab
Endif

Return fo.u_estab