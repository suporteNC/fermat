USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_stfp]    Script Date: 07/02/2023 19:17:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_STFP_Delete]
		@c1 char(25),
		@c2 char(25),
		@c3 varchar(50),
		@c4 text,
		@c5 varchar(4),
		@c6 varchar(50),
		@c7 bit,
		@c8 varchar(254),
		@c9 text,
		@c10 varchar(50),
		@c11 varchar(30),
		@c12 datetime,
		@c13 varchar(8),
		@c14 varchar(30),
		@c15 datetime,
		@c16 varchar(8),
		@c17 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[u_stfp] 
	where [u_stfpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_stfpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_stfp]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_stfp where u_stfpstamp=@c1