USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[NC_U_CAIXATMPAL_Delete]    Script Date: 30/01/2023 16:55:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_U_CAIXATMPAL_Delete]
		@c1 char(25),
		@c2 numeric(10,0),
		@c3 varchar(10),
		@c4 numeric(15,2),
		@c5 varchar(30),
		@c6 varchar(30),
		@c7 datetime,
		@c8 varchar(8),
		@c9 varchar(30),
		@c10 datetime,
		@c11 varchar(8),
		@c12 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PL].[u_caixatm] 
	where [u_caixatmstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixatmstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PL].[u_caixatm]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_caixatm where u_caixatmstamp=@c1