/****** Scripting replication configuration. Script Date: 08/09/2023 08:51:45 ******/
/****** Please Note: For security reasons, all password parameters were scripted with either NULL or an empty string. ******/

/****** Begin: Script to be run at Publisher ******/

/****** Installing the server as a Distributor. Script Date: 08/09/2023 08:51:45 ******/
use master
exec sp_adddistributor @distributor = N'SRVBD', @password = N''
GO

-- Adding the agent profiles
-- Updating the agent profile defaults
exec sp_MSupdate_agenttype_default @profile_id = 1
GO
exec sp_MSupdate_agenttype_default @profile_id = 2
GO
exec sp_MSupdate_agenttype_default @profile_id = 4
GO
exec sp_MSupdate_agenttype_default @profile_id = 6
GO
exec sp_MSupdate_agenttype_default @profile_id = 11
GO

-- Adding the distribution databases
use master
exec sp_adddistributiondb @database = N'distribution', @data_folder = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Data', @data_file = N'distribution.MDF', @data_file_size = 973, @log_folder = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Data', @log_file = N'distribution.LDF', @log_file_size = 585, @min_distretention = 0, @max_distretention = 72, @history_retention = 48, @security_mode = 1
GO

-- Adding the distribution publishers
exec sp_adddistpublisher @publisher = N'SRVBD', @distribution_db = N'distribution', @security_mode = 0, @login = N'sa', @password = N'', @working_directory = N'C:\Replication', @trusted = N'false', @thirdparty_flag = 0, @publisher_type = N'MSSQLSERVER'
GO

exec sp_addsubscriber @subscriber = N'FERMAT-SPAULO\sqlphc16', @type = 0, @description = N''
GO
exec sp_addsubscriber @subscriber = N'SERVERMND', @type = 0, @description = N''
GO
exec sp_addsubscriber @subscriber = N'SERVIDOR', @type = 0, @description = N''
GO
exec sp_addsubscriber @subscriber = N'SRVBD', @type = 0, @description = N''
GO


/****** End: Script to be run at Publisher ******/


-- Enabling the replication database
use master
exec sp_replicationdboption @dbname = N'FERMAT', @optname = N'publish', @value = N'true'
GO

exec [FERMAT].sys.sp_addlogreader_agent @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
GO
exec [FERMAT].sys.sp_addqreader_agent @job_login = null, @job_password = null, @frompublisher = 1
GO
-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_ANEXOS', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_ANEXOS', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'distributor_admin'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'lalmeida'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Valdemar'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'GRT'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'MM1'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'Teste_n'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'gg'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'domingos'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'sofia'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'ta'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'mslog'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'local'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'angelina'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'loc'
GO
exec sp_grant_publication_access @publication = N'PB_ANEXOS', @login = N'mauro'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_ANEXOS', @article = N'anexos', @source_owner = N'dbo', @source_object = N'anexos', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'anexos', @destination_owner = N'PG', @status = 16, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_ANEXOS_Insert]', @del_cmd = N'XCALL [NC_ANEXOS_Delete]', @upd_cmd = N'XCALL [NC_ANEXOS_Update]'

-- Adding the article's partition column(s)
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'anexosstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'oritable', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'tabnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'resumo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'grupo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'recstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'uniqueid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'descricao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'bdados', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'fullname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'fname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'fext', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'flen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'tipo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'passw', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'origem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'keylook', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'tpdos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'tpdoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ausrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ausrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ausrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'eusrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'eusrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'eusrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'anexopaistamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'assinatura', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'timestamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'anexoversaostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'versao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'idustamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'zipado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'bdadosstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'invisivel', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'checkout', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'cuserno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'cusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'usnoopen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'usnaopen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'isemail', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'emailid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'emaildata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'startwkf', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'wtwstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'emailsubj', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'privado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'nivel', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'lsgq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'u_bdorigem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ANEXOS', @article = N'anexos', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_ANEXOS', @article = N'anexos', @view_name = N'SYNC_anexos_1__132', @filter_clause = N'', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ANEXOS', @article = N'NC_ANEXOS_Delete', @source_owner = N'dbo', @source_object = N'NC_ANEXOS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_ANEXOS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ANEXOS', @article = N'NC_ANEXOS_Insert', @source_owner = N'dbo', @source_object = N'NC_ANEXOS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_ANEXOS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ANEXOS', @article = N'NC_ANEXOS_Update', @source_owner = N'dbo', @source_object = N'NC_ANEXOS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_ANEXOS_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_ANEXOS', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_ANEXOS', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_BO_PG', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_BO_PG', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_BO_PG', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'bi', @source_owner = N'dbo', @source_object = N'bi', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bi', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BILOJAS_Insert]', @del_cmd = N'XCALL [NC_BILOJAS_Delete]', @upd_cmd = N'XCALL [NC_BILOJAS_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_BO_PG', @article = N'bi', @filter_name = N'FLTR_bi_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_BO_PG', @article = N'bi', @view_name = N'SYNC_bi_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'bi2', @source_owner = N'dbo', @source_object = N'bi2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bi2', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BI2LOJAS_Insert]', @del_cmd = N'XCALL [NC_BI2LOJAS_Delete]', @upd_cmd = N'XCALL [NC_BI2LOJAS_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_BO_PG', @article = N'bi2', @filter_name = N'FLTR_bi2_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_BO_PG', @article = N'bi2', @view_name = N'SYNC_bi2_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'bo', @source_owner = N'dbo', @source_object = N'bo', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'bo', @destination_owner = N'PG', @status = 16, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_BOLOJAS_Insert]', @del_cmd = N'XCALL [NC_BOLOJAS_Delete]', @upd_cmd = N'XCALL [NC_BOLOJAS_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article's partition column(s)
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'nmdos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'obrano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'dataobra', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'nome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'totaldeb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'etotaldeb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tipo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'datafinal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'smoe4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'smoe3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'smoe2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'smoe1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'moetotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sdeb2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sdeb1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sdeb4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sdeb3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt14', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt13', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt12', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt11', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'sqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vendedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'vendnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'stot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'stot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'stot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'stot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'no', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'obranome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'boano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'dataopen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'datafecho', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'fechada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'nopat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'total', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tecnico', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tecnnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'nomquina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'maquina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'marca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'serie', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'zona', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'obs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ndos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'moeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'morada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'local', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'codpost', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ultfact', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'period', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ncont', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'segmento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'impresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'userimpresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'cobranca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ecusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'trab1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'trab2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'trab3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'trab4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'trab5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'custo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tabela1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'logi8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'fref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ccusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ncusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'infref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'lifref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'esdeb1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'esdeb2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'esdeb3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'esdeb4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'evqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'evqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'evqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'evqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'estot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'estot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'estot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'estot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'etotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_2tdesc1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_2tdesc2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_2tdes1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_2tdes2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'descc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'edescc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_1tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_2tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_1tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_2tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo11_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo11_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo11_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo11_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo21_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo21_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo21_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo21_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo31_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo31_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo31_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo31_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo41_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo41_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo41_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo41_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo51_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo51_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo51_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo51_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo61_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo61_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo61_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo61_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo12_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo12_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo12_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo12_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo22_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo22_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo22_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo22_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo32_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo32_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo32_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo32_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo42_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo42_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo42_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo42_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo52_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo52_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo52_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo52_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo62_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo62_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo62_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo62_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_totp1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'bo_totp2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_totp1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ebo_totp2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'edi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'memissao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'nome2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'iiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'iunit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'itotais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'iunitiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'itotaisiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'pastamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'snstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'mastamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'origem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'orinopat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'pnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'pno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'cxstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'cxusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ssstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ssusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'alldescli', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'alldesfor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'series', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'series2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'quarto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ocupacao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tabela2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'obstab2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'situacao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'lang', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'iemail', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'inome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ean', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'iecacodisen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'boclose', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'dtclose', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tpstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'tpdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'emconf', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'statuspda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'aprovado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_guiatran', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_fltransp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_pesotota', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_tvolume', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_naltqtt2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_utilizad', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_packn', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_bostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_volumen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_epackpda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_estabd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_BO_PG', @article = N'bo', @column = N'u_serv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_BO_PG', @article = N'bo', @filter_name = N'FLTR_bo_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_BO_PG', @article = N'bo', @view_name = N'SYNC_bo_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'bo2', @source_owner = N'dbo', @source_object = N'bo2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bo2', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BO2LOJAS_Insert]', @del_cmd = N'XCALL [NC_BO2LOJAS_Delete]', @upd_cmd = N'XCALL [NC_BO2LOJAS_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_BO_PG', @article = N'bo2', @filter_name = N'FLTR_bo2_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_BO_PG', @article = N'bo2', @view_name = N'SYNC_bo2_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'bo3', @source_owner = N'dbo', @source_object = N'bo3', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bo3', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BO3LOJAS_Insert]', @del_cmd = N'XCALL [NC_BO3LOJAS_Delete]', @upd_cmd = N'XCALL [NC_BO3LOJAS_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_BO_PG', @article = N'bo3', @filter_name = N'FLTR_bo3_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_BO_PG', @article = N'bo3', @view_name = N'SYNC_bo3_1__86', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BI2LOJAS_Delete', @source_owner = N'dbo', @source_object = N'NC_BI2LOJAS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2LOJAS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BI2LOJAS_Insert', @source_owner = N'dbo', @source_object = N'NC_BI2LOJAS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2LOJAS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BI2LOJAS_Update', @source_owner = N'dbo', @source_object = N'NC_BI2LOJAS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2LOJAS_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BILOJAS_Delete', @source_owner = N'dbo', @source_object = N'NC_BILOJAS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BILOJAS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BILOJAS_Insert', @source_owner = N'dbo', @source_object = N'NC_BILOJAS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BILOJAS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BILOJAS_Update', @source_owner = N'dbo', @source_object = N'NC_BILOJAS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BILOJAS_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO2LOJAS_Delete', @source_owner = N'dbo', @source_object = N'NC_BO2LOJAS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2LOJAS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO2LOJAS_Insert', @source_owner = N'dbo', @source_object = N'NC_BO2LOJAS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2LOJAS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO2LOJAS_Update', @source_owner = N'dbo', @source_object = N'NC_BO2LOJAS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2LOJAS_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO3LOJAS_Delete', @source_owner = N'dbo', @source_object = N'NC_BO3LOJAS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3LOJAS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO3LOJAS_Insert', @source_owner = N'dbo', @source_object = N'NC_BO3LOJAS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3LOJAS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BO3LOJAS_Update', @source_owner = N'dbo', @source_object = N'NC_BO3LOJAS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3LOJAS_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BOLOJAS_Delete', @source_owner = N'dbo', @source_object = N'NC_BOLOJAS_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOLOJAS_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BOLOJAS_Insert', @source_owner = N'dbo', @source_object = N'NC_BOLOJAS_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOLOJAS_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_BO_PG', @article = N'NC_BOLOJAS_Update', @source_owner = N'dbo', @source_object = N'NC_BOLOJAS_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOLOJAS_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_CL_Lojas', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'false', @alt_snapshot_folder = N'C:\Replication', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_CL_Lojas', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_CL_Lojas', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'cl', @source_owner = N'dbo', @source_object = N'cl', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'cl', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_CLLojas_Insert]', @del_cmd = N'XCALL [NC_CLLojas_Delete]', @upd_cmd = N'XCALL [NC_CLLojas_Update]', @filter_clause = N'[ousrinis]  <>''WEB'''

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_CL_Lojas', @article = N'cl', @filter_name = N'FLTR_cl_1__68', @filter_clause = N'[ousrinis]  <>''WEB''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_CL_Lojas', @article = N'cl', @view_name = N'syncobj_0x3444424634373530', @filter_clause = N'[ousrinis]  <>''WEB''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'cl2', @source_owner = N'dbo', @source_object = N'cl2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'cl2', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_CL2Lojas_Insert]', @del_cmd = N'XCALL [NC_CL2Lojas_Delete]', @upd_cmd = N'XCALL [NC_CL2Lojas_Update]', @filter_clause = N'[ousrinis] <>''WEB'''

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_CL_Lojas', @article = N'cl2', @filter_name = N'FLTR_cl2_1__68', @filter_clause = N'[ousrinis] <>''WEB''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_CL_Lojas', @article = N'cl2', @view_name = N'syncobj_0x3231324437394638', @filter_clause = N'[ousrinis] <>''WEB''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CL2Lojas_Delete', @source_owner = N'dbo', @source_object = N'NC_CL2Lojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CL2Lojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CL2Lojas_Insert', @source_owner = N'dbo', @source_object = N'NC_CL2Lojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CL2Lojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CL2Lojas_Update', @source_owner = N'dbo', @source_object = N'NC_CL2Lojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CL2Lojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CLLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_CLLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CLLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CLLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_CLLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CLLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_CLLojas_Update', @source_owner = N'dbo', @source_object = N'NC_CLLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CLLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_TPLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_TPLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_TPLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_TPLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'NC_TPLojas_Update', @source_owner = N'dbo', @source_object = N'NC_TPLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_CL_Lojas', @article = N'tp', @source_owner = N'dbo', @source_object = N'tp', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'tp', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_TPLojas_Insert]', @del_cmd = N'XCALL [NC_TPLojas_Delete]', @upd_cmd = N'XCALL [NC_TPLojas_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_CL_Lojas', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @destination_db = N'SPaulo', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_CL_Lojas', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @subscriber_db = N'SPaulo', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_CL_Lojas', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_CL_Lojas', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_FL_Mondego', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_FL_Mondego', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_FL_Mondego', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_FL_Mondego', @article = N'fl', @source_owner = N'dbo', @source_object = N'fl', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'fl', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FLPG_Insert]', @del_cmd = N'XCALL [NC_FLPG_Delete]', @upd_cmd = N'XCALL [NC_FLPG_Update]', @filter_clause = N'[u_mondego] =1'

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_FL_Mondego', @article = N'fl', @filter_name = N'FLTR_fl_1__60', @filter_clause = N'[u_mondego] =1', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_FL_Mondego', @article = N'fl', @view_name = N'syncobj_0x4131463130344443', @filter_clause = N'[u_mondego] =1', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_FL_Mondego', @article = N'NC_FLPG_Delete', @source_owner = N'dbo', @source_object = N'NC_FLPG_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FLPG_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_FL_Mondego', @article = N'NC_FLPG_Insert', @source_owner = N'dbo', @source_object = N'NC_FLPG_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FLPG_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_FL_Mondego', @article = N'NC_FLPG_Update', @source_owner = N'dbo', @source_object = N'NC_FLPG_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FLPG_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_FL_Mondego', @subscriber = N'SERVERMND', @destination_db = N'Mondego', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_FL_Mondego', @subscriber = N'SERVERMND', @subscriber_db = N'Mondego', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_INVENT_PG', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_INVENT_PG', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'distributor_admin'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'lalmeida'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Valdemar'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'GRT'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'MM1'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'Teste_n'
GO
exec sp_grant_publication_access @publication = N'PB_INVENT_PG', @login = N'gg'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIC_Delete', @source_owner = N'dbo', @source_object = N'NC_STIC_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIC_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIC_Insert', @source_owner = N'dbo', @source_object = N'NC_STIC_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIC_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIC_Update', @source_owner = N'dbo', @source_object = N'NC_STIC_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIC_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIL_Delete', @source_owner = N'dbo', @source_object = N'NC_STIL_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIL_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIL_Insert', @source_owner = N'dbo', @source_object = N'NC_STIL_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIL_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'NC_STIL_Update', @source_owner = N'dbo', @source_object = N'NC_STIL_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STIL_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'stic', @source_owner = N'dbo', @source_object = N'stic', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'stic', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_STIC_Insert]', @del_cmd = N'XCALL [NC_STIC_Delete]', @upd_cmd = N'XCALL [NC_STIC_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_INVENT_PG', @article = N'stil', @source_owner = N'dbo', @source_object = N'stil', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'stil', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_STIL_Insert]', @del_cmd = N'XCALL [NC_STIL_Delete]', @upd_cmd = N'XCALL [NC_STIL_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_INVENT_PG', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @destination_db = N'SPaulo', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_INVENT_PG', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @subscriber_db = N'SPaulo', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_INVENT_PG', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_INVENT_PG', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_Promo', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_Promo', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_Promo', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_SPLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_SPLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPLojas_Update', @source_owner = N'dbo', @source_object = N'NC_SPLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPPLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_SPPLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPPLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPPLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_SPPLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPPLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_SPPLojas_Update', @source_owner = N'dbo', @source_object = N'NC_SPPLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_SPPLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_TPVLLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_TPVLLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPVLLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_TPVLLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_TPVLLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPVLLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_TPVLLojas_Update', @source_owner = N'dbo', @source_object = N'NC_TPVLLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_TPVLLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTFAMILojas_Delete', @source_owner = N'dbo', @source_object = N'NC_U_SPSTFAMILojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTFAMILojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTFAMILojas_Insert', @source_owner = N'dbo', @source_object = N'NC_U_SPSTFAMILojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTFAMILojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTFAMILojas_Update', @source_owner = N'dbo', @source_object = N'NC_U_SPSTFAMILojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTFAMILojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_U_SPSTLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_U_SPSTLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTLojas_Update', @source_owner = N'dbo', @source_object = N'NC_U_SPSTLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTSUBFAMILojas_Delete', @source_owner = N'dbo', @source_object = N'NC_U_SPSTSUBFAMILojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTSUBFAMILojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTSUBFAMILojas_Insert', @source_owner = N'dbo', @source_object = N'NC_U_SPSTSUBFAMILojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTSUBFAMILojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPSTSUBFAMILojas_Update', @source_owner = N'dbo', @source_object = N'NC_U_SPSTSUBFAMILojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPSTSUBFAMILojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPUSTSUBFMLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_U_SPUSTSUBFMLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPUSTSUBFMLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPUSTSUBFMLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_U_SPUSTSUBFMLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPUSTSUBFMLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'NC_U_SPUSTSUBFMLojas_Update', @source_owner = N'dbo', @source_object = N'NC_U_SPUSTSUBFMLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_SPUSTSUBFMLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'sp', @source_owner = N'dbo', @source_object = N'sp', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'sp', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_SPLojas_Insert]', @del_cmd = N'XCALL [NC_SPLojas_Delete]', @upd_cmd = N'XCALL [NC_SPLojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'spp', @source_owner = N'dbo', @source_object = N'spp', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'spp', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_SPPLojas_Insert]', @del_cmd = N'XCALL [NC_SPPLojas_Delete]', @upd_cmd = N'XCALL [NC_SPPLojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'tpvl', @source_owner = N'dbo', @source_object = N'tpvl', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'tpvl', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_TPVLLojas_Insert]', @del_cmd = N'XCALL [NC_TPVLLojas_Delete]', @upd_cmd = N'XCALL [NC_TPVLLojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'u_spst', @source_owner = N'dbo', @source_object = N'u_spst', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_spst', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_SPSTLojas_Insert]', @del_cmd = N'XCALL [NC_U_SPSTLojas_Delete]', @upd_cmd = N'XCALL [NC_U_SPSTLojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'u_spstfami', @source_owner = N'dbo', @source_object = N'u_spstfami', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_spstfami', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_SPSTFAMILojas_Insert]', @del_cmd = N'XCALL [NC_U_SPSTFAMILojas_Delete]', @upd_cmd = N'XCALL [NC_U_SPSTFAMILojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'u_spstsubfam', @source_owner = N'dbo', @source_object = N'u_spstsubfam', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_spstsubfam', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_SPSTSUBFAMILojas_Insert]', @del_cmd = N'XCALL [NC_U_SPSTSUBFAMILojas_Delete]', @upd_cmd = N'XCALL [NC_U_SPSTSUBFAMILojas_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Promo', @article = N'u_spustsubfm', @source_owner = N'dbo', @source_object = N'u_spustsubfm', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_spustsubfm', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_SPUSTSUBFMLojas_Insert]', @del_cmd = N'XCALL [NC_U_SPUSTSUBFMLojas_Delete]', @upd_cmd = N'XCALL [NC_U_SPUSTSUBFMLojas_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_Promo', @subscriber = N'SRVBD', @destination_db = N'SINCPHC', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_Promo', @subscriber = N'SRVBD', @subscriber_db = N'SINCPHC', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_ST_Lojas', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'false', @alt_snapshot_folder = N'C:\Replication', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_ST_Lojas', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Lojas', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_STLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_STLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STLojas_Update', @source_owner = N'dbo', @source_object = N'NC_STLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STOBSLojas_Delete', @source_owner = N'dbo', @source_object = N'NC_STOBSLojas_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSLojas_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STOBSLojas_Insert', @source_owner = N'dbo', @source_object = N'NC_STOBSLojas_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSLojas_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_STOBSLojas_Update', @source_owner = N'dbo', @source_object = N'NC_STOBSLojas_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSLojas_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STFP_Delete', @source_owner = N'dbo', @source_object = N'NC_U_STFP_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STFP_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STFP_Insert', @source_owner = N'dbo', @source_object = N'NC_U_STFP_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STFP_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STFP_Update', @source_owner = N'dbo', @source_object = N'NC_U_STFP_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STFP_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STREL_Delete', @source_owner = N'dbo', @source_object = N'NC_U_STREL_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STREL_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STREL_Insert', @source_owner = N'dbo', @source_object = N'NC_U_STREL_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STREL_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'NC_U_STREL_Update', @source_owner = N'dbo', @source_object = N'NC_U_STREL_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STREL_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'st', @source_owner = N'dbo', @source_object = N'st', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'st', @destination_owner = N'PG', @status = 24, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_STLOJAS_Insert]', @del_cmd = N'XCALL [NC_STLOJAS_Delete]', @upd_cmd = N'XCALL [NC_STLOJAS_Update]'

-- Adding the article's partition column(s)
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ststamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'design', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'familia', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'stock', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epv1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pv1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'forref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'fornecedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'validade', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usaid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'uintr', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usrqtt', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eoq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcult', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcimp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcmoe', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pvcon', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pvultimo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pv3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pv2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'unidade', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ptoenc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'tabiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'local', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'fornec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'fornestab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttfor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttcli', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttrec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'udata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcpond', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttacin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttacout', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttvend', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pmvenda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'valin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'valout', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'stmax', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'stmin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'obs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'codigo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'uni2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'conversao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ivaincl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'nsujpp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecomissao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'imagem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pv4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pv5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'cpoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'containv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'contacev', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'contareo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'contacoe', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'peso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'bloqueado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'fobloq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'mfornec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'mfornec2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pentrega', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'consumo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'baixr', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'despimp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'mesescon', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marg1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marg2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marg3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marg4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marg5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'diaspto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'diaseoq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desc6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'noserie', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'clinica', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'vasilhame', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pbruto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'volume', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usalote', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'texteis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'garantia', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'opendata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'faminome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttesp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epv2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epv3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epv4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epv5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epcusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epcpond', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epcult', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epmvenda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epvultimo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epvcon', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iva1incl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iva2incl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iva3incl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iva4incl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iva5incl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ivapcincl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'stns', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'tipodesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usr6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'convunsup', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'nccod', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'massaliq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'url', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'vaiwww', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iectsug', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iectin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eiectin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iectinii', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'codfiscal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecasug', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecagrad', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecaref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecarefnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'txieca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'txiecanome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecautt', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecamultgrad', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'iecaisref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qlook', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'txtqlook', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'imgqlook', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'lang1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'langdes1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'lang2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'langdes2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'lang3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'langdes3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'lang4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'langdes4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'lang5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'langdes5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'nexist', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'statuspda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qttcat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'compnovo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecovalor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eecoval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecoval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecopl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eecopval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecopval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecoel', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eecoeval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecoeval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecorl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eecorval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecorval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecool', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'eecooval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecooval', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecopilha', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecoacumulador', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'qtttouch', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'semserprv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'contaieo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'inactivo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'orcamento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'refmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'descrmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epcmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'custo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'desci', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'descii', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'datar', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pcdisp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epcdisp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'pclab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'epclab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'custof', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecustof', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ofcstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'custog', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecustog', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'unidadef', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'codcmb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'cancpos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'datanovpv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'horanovpv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'tkhclass', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'stocktch', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'mod', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'restctprep', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'idudesign', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'notimpcp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'impfuelpos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'amostra', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'sujinv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_cpautal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_mondego', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_fermat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_impor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_frete', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_seguro', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_csubfam', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_subfam', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_ctpart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_tpart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_dcpautal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_kit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_local2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_local3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_local4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_local5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_local6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'sujretirs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_design', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_circuito', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_natureza', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_pais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_nfornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_fornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_rfornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_dtblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_userblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_dtfoblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_userfobl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_userina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_dtinact', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_lojaweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_ulojaweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_lwstock', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_prljweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_prcrweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_destaque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_didestq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_dfdestq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'ecoequipamento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Lojas', @article = N'st', @column = N'u_vimpr', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_ST_Lojas', @article = N'st', @view_name = N'SYNC_st_1__72', @filter_clause = N'', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'stobs', @source_owner = N'dbo', @source_object = N'stobs', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'stobs', @destination_owner = N'PG', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_STOBSLOJAS_Insert]', @del_cmd = N'XCALL [NC_STOBSLOJAS_Delete]', @upd_cmd = N'XCALL [NC_STOBSLOJAS_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'u_stfp', @source_owner = N'dbo', @source_object = N'u_stfp', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_stfp', @destination_owner = N'PG', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_STFP_Insert]', @del_cmd = N'XCALL [NC_U_STFP_Delete]', @upd_cmd = N'XCALL [NC_U_STFP_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Lojas', @article = N'u_strel', @source_owner = N'dbo', @source_object = N'u_strel', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_strel', @destination_owner = N'PG', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_STREL_Insert]', @del_cmd = N'XCALL [NC_U_STREL_Delete]', @upd_cmd = N'XCALL [NC_U_STREL_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_ST_Lojas', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @destination_db = N'SPaulo', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_ST_Lojas', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @subscriber_db = N'SPaulo', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_ST_Lojas', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_ST_Lojas', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_ST_Mondego', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'false', @alt_snapshot_folder = N'C:\Replication', @compress_snapshot = N'true', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_ST_Mondego', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_ST_Mondego', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STOBSPG_Delete', @source_owner = N'dbo', @source_object = N'NC_STOBSPG_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSPG_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STOBSPG_Insert', @source_owner = N'dbo', @source_object = N'NC_STOBSPG_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSPG_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STOBSPG_Update', @source_owner = N'dbo', @source_object = N'NC_STOBSPG_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STOBSPG_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STPG_Delete', @source_owner = N'dbo', @source_object = N'NC_STPG_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STPG_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STPG_Insert', @source_owner = N'dbo', @source_object = N'NC_STPG_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STPG_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'NC_STPG_Update', @source_owner = N'dbo', @source_object = N'NC_STPG_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STPG_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'st', @source_owner = N'dbo', @source_object = N'st', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'st', @destination_owner = N'PG', @status = 16, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_STPG_Insert]', @del_cmd = N'XCALL [NC_STPG_Delete]', @upd_cmd = N'XCALL [NC_STPG_Update]', @filter_clause = N'len(ltrim(rtrim(ref)))<=6 and stns=0'

-- Adding the article's partition column(s)
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ststamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'design', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'familia', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'forref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'fornecedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'unidade', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'fornec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'fornestab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'obs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'codigo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'uni2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'conversao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'imagem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'cpoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'containv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'contacev', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'mfornec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'mfornec2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'pentrega', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'despimp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'usalote', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'texteis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'opendata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'faminome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'stns', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'tipodesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'url', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'vaiwww', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'codfiscal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'lang1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'langdes1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'lang2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'langdes2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'lang3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'langdes3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'lang4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'langdes4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'lang5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'langdes5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'nexist', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'statuspda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'compnovo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'inactivo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'refmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'descrmo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'desci', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'descii', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'ofcstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'unidadef', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'tkhclass', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'amostra', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_cpautal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_mondego', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_fermat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_impor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_frete', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_seguro', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_csubfam', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_subfam', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_ctpart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_tpart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_dcpautal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_kit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_local2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_local3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_local4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_local5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_local6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_design', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_circuito', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_natureza', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_pais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_nfornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_fornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_rfornec1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_dtblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_userblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_dtfoblo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_userfobl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_userina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_dtinact', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_lojaweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_ulojaweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_lwstock', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_prljweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_prcrweb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_destaque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_didestq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_dfdestq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_articlecolumn @publication = N'PB_ST_Mondego', @article = N'st', @column = N'u_vimpr', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article filter
exec sp_articlefilter @publication = N'PB_ST_Mondego', @article = N'st', @filter_name = N'FLTR_st_1__69', @filter_clause = N'len(ltrim(rtrim(ref)))<=6 and stns=0', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec sp_articleview @publication = N'PB_ST_Mondego', @article = N'st', @view_name = N'SYNC_st_1__69', @filter_clause = N'len(ltrim(rtrim(ref)))<=6 and stns=0', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_ST_Mondego', @article = N'stobs', @source_owner = N'dbo', @source_object = N'stobs', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'stobs', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_STOBSPG_Insert]', @del_cmd = N'XCALL [NC_STOBSPG_Delete]', @upd_cmd = N'XCALL [NC_STOBSPG_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_ST_Mondego', @subscriber = N'SERVERMND', @destination_db = N'Mondego', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_ST_Mondego', @subscriber = N'SERVERMND', @subscriber_db = N'Mondego', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_StoredProcedures', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'false', @alt_snapshot_folder = N'C:\Replication', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_StoredProcedures', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_StoredProcedures', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_StoredProcedures', @article = N'NC_First', @source_owner = N'dbo', @source_object = N'NC_First', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_First', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_StoredProcedures', @article = N'NC_Sinc_Insert', @source_owner = N'dbo', @source_object = N'NC_Sinc_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_Sinc_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_StoredProcedures', @article = N'NC_Sinc_Update', @source_owner = N'dbo', @source_object = N'NC_Sinc_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_Sinc_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_StoredProcedures', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @destination_db = N'SPaulo', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_StoredProcedures', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @subscriber_db = N'SPaulo', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_StoredProcedures', @subscriber = N'SERVERMND', @destination_db = N'Mondego', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_StoredProcedures', @subscriber = N'SERVERMND', @subscriber_db = N'Mondego', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_StoredProcedures', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_StoredProcedures', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_Tabelas_Comuns', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'false', @alt_snapshot_folder = N'C:\Replication', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_Tabelas_Comuns', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_Tabelas_Comuns', @login = N'distributor_admin'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'bc', @source_owner = N'dbo', @source_object = N'bc', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bc', @destination_owner = N'PG', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BC_Insert]', @del_cmd = N'XCALL [NC_BC_Delete]', @upd_cmd = N'XCALL [NC_BC_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_BC_Delete', @source_owner = N'dbo', @source_object = N'NC_BC_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BC_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_BC_Insert', @source_owner = N'dbo', @source_object = N'NC_BC_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BC_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_BC_Update', @source_owner = N'dbo', @source_object = N'NC_BC_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BC_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_STFAMI_Delete', @source_owner = N'dbo', @source_object = N'NC_STFAMI_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STFAMI_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_STFAMI_Insert', @source_owner = N'dbo', @source_object = N'NC_STFAMI_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STFAMI_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_STFAMI_Update', @source_owner = N'dbo', @source_object = N'NC_STFAMI_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_STFAMI_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STSUBFAM_Delete', @source_owner = N'dbo', @source_object = N'NC_U_STSUBFAM_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STSUBFAM_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STSUBFAM_Insert', @source_owner = N'dbo', @source_object = N'NC_U_STSUBFAM_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STSUBFAM_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STSUBFAM_Update', @source_owner = N'dbo', @source_object = N'NC_U_STSUBFAM_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STSUBFAM_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STTPART_Delete', @source_owner = N'dbo', @source_object = N'NC_U_STTPART_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STTPART_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STTPART_Insert', @source_owner = N'dbo', @source_object = N'NC_U_STTPART_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STTPART_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'NC_U_STTPART_Update', @source_owner = N'dbo', @source_object = N'NC_U_STTPART_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_STTPART_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'stfami', @source_owner = N'dbo', @source_object = N'stfami', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'stfami', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_STFAMI_Insert]', @del_cmd = N'XCALL [NC_STFAMI_Delete]', @upd_cmd = N'XCALL [NC_STFAMI_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'u_stsubfam', @source_owner = N'dbo', @source_object = N'u_stsubfam', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_stsubfam', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_STSUBFAM_Insert]', @del_cmd = N'XCALL [NC_U_STSUBFAM_Delete]', @upd_cmd = N'XCALL [NC_U_STSUBFAM_Update]'
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_Tabelas_Comuns', @article = N'u_sttpart', @source_owner = N'dbo', @source_object = N'u_sttpart', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_sttpart', @destination_owner = N'PG', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_STTPART_Insert]', @del_cmd = N'XCALL [NC_U_STTPART_Delete]', @upd_cmd = N'XCALL [NC_U_STTPART_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_Tabelas_Comuns', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @destination_db = N'SPaulo', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_Tabelas_Comuns', @subscriber = N'FERMAT-SPAULO\SQLPHC16', @subscriber_db = N'SPaulo', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_Tabelas_Comuns', @subscriber = N'SERVERMND', @destination_db = N'Mondego', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_Tabelas_Comuns', @subscriber = N'SERVERMND', @subscriber_db = N'Mondego', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO
use [FERMAT]
exec sp_addsubscription @publication = N'PB_Tabelas_Comuns', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_Tabelas_Comuns', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
use [FERMAT]
exec sp_addpublication @publication = N'PB_U_ATTACHMENT', @description = N'Transactional publication of database ''FERMAT'' from Publisher ''SRVBD''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec sp_addpublication_snapshot @publication = N'PB_U_ATTACHMENT', @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'SERVIDOR\Totalsoft'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'NT Service\MSSQLSERVER'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'NT SERVICE\SQLSERVERAGENT'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'tts'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'CAIXA1'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'nelsons'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'SA_ADM'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'servidor'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Rosa.Ferreira'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Gertrudes'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'CarlosV'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Helda'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'S.Gonçalves'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Carlosa'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Soniag'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'carla.manuel'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'tt'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'jpereira'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'rabreu'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'alerta'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'asilva'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'scosta'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'npio'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Sangueve'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'cmoura'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'filipe'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'edbrande'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'apascoal'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'cchico'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'amalua'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'bcosta'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'emajor'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'tt2'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'distributor_admin'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'lalmeida'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Valdemar'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'GRT'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'MM1'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'Teste_n'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'gg'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'domingos'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'sofia'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'ta'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'mslog'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'local'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'angelina'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'loc'
GO
exec sp_grant_publication_access @publication = N'PB_U_ATTACHMENT', @login = N'mauro'
GO

-- Adding the transactional articles
use [FERMAT]
exec sp_addarticle @publication = N'PB_U_ATTACHMENT', @article = N'NC_UATT_Delete', @source_owner = N'dbo', @source_object = N'NC_UATT_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_UATT_Delete', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_U_ATTACHMENT', @article = N'NC_UATT_Insert', @source_owner = N'dbo', @source_object = N'NC_UATT_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_UATT_Insert', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_U_ATTACHMENT', @article = N'NC_UATT_Update', @source_owner = N'dbo', @source_object = N'NC_UATT_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_UATT_Update', @destination_owner = N'dbo', @status = 16
GO
use [FERMAT]
exec sp_addarticle @publication = N'PB_U_ATTACHMENT', @article = N'u_attachment', @source_owner = N'dbo', @source_object = N'u_attachment', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_attachment', @destination_owner = N'PG', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_UATT_Insert]', @del_cmd = N'XCALL [NC_UATT_Delete]', @upd_cmd = N'XCALL [NC_UATT_Update]'
GO

-- Adding the transactional subscriptions
use [FERMAT]
exec sp_addsubscription @publication = N'PB_U_ATTACHMENT', @subscriber = N'SERVIDOR', @destination_db = N'FERMAT_LOJA', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'PB_U_ATTACHMENT', @subscriber = N'SERVIDOR', @subscriber_db = N'FERMAT_LOJA', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @frequency_type = 64, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 0, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @dts_package_location = N'Distributor'
GO



