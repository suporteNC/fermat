** Ricardo Vaz
** 2022-11-17
Public p_estab,p_ccusto,p_supervisor,p_armazem,p_armtrans
Public p_dielocal,p_dieencforn,p_dietrfarm,p_dieclimist,p_diepedloj
Public p_dieentrmat,p_diequebra,p_dieconsint,p_dieenccli


Local loWMIService, lcQuery, loNetworkAdapterConfigurations, loNIC, lcIPAddress
lcQuery = "Select * from Win32_NetworkAdapterConfiguration"
loWMIService = Getobject('winmgmts:\\.\root\cimv2')
loNetworkAdapterConfigurations = loWMIService.ExecQuery(lcQuery)
For Each loNIC In loNetworkAdapterConfigurations
	If loNIC.IPEnabled
		For Each lcIPAddress In loNIC.IPAddress
			p_estab=0
			if at('192.168.', astr(loNIC.IPAddress))>0
				**
				** ParqueGest
				**
				If At('192.168.29', lcIPAddress)>0
					p_estab=2
				Endif
				**
				** Palanca
				**
				If At('192.168.28', lcIPAddress)>0
					p_estab=3
				Endif
				**
				** SPaulo
				**
				If At('192.168.26', lcIPAddress)>0
					p_estab=5
				Endif
				******************* PARA TESTES *********************
				**
				** ParqueGest
				**
				If At('192.168.2.20', lcIPAddress)>0
					p_estab=2
				Endif
				**
				** Palanca
				**
				If At('192.168.2.21', lcIPAddress)>0
					p_estab=3
				Endif
				**
				** SPaulo
				**
				If At('192.168.2.22', lcIPAddress)>0
					p_estab=5
				Endif
				*****************************************************
			endif
		Endfor

	Endif
Endfor

u_sqlexec("select * from u_defslj where pestab=?p_estab","c_e1")

p_ccusto =  c_e1.ccusto
p_armazem =  c_e1.armazem
p_armtrans =  c_e1.armtrans
p_ccusto =  c_e1.ccusto
p_supervisor = ""

fecha("c_e1")

p_dielocal = Tts_ValidaNdos("u_elocal")
p_dieencforn = Tts_ValidaNdos("u_eencforn")
p_dietrfarm = Tts_ValidaNdos("u_etrfarm")
p_dieclimist = Tts_ValidaNdos("u_eclimist")
p_diepedloj = Tts_ValidaNdos("u_epedloj")
p_dieentrmat = Tts_ValidaNdos("u_eentrmat")
p_diequebra = Tts_ValidaNdos("u_equebra")
p_dieconsint = Tts_ValidaNdos("u_econsint")
p_dieenccli = Tts_ValidaNdos("u_eenccli")


Function Tts_ValidaNdos
	Lparameters uCampoConfig

	Local uRetval 
	uRetval = 0
	TEXT TO msel NOSHOW TEXTMERGE
		SELECT  TOP 1 ndos
		FROM ts (nolock)
		Where <<ALLTRIM(uCampoConfig)>> = 1
		And u_estab = <<Astr(p_estab)>>
	ENDTEXT

	If u_sqlexec(msel,"c_ts") And Reccount("c_ts")>0
		uRetval = c_ts.ndos
	Endif
	Fecha("c_ts")
	Return uRetval 
Endfunc