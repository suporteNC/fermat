If Used("a_erros")
                fecha("a_erros")
Endif
If Used("ExcelDados")
                fecha("ExcelDados")
Endif
If Used("cExcelDados")
                fecha("cExcelDados")
Endif
If Used("xlResults")
                fecha("xlResults")
Endif

If !Used("CursorErros")
                Create Cursor CursorErros (Obs c(150))
Endif
****** Saber do ficheiro ***************************
cFileName = Getfile("XLS,XLSX")
If Empty(cFileName)
                Return .f.  
Endif
********** Importar ficheiro excel **********************
=ImportaExcel(cFileName,"xlResults")
If Not Used("xlResults") Or Not Reccount("xlResults") > 0
                Messagebox("N�o existem registos no excel",16)
                Return .f.  
ENDIF

return .t.  


Procedure ImportaExcel
                Parameters cFileName As Character,cCursorName As Character
                Local cXbText,nTotalCol,nTotalLin,cNomeFolha
                cXbText = ""
                nTotalCol = 0
                nTotalLin = 0
                cNomeFolha = ""
                *** Abrir a folha excel
                Local oXobj
                oXobj = Createobject("Excel.Application")
                oXobj.Workbooks.Open(cFileName)
                *** escolher a folha a importar
                nFolhas =oXobj.sheets.Count()
                Declare a_folhas(nFolhas,1)
                For nI =1 To  nFolhas
                               a_folhas(nI) = oXobj.sheets(nI).Name
                Endfor
                cNomeFolha = a_folhas(1)
                If nFolhas > 1
                               cNomeFolha = getnome("Escolha a folha a Importar",a_folhas(1),"Folhas Excel","",1,.F.,"a_folhas")
                Endif
                If Not Empty(cNomeFolha)
                               oXobj.sheets(cNomeFolha).Select
                               **** determinar tamanho da folha
                               nTotalCol=oXobj.sheets(cNomeFolha).UsedRange.Columns.Count
                               nTotalLin=oXobj.sheets(cNomeFolha).UsedRange.Rows.Count
                               *!*                                        oXobj.sheets.item(2).select
                               *!*                                        nTotalCol=oXobj.sheets.item(1).UsedRange.columns.count
                               *!*                                        nTotalLin=oXobj.sheets.item(1).UsedRange.rows.count
                               ****** Criar o cursor de dados
                               For I =1 To nTotalCol
                                               If Not Empty(cXbText)
                                                               cXbText = cXbText + ","
                                               Endif
                                               cXbText = cXbText + "A" + Alltrim(Str(I)) + " c(60)"
                               Endfor
                               cXbText = "create cursor " + Alltrim(cCursorName) + "(" + cXbText + ")"
                               &cXbText
                               ********Carregar os dados do excel para o cursor *******
                               Wait Window "Carregar dados do excel" Nowait
                               regua(0,nTotalLin,"Carregar dados do Excel")
                               For I =1  To nTotalLin
                                               cXbText = "SELECT "  + Alltrim(cCursorName)
                                               &cXbText
                                               Append Blank
                                               For J=1 To nTotalCol
                                                               cXbText="REPLACE " + Alltrim(cCursorName) + ".A" + Alltrim(Str(J))
                                                               cXbText= cXbText + " WITH IIF(ISNULL(oXobj.Cells(" + Alltrim(Str(I)) + "," + Alltrim(Str(J)) + ").Value)"
                                                               cXbText= cXbText + ",'',GetValue(oXobj.Cells("+ Alltrim(Str(I)) + "," + Alltrim(Str(J)) + ").Value))"
                                                               &cXbText
                                               Endfor
                                               regua(1,I,astr(I) + "/" + astr(nTotalLin))
                               Endfor
                               regua(2)
                Endif
                oXobj.Quit &&Fechar o excel
Endproc
**** Fun��o que verifica o Tipo de dados e retorna o valor em AlfaNumerico
Function GetValue
                Parameters vValue
                Local cValor,cTipo
                cValor = ""
                cTipo = Type("vValue")
                Do Case
                               Case cTipo = "C" Or cTipo = "M"
                                               cValor = Alltrim(Strtran(vValue,"'","�"))
                               Case cTipo = "N" Or Alltrim(cTipo) == "Q" Or Alltrim(cTipo) == "Y"
                                               If Not Int(vValue) = vValue
                                                               cValor = Strtran(Alltrim(Str(vValue,16,6)),",",".")
                                               Else
                                                               cValor = Strtran(Alltrim(Str(vValue,16,0)),",",".")
                                               Endif
                               Case cTipo = "D" Or cTipo = "T"
                                               cValor = Alltrim(Dtos(vValue))
                               Case cTipo = "L"
                                               cValor = Iif(vValue,"1","0")
                               Otherwise
                                               cValor = ""
                Endcase
                Return cValor
Endfunc
Procedure ConvertToNumeric
                Parameters cValor
                Local cInteiro,cDecimal,cDigito,lDec,nValor
                cInteiro = ""
                cDecimal = ""
                cDigito = ""
                lDec = .F.
                nValor = 0
                If Not Empty(cValor)
                               ***** se o ponto � o decimal limpa a virgula e vice-versa.
                               cValor = Alltrim(cValor)
                               If At(".",cValor) > At(",",cValor)
                                               cValor = Strtran(cValor,",","")
                               Else
                                               cValor = Strtran(cValor,".","")
                               Endif
                               ***** separar a parte inteira da decimal
                               For I=1 To Len(cValor)
                                               cDigito = Substr(cValor,I,1)
                                               If Inlist(cDigito,",",".")
                                                               lDec = .T.
                                               Endif
                                               If Inlist(cDigito,"0","1","2","3","4","5","6","7","8","9") And Not lDec
                                                               cInteiro = cInteiro + cDigito
                                               Endif
                                               If Inlist(cDigito,"0","1","2","3","4","5","6","7","8","9") And lDec
                                                               cDecimal = cDecimal + cDigito
                                               Endif
                               Endfor
                               **** Passar o valor inteiro para numerico.
                               If Not Empty(cInteiro)
                                               nValor = Val(cInteiro)
                               Endif
                               **** Passar o valor decimal para numerico.
                               If Not Empty(cDecimal)
                                               nValor = nValor + (Val(cDecimal)/(10 ^ Len(cDecimal)))
                               Endif
                Endif
                Return nValor
Endproc
Function ConvertToData
                Parameters cValor As Character
                Local dData,cPart1,cPart2,cPart3,cAno,cMes,cDia,cSep,cResto
                dData = Date(1900,1,1)
                cPart1 = ""
                cPart2 = ""
                cPart3 = ""
                cAno = ""
                cMes = ""
                cDia = ""
                cSep = ""
                = SeparaDigitos(@cPart1,@cSep,@cValor)
                = SeparaDigitos(@cPart2,@cSep,@cValor)
                = SeparaDigitos(@cPart3,@cSep,@cValor)
                Return dData
Endfunc
Function SysToData
                Parameters cData
                Local dData,cAno,cMes,cDia
                dData = Date(1900,1,1)
                If Not Empty(cData)
                               cAno = Left(Alltrim(cData),4)
                               cMes = Substr(Alltrim(cData),5,2)
                               cDia = Substr(Alltrim(cData),7,2)
                Else
                               Return Date(1900,1,1)
                Endif
                If (Val(cAno)> 1900 And Val(cAno) < 2051) And (Val(cMes) > 0 And Val(cMes) < 13) And (Val(cDia) > 0 And Val(cDia) < 32)
                               dData = Date(Val(cAno),Val(cMes),Val(cDia))
                Endif
                Return dData
Endfunc
Procedure InsereErro
                Lparameters cErro
                If Not Used("CursorErros")
                               Create Cursor CursorErros (Obs c(150))
                Endif
                Select CursorErros
                Append Blank
                Replace CursorErros.Obs With cErro
Endproc
Procedure MostrarErros
                If Not Used("CursorErros")
                               Return
                Endif
                Select CursorErros
                Declare list_tit(1),list_cam(1),list_tam(1),list_pic(1)
                list_tit(1)= "Mensagem"
                list_cam(1)= "CursorErros.Obs"
                list_tam(1)= 8*60
                list_pic(1)= ""
                m.escolheu=.F.
                browlist('Lista de Mensagens','CursorErros','CursorErrostmp',.F.,.F.,.F.,.F.,.F.)
                fecha("CursorErros")
Endproc
Procedure mytimer
                For Each oForm In _Screen.Forms
                               If Upper(Alltrim(oForm.Name)) == "SFT"
                                               If Not Vartype(oForm.oTimerIMP) == "O"
                                                               oForm.AddObject("oTimerIMP","TimerIMP")
                                                               oForm.oTimerIMP.Interval=100
                                                               oForm.oTimerIMP.Enabled=.T.
                                               Endif
                               Endif
                Endfor
Endproc
Define Class TimerIMP As Timer
                Procedure Timer
                               For Each oFormT In _Screen.Forms
                                               If Upper(Alltrim(oFormT.Name)) == "CMENSAGEM" And Alltrim(oFormT.Edit1.Text) = "O documento de factura��o foi assinado."
                                                               oFormT.Ok1.Nossobutton1.Click()
                                               Endif
                               Endfor
                Endproc
Enddefine