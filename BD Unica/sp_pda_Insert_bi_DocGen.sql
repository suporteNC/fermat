USE [SINCPHC]
GO
/****** Object:  StoredProcedure [dbo].[sp_pda_Insert_bi_DocGen]    Script Date: 28/11/2022 17:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_pda_Insert_bi_DocGen]
	@bostamp char(25),
	@ref varchar(18),
	@codigo varchar(50),
	@design varchar(60),
	@qtt numeric(18,3),
	@unidade as varchar(10),
	@uni2qtt numeric(18,3),
	@unidad2 as varchar(10),
	@stipo int = 1,
	@arm numeric(5),
	@username as varchar(30),
	@iniciais varchar(3)
AS
BEGIN
	BEGIN TRAN

	declare @epv numeric(15,3), @epcusto numeric(15,3), @ivaincl bit
	
	select	@epv = epv1
			, @ivaincl = iva1incl
			, @epcusto = epcpond
	from	st (nolock)
	where	st.ref = @ref

	declare @vendedor numeric(4), @vendnm varchar(20)
	set @vendedor = 0
	set @vendnm = ''

	-- Consulto o vendedor se existir
	if (select count(*) from us (nolock) where iniciais = @iniciais and username = @username and vendedor > 0) > 0
	begin
		select	@vendedor = cm, @vendnm = cmdesc
		from	cm3 (nolock)
		where	cm = (select vendedor from us (nolock) where iniciais = @iniciais and username = @username)
	end 

	-- Verifico se a referência já existe e se existir soma
	if (select count(*) from bi (nolock) where bi.bostamp = @bostamp and bi.ref = @ref) > 0
	begin
		update	bi
		set		qtt = qtt + @qtt
				, ttdeb = round(((qtt + @qtt) * debito),2)
				, ettdeb = round(((qtt + @qtt) * edebito),2)
				, ttmoeda = round(((qtt + @qtt) * vumoeda),2)
		where	bi.ref = @ref and bi.bostamp = @bostamp

		if @@error <> 0 GOTO FAILURE
	end
	else
	begin
		DECLARE @retcode int

		DECLARE @bistamp char(25)
		DECLARE @lordem int
		DECLARE @armazem int

		SET @armazem = @arm
	
		SET @bistamp =  (right(newid(),11) + left(newid(),8) + right(newid(),5))

		SELECT	@lordem = ISNULL(MAX(lordem), 0) + 10000 
		FROM	bi (nolock)
		WHERE	bostamp = @bostamp

		INSERT 	INTO bi2 (bi2stamp, bostamp, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
		SELECT	@bistamp, @bostamp,
				@iniciais,
				--isnull(substring((select iniciais from us (nolock) where usercode = suser_sname()),PATINDEX ('%\%' ,(select iniciais from us (nolock) where usercode = suser_sname()))+1,len((select iniciais from us (nolock) where usercode = suser_sname()))), ''),
				convert(char(8),getdate(),112),
				convert(char(10), getdate(), 108),
				--isnull(substring((select iniciais from us (nolock) where usercode = suser_sname()),PATINDEX ('%\%' ,(select iniciais from us (nolock) where usercode = suser_sname()))+1,len((select iniciais from us (nolock) where usercode = suser_sname()))), ''),
				@iniciais,
				convert(char(8),getdate(),112),
				convert(char(10), getdate(), 108)

		if @@error <> 0 GOTO FAILURE

		INSERT 	INTO bi (bistamp, bostamp, ndos, nmdos, obrano, lordem, stipo, armazem, no, nome, 
				ref, codigo, design, qtt, unidade, uni2qtt, unidad2, tabiva, ousrinis, ousrdata, ousrhora
				, usrinis, usrdata, usrhora, vendedor, vendnm
				, Pu, prorc, vumoeda, ttmoeda, epu, edebito, eprorc, ttdeb, ettdeb, debitoori, edebitoori, ivaincl, debito, 
				pcusto, epcusto, custoind, ecustoind)
		SELECT	@bistamp, @bostamp, ndos, nmdos, obrano, @lordem, @stipo, @armazem, no, nome, 
				@ref, @codigo, @design, @qtt, @unidade, @uni2qtt, @unidad2, (select tabiva from st (nolock) where st.ref = @ref),
				--isnull(substring((select iniciais from us (nolock) where usercode = suser_sname()),PATINDEX ('%\%' ,(select iniciais from us (nolock) where usercode = suser_sname()))+1,len((select iniciais from us (nolock) where usercode = suser_sname()))), ''),
				@iniciais,
				convert(char(8),getdate(),112),
				convert(char(10), getdate(), 108),
				--isnull(substring((select iniciais from us (nolock) where usercode = suser_sname()),PATINDEX ('%\%' ,(select iniciais from us (nolock) where usercode = suser_sname()))+1,len((select iniciais from us (nolock) where usercode = suser_sname()))), ''),
				@iniciais,
				convert(char(8),getdate(),112),
				convert(char(10), getdate(), 108),
				@vendedor, @vendnm
				, @epv, @epv, @epv, (@qtt*@epv), @epv, @epv, @epv, (@qtt*@epv), (@qtt*@epv), @epv, @epv, @ivaincl, @epv
				, @epcusto, @epcusto, @epcusto, @epcusto
		FROM	bo (nolock)
		WHERE	bostamp = @bostamp

		if @@error <> 0 GOTO FAILURE

		exec @retcode = sp_pda_Dossier_RecalculaTotais @bostamp

		if @@error <> 0 or @retcode <> 0 
		begin
			GOTO FAILURE
		end

	end 

	COMMIT

	RETURN 0

FAILURE:
	IF (@@trancount > 0)
	BEGIN
		DECLARE @lerror int
		SELECT @lerror = @@error
		ROLLBACK TRAN 
		--RAISERROR (@lerror,16,-1)
		RETURN 1
	END
	ELSE
		RETURN 1

END
