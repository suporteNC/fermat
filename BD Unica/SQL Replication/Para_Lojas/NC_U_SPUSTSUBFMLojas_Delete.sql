USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_spustsubfm]    Script Date: 09/02/2023 18:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_SPUSTSUBFMLojas_Delete]
		@c1 char(25),
		@c2 char(25),
		@c3 varchar(10),
		@c4 varchar(30),
		@c5 varchar(30),
		@c6 datetime,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[u_spustsubfm] 
	where [u_spustsubfmstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_spustsubfmstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_spustsubfm]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_spustsubfm where u_spustsubfmstamp=@c1