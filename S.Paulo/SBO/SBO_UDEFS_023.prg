*******************************************
** Guarda estabelecimento
**
** Criado por Rui Vale
** Criado em  22/09/2017
*******************************************

Select bo

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab, u_estabd
	From ts(nolock)
	Where ts.ndos=<<STR(bo.ndos)>>
ENDTEXT

If u_sqlexec(uSql,"uCurTs") And Reccount("uCurTs")>0
	Select uCurTs
	Replace bo.u_estab With uCurTs.u_estab
	Replace bo.u_estabd With uCurTs.u_estabd

	fecha("uCurTs")
	Return bo.u_estab
Endif

Return bo.u_estab