** Criado por: Am�ndio Pacheco
** Data cria��o: 14.03.2014
select bo
create cursor cur_boclose (bostamp C(25), marcada L, ndos N(3), nmdos c(50), dataobra D, obrano N(10))
select bi
goto top
scan for not empty(bi.nmdoc)
	select cur_boclose 
	GOTO top
	LOCATE FOR ALLTRIM(cur_boclose.bostamp) == ALLTRIM(bi.nmdoc)
	IF NOT FOUND()
		append blank
		replace cur_boclose.bostamp with bi.nmdoc 
		replace cur_boclose.marcada with .t.
		replace cur_boclose.ndos with bi.ndoc
	ENDIF
endscan
if reccount("cur_boclose") > 0
	select cur_boclose
	scan 
		if u_sqlexec("select dataobra, nmdos, obrano from bo (nolock) where bostamp = '" + alltrim(cur_boclose.bostamp) + "'", "bopvtemp") and reccount("bopvtemp") > 0
			select bopvtemp
			replace cur_boclose.dataobra WITH bopvtemp.dataobra
			replace cur_boclose.nmdos WITH bopvtemp.nmdos
			replace cur_boclose.obrano WITH bopvtemp.obrano
		endif
	ENDSCAN
	
	var_i = 4
	DECLARE list_tit(var_i),list_cam(var_i),list_pic(var_i),list_tam(var_i),list_ronly(var_i), list_pic(var_i), list_combo(var_i) ,list_rot(var_i)
	SELECT cur_boclose
	var_i = 1
	list_tit(var_i)="Fechar?"
	list_cam(var_i)="cur_boclose.marcada"
	list_pic(var_i)="LOGIC"
	list_tam(var_i)=8*8
	list_ronly(var_i)=.F.
	var_i = var_i +1
	list_tit(var_i)="Data"
	list_cam(var_i)="cur_boclose.dataobra"
	list_pic(var_i)=""
	list_tam(var_i)=8*8
	list_ronly(var_i)=.T.
	var_i = var_i +1
	list_tit(var_i)="Dossier"
	list_cam(var_i)="cur_boclose.nmdos"
	list_pic(var_i)=""
	list_tam(var_i)=8*50
	list_ronly(var_i)=.T.
	var_i = var_i +1
	list_tit(var_i)="N� Dossier"
	list_cam(var_i)="cur_boclose.obrano"
	list_pic(var_i)=""
	list_tam(var_i)=8*50
	list_ronly(var_i)=.T.
	m.escolheu = .F.
	=CURSORSETPROP('Buffering',5,"cur_boclose")
	browlist('Escolha','cur_boclose', 'cur_boclose',.T.,.T.,.F.,.F.,.T.,'',.T.)
	IF m.escolheu = .F.
		msg("Opera��o cancelada")
		RETURN
	ELSE
		SELECT cur_boclose
		SCAN FOR cur_boclose.marcada
			u_sqlexec("update bo set fechada = 1, datafecho = convert(char(8), getdate(), 112) where bostamp = '" + ALLTRIM(cur_boclose.bostamp) + "'")
		ENDSCAN
	ENDIF
ENDIF
fecha("cur_boclose")