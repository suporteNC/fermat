** J�lio Ricardo
if u_caixa.fechada = .t. and u_caixa.usrl8 = .t.

	TEXT TO msel NOSHOW TEXTMERGE
	SELECT  username , pwpos FROM us (nolock)
	where userno = <<ASTR(m.ch_userno)>> and  U_adminpos =  1
	ENDTEXT

	If Not u_sqlexec(msel,"c_temp")
		msg(msel)
		Return .F.
	Endif

	If Reccount("c_temp")= 0
		msg("O utilizador n�o existe ou n�o � supervisor. N�o pode alterar uma caixa fechada")
		fecha("c_temp")
		Return .F.
	Endif

	cPASS = getnome('Introduza a password de adiministrador POS, para gravar','','','',1,.T.)

	IF ALLTRIM(cpass)<>ALLTRIM(c_temp.pwpos) 
		msg("Password errada. N�o pode alterar uma caixa fechada")
		return .f.
	ENDIF

endif

select u_caixa
replace u_caixa.usrl8 with .t. 

If  u_caixa.fdia = .T.

	Select  u_caixa
	If  u_caixa.fechada = .T. And   (Oldval('fechada','u_caixa') = .T. And su_caixa.editing) 
		msg("N�o pode alterar uma caixa fechada.")
		IF m.sqlsa = .F.  
			Return .F.
		ENDIF  
	Endif

	cPASS = getnome('Introduza a password para fechar a caixa dia.','','','',1,.T.)
	TEXT TO msel NOSHOW TEXTMERGE
	SELECT  username FROM us (nolock)
	where pwpos = '<<ALLTRIM(cPASS)>>'  and  U_SUPERV =  1
	ENDTEXT

	If Not u_sqlexec(msel,"c_temp")
		msg(msel)
		Return .F.
	Endif

	If Reccount("c_temp")= 0
		msg("O utilizador n�o existe ou n�o � supervisor.")
		fecha("c_temp")
		Return .F.
	Endif

	If u_caixa.deptot = 0.00
		IF not pergunta("Tal�o de dep�sito a ZERO. Pretende Continuar",2,"Devera preencher Valor Depoisto caso exista",.T.)
			RETURN .F.
		ELSE	
			cPASS = getnome('Introduza a password para confirmar DEPOSITO a ZERO.','','','',1,.T.)
			TEXT TO msel NOSHOW TEXTMERGE
			SELECT  username FROM us (nolock)
			where pwpos = '<<ALLTRIM(cPASS)>>'  and  U_gerente =  1
			ENDTEXT

			If Not u_sqlexec(msel,"c_temp")
				msg(msel)
				Return .F.
			Endif

			If Reccount("c_temp")= 0
				msg("O utilizador n�o existe ou n�o � Gerente.")
				fecha("c_temp")
				Return .F.
			Endif			

		ENDIF   
		***msg("Preencha o valor do tal�o de dep�sito")
		***Return .F.
	ENDIF
	
	SELECT  u_caixa
	Replace u_caixa.userfechada With  c_temp.username

	If u_caixa.totnum - u_caixa.maneio + u_caixa.sangria  <> u_caixa.deptot
		msg("O valor do dep�sito n�o corresponde ao valor declarado deduzido do fundo maneio" +chr(13)+ "Valor a depositar " + TRANSFORM((u_caixa.totnum - u_caixa.maneio + u_caixa.sangria),"9.999.999.999,99")) 
		Return  .F.
	Endif

	Return .T.
Endif


** pede paw de fecho de valores
If  u_caixa.fvalores = .F.
	cPASS = getnome('Introduza a password para fechar valores','','','',1,.T.)
	TEXT TO msel NOSHOW TEXTMERGE
		SELECT  username FROM us (nolock)
		where pwpos = '<<cPASS>>'
	ENDTEXT

	If Not u_sqlexec(msel,"c_temp")
		msg(msel)
		Return .F.
	Endif

	If Reccount("c_temp")= 0
		msg("O utilizador n�o existe.")
		fecha("c_temp")
		Return .F.
	Endif

	If  Alltrim(c_temp.username) <> Alltrim(u_caixa.cxusername)
msg(c_temp.username)
msg(u_caixa.cxusername)
		msg("N�o foi este utilizador que fechou a caixa.")
		fecha("c_temp")
		Return .F.
	Endif

	Select u_caixa
	Replace u_caixa.fvalores With .T.
	Replace u_caixa.userfvalores With  c_temp.username
	Replace u_caixa.paga1com With u_caixa.paga1-u_caixa.paga1liq
	Replace u_caixa.paga2com With u_caixa.paga2-u_caixa.paga2liq
	fecha("c_temp")
	Return .T.

Endif

**** J� tem o fecho de valores realizado

cPASS = getnome('Introduza a password para fechar a CAIXA.','','','',1,.T.)
TEXT TO msel NOSHOW TEXTMERGE
	SELECT  username FROM us (nolock)
	where pwpos = '<<ALLTRIM(cPASS)>>'  and  U_SUPERV =  1
ENDTEXT

If Not u_sqlexec(msel,"c_temp")
	msg(msel)
	Return .F.
Endif

If Reccount("c_temp")= 0
	msg("O utilizador n�o existe ou n�o � supervisor.")
	fecha("c_temp")
	Return .F.
Endif



Select  u_caixa
Replace u_caixa.userfechada With  c_temp.username
If  u_caixa.fechada = .T. And   (Oldval('fechada','u_caixa') = .T. And su_caixa.editing)
	msg("N�o pode alterar uma caixa fechada.")
		IF m.sqlsa = .F.  
			Return .F.
		ENDIF
Endif



Select u_caixa
If u_caixa.paga1 <> 0  And   u_caixa.paga1liq = 0
	msg("Introduza o valor Liquido do Cart�o.")
	Return .F.
Endif
If u_caixa.paga1 = 0  And   u_caixa.paga1liq <> 0
	msg("Introduza o valor Bruto do Cart�o.")
	Return .F.
Endif


Select u_caixa
If u_caixa.paga2 <> 0  And   u_caixa.paga2liq = 0
	msg("Introduza o valor Liquido do Cart�o.")
	Return .F.
Endif
If u_caixa.paga2 = 0  And   u_caixa.paga2liq <> 0
	msg("Introduza o valor Bruto do Cart�o.")
	Return .F.
Endif

**** Calcula fecho de caixa
TEXT TO msel NOSHOW TEXTMERGE
	select  sum(totreal)totreal, sum(cheqreal)cheqreal, sum(paga1r)paga1r, sum(paga2r)paga2r, sum(paga3r)paga3r
	, sum(paga4r)paga4r, sum(paga5r)paga5r, sum(paga6r)paga6r
	from (
		select
     		isnull(sum(round(evdinheiro-etroco,2)),0) totreal
            ,isnull(sum(round(echtotal,2)),0) cheqreal
            ,isnull(sum(round(epaga1,2)),0)  paga1r
            ,isnull(sum(round(epaga2,2)),0)  paga2r
            ,isnull(sum(round(epaga3,2)),0)  paga3r
            ,isnull(sum(round(epaga4,2)),0)  paga4r
            ,isnull(sum(round(epaga5,2)),0)  paga5r
            ,isnull(sum(round(epaga6,2)),0) paga6r
            ,isnull(sum(round(etotal,2)), 0) Total
            ,isnull (sum(round(ft.etotal-ft.ettiva,2)),0) as Base_Inc
            ,isnull (sum(round(ettiva,2)),0) IVA
            ,isnull(sum(round(edescc,2)), 0) descc
            ,isnull(sum(round(efinv,2)),0) descf
			, isnull(sum(round(case when  ft.tipodoc= 3 then abs(etotal) else 0 end  ,2)), 0) devdia
			from ft (nolock)
			inner join td(nolock) on ft.ndoc = td.ndoc and td.regrd = 0 and td.cmsl not in (75) and  td.nmdoc not like '%Credito%'
			left join ft2  (nolock) on ft.ftstamp = ft2.ft2stamp
			where ft.cxstamp  = '<<u_caixa.u_caixastamp>>' and ft.anulado = 0
		union all
			select
     			isnull(sum(round(evdinheiro,2)),0) totreal
				,isnull(sum(round(echtotal,2)),0) cheqreal
				,isnull(sum(round(epaga1,2)),0)  paga1r
				,isnull(sum(round(epaga2,2)),0)  paga2r
				,isnull(sum(round(epaga3,2)),0)  paga3r
				,isnull(sum(round(epaga4,2)),0)  paga4r
				,isnull(sum(round(epaga5,2)),0)  paga5r
				,isnull(sum(round(epaga6,2)),0) paga6r
				,0 Total
				,0 as Base_Inc
				,0 IVA
				,0 descc
				,0 descf
				, 0 devdia
				from rd (nolock)
				where rd.cxstamp  = '<<u_caixa.u_caixastamp>>' and rd.anulado = 0
		union all
			select
     			isnull(sum(round(evdinheiro,2)),0) totreal
				,isnull(sum(round(echtotal,2)),0) cheqreal
				,isnull(sum(round(epaga1,2)),0)  paga1r
				,isnull(sum(round(epaga2,2)),0)  paga2r
				,isnull(sum(round(epaga3,2)),0)  paga3r
				,isnull(sum(round(epaga4,2)),0)  paga4r
				,isnull(sum(round(epaga5,2)),0)  paga5r
				,isnull(sum(round(epaga6,2)),0) paga6r
				,0 Total
				,0 as Base_Inc
				,0 IVA
				,0 descc
				,0 descf
				, 0 devdia
				from re (nolock)
				where re.cxstamp  = '<<u_caixa.u_caixastamp>>' and re.process = 1
		) aa
ENDTEXT
If Not  u_sqlexec(msel,"c_ftcx")
	msg(msel)
	Return
Endif
Select  c_ftcx
Select  c_ftcx
If Reccount("c_ftcx") > 0
	Select u_caixa
	Replace  u_caixa.totreal 	With c_ftcx.totreal
	Replace  u_caixa.cheqreal 	With c_ftcx.cheqreal
	Replace  u_caixa.paga1r		With c_ftcx.paga1r
	Replace  u_caixa.paga2r		With c_ftcx.paga2r
	Replace  u_caixa.paga3r		With c_ftcx.paga3r
	Replace  u_caixa.paga4r		With c_ftcx.paga4r
	Replace  u_caixa.paga5r		With c_ftcx.paga5r
	Replace  u_caixa.paga6r		With c_ftcx.paga6r
	*Replace  u_caixa.paga2		With c_ftcx.paga2r
	*Replace  u_caixa.paga3		With c_ftcx.paga3r
	*Replace  u_caixa.paga4		With c_ftcx.paga4r
	*Replace  u_caixa.paga5		With c_ftcx.paga5r
	*Replace  u_caixa.paga6		With c_ftcx.paga6r

Endif
fecha("c_ftcx")

** sangria
TEXT TO msel  NOSHOW TEXTMERGE
			select  isnull(sum(valor),0) valor from u_sangria (nolock) where cxstamp = '<<u_caixa.u_caixastamp>>'
ENDTEXT

If  Not  u_sqlexec(msel,"c_sang")
	msg(msel)
	Return .F.
Endif
Select  c_sang
If c_sang.valor <> 0
	Select u_caixa
	Replace  u_caixa.sangria With c_sang.valor
Endif
fecha("c_sang")

Select u_caixa
Replace u_caixa.paga1com With u_caixa.paga1-u_caixa.paga1liq
Replace u_caixa.paga2com With u_caixa.paga2-u_caixa.paga2liq
Replace u_caixa.comtot With  (u_caixa.paga1 -  u_caixa.paga1liq) +(u_caixa.paga2 -  u_caixa.paga2liq)
Replace u_caixa.faltas With  0
Replace u_caixa.sobras With  0

If  ( u_caixa.totnum+ u_caixa.cheques + u_caixa.paga1 + u_caixa.paga2 + u_caixa.paga3 + u_caixa.paga4 + u_caixa.paga5 + u_caixa.paga6+u_caixa.sangria)-(  u_caixa.maneio+u_caixa.totreal+u_caixa.cheqreal+u_caixa.paga1r+u_caixa.paga2r+u_caixa.paga3r+u_caixa.paga4r+u_caixa.paga5r+u_caixa.paga6r)> 0
	Select u_caixa
	Replace u_caixa.sobras With ( u_caixa.totnum+ u_caixa.cheques + u_caixa.paga1 + u_caixa.paga2 + u_caixa.paga3 + u_caixa.paga4 + u_caixa.paga5 + u_caixa.paga6+u_caixa.sangria)-( u_caixa.maneio+u_caixa.totreal+u_caixa.cheqreal+u_caixa.paga1r+u_caixa.paga2r+u_caixa.paga3r+u_caixa.paga4r+u_caixa.paga5r+u_caixa.paga6r)
Else
	Select u_caixa
	Replace u_caixa.faltas With Abs(( u_caixa.totnum+ u_caixa.cheques + u_caixa.paga1 + u_caixa.paga2 + u_caixa.paga3 + u_caixa.paga4 + u_caixa.paga5 + u_caixa.paga6+ u_caixa.sangria)-(u_caixa.maneio+u_caixa.totreal+u_caixa.cheqreal+u_caixa.paga1r+u_caixa.paga2r+u_caixa.paga3r+u_caixa.paga4r+u_caixa.paga5r+u_caixa.paga6r))
Endif
If u_caixa.faltas <> 0 And  Empty(u_caixa.just1)
	su_caixa.Hide()
	cjust = ''
	Do While Empty(cjust)
		cjust = getnome("Preencha a justifica��o para a  falta de caixa." ,"", "Falta de caixa: " + Ltrim(Str(u_caixa.faltas,15,2)))
	Enddo
	Select u_caixa
	Replace u_caixa.just1  With  cjust
	su_caixa.Show()
Endif

Select u_caixa
Replace u_caixa.paga1d With u_caixa.paga1-u_caixa.paga1r
Replace u_caixa.paga2d With u_caixa.paga2-u_caixa.paga2r
Replace u_caixa.paga3d With u_caixa.paga3-u_caixa.paga3r
Replace u_caixa.chequesd With u_caixa.cheques-u_caixa.cheqreal
Replace u_caixa.totreald With u_caixa.totnum-u_caixa.totreal


** valida se o fecho de valores foi efetuado
If u_caixa.fvalores = .F.
	msg("O fecho de valores n�o foi efetuado.")
	Return .F.
Endif

select  u_caixa  
replace u_caixa.loja  with  p_estab

su_caixa.Refresh
Return  .T.
