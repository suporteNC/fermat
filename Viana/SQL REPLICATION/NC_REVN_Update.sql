USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbore]    Script Date: 15/05/2023 10:07:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_REVN_Update]
		@c1 char(25),
		@c2 varchar(20),
		@c3 numeric(10,0),
		@c4 datetime,
		@c5 char(55),
		@c6 numeric(18,5),
		@c7 numeric(19,6),
		@c8 numeric(3,0),
		@c9 numeric(10,0),
		@c10 varchar(55),
		@c11 varchar(43),
		@c12 varchar(45),
		@c13 varchar(20),
		@c14 numeric(4,0),
		@c15 varchar(12),
		@c16 varchar(1),
		@c17 numeric(15,2),
		@c18 varchar(11),
		@c19 varchar(100),
		@c20 varchar(50),
		@c21 varchar(20),
		@c22 varchar(20),
		@c23 varchar(20),
		@c24 numeric(4,0),
		@c25 bit,
		@c26 varchar(22),
		@c27 varchar(28),
		@c28 numeric(6,2),
		@c29 numeric(18,5),
		@c30 numeric(15,3),
		@c31 bit,
		@c32 varchar(60),
		@c33 varchar(20),
		@c34 varchar(20),
		@c35 datetime,
		@c36 datetime,
		@c37 varchar(20),
		@c38 varchar(50),
		@c39 varchar(33),
		@c40 bit,
		@c41 numeric(4,0),
		@c42 varchar(20),
		@c43 char(25),
		@c44 char(25),
		@c45 varchar(25),
		@c46 char(25),
		@c47 numeric(19,6),
		@c48 numeric(19,6),
		@c49 numeric(18,5),
		@c50 varchar(20),
		@c51 numeric(1,0),
		@c52 numeric(3,0),
		@c53 numeric(18,5),
		@c54 numeric(19,6),
		@c55 numeric(18,5),
		@c56 numeric(19,6),
		@c57 numeric(18,5),
		@c58 numeric(19,6),
		@c59 numeric(18,5),
		@c60 numeric(19,6),
		@c61 numeric(18,5),
		@c62 numeric(19,6),
		@c63 numeric(18,5),
		@c64 numeric(19,6),
		@c65 numeric(18,5),
		@c66 numeric(19,6),
		@c67 numeric(18,5),
		@c68 numeric(19,6),
		@c69 numeric(18,5),
		@c70 numeric(19,6),
		@c71 numeric(19,6),
		@c72 varchar(4),
		@c73 numeric(18,5),
		@c74 varchar(20),
		@c75 varchar(20),
		@c76 bit,
		@c77 bit,
		@c78 bit,
		@c79 bit,
		@c80 bit,
		@c81 datetime,
		@c82 varchar(10),
		@c83 varchar(55),
		@c84 bit,
		@c85 datetime,
		@c86 varchar(40),
		@c87 numeric(10,0),
		@c88 numeric(2,0),
		@c89 bit,
		@c90 char(25),
		@c91 numeric(18,5),
		@c92 numeric(19,6),
		@c93 varchar(50),
		@c94 varchar(1),
		@c95 numeric(4,0),
		@c96 numeric(18,5),
		@c97 numeric(19,6),
		@c98 varchar(11),
		@c99 numeric(15,2),
		@c100 varchar(20),
		@c101 varchar(20),
		@c102 numeric(3,0),
		@c103 char(25),
		@c104 varchar(30),
		@c105 char(25),
		@c106 varchar(30),
		@c107 numeric(18,5),
		@c108 numeric(19,6),
		@c109 numeric(19,6),
		@c110 varchar(15),
		@c111 numeric(19,6),
		@c112 numeric(18,5),
		@c113 numeric(15,3),
		@c114 numeric(19,6),
		@c115 numeric(18,5),
		@c116 numeric(15,3),
		@c117 varchar(15),
		@c118 numeric(19,6),
		@c119 numeric(18,5),
		@c120 numeric(15,3),
		@c121 numeric(19,6),
		@c122 numeric(18,5),
		@c123 numeric(15,3),
		@c124 varchar(15),
		@c125 numeric(19,6),
		@c126 numeric(18,5),
		@c127 numeric(15,3),
		@c128 numeric(19,6),
		@c129 numeric(18,5),
		@c130 numeric(15,3),
		@c131 varchar(15),
		@c132 numeric(19,6),
		@c133 numeric(18,5),
		@c134 numeric(15,3),
		@c135 numeric(19,6),
		@c136 numeric(18,5),
		@c137 numeric(15,3),
		@c138 varchar(15),
		@c139 numeric(19,6),
		@c140 numeric(18,5),
		@c141 numeric(15,3),
		@c142 numeric(19,6),
		@c143 numeric(18,5),
		@c144 numeric(15,3),
		@c145 varchar(15),
		@c146 numeric(19,6),
		@c147 numeric(18,5),
		@c148 numeric(15,3),
		@c149 numeric(19,6),
		@c150 numeric(18,5),
		@c151 numeric(15,3),
		@c152 numeric(18,5),
		@c153 numeric(19,6),
		@c154 numeric(19,6),
		@c155 bit,
		@c156 numeric(18,5),
		@c157 numeric(19,6),
		@c158 varchar(25),
		@c159 numeric(18,5),
		@c160 numeric(19,6),
		@c161 varchar(11),
		@c162 numeric(15,2),
		@c163 numeric(15,2),
		@c164 numeric(15,2),
		@c165 numeric(15,2),
		@c166 varchar(11),
		@c167 numeric(15,2),
		@c168 varchar(25),
		@c169 varchar(25),
		@c170 char(25),
		@c171 char(25),
		@c172 varchar(30),
		@c173 datetime,
		@c174 varchar(8),
		@c175 varchar(30),
		@c176 datetime,
		@c177 varchar(8),
		@c178 bit,
		@c179 numeric(18,5),
		@c180 numeric(19,6),
		@c181 numeric(10,0),
		@c182 bit,
		@c183 varchar(20),
		@c184 varchar(40),
		@c185 bit,
		@c186 numeric(18,5),
		@c187 numeric(19,6),
		@c188 varchar(35),
		@c189 varchar(35),
		@c190 bit,
		@c191 numeric(13,0),
		@c192 varchar(50),
		@c193 bit,
		@c194 varchar(60),
		@c195 varchar(1),
		@c196 bit,
		@c197 datetime,
		@c198 numeric(5,0),
		@c199 bit,
		@c200 varchar(30),
		@c201 datetime,
		@c202 varchar(8),
		@c203 numeric(19,6),
		@c204 numeric(19,6),
		@c205 numeric(18,5),
		@c206 numeric(19,6),
		@c207 numeric(10,0),
		@c208 datetime,
		@c209 varchar(100),
		@c210 numeric(1,0),
		@c211 varchar(254),
		@c212 char(25),
		@c213 varchar(20),
		@c214 numeric(10,0),
		@c215 datetime,
		@c216 char(55),
		@c217 numeric(18,5),
		@c218 numeric(19,6),
		@c219 numeric(3,0),
		@c220 numeric(10,0),
		@c221 varchar(55),
		@c222 varchar(43),
		@c223 varchar(45),
		@c224 varchar(20),
		@c225 numeric(4,0),
		@c226 varchar(12),
		@c227 varchar(1),
		@c228 numeric(15,2),
		@c229 varchar(11),
		@c230 varchar(100),
		@c231 varchar(50),
		@c232 varchar(20),
		@c233 varchar(20),
		@c234 varchar(20),
		@c235 numeric(4,0),
		@c236 bit,
		@c237 varchar(22),
		@c238 varchar(28),
		@c239 numeric(6,2),
		@c240 numeric(18,5),
		@c241 numeric(15,3),
		@c242 bit,
		@c243 varchar(60),
		@c244 varchar(20),
		@c245 varchar(20),
		@c246 datetime,
		@c247 datetime,
		@c248 varchar(20),
		@c249 varchar(50),
		@c250 varchar(33),
		@c251 bit,
		@c252 numeric(4,0),
		@c253 varchar(20),
		@c254 char(25),
		@c255 char(25),
		@c256 varchar(25),
		@c257 char(25),
		@c258 numeric(19,6),
		@c259 numeric(19,6),
		@c260 numeric(18,5),
		@c261 varchar(20),
		@c262 numeric(1,0),
		@c263 numeric(3,0),
		@c264 numeric(18,5),
		@c265 numeric(19,6),
		@c266 numeric(18,5),
		@c267 numeric(19,6),
		@c268 numeric(18,5),
		@c269 numeric(19,6),
		@c270 numeric(18,5),
		@c271 numeric(19,6),
		@c272 numeric(18,5),
		@c273 numeric(19,6),
		@c274 numeric(18,5),
		@c275 numeric(19,6),
		@c276 numeric(18,5),
		@c277 numeric(19,6),
		@c278 numeric(18,5),
		@c279 numeric(19,6),
		@c280 numeric(18,5),
		@c281 numeric(19,6),
		@c282 numeric(19,6),
		@c283 varchar(4),
		@c284 numeric(18,5),
		@c285 varchar(20),
		@c286 varchar(20),
		@c287 bit,
		@c288 bit,
		@c289 bit,
		@c290 bit,
		@c291 bit,
		@c292 datetime,
		@c293 varchar(10),
		@c294 varchar(55),
		@c295 bit,
		@c296 datetime,
		@c297 varchar(40),
		@c298 numeric(10,0),
		@c299 numeric(2,0),
		@c300 bit,
		@c301 char(25),
		@c302 numeric(18,5),
		@c303 numeric(19,6),
		@c304 varchar(50),
		@c305 varchar(1),
		@c306 numeric(4,0),
		@c307 numeric(18,5),
		@c308 numeric(19,6),
		@c309 varchar(11),
		@c310 numeric(15,2),
		@c311 varchar(20),
		@c312 varchar(20),
		@c313 numeric(3,0),
		@c314 char(25),
		@c315 varchar(30),
		@c316 char(25),
		@c317 varchar(30),
		@c318 numeric(18,5),
		@c319 numeric(19,6),
		@c320 numeric(19,6),
		@c321 varchar(15),
		@c322 numeric(19,6),
		@c323 numeric(18,5),
		@c324 numeric(15,3),
		@c325 numeric(19,6),
		@c326 numeric(18,5),
		@c327 numeric(15,3),
		@c328 varchar(15),
		@c329 numeric(19,6),
		@c330 numeric(18,5),
		@c331 numeric(15,3),
		@c332 numeric(19,6),
		@c333 numeric(18,5),
		@c334 numeric(15,3),
		@c335 varchar(15),
		@c336 numeric(19,6),
		@c337 numeric(18,5),
		@c338 numeric(15,3),
		@c339 numeric(19,6),
		@c340 numeric(18,5),
		@c341 numeric(15,3),
		@c342 varchar(15),
		@c343 numeric(19,6),
		@c344 numeric(18,5),
		@c345 numeric(15,3),
		@c346 numeric(19,6),
		@c347 numeric(18,5),
		@c348 numeric(15,3),
		@c349 varchar(15),
		@c350 numeric(19,6),
		@c351 numeric(18,5),
		@c352 numeric(15,3),
		@c353 numeric(19,6),
		@c354 numeric(18,5),
		@c355 numeric(15,3),
		@c356 varchar(15),
		@c357 numeric(19,6),
		@c358 numeric(18,5),
		@c359 numeric(15,3),
		@c360 numeric(19,6),
		@c361 numeric(18,5),
		@c362 numeric(15,3),
		@c363 numeric(18,5),
		@c364 numeric(19,6),
		@c365 numeric(19,6),
		@c366 bit,
		@c367 numeric(18,5),
		@c368 numeric(19,6),
		@c369 varchar(25),
		@c370 numeric(18,5),
		@c371 numeric(19,6),
		@c372 varchar(11),
		@c373 numeric(15,2),
		@c374 numeric(15,2),
		@c375 numeric(15,2),
		@c376 numeric(15,2),
		@c377 varchar(11),
		@c378 numeric(15,2),
		@c379 varchar(25),
		@c380 varchar(25),
		@c381 char(25),
		@c382 char(25),
		@c383 varchar(30),
		@c384 datetime,
		@c385 varchar(8),
		@c386 varchar(30),
		@c387 datetime,
		@c388 varchar(8),
		@c389 bit,
		@c390 numeric(18,5),
		@c391 numeric(19,6),
		@c392 numeric(10,0),
		@c393 bit,
		@c394 varchar(20),
		@c395 varchar(40),
		@c396 bit,
		@c397 numeric(18,5),
		@c398 numeric(19,6),
		@c399 varchar(35),
		@c400 varchar(35),
		@c401 bit,
		@c402 numeric(13,0),
		@c403 varchar(50),
		@c404 bit,
		@c405 varchar(60),
		@c406 varchar(1),
		@c407 bit,
		@c408 datetime,
		@c409 numeric(5,0),
		@c410 bit,
		@c411 varchar(30),
		@c412 datetime,
		@c413 varchar(8),
		@c414 numeric(19,6),
		@c415 numeric(19,6),
		@c416 numeric(18,5),
		@c417 numeric(19,6),
		@c418 numeric(10,0),
		@c419 datetime,
		@c420 varchar(100),
		@c421 numeric(1,0),
		@c422 varchar(254)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c219 = @c8) or
 not (@c214 = @c3) or
 not (@c225 = @c14)
begin 
update [VN].[re] set
		[restamp] = @c212,
		[nmdoc] = @c213,
		[rno] = @c214,
		[rdata] = @c215,
		[nome] = @c216,
		[total] = @c217,
		[etotal] = @c218,
		[ndoc] = @c219,
		[no] = @c220,
		[morada] = @c221,
		[local] = @c222,
		[codpost] = @c223,
		[ncont] = @c224,
		[reano] = @c225,
		[olcodigo] = @c226,
		[telocal] = @c227,
		[totalmoeda] = @c228,
		[moeda] = @c229,
		[desc1] = @c230,
		[desc2] = @c231,
		[fref] = @c232,
		[ccusto] = @c233,
		[ncusto] = @c234,
		[contado] = @c235,
		[process] = @c236,
		[cobranca] = @c237,
		[nib] = @c238,
		[fin] = @c239,
		[finv] = @c240,
		[finvmoeda] = @c241,
		[impresso] = @c242,
		[userimpresso] = @c243,
		[clbanco] = @c244,
		[clcheque] = @c245,
		[procdata] = @c246,
		[vdata] = @c247,
		[zona] = @c248,
		[ollocal] = @c249,
		[descba] = @c250,
		[plano] = @c251,
		[vendedor] = @c252,
		[vendnm] = @c253,
		[olstamp] = @c254,
		[ccstamp] = @c255,
		[segmento] = @c256,
		[cc2stamp] = @c257,
		[efinv] = @c258,
		[edifcambio] = @c259,
		[difcambio] = @c260,
		[tipo] = @c261,
		[pais] = @c262,
		[estab] = @c263,
		[ivav1] = @c264,
		[eivav1] = @c265,
		[ivav2] = @c266,
		[eivav2] = @c267,
		[ivav3] = @c268,
		[eivav3] = @c269,
		[ivav4] = @c270,
		[eivav4] = @c271,
		[ivav5] = @c272,
		[eivav5] = @c273,
		[ivav6] = @c274,
		[eivav6] = @c275,
		[ivav7] = @c276,
		[eivav7] = @c277,
		[ivav8] = @c278,
		[eivav8] = @c279,
		[ivav9] = @c280,
		[eivav9] = @c281,
		[earred] = @c282,
		[memissao] = @c283,
		[arred] = @c284,
		[cobrador] = @c285,
		[rota] = @c286,
		[introfin] = @c287,
		[tbok] = @c288,
		[faztrf] = @c289,
		[procomss] = @c290,
		[cheque] = @c291,
		[chdata] = @c292,
		[intid] = @c293,
		[nome2] = @c294,
		[regiva] = @c295,
		[dplano] = @c296,
		[dinoplano] = @c297,
		[dilnoplano] = @c298,
		[diaplano] = @c299,
		[planoonline] = @c300,
		[dostamp] = @c301,
		[totol2] = @c302,
		[etotol2] = @c303,
		[ollocal2] = @c304,
		[telocal2] = @c305,
		[contado2] = @c306,
		[chtotal] = @c307,
		[echtotal] = @c308,
		[chmoeda] = @c309,
		[chtotalm] = @c310,
		[site] = @c311,
		[pnome] = @c312,
		[pno] = @c313,
		[cxstamp] = @c314,
		[cxusername] = @c315,
		[ssstamp] = @c316,
		[ssusername] = @c317,
		[vdinheiro] = @c318,
		[evdinheiro] = @c319,
		[mvdinheiro] = @c320,
		[modop1] = @c321,
		[epaga1] = @c322,
		[paga1] = @c323,
		[mpaga1] = @c324,
		[ecompaga1] = @c325,
		[compaga1] = @c326,
		[mcompaga1] = @c327,
		[modop2] = @c328,
		[epaga2] = @c329,
		[paga2] = @c330,
		[mpaga2] = @c331,
		[ecompaga2] = @c332,
		[compaga2] = @c333,
		[mcompaga2] = @c334,
		[modop3] = @c335,
		[epaga3] = @c336,
		[paga3] = @c337,
		[mpaga3] = @c338,
		[ecompaga3] = @c339,
		[compaga3] = @c340,
		[mcompaga3] = @c341,
		[modop4] = @c342,
		[epaga4] = @c343,
		[paga4] = @c344,
		[mpaga4] = @c345,
		[ecompaga4] = @c346,
		[compaga4] = @c347,
		[mcompaga4] = @c348,
		[modop5] = @c349,
		[epaga5] = @c350,
		[paga5] = @c351,
		[mpaga5] = @c352,
		[ecompaga5] = @c353,
		[compaga5] = @c354,
		[mcompaga5] = @c355,
		[modop6] = @c356,
		[epaga6] = @c357,
		[paga6] = @c358,
		[mpaga6] = @c359,
		[ecompaga6] = @c360,
		[compaga6] = @c361,
		[mcompaga6] = @c362,
		[acerto] = @c363,
		[eacerto] = @c364,
		[macerto] = @c365,
		[exportado] = @c366,
		[totow] = @c367,
		[etotow] = @c368,
		[tptit] = @c369,
		[virs] = @c370,
		[evirs] = @c371,
		[moeda2] = @c372,
		[valorowm2] = @c373,
		[valorm2] = @c374,
		[valor2m2] = @c375,
		[earredm2] = @c376,
		[moeda3] = @c377,
		[valor2m3] = @c378,
		[contrato] = @c379,
		[cessao] = @c380,
		[facstamp] = @c381,
		[faccstamp] = @c382,
		[ousrinis] = @c383,
		[ousrdata] = @c384,
		[ousrhora] = @c385,
		[usrinis] = @c386,
		[usrdata] = @c387,
		[usrhora] = @c388,
		[marcada] = @c389,
		[totoladi] = @c390,
		[etotoladi] = @c391,
		[cbbno] = @c392,
		[luserfin] = @c393,
		[bic] = @c394,
		[iban] = @c395,
		[processsepa] = @c396,
		[totalimp] = @c397,
		[etotalimp] = @c398,
		[sepagh] = @c399,
		[sepapi] = @c400,
		[operext] = @c401,
		[npedido] = @c402,
		[tiporeg] = @c403,
		[temch] = @c404,
		[paymentrefnoori] = @c405,
		[upddespacho] = @c406,
		[revogado] = @c407,
		[revdata] = @c408,
		[u_estab] = @c409,
		[anulado] = @c410,
		[anulinis] = @c411,
		[anuldata] = @c412,
		[anulhora] = @c413,
		[esirca] = @c414,
		[ecativa] = @c415,
		[cativa] = @c416,
		[mcativa] = @c417,
		[noimps] = @c418,
		[u_ttssinc] = @c419,
		[atcud] = @c420,
		[contingencia] = @c421,
		[nomeat] = @c422
	where [ndoc] = @c8
  and [rno] = @c3
  and [reano] = @c14
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ndoc] = ' + convert(nvarchar(100),@c8,1) + ', '
			set @primarykey_text = @primarykey_text + '[rno] = ' + convert(nvarchar(100),@c3,1) + ', '
			set @primarykey_text = @primarykey_text + '[reano] = ' + convert(nvarchar(100),@c14,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[re]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [VN].[re] set
		[restamp] = @c212,
		[nmdoc] = @c213,
		[rdata] = @c215,
		[nome] = @c216,
		[total] = @c217,
		[etotal] = @c218,
		[no] = @c220,
		[morada] = @c221,
		[local] = @c222,
		[codpost] = @c223,
		[ncont] = @c224,
		[olcodigo] = @c226,
		[telocal] = @c227,
		[totalmoeda] = @c228,
		[moeda] = @c229,
		[desc1] = @c230,
		[desc2] = @c231,
		[fref] = @c232,
		[ccusto] = @c233,
		[ncusto] = @c234,
		[contado] = @c235,
		[process] = @c236,
		[cobranca] = @c237,
		[nib] = @c238,
		[fin] = @c239,
		[finv] = @c240,
		[finvmoeda] = @c241,
		[impresso] = @c242,
		[userimpresso] = @c243,
		[clbanco] = @c244,
		[clcheque] = @c245,
		[procdata] = @c246,
		[vdata] = @c247,
		[zona] = @c248,
		[ollocal] = @c249,
		[descba] = @c250,
		[plano] = @c251,
		[vendedor] = @c252,
		[vendnm] = @c253,
		[olstamp] = @c254,
		[ccstamp] = @c255,
		[segmento] = @c256,
		[cc2stamp] = @c257,
		[efinv] = @c258,
		[edifcambio] = @c259,
		[difcambio] = @c260,
		[tipo] = @c261,
		[pais] = @c262,
		[estab] = @c263,
		[ivav1] = @c264,
		[eivav1] = @c265,
		[ivav2] = @c266,
		[eivav2] = @c267,
		[ivav3] = @c268,
		[eivav3] = @c269,
		[ivav4] = @c270,
		[eivav4] = @c271,
		[ivav5] = @c272,
		[eivav5] = @c273,
		[ivav6] = @c274,
		[eivav6] = @c275,
		[ivav7] = @c276,
		[eivav7] = @c277,
		[ivav8] = @c278,
		[eivav8] = @c279,
		[ivav9] = @c280,
		[eivav9] = @c281,
		[earred] = @c282,
		[memissao] = @c283,
		[arred] = @c284,
		[cobrador] = @c285,
		[rota] = @c286,
		[introfin] = @c287,
		[tbok] = @c288,
		[faztrf] = @c289,
		[procomss] = @c290,
		[cheque] = @c291,
		[chdata] = @c292,
		[intid] = @c293,
		[nome2] = @c294,
		[regiva] = @c295,
		[dplano] = @c296,
		[dinoplano] = @c297,
		[dilnoplano] = @c298,
		[diaplano] = @c299,
		[planoonline] = @c300,
		[dostamp] = @c301,
		[totol2] = @c302,
		[etotol2] = @c303,
		[ollocal2] = @c304,
		[telocal2] = @c305,
		[contado2] = @c306,
		[chtotal] = @c307,
		[echtotal] = @c308,
		[chmoeda] = @c309,
		[chtotalm] = @c310,
		[site] = @c311,
		[pnome] = @c312,
		[pno] = @c313,
		[cxstamp] = @c314,
		[cxusername] = @c315,
		[ssstamp] = @c316,
		[ssusername] = @c317,
		[vdinheiro] = @c318,
		[evdinheiro] = @c319,
		[mvdinheiro] = @c320,
		[modop1] = @c321,
		[epaga1] = @c322,
		[paga1] = @c323,
		[mpaga1] = @c324,
		[ecompaga1] = @c325,
		[compaga1] = @c326,
		[mcompaga1] = @c327,
		[modop2] = @c328,
		[epaga2] = @c329,
		[paga2] = @c330,
		[mpaga2] = @c331,
		[ecompaga2] = @c332,
		[compaga2] = @c333,
		[mcompaga2] = @c334,
		[modop3] = @c335,
		[epaga3] = @c336,
		[paga3] = @c337,
		[mpaga3] = @c338,
		[ecompaga3] = @c339,
		[compaga3] = @c340,
		[mcompaga3] = @c341,
		[modop4] = @c342,
		[epaga4] = @c343,
		[paga4] = @c344,
		[mpaga4] = @c345,
		[ecompaga4] = @c346,
		[compaga4] = @c347,
		[mcompaga4] = @c348,
		[modop5] = @c349,
		[epaga5] = @c350,
		[paga5] = @c351,
		[mpaga5] = @c352,
		[ecompaga5] = @c353,
		[compaga5] = @c354,
		[mcompaga5] = @c355,
		[modop6] = @c356,
		[epaga6] = @c357,
		[paga6] = @c358,
		[mpaga6] = @c359,
		[ecompaga6] = @c360,
		[compaga6] = @c361,
		[mcompaga6] = @c362,
		[acerto] = @c363,
		[eacerto] = @c364,
		[macerto] = @c365,
		[exportado] = @c366,
		[totow] = @c367,
		[etotow] = @c368,
		[tptit] = @c369,
		[virs] = @c370,
		[evirs] = @c371,
		[moeda2] = @c372,
		[valorowm2] = @c373,
		[valorm2] = @c374,
		[valor2m2] = @c375,
		[earredm2] = @c376,
		[moeda3] = @c377,
		[valor2m3] = @c378,
		[contrato] = @c379,
		[cessao] = @c380,
		[facstamp] = @c381,
		[faccstamp] = @c382,
		[ousrinis] = @c383,
		[ousrdata] = @c384,
		[ousrhora] = @c385,
		[usrinis] = @c386,
		[usrdata] = @c387,
		[usrhora] = @c388,
		[marcada] = @c389,
		[totoladi] = @c390,
		[etotoladi] = @c391,
		[cbbno] = @c392,
		[luserfin] = @c393,
		[bic] = @c394,
		[iban] = @c395,
		[processsepa] = @c396,
		[totalimp] = @c397,
		[etotalimp] = @c398,
		[sepagh] = @c399,
		[sepapi] = @c400,
		[operext] = @c401,
		[npedido] = @c402,
		[tiporeg] = @c403,
		[temch] = @c404,
		[paymentrefnoori] = @c405,
		[upddespacho] = @c406,
		[revogado] = @c407,
		[revdata] = @c408,
		[u_estab] = @c409,
		[anulado] = @c410,
		[anulinis] = @c411,
		[anuldata] = @c412,
		[anulhora] = @c413,
		[esirca] = @c414,
		[ecativa] = @c415,
		[cativa] = @c416,
		[mcativa] = @c417,
		[noimps] = @c418,
		[u_ttssinc] = @c419,
		[atcud] = @c420,
		[contingencia] = @c421,
		[nomeat] = @c422
	where [ndoc] = @c8
  and [rno] = @c3
  and [reano] = @c14
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ndoc] = ' + convert(nvarchar(100),@c8,1) + ', '
			set @primarykey_text = @primarykey_text + '[rno] = ' + convert(nvarchar(100),@c3,1) + ', '
			set @primarykey_text = @primarykey_text + '[reano] = ' + convert(nvarchar(100),@c14,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[re]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','VN','re',@c1, 'restamp', 'reid'