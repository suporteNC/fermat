USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbofcx]    Script Date: 27/01/2023 13:12:59 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_FCXPAL_Delete]
		@c1 char(25),
		@c2 varchar(20),
		@c3 varchar(20),
		@c4 numeric(3,0),
		@c5 char(25),
		@c6 varchar(30),
		@c7 varchar(1),
		@c8 numeric(18,5),
		@c9 numeric(19,6),
		@c10 numeric(18,3),
		@c11 varchar(15),
		@c12 numeric(18,5),
		@c13 numeric(19,6),
		@c14 numeric(15,3),
		@c15 varchar(15),
		@c16 numeric(18,5),
		@c17 numeric(19,6),
		@c18 numeric(15,3),
		@c19 varchar(15),
		@c20 numeric(18,5),
		@c21 numeric(19,6),
		@c22 numeric(15,3),
		@c23 varchar(15),
		@c24 numeric(18,5),
		@c25 numeric(19,6),
		@c26 numeric(15,3),
		@c27 varchar(15),
		@c28 numeric(18,5),
		@c29 numeric(19,6),
		@c30 numeric(15,3),
		@c31 varchar(15),
		@c32 numeric(18,5),
		@c33 numeric(19,6),
		@c34 numeric(15,3),
		@c35 numeric(18,5),
		@c36 numeric(19,6),
		@c37 numeric(18,3),
		@c38 varchar(11),
		@c39 numeric(18,5),
		@c40 numeric(19,6),
		@c41 numeric(15,3),
		@c42 numeric(10,0),
		@c43 varchar(30),
		@c44 datetime,
		@c45 varchar(8),
		@c46 varchar(30),
		@c47 datetime,
		@c48 varchar(8),
		@c49 bit,
		@c50 varchar(50)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PL].[fcx] 
	where [fcxstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fcxstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PL].[fcx]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from fcx where fcxstamp=@c1