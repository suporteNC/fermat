USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_sttpart]    Script Date: 24/01/2023 18:00:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_STTPART_Delete]
		@c1 char(25),
		@c2 varchar(10),
		@c3 varchar(30),
		@c4 varchar(30),
		@c5 datetime,
		@c6 varchar(8),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 bit,
		@c11 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[u_sttpart] 
	where [u_sttpartstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_sttpartstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_sttpart]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_sttpart where u_sttpartstamp=@c1