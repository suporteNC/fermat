select u_sc
** obriga a preencher os contactos 
if empty(u_sc.contactos)
	txtoriaux = 'Desculpe, tem que preencher o campo [contactos] no servi�o cliente'		
	mensagem(txtoriaux,"directa")	
	su_sc.pageframe1.page1.contactos.setfocus()
	return .f.
endif
** n�o pode alterar sc de cliente fechados 
if  u_sc.fechado = 'S'
	txtoriaux = 'Desculpe, mas n�o alterar servi�o clientes j� fechados.'
	mensagem(txtoriaux,"directa")
	return .f.
endif

*actualiza o fechado do cabe�alho
lFechado = .t. 
select u_scl
scan
	if  empty(u_scl.estado) 
		msg("O estado da linha com a refer�ncia " + rtrim(u_scl.ref) + " est� vazio ")
		return .f. 
	endif  
	if u_scl.estado <> 'FECHADO'
		lFechado = .f. 
	endif
	if empty(u_scl.datap) or dtos(u_scl.datap) = '19000101' 
		msg("A linha com a refer�ncia " + rtrim(u_scl.ref) + " n�o tem a data prevista preenchida")
		return .f. 
	endif 
endscan
if reccount("u_scl")=0
	lFechado = .f. 
	select u_sc
	replace u_sc.concluido with  .f.
endif 
if  lFechado = .t. 
	select u_sc
	replace u_sc.concluido with  lFechado 
else  
	if u_sc.concluido = .t. and  lFechado  = .f.
		m.lPergunta = dpergunta(2,2,"","Existem linhas em aberto","","Fechar servi�o cliente","Deixar aberto o servi�o de cliente")
		select u_sc
		if m.lPergunta = 1 
			replace u_sc.concluido with  .t.
		else  
			replace u_sc.concluido with  .f.
			
		endif 			
	endif   
endif 
if u_sc.concluido  = .t.
	replace u_sc.dtfecho with date() in u_sc
	replace u_sc.tempo with astr(date()- u_sc.data) in u_sc
else 
	replace u_sc.tempo with astr(0) in u_sc
endif 

* introduz o n� do servi�o de cliente (deve ser a ultima regra)
if su_sc.adding = .t. 
	msel = "select isnull(max(numero), convert(numeric(10), '" + astr(p_estab) +"' + '000000' ) ) + 1  as numero from u_sc(nolock) where ano = '"+astr(year(u_sc.data))+"'"
	if not u_sqlexec(msel,"c_u_sc") 
		msg(msel)
	endif
	replace u_sc.numero with c_u_sc.numero
	fecha("c_u_sc")
endif