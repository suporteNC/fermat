****************************************
*** Preenche n� do fornecedor
***
*** Criado por Rui Vale
***  Criado em 28/09/2017
****************************************

If fl.estab=0
	TEXT TO uSql TEXTMERGE noshow
		Select isnull(max(fl.no)+1,isnull((select e1.u_noflini from e1 (nolock) where estab=0 and e1.u_noflini<>0),1)) as no
		From fl (nolock)
		Where fl.no between isnull((select e1.u_noflini from e1 (nolock) where estab=0 and e1.u_noflini<>0),0)
			and isnull((select e1.u_noflfim from e1 (nolock) where estab=0 and e1.u_noflfim <>0),9999999999)
	ENDTEXT

	If u_sqlexec(uSql,"uCurNumFl") And Reccount("uCurNumFl")>0
		Select uCurNumFl
		Return uCurNumFl.no
	Else
		Return 1
	Endif
Else
	Return fl.no
Endif