USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbobi2]    Script Date: 23/02/2023 19:17:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_BI2LOJAS_Delete]
		@c1 char(25),
		@c2 char(25),
		@c3 numeric(14,4),
		@c4 numeric(14,4),
		@c5 numeric(14,4),
		@c6 numeric(14,4),
		@c7 numeric(14,4),
		@c8 numeric(14,4),
		@c9 numeric(14,4),
		@c10 numeric(14,4),
		@c11 numeric(14,4),
		@c12 numeric(14,4),
		@c13 numeric(14,4),
		@c14 numeric(14,4),
		@c15 numeric(14,4),
		@c16 numeric(18,5),
		@c17 numeric(19,6),
		@c18 numeric(15,2),
		@c19 numeric(18,5),
		@c20 numeric(19,6),
		@c21 numeric(18,5),
		@c22 numeric(19,6),
		@c23 numeric(18,5),
		@c24 numeric(19,6),
		@c25 numeric(18,5),
		@c26 numeric(19,6),
		@c27 numeric(18,5),
		@c28 numeric(19,6),
		@c29 numeric(18,5),
		@c30 numeric(19,6),
		@c31 numeric(15,2),
		@c32 varchar(40),
		@c33 bit,
		@c34 char(60),
		@c35 char(25),
		@c36 varchar(25),
		@c37 varchar(55),
		@c38 varchar(25),
		@c39 varchar(30),
		@c40 varchar(25),
		@c41 varchar(55),
		@c42 varchar(25),
		@c43 varchar(30),
		@c44 numeric(14,4),
		@c45 numeric(14,4),
		@c46 varchar(20),
		@c47 numeric(10,0),
		@c48 numeric(4,0),
		@c49 varchar(24),
		@c50 char(25),
		@c51 bit,
		@c52 varchar(25),
		@c53 varchar(20),
		@c54 varchar(20),
		@c55 bit,
		@c56 char(25),
		@c57 bit,
		@c58 varchar(50),
		@c59 varchar(55),
		@c60 varchar(43),
		@c61 varchar(45),
		@c62 varchar(20),
		@c63 varchar(60),
		@c64 varchar(50),
		@c65 varchar(100),
		@c66 varchar(25),
		@c67 numeric(10,0),
		@c68 numeric(2,0),
		@c69 varchar(25),
		@c70 numeric(19,6),
		@c71 numeric(19,6),
		@c72 numeric(18,5),
		@c73 numeric(18,5),
		@c74 varchar(2),
		@c75 varchar(70),
		@c76 numeric(6,2),
		@c77 bit,
		@c78 numeric(14,4),
		@c79 char(25),
		@c80 varchar(30),
		@c81 datetime,
		@c82 varchar(8),
		@c83 varchar(30),
		@c84 datetime,
		@c85 varchar(8),
		@c86 bit,
		@c87 numeric(5,0),
		@c88 varchar(50),
		@c89 numeric(10,0),
		@c90 bit,
		@c91 numeric(5,0),
		@c92 datetime,
		@c93 bit,
		@c94 varchar(15)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[bi2] 
	where [bi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[bi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[bi2]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from bi2 where bi2stamp=@c1