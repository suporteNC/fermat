USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[NC_STPG_Insert]    Script Date: 01/02/2023 17:42:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_STPG_Insert]
     @c1 char(25),
    @c2 char(18),
    @c3 char(60),
    @c4 varchar(18),
    @c5 char(20),
    @c6 varchar(55),
    @c7 varchar(4),
    @c8 numeric(10,0),
    @c9 numeric(3,0),
    @c10 varchar(68),
    @c11 char(40),
    @c12 varchar(4),
    @c13 numeric(15,7),
    @c14 varchar(120),
    @c15 numeric(6,0),
    @c16 varchar(15),
    @c17 varchar(15),
    @c18 numeric(6,2),
    @c19 numeric(6,2),
    @c20 numeric(3,0),
    @c21 numeric(5,2),
    @c22 bit,
    @c23 bit,
    @c24 datetime,
    @c25 varchar(60),
    @c26 bit,
    @c27 varchar(60),
    @c28 varchar(100),
    @c29 numeric(1,0),
    @c30 varchar(20),
    @c31 char(20),
    @c32 char(60),
    @c33 char(20),
    @c34 char(60),
    @c35 char(20),
    @c36 char(60),
    @c37 char(20),
    @c38 char(60),
    @c39 char(20),
    @c40 char(60),
    @c41 bit,
    @c42 varchar(1),
    @c43 bit,
    @c44 varchar(30),
    @c45 datetime,
    @c46 varchar(8),
    @c47 varchar(30),
    @c48 datetime,
    @c49 varchar(8),
    @c50 bit,
    @c51 bit,
    @c52 char(18),
    @c53 char(60),
    @c54 numeric(6,2),
    @c55 numeric(6,2),
    @c56 char(25),
    @c57 varchar(4),
    @c58 varchar(3),
    @c59 bit,
    @c60 varchar(25),
    @c61 bit,
    @c62 bit,
    @c63 bit,
    @c64 bit,
    @c65 bit,
    @c66 varchar(10),
    @c67 varchar(30),
    @c68 varchar(10),
    @c69 varchar(30),
    @c70 varchar(60),
    @c71 bit,
    @c72 varchar(20),
    @c73 varchar(20),
    @c74 varchar(20),
    @c75 varchar(20),
    @c76 varchar(20),
    @c77 varchar(60),
    @c78 varchar(20),
    @c79 varchar(20),
    @c80 varchar(15),
    @c81 varchar(55),
    @c82 numeric(10,0),
    @c83 varchar(20),
    @c84 datetime,
    @c85 varchar(20),
    @c86 datetime,
    @c87 varchar(20),
    @c88 varchar(20),
    @c89 datetime,
    @c90 datetime,
    @c91 bit,
    @c92 bit,
    @c93 bit,
    @c94 numeric(10,2),
    @c95 numeric(10,2),
    @c96 bit,
    @c97 datetime,
    @c98 datetime,
    @c99 numeric(10,2)
as
begin  
-- Set u_mondego a true
set @c51=1
if @c61=1
begin
	set @c51=0
end
set @c15='1'
set @c16='321'
set @c17='611'
	insert into [PG].[st] (
		[ststamp],
		[ref],
		[design],
		[familia],
		[forref],
		[fornecedor],
		[unidade],
		[fornec],
		[fornestab],
		[obs],
		[codigo],
		[uni2],
		[conversao],
		[imagem],
		[cpoc],
		[containv],
		[contacev],
		[mfornec],
		[mfornec2],
		[pentrega],
		[despimp],
		[usalote],
		[texteis],
		[opendata],
		[faminome],
		[stns],
		[tipodesc],
		[url],
		[vaiwww],
		[codfiscal],
		[lang1],
		[langdes1],
		[lang2],
		[langdes2],
		[lang3],
		[langdes3],
		[lang4],
		[langdes4],
		[lang5],
		[langdes5],
		[nexist],
		[statuspda],
		[compnovo],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[inactivo],
		[refmo],
		[descrmo],
		[desci],
		[descii],
		[ofcstamp],
		[unidadef],
		[tkhclass],
		[amostra],
		[u_cpautal],
		[u_mondego],
		[u_fermat],
		[u_impor],
		[u_frete],
		[u_seguro],
		[u_csubfam],
		[u_subfam],
		[u_ctpart],
		[u_tpart],
		[u_dcpautal],
		[u_kit],
		[u_local2],
		[u_local3],
		[u_local4],
		[u_local5],
		[u_local6],
		[u_design],
		[u_circuito],
		[u_natureza],
		[u_pais],
		[u_nfornec1],
		[u_fornec1],
		[u_rfornec1],
		[u_dtblo],
		[u_userblo],
		[u_dtfoblo],
		[u_userfobl],
		[u_userina],
		[u_dtinact],
		[u_ttssinc],
		[u_lojaweb],
		[u_ulojaweb],
		[u_lwstock],
		[u_prljweb],
		[u_prcrweb],
		[u_destaque],
		[u_didestq],
		[u_dfdestq],
		[u_vimpr]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57,
		@c58,
		@c59,
		@c60,
		@c61,
		@c62,
		@c63,
		@c64,
		@c65,
		@c66,
		@c67,
		@c68,
		@c69,
		@c70,
		@c71,
		@c72,
		@c73,
		@c74,
		@c75,
		@c76,
		@c77,
		@c78,
		@c79,
		@c80,
		@c81,
		@c82,
		@c83,
		@c84,
		@c85,
		@c86,
		@c87,
		@c88,
		@c89,
		@c90,
		@c91,
		@c92,
		@c93,
		@c94,
		@c95,
		@c96,
		@c97,
		@c98,
		@c99	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','st',@c1, 'ststamp'

end  
