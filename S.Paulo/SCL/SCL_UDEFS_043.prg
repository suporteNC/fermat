****************************************
*** Preenche n� do cliente
***
*** Criado por Rui Vale
***  Criado em 13/11/2017
****************************************

If cl.estab=0
	TEXT TO uSql TEXTMERGE noshow
		Select isnull(max(cl.no)+1,isnull((select e1.u_noclini from e1 (nolock) where estab=0 and e1.u_noclini<>0),1)) as no
		From cl (nolock)
		Where cl.no between isnull((select e1.u_noclini from e1 (nolock) where estab=0 and e1.u_noclini<>0),0)
			and isnull((select e1.u_noclfim from e1 (nolock) where estab=0 and e1.u_noclfim <>0),9999999999)
	ENDTEXT

	If u_sqlexec(uSql,"uCurNumCl") And Reccount("uCurNumCl")>0
		Select uCurNumCl
		Return uCurNumCl.no
	Else
		Return 1
	Endif
Else
	Return cl.no
Endif