*******************************************
** Guarda estabelecimento
**
** Criado por J�lio Ricardo
** Criado em  25/01/2018
*******************************************

Select re

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From tsre(nolock)
	Where ndoc=<<STR(re.ndoc)>>
ENDTEXT

If u_sqlexec(uSql,"uCurTs") And Reccount("uCurTs")>0
	Select uCurTsr
	Replace re.u_estab With uCurTsr.u_estab

	fecha("uCurTsr")
	Return re.u_estab
Endif

Return re.u_estab