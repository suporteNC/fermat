*************************************
*** Importa��o de artigos para Dossier
*** Criado em 06/07/2017
*** Criado por Rui Vale
*************************************
If !SBO.adding And !SBO.editing
	Msg("O ecr� tem de estar em modo de edi��o!!")
	Return
Endif
Msg("Aten��o, a estrutura do ficheiro tem de ser REF,DESIGN,QTT,UNIDADE,UNI2QTT,UNIDADE2,PRECO,DESC1,DESC2,ARMAZEM !!")
Create Cursor uCurErros ( Erro C(254) )
Create Cursor uDadosSt (Ref C(18), Design C(60),Qtt N(16,3),Unidade C(4),uni2qtt N(16,3),Unidad2 C(4),Preco N(16,3),Desc1 N(10,2),Desc2 N(10,2),Armazem N(10))
** Vou pedir o ficheiro para importar
uNomeFicheiroZip = Getfile("","Ficheiro a abrir","Abrir",2,"Abrir")
If Empty(uNomeFicheiroZip)
	Msg("Cancelado pelo utilizador!!!")
	Return
Endif
uFolhas = ""
uExtFile = Upper(Justext(uNomeFicheiroZip))
If !Alltrim(Upper(uExtFile))=="XLS" And !Alltrim(Upper(uExtFile))=="XLSX"
	Msg("Tipo de ficheiro inv�lido!!!")
	Return
Endif
** Se � um XLSX, transformo em XLS
If uExtFile = "XLSX"
	u_nficheiro = Justpath(uNomeFicheiroZip)+"\"+Strtran(Justfname(uNomeFicheiroZip),".XLSX","_XLS.XLS")
	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)
	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next
	oExcel.Visible = .T.
	oExcel.DisplayAlerts = .F.
	oExcel.ActiveWorkbook.SaveAs(u_nficheiro, 39)
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()
	uNomeFicheiroZip = u_nficheiro
Else
	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)
	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()
Endif
**************
Create Cursor xVars ( No N(5), tipo C(1), Nome C(40), Pict C(100), lOrdem N(10), nValor N(18,5), cValor C(250), lValor l, dValor d, tbval M )
Select xVars
Append Blank
Replace xVars.No With 1
Replace xVars.tipo With "T"
Replace xVars.Nome With "Folha"
Replace xVars.Pict With ""
Replace xVars.lOrdem With 1
Replace xVars.tbval With uFolhas
m.mCaption = "Qual a Folha a importar"
m.escolheu=.F.
docomando("do form usqlvar with 'xvars',m.mCaption")
If ! m.escolheu
	Msg("Cancelado pelo utilizador")
	Return
Else
	Select xVars
	Locate For No=1
	uNomeFolha = xVars.cValor
Endif
uFechar = .F.
uNomeFicheiroZip = ["] + uNomeFicheiroZip + ["]
uNomeFolha = ["] + Alltrim(uNomeFolha) + ["]
Try
	Select uDadosSt
	Append From &uNomeFicheiroZip Type Xl5 Sheet &uNomeFolha
Catch
	Msg("Os dados a importar n�o est�o de acordo com o esperado!!!")
	uFechar = .T.
Endtry
If uFechar
	Return
Endif
Select uDadosSt
Go Top
Skip
Do While !Eof()
	TEXT TO uSql TEXTMERGE noshow
		SELECT *
		From st (nolock)
		Where ref = '<<uDadosSt.ref>>'
	ENDTEXT
	If u_Sqlexec(uSql,"uCurArtigos") And Reccount("uCurArtigos")>0
		Do Boine2in
		Select bi
		Replace bi.Ref With uDadosSt.Ref
		Do BOACTREF With 'NORMAL',.T.,'OKPRECOS','bi','OKDESCONTO'
		Select bi
		Replace bi.Qtt With uDadosSt.Qtt
		If Alltrim(Upper(bo.moeda))=="EURO" And !empty(uDadosSt.Preco)
			Replace bi.debito With uDadosSt.Preco * 100
			Replace bi.edebito With uDadosSt.Preco
			Replace bi.pu With uDadosSt.Preco * 100
			Replace bi.epu With uDadosSt.Preco
		Else
		 	If !empty(uDadosSt.Preco)
				Replace bi.vumoeda With uDadosSt.Preco
			Endif
		Endif
		If !Empty(uDadosSt.Armazem)
			Replace bi.Armazem With uDadosSt.Armazem
		Endif
		If !Empty(uDadosSt.Unidade)
			Replace bi.Unidade With uDadosSt.Unidade
		Endif
		If !Empty(uDadosSt.uni2qtt)
			Replace bi.uni2qtt With uDadosSt.uni2qtt
		Endif
		If !Empty(uDadosSt.Unidad2)
			Replace bi.Unidad2 With uDadosSt.Unidad2
		Endif
		If !Empty(uDadosSt.Desc1)
			Replace bi.desconto With uDadosSt.Desc1
		Endif
		If !Empty(uDadosSt.Desc2)
			Replace bi.Desc2 With uDadosSt.Desc2
		Endif
		Do u_bottdeb With 'bi'
	Else
		Select uCurErros
		Append Blank
		Replace uCurErros.Erro With "O Artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + " n�o existe!!"
	Endif
	Select uDadosSt
	Skip
Enddo
If Reccount("uCurErros")>0
	mostrameisto("uCurErros")
Endif
