IF !TYPE("P_OPERINIS")="U"

release P_OPERINIS

ENDIF

SELECT  bo 
IF  bo.ndos =  p_dietrfarm && transferencia 

	**  fecha documento de  origem  
	cobistamp = ''
	SELECT  bi  
	goto top  
	SCAN for NOT EMPTY(bi.obistamp)
		cobistamp = bi.obistamp	
		exit
	ENDSCAN
	IF  NOT EMPTY(cobistamp)
		TEXT TO  msel NOSHOW TEXTMERGE
			DECLARE @bostamp varchar(25)
			SET @bostamp = ''
			SELECT  @bostamp = bostamp FROM bi (nolock) WHERE  bistamp = '<<cobistamp>>'
			IF  @bostamp <> '' 
			BEGIN 
				UPDATE bo SET  fechada = 1, datafecho = convert(varchar(8),getdate(),112) from bo  WHERE bostamp = @bostamp
				UPDATE bi SET  fechada = 1, datafecho = convert(varchar(8),getdate(),112) from bi  WHERE bostamp = @bostamp
			END
		ENDTEXT

			IF NOT  u_sqlexec(msel)
				msg(msel)
			ENDIF 
	ENDIF

ENDIF