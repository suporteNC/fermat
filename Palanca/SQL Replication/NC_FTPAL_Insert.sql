USE [FERMAT_LOJA]
GO

/****** Object:  StoredProcedure [dbo].[sp_MSins_dboft]    Script Date: 19/01/2023 19:31:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[NC_FTPAL_Insert]
    @c1 char(25),
    @c2 numeric(1,0),
    @c3 varchar(20),
    @c4 numeric(10,0),
    @c5 numeric(10,0),
    @c6 char(55),
    @c7 varchar(55),
    @c8 varchar(43),
    @c9 varchar(45),
    @c10 varchar(20),
    @c11 varchar(20),
    @c12 datetime,
    @c13 varchar(25),
    @c14 varchar(60),
    @c15 varchar(20),
    @c16 numeric(4,0),
    @c17 varchar(20),
    @c18 datetime,
    @c19 numeric(4,0),
    @c20 varchar(15),
    @c21 varchar(28),
    @c22 datetime,
    @c23 varchar(28),
    @c24 varchar(60),
    @c25 varchar(60),
    @c26 varchar(5),
    @c27 numeric(5,2),
    @c28 numeric(5,2),
    @c29 numeric(5,2),
    @c30 numeric(5,2),
    @c31 varchar(75),
    @c32 numeric(3,0),
    @c33 numeric(2,0),
    @c34 varchar(11),
    @c35 varchar(20),
    @c36 varchar(20),
    @c37 varchar(20),
    @c38 bit,
    @c39 numeric(10,0),
    @c40 varchar(20),
    @c41 numeric(3,0),
    @c42 datetime,
    @c43 numeric(5,2),
    @c44 numeric(14,3),
    @c45 bit,
    @c46 varchar(25),
    @c47 numeric(15,3),
    @c48 numeric(16,3),
    @c49 numeric(16,3),
    @c50 numeric(16,3),
    @c51 numeric(16,3),
    @c52 varchar(20),
    @c53 bit,
    @c54 varchar(60),
    @c55 bit,
    @c56 varchar(22),
    @c57 bit,
    @c58 numeric(1,0),
    @c59 varchar(12),
    @c60 varchar(5),
    @c61 numeric(5,2),
    @c62 numeric(5,2),
    @c63 numeric(5,2),
    @c64 numeric(5,2),
    @c65 numeric(5,2),
    @c66 bit,
    @c67 varchar(4),
    @c68 varchar(20),
    @c69 varchar(20),
    @c70 bit,
    @c71 bit,
    @c72 bit,
    @c73 varchar(20),
    @c74 varchar(20),
    @c75 numeric(18,5),
    @c76 numeric(19,6),
    @c77 numeric(15,3),
    @c78 varchar(11),
    @c79 bit,
    @c80 numeric(18,5),
    @c81 numeric(18,5),
    @c82 numeric(18,5),
    @c83 numeric(18,5),
    @c84 numeric(18,5),
    @c85 numeric(18,5),
    @c86 numeric(18,5),
    @c87 numeric(19,6),
    @c88 numeric(19,6),
    @c89 numeric(19,6),
    @c90 numeric(19,6),
    @c91 numeric(19,6),
    @c92 numeric(19,6),
    @c93 numeric(19,6),
    @c94 numeric(19,6),
    @c95 numeric(19,6),
    @c96 numeric(19,6),
    @c97 numeric(19,6),
    @c98 numeric(19,6),
    @c99 numeric(19,6),
    @c100 numeric(19,6),
    @c101 numeric(19,6),
    @c102 numeric(19,6),
    @c103 numeric(19,6),
    @c104 numeric(19,6),
    @c105 numeric(19,6),
    @c106 numeric(19,6),
    @c107 numeric(19,6),
    @c108 numeric(19,6),
    @c109 numeric(19,6),
    @c110 numeric(19,6),
    @c111 numeric(19,6),
    @c112 numeric(19,6),
    @c113 numeric(19,6),
    @c114 numeric(19,6),
    @c115 numeric(19,6),
    @c116 numeric(19,6),
    @c117 numeric(19,6),
    @c118 numeric(18,5),
    @c119 numeric(15,3),
    @c120 numeric(18,5),
    @c121 numeric(13,3),
    @c122 numeric(18,5),
    @c123 numeric(16,3),
    @c124 numeric(18,5),
    @c125 numeric(16,3),
    @c126 numeric(18,5),
    @c127 numeric(16,3),
    @c128 numeric(18,5),
    @c129 numeric(16,3),
    @c130 numeric(18,5),
    @c131 numeric(16,3),
    @c132 numeric(18,5),
    @c133 numeric(16,3),
    @c134 numeric(18,5),
    @c135 numeric(16,3),
    @c136 numeric(18,5),
    @c137 numeric(16,3),
    @c138 numeric(18,5),
    @c139 numeric(16,3),
    @c140 numeric(18,5),
    @c141 numeric(15,3),
    @c142 numeric(18,5),
    @c143 numeric(15,3),
    @c144 numeric(18,5),
    @c145 numeric(15,3),
    @c146 numeric(18,5),
    @c147 numeric(15,3),
    @c148 numeric(18,5),
    @c149 numeric(15,3),
    @c150 numeric(18,5),
    @c151 numeric(15,3),
    @c152 numeric(18,5),
    @c153 numeric(15,3),
    @c154 numeric(18,5),
    @c155 numeric(15,3),
    @c156 numeric(18,5),
    @c157 numeric(15,3),
    @c158 numeric(18,5),
    @c159 numeric(15,3),
    @c160 numeric(18,5),
    @c161 numeric(15,3),
    @c162 numeric(18,5),
    @c163 numeric(13,3),
    @c164 numeric(18,5),
    @c165 numeric(15,3),
    @c166 varchar(10),
    @c167 varchar(55),
    @c168 char(25),
    @c169 char(25),
    @c170 char(25),
    @c171 varchar(55),
    @c172 char(25),
    @c173 numeric(19,6),
    @c174 numeric(18,5),
    @c175 numeric(15,3),
    @c176 char(25),
    @c177 datetime,
    @c178 varchar(40),
    @c179 numeric(10,0),
    @c180 numeric(2,0),
    @c181 bit,
    @c182 char(25),
    @c183 bit,
    @c184 bit,
    @c185 datetime,
    @c186 varchar(2),
    @c187 numeric(2,0),
    @c188 numeric(2,0),
    @c189 varchar(43),
    @c190 varchar(3),
    @c191 numeric(2,0),
    @c192 varchar(70),
    @c193 varchar(70),
    @c194 varchar(70),
    @c195 varchar(70),
    @c196 varchar(70),
    @c197 bit,
    @c198 bit,
    @c199 bit,
    @c200 bit,
    @c201 text,
    @c202 text,
    @c203 numeric(20,12),
    @c204 varchar(20),
    @c205 varchar(20),
    @c206 numeric(3,0),
    @c207 char(25),
    @c208 varchar(30),
    @c209 char(25),
    @c210 varchar(30),
    @c211 bit,
    @c212 varchar(25),
    @c213 varchar(100),
    @c214 datetime,
    @c215 datetime,
    @c216 varchar(100),
    @c217 bit,
    @c218 varchar(35),
    @c219 varchar(35),
    @c220 char(20),
    @c221 varchar(25),
    @c222 numeric(18,5),
    @c223 numeric(19,6),
    @c224 numeric(15,2),
    @c225 char(25),
    @c226 numeric(10,0),
    @c227 varchar(4),
    @c228 varchar(20),
    @c229 varchar(6),
    @c230 bit,
    @c231 varchar(30),
    @c232 datetime,
    @c233 varchar(8),
    @c234 varchar(30),
    @c235 datetime,
    @c236 varchar(8),
    @c237 bit,
    @c238 bit,
    @c239 varchar(25),
    @c240 varchar(25),
    @c241 numeric(5,0),
    @c242 varchar(15),
    @c243 varchar(15),
    @c244 varchar(15),
    @c245 datetime
as
begin  
	insert into [PL].[ft] (
		[ftstamp],
		[pais],
		[nmdoc],
		[fno],
		[no],
		[nome],
		[morada],
		[local],
		[codpost],
		[ncont],
		[bino],
		[bidata],
		[bilocal],
		[telefone],
		[zona],
		[vendedor],
		[vendnm],
		[fdata],
		[ftano],
		[encomenda],
		[pagamento],
		[pdata],
		[expedicao],
		[carga],
		[descar],
		[saida],
		[ivatx1],
		[ivatx2],
		[ivatx3],
		[fin],
		[final],
		[ndoc],
		[ftpos],
		[moeda],
		[fref],
		[ccusto],
		[ncusto],
		[facturada],
		[fnoft],
		[nmdocft],
		[estab],
		[cdata],
		[ivatx4],
		[peso],
		[plano],
		[segmento],
		[totqtt],
		[qtt1],
		[qtt2],
		[qtt3],
		[qtt4],
		[tipo],
		[impresso],
		[userimpresso],
		[cobrado],
		[cobranca],
		[lifref],
		[tipodoc],
		[matricula],
		[chora],
		[ivatx5],
		[ivatx6],
		[ivatx7],
		[ivatx8],
		[ivatx9],
		[cambiofixo],
		[memissao],
		[cobrador],
		[rota],
		[multi],
		[introfin],
		[cheque],
		[clbanco],
		[clcheque],
		[chtotal],
		[echtotal],
		[chtmoeda],
		[chmoeda],
		[jaexpedi],
		[tot1],
		[tot2],
		[tot3],
		[tot4],
		[portes],
		[custo],
		[diferido],
		[eivain1],
		[eivain2],
		[eivain3],
		[eivav1],
		[eivav2],
		[eivav3],
		[ettiliq],
		[edescc],
		[ettiva],
		[etotal],
		[eivain4],
		[eivav4],
		[ediferido],
		[etot1],
		[etot2],
		[etot3],
		[etot4],
		[efinv],
		[eportes],
		[ecusto],
		[eivain5],
		[eivav5],
		[edebreg],
		[eivain6],
		[eivav6],
		[eivain7],
		[eivav7],
		[eivain8],
		[eivav8],
		[eivain9],
		[eivav9],
		[total],
		[totalmoeda],
		[finv],
		[finvm],
		[ivain1],
		[ivamin1],
		[ivain2],
		[ivamin2],
		[ivain3],
		[ivamin3],
		[ivain4],
		[ivamin4],
		[ivain5],
		[ivamin5],
		[ivain6],
		[ivamin6],
		[ivain7],
		[ivamin7],
		[ivain8],
		[ivamin8],
		[ivain9],
		[ivamin9],
		[ivav1],
		[ivamv1],
		[ivav2],
		[ivamv2],
		[ivav3],
		[ivamv3],
		[ivav4],
		[ivamv4],
		[ivav5],
		[ivamv5],
		[ivav6],
		[ivamv6],
		[ivav7],
		[ivamv7],
		[ivav8],
		[ivamv8],
		[ivav9],
		[ivamv9],
		[ttiliq],
		[tmiliq],
		[ttiva],
		[tmiva],
		[descc],
		[descm],
		[debreg],
		[debregm],
		[intid],
		[nome2],
		[lrstamp],
		[lpstamp],
		[tpstamp],
		[tpdesc],
		[snstamp],
		[erdtotal],
		[rdtotal],
		[rdtotalm],
		[mhstamp],
		[dplano],
		[dinoplano],
		[dilnoplano],
		[diaplano],
		[planoonline],
		[dostamp],
		[optri],
		[meiost],
		[chdata],
		[pscm],
		[zncm],
		[excm],
		[ptcm],
		[encm],
		[ntcm],
		[pscmdesc],
		[znregiao],
		[excmdesc],
		[ptcmdesc],
		[encmdesc],
		[ncin],
		[ncout],
		[usaintra],
		[iectisento],
		[series],
		[series2],
		[cambio],
		[site],
		[pnome],
		[pno],
		[cxstamp],
		[cxusername],
		[ssstamp],
		[ssusername],
		[anulado],
		[rpclstamp],
		[rpclnome],
		[rpcldini],
		[rpcldfim],
		[classe],
		[procomss],
		[eanft],
		[eancl],
		[lang],
		[tptit],
		[virs],
		[evirs],
		[valorm2],
		[arstamp],
		[arno],
		[iecacodisen],
		[niec],
		[iecadoccod],
		[aprovado],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[nprotri],
		[u_ftstamp],
		[u_vlstamp],
		[u_estab],
		[u_docvdsap],
		[u_fnosap],
		[u_norefsap],
		[u_ttssinc]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57,
		@c58,
		@c59,
		@c60,
		@c61,
		@c62,
		@c63,
		@c64,
		@c65,
		@c66,
		@c67,
		@c68,
		@c69,
		@c70,
		@c71,
		@c72,
		@c73,
		@c74,
		@c75,
		@c76,
		@c77,
		@c78,
		@c79,
		@c80,
		@c81,
		@c82,
		@c83,
		@c84,
		@c85,
		@c86,
		@c87,
		@c88,
		@c89,
		@c90,
		@c91,
		@c92,
		@c93,
		@c94,
		@c95,
		@c96,
		@c97,
		@c98,
		@c99,
		@c100,
		@c101,
		@c102,
		@c103,
		@c104,
		@c105,
		@c106,
		@c107,
		@c108,
		@c109,
		@c110,
		@c111,
		@c112,
		@c113,
		@c114,
		@c115,
		@c116,
		@c117,
		@c118,
		@c119,
		@c120,
		@c121,
		@c122,
		@c123,
		@c124,
		@c125,
		@c126,
		@c127,
		@c128,
		@c129,
		@c130,
		@c131,
		@c132,
		@c133,
		@c134,
		@c135,
		@c136,
		@c137,
		@c138,
		@c139,
		@c140,
		@c141,
		@c142,
		@c143,
		@c144,
		@c145,
		@c146,
		@c147,
		@c148,
		@c149,
		@c150,
		@c151,
		@c152,
		@c153,
		@c154,
		@c155,
		@c156,
		@c157,
		@c158,
		@c159,
		@c160,
		@c161,
		@c162,
		@c163,
		@c164,
		@c165,
		@c166,
		@c167,
		@c168,
		@c169,
		@c170,
		@c171,
		@c172,
		@c173,
		@c174,
		@c175,
		@c176,
		@c177,
		@c178,
		@c179,
		@c180,
		@c181,
		@c182,
		@c183,
		@c184,
		@c185,
		@c186,
		@c187,
		@c188,
		@c189,
		@c190,
		@c191,
		@c192,
		@c193,
		@c194,
		@c195,
		@c196,
		@c197,
		@c198,
		@c199,
		@c200,
		@c201,
		@c202,
		@c203,
		@c204,
		@c205,
		@c206,
		@c207,
		@c208,
		@c209,
		@c210,
		@c211,
		@c212,
		@c213,
		@c214,
		@c215,
		@c216,
		@c217,
		@c218,
		@c219,
		@c220,
		@c221,
		@c222,
		@c223,
		@c224,
		@c225,
		@c226,
		@c227,
		@c228,
		@c229,
		@c230,
		@c231,
		@c232,
		@c233,
		@c234,
		@c235,
		@c236,
		@c237,
		@c238,
		@c239,
		@c240,
		@c241,
		@c242,
		@c243,
		@c244,
		@c245	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PL','ft',@c1, 'ftstamp'

end  
GO


