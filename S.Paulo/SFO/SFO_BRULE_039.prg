select FN
go top
scan

IF EMPTY(FN.REF) and !EMPTY(FN.OREF) and !EMPTY(fn.bistamp)

	text to msel textmerge noshow
		select * 
		from bi (nolock) 
		where bistamp = '<<fn.bistamp>>'
	endtext
	IF !U_SQLEXEC(msel,'CR_BITMP')
		msg('Erro Encontrado')
		msg(msel)
		return
	ENDIF
	select CR_BITMP
	IF cr_BITMP.NDOS = 30
		text to mupdbi textmerge noshow
			update bi set 
				bi.qtt=bi.qtt
				,bi.EPCUSTO = replace('<<FN.ETILIQUIDO/FN.QTT>>',',','.')
				,bi.PCUSTO = replace('<<FN.TILIQUIDO/FN.QTT>>',',','.')
				,bi.EDEBITO = replace('<<FN.EPV>>',',','.')
				,bi.DEBITO = replace('<<FN.PV>>',',','.')
				,bi.ETTDEB = replace('<<FN.ETILIQUIDO>>',',','.')
				,bi.TTDEB = replace('<<FN.TILIQUIDO>>',',','.')
				,bi.DESCONTO = replace('<<FN.DESCONTO>>',',','.')
				,bi.DESC2 = replace('<<FN.DESC2>>',',','.')
				,bi.ESLVU = replace('<<FN.ETILIQUIDO/FN.QTT>>',',','.')
				,bi.SLVU = replace('<<FN.TILIQUIDO/FN.QTT>>',',','.')
				,bi.ESLTT = replace('<<FN.ETILIQUIDO>>',',','.')
				,bi.SLTT = replace('<<FN.TILIQUIDO>>',',','.')
			where bi.bistamp = '<<FN.BISTAMP>>'
		endtext 
		IF !U_SQLEXEC(mupdBI)
			msg('Erro Encontrado')
			msg(mupdbi)
			return
		ENDIF

		text to mupdST textmerge noshow
		update st set 
			st.EPCUSTO = replace('<<FN.ETILIQUIDO/FN.QTT>>',',','.')
			,st.PCUSTO = replace('<<FN.TILIQUIDO/FN.QTT>>',',','.')	
		where st.ref = '<<FN.OREF>>'		
		endtext
		IF !U_SQLEXEC(mupdST)
			msg('Erro Encontrado')
			msg(mupdst)
			return
		ENDIF
		msg('Guia de Recep��o atualizada com sucesso')

	ENDIF
ENDIF

ENDSCAN

return .t.