USE [SPAULO]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbofi]    Script Date: 23/01/2023 12:59:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_FISP_Insert]
    @c1 char(25),
    @c2 varchar(20),
    @c3 numeric(10,0),
    @c4 varchar(18),
    @c5 varchar(60),
    @c6 numeric(12,3),
    @c7 numeric(18,5),
    @c8 numeric(19,6),
    @c9 varchar(4),
    @c10 varchar(4),
    @c11 numeric(5,2),
    @c12 bit,
    @c13 varchar(40),
    @c14 numeric(1,0),
    @c15 numeric(3,0),
    @c16 numeric(5,0),
    @c17 numeric(10,0),
    @c18 numeric(3,0),
    @c19 numeric(4,0),
    @c20 char(25),
    @c21 bit,
    @c22 varchar(40),
    @c23 varchar(40),
    @c24 varchar(40),
    @c25 varchar(40),
    @c26 datetime,
    @c27 bit,
    @c28 numeric(3,0),
    @c29 numeric(6,0),
    @c30 bit,
    @c31 bit,
    @c32 varchar(25),
    @c33 numeric(10,0),
    @c34 bit,
    @c35 numeric(13,3),
    @c36 numeric(13,3),
    @c37 numeric(13,3),
    @c38 numeric(13,3),
    @c39 numeric(4,1),
    @c40 varchar(18),
    @c41 bit,
    @c42 varchar(30),
    @c43 varchar(20),
    @c44 varchar(20),
    @c45 varchar(35),
    @c46 varchar(20),
    @c47 varchar(120),
    @c48 varchar(30),
    @c49 char(25),
    @c50 char(25),
    @c51 char(25),
    @c52 numeric(1,0),
    @c53 numeric(9,3),
    @c54 varchar(20),
    @c55 varchar(50),
    @c56 varchar(20),
    @c57 numeric(11,3),
    @c58 numeric(1,0),
    @c59 varchar(18),
    @c60 numeric(14,3),
    @c61 numeric(14,3),
    @c62 char(25),
    @c63 varchar(40),
    @c64 bit,
    @c65 varchar(12),
    @c66 char(25),
    @c67 varchar(20),
    @c68 varchar(20),
    @c69 bit,
    @c70 bit,
    @c71 char(25),
    @c72 char(25),
    @c73 numeric(18,5),
    @c74 numeric(19,6),
    @c75 numeric(19,6),
    @c76 numeric(13,3),
    @c77 numeric(18,5),
    @c78 numeric(19,6),
    @c79 numeric(18,5),
    @c80 numeric(19,6),
    @c81 numeric(18,5),
    @c82 numeric(19,6),
    @c83 numeric(18,5),
    @c84 numeric(19,6),
    @c85 numeric(18,5),
    @c86 numeric(19,6),
    @c87 numeric(18,5),
    @c88 numeric(19,6),
    @c89 numeric(15,2),
    @c90 numeric(15,2),
    @c91 numeric(16,3),
    @c92 numeric(14,4),
    @c93 numeric(18,5),
    @c94 numeric(19,6),
    @c95 varchar(13),
    @c96 varchar(25),
    @c97 bit,
    @c98 bit,
    @c99 char(25),
    @c100 char(25),
    @c101 bit,
    @c102 numeric(4,0),
    @c103 varchar(20),
    @c104 text,
    @c105 text,
    @c106 numeric(6,2),
    @c107 numeric(5,2),
    @c108 numeric(5,2),
    @c109 numeric(5,2),
    @c110 numeric(5,2),
    @c111 numeric(5,2),
    @c112 numeric(18,5),
    @c113 numeric(19,6),
    @c114 numeric(18,5),
    @c115 numeric(19,6),
    @c116 numeric(6,2),
    @c117 numeric(18,5),
    @c118 numeric(19,6),
    @c119 numeric(18,5),
    @c120 numeric(19,6),
    @c121 numeric(13,3),
    @c122 numeric(18,5),
    @c123 numeric(19,6),
    @c124 numeric(13,3),
    @c125 numeric(18,5),
    @c126 numeric(19,6),
    @c127 numeric(13,3),
    @c128 numeric(18,5),
    @c129 numeric(19,6),
    @c130 numeric(13,3),
    @c131 varchar(20),
    @c132 bit,
    @c133 numeric(18,5),
    @c134 numeric(19,6),
    @c135 varchar(20),
    @c136 numeric(18,5),
    @c137 numeric(19,6),
    @c138 numeric(18,5),
    @c139 numeric(19,6),
    @c140 numeric(13,3),
    @c141 numeric(11,3),
    @c142 bit,
    @c143 numeric(7,3),
    @c144 varchar(4),
    @c145 numeric(19,3),
    @c146 numeric(19,5),
    @c147 char(25),
    @c148 varchar(25),
    @c149 numeric(1,0),
    @c150 bit,
    @c151 char(25),
    @c152 char(25),
    @c153 char(25),
    @c154 char(25),
    @c155 numeric(18,5),
    @c156 numeric(19,6),
    @c157 varchar(18),
    @c158 bit,
    @c159 char(25),
    @c160 bit,
    @c161 numeric(18,5),
    @c162 numeric(19,6),
    @c163 numeric(18,5),
    @c164 numeric(19,6),
    @c165 numeric(18,5),
    @c166 numeric(19,6),
    @c167 numeric(18,5),
    @c168 numeric(19,6),
    @c169 bit,
    @c170 varchar(30),
    @c171 datetime,
    @c172 varchar(8),
    @c173 varchar(30),
    @c174 datetime,
    @c175 varchar(8),
    @c176 bit,
    @c177 numeric(18,5),
    @c178 numeric(19,6),
    @c179 bit,
    @c180 varchar(25),
    @c181 varchar(55),
    @c182 varchar(25),
    @c183 varchar(30),
    @c184 char(25),
    @c185 char(25),
    @c186 bit,
    @c187 varchar(50),
    @c188 varchar(55),
    @c189 varchar(43),
    @c190 varchar(45),
    @c191 varchar(20),
    @c192 varchar(60),
    @c193 varchar(50),
    @c194 varchar(100),
    @c195 varchar(25),
    @c196 numeric(10,0),
    @c197 numeric(2,0),
    @c198 varchar(25),
    @c199 numeric(19,6),
    @c200 numeric(19,6),
    @c201 numeric(18,5),
    @c202 numeric(18,5),
    @c203 numeric(1,0),
    @c204 bit,
    @c205 numeric(5,2),
    @c206 char(25),
    @c207 numeric(18,5),
    @c208 numeric(19,6),
    @c209 varchar(60),
    @c210 varchar(3),
    @c211 bit,
    @c212 varchar(6),
    @c213 char(25),
    @c214 datetime,
    @c215 numeric(5,2),
    @c216 numeric(10,0),
    @c217 numeric(3,0),
    @c218 varchar(40),
    @c219 numeric(3,0),
    @c220 varchar(50),
    @c221 varchar(250),
    @c222 varchar(25),
    @c223 numeric(5,0),
    @c224 numeric(14,4),
    @c225 numeric(15,3),
    @c226 bit,
    @c227 numeric(15,4),
    @c228 numeric(15,6),
    @c229 bit,
    @c230 datetime
as
begin  
	insert into [SP].[fi] (
		[fistamp],
		[nmdoc],
		[fno],
		[ref],
		[design],
		[qtt],
		[tiliquido],
		[etiliquido],
		[unidade],
		[unidad2],
		[iva],
		[ivaincl],
		[codigo],
		[tabiva],
		[ndoc],
		[armazem],
		[fnoft],
		[ndocft],
		[ftanoft],
		[ftregstamp],
		[facturada],
		[lobs2],
		[litem2],
		[litem],
		[lobs3],
		[rdata],
		[nsujpp],
		[ecomissao],
		[cpoc],
		[composto],
		[compostoori],
		[lrecno],
		[lordem],
		[fmarcada],
		[partes],
		[altura],
		[largura],
		[espessura],
		[pctcom],
		[oref],
		[livacusto],
		[lote],
		[usr1],
		[usr2],
		[usr3],
		[usr4],
		[usr5],
		[usr6],
		[ftstamp],
		[cor],
		[tam],
		[stipo],
		[grau],
		[cliref],
		[serie],
		[fifref],
		[uni2qtt],
		[tipodoc],
		[familia],
		[peso],
		[pbruto],
		[bistamp],
		[nmobs],
		[stns],
		[matricula],
		[vhstamp],
		[ficcusto],
		[fincusto],
		[usalote],
		[texteis],
		[ofistamp],
		[mhstamp],
		[pv],
		[pvmoeda],
		[epv],
		[tmoeda],
		[extracom],
		[eextracom],
		[diferido],
		[ediferido],
		[aquisicao],
		[eaquisicao],
		[custo],
		[ecusto],
		[slvu],
		[eslvu],
		[sltt],
		[esltt],
		[slvumoeda],
		[slttmoeda],
		[ncmassa],
		[ncunsup],
		[ncvest],
		[encvest],
		[nccod],
		[ncinteg],
		[promo],
		[epromo],
		[lrstamp],
		[lpstamp],
		[noserie],
		[fivendedor],
		[fivendnm],
		[series],
		[series2],
		[desconto],
		[desc2],
		[desc3],
		[desc4],
		[desc5],
		[desc6],
		[iectin],
		[eiectin],
		[iectfx],
		[eiectfx],
		[iectadp],
		[iectadv],
		[eiectadv],
		[iecttot],
		[eiecttot],
		[iectmtot],
		[iectivt],
		[eiectivt],
		[iectmivt],
		[vvenda],
		[evvenda],
		[mvvenda],
		[vbase],
		[evbase],
		[mvbase],
		[codfiscal],
		[iectinii],
		[iectfxtt],
		[eiectfxtt],
		[tipoiect],
		[iectuivt],
		[eiectuivt],
		[tieca],
		[etieca],
		[mtieca],
		[volume],
		[iecasug],
		[iecagrad],
		[iecacodisen],
		[tliquido],
		[num1],
		[rpclstamp],
		[firpcl],
		[preco],
		[sujirs],
		[agstcstamp],
		[opgstamp],
		[autostamp],
		[fminstamp],
		[pcp],
		[epcp],
		[beref],
		[fechabo],
		[rvpstamp],
		[temeco],
		[ecoval],
		[eecoval],
		[tecoval],
		[etecoval],
		[ecoval2],
		[eecoval2],
		[tecoval2],
		[etecoval2],
		[econotcalc],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[pvori],
		[epvori],
		[nprotri],
		[alvstamp],
		[identificacao],
		[szzstamp],
		[zona],
		[mrstamp],
		[mnstamp],
		[cladrscl2],
		[cladrsdesc],
		[morada],
		[local],
		[codpost],
		[cladrszona],
		[telefone],
		[contacto],
		[email],
		[cladrsstamp],
		[tkhpmp],
		[tkhcodcmb],
		[tkhposlstamp],
		[eftaxamt_a],
		[eftaxamt_b],
		[ftaxamt_a],
		[ftaxamt_b],
		[tpromo],
		[amostra],
		[ivarec],
		[snstamp],
		[valdesc],
		[evaldesc],
		[motiseimp],
		[codmotiseimp],
		[activo],
		[cecope],
		[oftstamp],
		[taxpointdt],
		[txirs],
		[promoordem],
		[tpvlno],
		[idvale],
		[promotpvlno],
		[modalidade],
		[u_imagem],
		[u_fistamp],
		[u_estab],
		[u_qttent],
		[u_qttexp],
		[u_ga],
		[u_qtt2],
		[u_qtt],
		[tfcaucao],
		[u_ttssinc]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57,
		@c58,
		@c59,
		@c60,
		@c61,
		@c62,
		@c63,
		@c64,
		@c65,
		@c66,
		@c67,
		@c68,
		@c69,
		@c70,
		@c71,
		@c72,
		@c73,
		@c74,
		@c75,
		@c76,
		@c77,
		@c78,
		@c79,
		@c80,
		@c81,
		@c82,
		@c83,
		@c84,
		@c85,
		@c86,
		@c87,
		@c88,
		@c89,
		@c90,
		@c91,
		@c92,
		@c93,
		@c94,
		@c95,
		@c96,
		@c97,
		@c98,
		@c99,
		@c100,
		@c101,
		@c102,
		@c103,
		@c104,
		@c105,
		@c106,
		@c107,
		@c108,
		@c109,
		@c110,
		@c111,
		@c112,
		@c113,
		@c114,
		@c115,
		@c116,
		@c117,
		@c118,
		@c119,
		@c120,
		@c121,
		@c122,
		@c123,
		@c124,
		@c125,
		@c126,
		@c127,
		@c128,
		@c129,
		@c130,
		@c131,
		@c132,
		@c133,
		@c134,
		@c135,
		@c136,
		@c137,
		@c138,
		@c139,
		@c140,
		@c141,
		@c142,
		@c143,
		@c144,
		@c145,
		@c146,
		@c147,
		@c148,
		@c149,
		@c150,
		@c151,
		@c152,
		@c153,
		@c154,
		@c155,
		@c156,
		@c157,
		@c158,
		@c159,
		@c160,
		@c161,
		@c162,
		@c163,
		@c164,
		@c165,
		@c166,
		@c167,
		@c168,
		@c169,
		@c170,
		@c171,
		@c172,
		@c173,
		@c174,
		@c175,
		@c176,
		@c177,
		@c178,
		@c179,
		@c180,
		@c181,
		@c182,
		@c183,
		@c184,
		@c185,
		@c186,
		@c187,
		@c188,
		@c189,
		@c190,
		@c191,
		@c192,
		@c193,
		@c194,
		@c195,
		@c196,
		@c197,
		@c198,
		@c199,
		@c200,
		@c201,
		@c202,
		@c203,
		@c204,
		@c205,
		@c206,
		@c207,
		@c208,
		@c209,
		@c210,
		@c211,
		@c212,
		@c213,
		@c214,
		@c215,
		@c216,
		@c217,
		@c218,
		@c219,
		@c220,
		@c221,
		@c222,
		@c223,
		@c224,
		@c225,
		@c226,
		@c227,
		@c228,
		@c229,
		@c230	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','SP','fi',@c1, 'fistamp'

end  
