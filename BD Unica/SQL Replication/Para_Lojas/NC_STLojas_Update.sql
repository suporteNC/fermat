USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[NC_STPG_Update]    Script Date: 24/01/2023 19:21:14 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREaTE procedure [dbo].[NC_STLojas_Update]
		@c1 char(25),
		@c2 char(18),
		@c3 char(60),
		@c4 varchar(18),
		@c5 numeric(13,3),
		@c6 numeric(19,6),
		@c7 numeric(18,5),
		@c8 char(20),
		@c9 varchar(55),
		@c10 varchar(60),
		@c11 varchar(60),
		@c12 varchar(60),
		@c13 varchar(20),
		@c14 varchar(20),
		@c15 datetime,
		@c16 datetime,
		@c17 datetime,
		@c18 numeric(13,3),
		@c19 numeric(13,3),
		@c20 numeric(18,5),
		@c21 numeric(19,6),
		@c22 varchar(3),
		@c23 numeric(18,5),
		@c24 numeric(18,5),
		@c25 numeric(18,5),
		@c26 numeric(18,5),
		@c27 varchar(4),
		@c28 numeric(10,3),
		@c29 numeric(1,0),
		@c30 varchar(20),
		@c31 numeric(10,0),
		@c32 numeric(3,0),
		@c33 numeric(13,3),
		@c34 numeric(13,3),
		@c35 numeric(13,3),
		@c36 datetime,
		@c37 numeric(18,5),
		@c38 numeric(18,5),
		@c39 numeric(13,3),
		@c40 numeric(13,3),
		@c41 numeric(6,0),
		@c42 numeric(18,5),
		@c43 numeric(18,5),
		@c44 numeric(18,5),
		@c45 numeric(13,3),
		@c46 numeric(13,3),
		@c47 varchar(68),
		@c48 char(40),
		@c49 varchar(4),
		@c50 numeric(15,7),
		@c51 bit,
		@c52 bit,
		@c53 numeric(3,0),
		@c54 varchar(120),
		@c55 numeric(18,5),
		@c56 numeric(18,5),
		@c57 numeric(6,0),
		@c58 varchar(15),
		@c59 varchar(15),
		@c60 varchar(15),
		@c61 varchar(15),
		@c62 numeric(14,3),
		@c63 bit,
		@c64 bit,
		@c65 numeric(6,2),
		@c66 numeric(6,2),
		@c67 numeric(3,0),
		@c68 numeric(13,3),
		@c69 bit,
		@c70 numeric(5,2),
		@c71 numeric(3,0),
		@c72 numeric(16,3),
		@c73 numeric(16,3),
		@c74 numeric(16,3),
		@c75 numeric(16,3),
		@c76 numeric(16,3),
		@c77 numeric(3,0),
		@c78 numeric(3,0),
		@c79 varchar(60),
		@c80 varchar(60),
		@c81 varchar(60),
		@c82 bit,
		@c83 bit,
		@c84 bit,
		@c85 numeric(14,3),
		@c86 numeric(11,3),
		@c87 bit,
		@c88 bit,
		@c89 numeric(5,0),
		@c90 datetime,
		@c91 varchar(60),
		@c92 varchar(35),
		@c93 varchar(20),
		@c94 varchar(120),
		@c95 numeric(11,3),
		@c96 numeric(19,6),
		@c97 numeric(19,6),
		@c98 numeric(19,6),
		@c99 numeric(19,6),
		@c100 numeric(19,6),
		@c101 numeric(19,6),
		@c102 numeric(19,6),
		@c103 numeric(19,6),
		@c104 numeric(19,6),
		@c105 numeric(19,6),
		@c106 bit,
		@c107 bit,
		@c108 bit,
		@c109 bit,
		@c110 bit,
		@c111 bit,
		@c112 bit,
		@c113 varchar(60),
		@c114 varchar(30),
		@c115 numeric(15,7),
		@c116 varchar(8),
		@c117 numeric(18,3),
		@c118 varchar(100),
		@c119 numeric(1,0),
		@c120 bit,
		@c121 numeric(18,5),
		@c122 numeric(19,6),
		@c123 bit,
		@c124 varchar(20),
		@c125 bit,
		@c126 numeric(7,3),
		@c127 varchar(18),
		@c128 varchar(60),
		@c129 varchar(18),
		@c130 varchar(115),
		@c131 varchar(4),
		@c132 bit,
		@c133 bit,
		@c134 varchar(20),
		@c135 bit,
		@c136 varchar(30),
		@c137 varchar(120),
		@c138 char(20),
		@c139 char(60),
		@c140 char(20),
		@c141 char(60),
		@c142 char(20),
		@c143 char(60),
		@c144 char(20),
		@c145 char(60),
		@c146 char(20),
		@c147 char(60),
		@c148 bit,
		@c149 varchar(1),
		@c150 numeric(13,3),
		@c151 bit,
		@c152 bit,
		@c153 numeric(19,6),
		@c154 numeric(18,5),
		@c155 bit,
		@c156 numeric(19,6),
		@c157 numeric(18,5),
		@c158 bit,
		@c159 numeric(19,6),
		@c160 numeric(18,5),
		@c161 bit,
		@c162 numeric(19,6),
		@c163 numeric(18,5),
		@c164 bit,
		@c165 numeric(19,6),
		@c166 numeric(18,5),
		@c167 bit,
		@c168 bit,
		@c169 varchar(30),
		@c170 datetime,
		@c171 varchar(8),
		@c172 varchar(30),
		@c173 datetime,
		@c174 varchar(8),
		@c175 bit,
		@c176 bit,
		@c177 bit,
		@c178 varchar(15),
		@c179 bit,
		@c180 bit,
		@c181 char(18),
		@c182 char(60),
		@c183 numeric(18,5),
		@c184 numeric(19,6),
		@c185 numeric(18,5),
		@c186 numeric(19,6),
		@c187 numeric(6,2),
		@c188 numeric(6,2),
		@c189 datetime,
		@c190 numeric(18,5),
		@c191 numeric(19,6),
		@c192 numeric(18,5),
		@c193 numeric(19,6),
		@c194 numeric(18,5),
		@c195 numeric(19,6),
		@c196 char(25),
		@c197 numeric(18,5),
		@c198 numeric(19,6),
		@c199 varchar(4),
		@c200 numeric(2,0),
		@c201 bit,
		@c202 datetime,
		@c203 varchar(5),
		@c204 varchar(3),
		@c205 bit,
		@c206 bit,
		@c207 varchar(60),
		@c208 varchar(60),
		@c209 bit,
		@c210 bit,
		@c211 bit,
		@c212 bit,
		@c213 varchar(25),
		@c214 bit,
		@c215 bit,
		@c216 bit,
		@c217 bit,
		@c218 bit,
		@c219 varchar(10),
		@c220 varchar(30),
		@c221 varchar(10),
		@c222 varchar(30),
		@c223 varchar(60),
		@c224 bit,
		@c225 varchar(20),
		@c226 varchar(20),
		@c227 varchar(20),
		@c228 varchar(20),
		@c229 varchar(20),
		@c230 bit,
		@c231 varchar(60),
		@c232 varchar(20),
		@c233 varchar(20),
		@c234 varchar(15),
		@c235 varchar(55),
		@c236 numeric(10,0),
		@c237 varchar(20),
		@c238 datetime,
		@c239 varchar(20),
		@c240 datetime,
		@c241 varchar(20),
		@c242 varchar(20),
		@c243 datetime,
		@c244 datetime,
		@c245 bit,
		@c246 bit,
		@c247 bit,
		@c248 numeric(10,2),
		@c249 numeric(10,2),
		@c250 bit,
		@c251 datetime,
		@c252 datetime,
		@c253 bit,
		@c254 numeric(10,2),
		@c255 char(25),
		@c256 char(18),
		@c257 char(60),
		@c258 varchar(18),
		@c259 numeric(13,3),
		@c260 numeric(19,6),
		@c261 numeric(18,5),
		@c262 char(20),
		@c263 varchar(55),
		@c264 varchar(60),
		@c265 varchar(60),
		@c266 varchar(60),
		@c267 varchar(20),
		@c268 varchar(20),
		@c269 datetime,
		@c270 datetime,
		@c271 datetime,
		@c272 numeric(13,3),
		@c273 numeric(13,3),
		@c274 numeric(18,5),
		@c275 numeric(19,6),
		@c276 varchar(3),
		@c277 numeric(18,5),
		@c278 numeric(18,5),
		@c279 numeric(18,5),
		@c280 numeric(18,5),
		@c281 varchar(4),
		@c282 numeric(10,3),
		@c283 numeric(1,0),
		@c284 varchar(20),
		@c285 numeric(10,0),
		@c286 numeric(3,0),
		@c287 numeric(13,3),
		@c288 numeric(13,3),
		@c289 numeric(13,3),
		@c290 datetime,
		@c291 numeric(18,5),
		@c292 numeric(18,5),
		@c293 numeric(13,3),
		@c294 numeric(13,3),
		@c295 numeric(6,0),
		@c296 numeric(18,5),
		@c297 numeric(18,5),
		@c298 numeric(18,5),
		@c299 numeric(13,3),
		@c300 numeric(13,3),
		@c301 varchar(68),
		@c302 char(40),
		@c303 varchar(4),
		@c304 numeric(15,7),
		@c305 bit,
		@c306 bit,
		@c307 numeric(3,0),
		@c308 varchar(120),
		@c309 numeric(18,5),
		@c310 numeric(18,5),
		@c311 numeric(6,0),
		@c312 varchar(15),
		@c313 varchar(15),
		@c314 varchar(15),
		@c315 varchar(15),
		@c316 numeric(14,3),
		@c317 bit,
		@c318 bit,
		@c319 numeric(6,2),
		@c320 numeric(6,2),
		@c321 numeric(3,0),
		@c322 numeric(13,3),
		@c323 bit,
		@c324 numeric(5,2),
		@c325 numeric(3,0),
		@c326 numeric(16,3),
		@c327 numeric(16,3),
		@c328 numeric(16,3),
		@c329 numeric(16,3),
		@c330 numeric(16,3),
		@c331 numeric(3,0),
		@c332 numeric(3,0),
		@c333 varchar(60),
		@c334 varchar(60),
		@c335 varchar(60),
		@c336 bit,
		@c337 bit,
		@c338 bit,
		@c339 numeric(14,3),
		@c340 numeric(11,3),
		@c341 bit,
		@c342 bit,
		@c343 numeric(5,0),
		@c344 datetime,
		@c345 varchar(60),
		@c346 varchar(35),
		@c347 varchar(20),
		@c348 varchar(120),
		@c349 numeric(11,3),
		@c350 numeric(19,6),
		@c351 numeric(19,6),
		@c352 numeric(19,6),
		@c353 numeric(19,6),
		@c354 numeric(19,6),
		@c355 numeric(19,6),
		@c356 numeric(19,6),
		@c357 numeric(19,6),
		@c358 numeric(19,6),
		@c359 numeric(19,6),
		@c360 bit,
		@c361 bit,
		@c362 bit,
		@c363 bit,
		@c364 bit,
		@c365 bit,
		@c366 bit,
		@c367 varchar(60),
		@c368 varchar(30),
		@c369 numeric(15,7),
		@c370 varchar(8),
		@c371 numeric(18,3),
		@c372 varchar(100),
		@c373 numeric(1,0),
		@c374 bit,
		@c375 numeric(18,5),
		@c376 numeric(19,6),
		@c377 bit,
		@c378 varchar(20),
		@c379 bit,
		@c380 numeric(7,3),
		@c381 varchar(18),
		@c382 varchar(60),
		@c383 varchar(18),
		@c384 varchar(115),
		@c385 varchar(4),
		@c386 bit,
		@c387 bit,
		@c388 varchar(20),
		@c389 bit,
		@c390 varchar(30),
		@c391 varchar(120),
		@c392 char(20),
		@c393 char(60),
		@c394 char(20),
		@c395 char(60),
		@c396 char(20),
		@c397 char(60),
		@c398 char(20),
		@c399 char(60),
		@c400 char(20),
		@c401 char(60),
		@c402 bit,
		@c403 varchar(1),
		@c404 numeric(13,3),
		@c405 bit,
		@c406 bit,
		@c407 numeric(19,6),
		@c408 numeric(18,5),
		@c409 bit,
		@c410 numeric(19,6),
		@c411 numeric(18,5),
		@c412 bit,
		@c413 numeric(19,6),
		@c414 numeric(18,5),
		@c415 bit,
		@c416 numeric(19,6),
		@c417 numeric(18,5),
		@c418 bit,
		@c419 numeric(19,6),
		@c420 numeric(18,5),
		@c421 bit,
		@c422 bit,
		@c423 varchar(30),
		@c424 datetime,
		@c425 varchar(8),
		@c426 varchar(30),
		@c427 datetime,
		@c428 varchar(8),
		@c429 bit,
		@c430 bit,
		@c431 bit,
		@c432 varchar(15),
		@c433 bit,
		@c434 bit,
		@c435 char(18),
		@c436 char(60),
		@c437 numeric(18,5),
		@c438 numeric(19,6),
		@c439 numeric(18,5),
		@c440 numeric(19,6),
		@c441 numeric(6,2),
		@c442 numeric(6,2),
		@c443 datetime,
		@c444 numeric(18,5),
		@c445 numeric(19,6),
		@c446 numeric(18,5),
		@c447 numeric(19,6),
		@c448 numeric(18,5),
		@c449 numeric(19,6),
		@c450 char(25),
		@c451 numeric(18,5),
		@c452 numeric(19,6),
		@c453 varchar(4),
		@c454 numeric(2,0),
		@c455 bit,
		@c456 datetime,
		@c457 varchar(5),
		@c458 varchar(3),
		@c459 bit,
		@c460 bit,
		@c461 varchar(60),
		@c462 varchar(60),
		@c463 bit,
		@c464 bit,
		@c465 bit,
		@c466 bit,
		@c467 varchar(25),
		@c468 bit,
		@c469 bit,
		@c470 bit,
		@c471 bit,
		@c472 bit,
		@c473 varchar(10),
		@c474 varchar(30),
		@c475 varchar(10),
		@c476 varchar(30),
		@c477 varchar(60),
		@c478 bit,
		@c479 varchar(20),
		@c480 varchar(20),
		@c481 varchar(20),
		@c482 varchar(20),
		@c483 varchar(20),
		@c484 bit,
		@c485 varchar(60),
		@c486 varchar(20),
		@c487 varchar(20),
		@c488 varchar(15),
		@c489 varchar(55),
		@c490 numeric(10,0),
		@c491 varchar(20),
		@c492 datetime,
		@c493 varchar(20),
		@c494 datetime,
		@c495 varchar(20),
		@c496 varchar(20),
		@c497 datetime,
		@c498 datetime,
		@c499 bit,
		@c500 bit,
		@c501 bit,
		@c502 numeric(10,2),
		@c503 numeric(10,2),
		@c504 bit,
		@c505 datetime,
		@c506 datetime,
		@c507 bit,
		@c508 numeric(10,2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c256 = @c2)
begin 
update [PG].[st] set
		[ststamp] = @c255,
		[ref] = @c256,
		[design] = @c257,
		[familia] = @c258,
		[stock] = @c259,
		[epv1] = @c260,
		[pv1] = @c261,
		[forref] = @c262,
		[fornecedor] = @c263,
		[desc3] = @c264,
		[desc2] = @c265,
		[desc1] = @c266,
		[usr2] = @c267,
		[usr1] = @c268,
		[validade] = @c269,
		[usaid] = @c270,
		[uintr] = @c271,
		[usrqtt] = @c272,
		[eoq] = @c273,
		[pcult] = @c274,
		[pcimp] = @c275,
		[pcmoe] = @c276,
		[pvcon] = @c277,
		[pvultimo] = @c278,
		[pv3] = @c279,
		[pv2] = @c280,
		[unidade] = @c281,
		[ptoenc] = @c282,
		[tabiva] = @c283,
		[local] = @c284,
		[fornec] = @c285,
		[fornestab] = @c286,
		[qttfor] = @c287,
		[qttcli] = @c288,
		[qttrec] = @c289,
		[udata] = @c290,
		[pcusto] = @c291,
		[pcpond] = @c292,
		[qttacin] = @c293,
		[qttacout] = @c294,
		[qttvend] = @c295,
		[pmvenda] = @c296,
		[valin] = @c297,
		[valout] = @c298,
		[stmax] = @c299,
		[stmin] = @c300,
		[obs] = @c301,
		[codigo] = @c302,
		[uni2] = @c303,
		[conversao] = @c304,
		[ivaincl] = @c305,
		[nsujpp] = @c306,
		[ecomissao] = @c307,
		[imagem] = @c308,
		[pv4] = @c309,
		[pv5] = @c310,
		[cpoc] = @c311,
		[containv] = @c312,
		[contacev] = @c313,
		[contareo] = @c314,
		[contacoe] = @c315,
		[peso] = @c316,
		[bloqueado] = @c317,
		[fobloq] = @c318,
		[mfornec] = @c319,
		[mfornec2] = @c320,
		[pentrega] = @c321,
		[consumo] = @c322,
		[baixr] = @c323,
		[despimp] = @c324,
		[mesescon] = @c325,
		[marg1] = @c326,
		[marg2] = @c327,
		[marg3] = @c328,
		[marg4] = @c329,
		[marg5] = @c330,
		[diaspto] = @c331,
		[diaseoq] = @c332,
		[desc4] = @c333,
		[desc5] = @c334,
		[desc6] = @c335,
		[noserie] = @c336,
		[clinica] = @c337,
		[vasilhame] = @c338,
		[pbruto] = @c339,
		[volume] = @c340,
		[usalote] = @c341,
		[texteis] = @c342,
		[garantia] = @c343,
		[opendata] = @c344,
		[faminome] = @c345,
		[usr3] = @c346,
		[usr4] = @c347,
		[usr5] = @c348,
		[qttesp] = @c349,
		[epv2] = @c350,
		[epv3] = @c351,
		[epv4] = @c352,
		[epv5] = @c353,
		[epcusto] = @c354,
		[epcpond] = @c355,
		[epcult] = @c356,
		[epmvenda] = @c357,
		[epvultimo] = @c358,
		[epvcon] = @c359,
		[iva1incl] = @c360,
		[iva2incl] = @c361,
		[iva3incl] = @c362,
		[iva4incl] = @c363,
		[iva5incl] = @c364,
		[ivapcincl] = @c365,
		[stns] = @c366,
		[tipodesc] = @c367,
		[usr6] = @c368,
		[convunsup] = @c369,
		[nccod] = @c370,
		[massaliq] = @c371,
		[url] = @c372,
		[vaiwww] = @c373,
		[iectsug] = @c374,
		[iectin] = @c375,
		[eiectin] = @c376,
		[iectinii] = @c377,
		[codfiscal] = @c378,
		[iecasug] = @c379,
		[iecagrad] = @c380,
		[iecaref] = @c381,
		[iecarefnome] = @c382,
		[txieca] = @c383,
		[txiecanome] = @c384,
		[iecautt] = @c385,
		[iecamultgrad] = @c386,
		[iecaisref] = @c387,
		[site] = @c388,
		[qlook] = @c389,
		[txtqlook] = @c390,
		[imgqlook] = @c391,
		[lang1] = @c392,
		[langdes1] = @c393,
		[lang2] = @c394,
		[langdes2] = @c395,
		[lang3] = @c396,
		[langdes3] = @c397,
		[lang4] = @c398,
		[langdes4] = @c399,
		[lang5] = @c400,
		[langdes5] = @c401,
		[nexist] = @c402,
		[statuspda] = @c403,
		[qttcat] = @c404,
		[compnovo] = @c405,
		[ecovalor] = @c406,
		[eecoval] = @c407,
		[ecoval] = @c408,
		[ecopl] = @c409,
		[eecopval] = @c410,
		[ecopval] = @c411,
		[ecoel] = @c412,
		[eecoeval] = @c413,
		[ecoeval] = @c414,
		[ecorl] = @c415,
		[eecorval] = @c416,
		[ecorval] = @c417,
		[ecool] = @c418,
		[eecooval] = @c419,
		[ecooval] = @c420,
		[ecopilha] = @c421,
		[ecoacumulador] = @c422,
		[ousrinis] = @c423,
		[ousrdata] = @c424,
		[ousrhora] = @c425,
		[usrinis] = @c426,
		[usrdata] = @c427,
		[usrhora] = @c428,
		[marcada] = @c429,
		[qtttouch] = @c430,
		[semserprv] = @c431,
		[contaieo] = @c432,
		[inactivo] = @c433,
		[orcamento] = @c434,
		[refmo] = @c435,
		[descrmo] = @c436,
		[pcmo] = @c437,
		[epcmo] = @c438,
		[custo] = @c439,
		[ecusto] = @c440,
		[desci] = @c441,
		[descii] = @c442,
		[datar] = @c443,
		[pcdisp] = @c444,
		[epcdisp] = @c445,
		[pclab] = @c446,
		[epclab] = @c447,
		[custof] = @c448,
		[ecustof] = @c449,
		[ofcstamp] = @c450,
		[custog] = @c451,
		[ecustog] = @c452,
		[unidadef] = @c453,
		[codcmb] = @c454,
		[cancpos] = @c455,
		[datanovpv] = @c456,
		[horanovpv] = @c457,
		[tkhclass] = @c458,
		[stocktch] = @c459,
		[mod] = @c460,
		[restctprep] = @c461,
		[idudesign] = @c462,
		[notimpcp] = @c463,
		[impfuelpos] = @c464,
		[amostra] = @c465,
		[sujinv] = @c466,
		[u_cpautal] = @c467,
		[u_mondego] = @c468,
		[u_fermat] = @c469,
		[u_impor] = @c470,
		[u_frete] = @c471,
		[u_seguro] = @c472,
		[u_csubfam] = @c473,
		[u_subfam] = @c474,
		[u_ctpart] = @c475,
		[u_tpart] = @c476,
		[u_dcpautal] = @c477,
		[u_kit] = @c478,
		[u_local2] = @c479,
		[u_local3] = @c480,
		[u_local4] = @c481,
		[u_local5] = @c482,
		[u_local6] = @c483,
		[sujretirs] = @c484,
		[u_design] = @c485,
		[u_circuito] = @c486,
		[u_natureza] = @c487,
		[u_pais] = @c488,
		[u_nfornec1] = @c489,
		[u_fornec1] = @c490,
		[u_rfornec1] = @c491,
		[u_dtblo] = @c492,
		[u_userblo] = @c493,
		[u_dtfoblo] = @c494,
		[u_userfobl] = @c495,
		[u_userina] = @c496,
		[u_dtinact] = @c497,
		[u_ttssinc] = @c498,
		[u_lojaweb] = @c499,
		[u_ulojaweb] = @c500,
		[u_lwstock] = @c501,
		[u_prljweb] = @c502,
		[u_prcrweb] = @c503,
		[u_destaque] = @c504,
		[u_didestq] = @c505,
		[u_dfdestq] = @c506,
		[ecoequipamento] = @c507,
		[u_vimpr] = @c508
	where [ref] = @c2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ref] = ' + convert(nvarchar(100),@c2,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[st]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[st] set
		[ststamp] = @c255,
		[design] = @c257,
		[familia] = @c258,
		[stock] = @c259,
		[epv1] = @c260,
		[pv1] = @c261,
		[forref] = @c262,
		[fornecedor] = @c263,
		[desc3] = @c264,
		[desc2] = @c265,
		[desc1] = @c266,
		[usr2] = @c267,
		[usr1] = @c268,
		[validade] = @c269,
		[usaid] = @c270,
		[uintr] = @c271,
		[usrqtt] = @c272,
		[eoq] = @c273,
		[pcult] = @c274,
		[pcimp] = @c275,
		[pcmoe] = @c276,
		[pvcon] = @c277,
		[pvultimo] = @c278,
		[pv3] = @c279,
		[pv2] = @c280,
		[unidade] = @c281,
		[ptoenc] = @c282,
		[tabiva] = @c283,
		[local] = @c284,
		[fornec] = @c285,
		[fornestab] = @c286,
		[qttfor] = @c287,
		[qttcli] = @c288,
		[qttrec] = @c289,
		[udata] = @c290,
		[pcusto] = @c291,
		[pcpond] = @c292,
		[qttacin] = @c293,
		[qttacout] = @c294,
		[qttvend] = @c295,
		[pmvenda] = @c296,
		[valin] = @c297,
		[valout] = @c298,
		[stmax] = @c299,
		[stmin] = @c300,
		[obs] = @c301,
		[codigo] = @c302,
		[uni2] = @c303,
		[conversao] = @c304,
		[ivaincl] = @c305,
		[nsujpp] = @c306,
		[ecomissao] = @c307,
		[imagem] = @c308,
		[pv4] = @c309,
		[pv5] = @c310,
		[cpoc] = @c311,
		[containv] = @c312,
		[contacev] = @c313,
		[contareo] = @c314,
		[contacoe] = @c315,
		[peso] = @c316,
		[bloqueado] = @c317,
		[fobloq] = @c318,
		[mfornec] = @c319,
		[mfornec2] = @c320,
		[pentrega] = @c321,
		[consumo] = @c322,
		[baixr] = @c323,
		[despimp] = @c324,
		[mesescon] = @c325,
		[marg1] = @c326,
		[marg2] = @c327,
		[marg3] = @c328,
		[marg4] = @c329,
		[marg5] = @c330,
		[diaspto] = @c331,
		[diaseoq] = @c332,
		[desc4] = @c333,
		[desc5] = @c334,
		[desc6] = @c335,
		[noserie] = @c336,
		[clinica] = @c337,
		[vasilhame] = @c338,
		[pbruto] = @c339,
		[volume] = @c340,
		[usalote] = @c341,
		[texteis] = @c342,
		[garantia] = @c343,
		[opendata] = @c344,
		[faminome] = @c345,
		[usr3] = @c346,
		[usr4] = @c347,
		[usr5] = @c348,
		[qttesp] = @c349,
		[epv2] = @c350,
		[epv3] = @c351,
		[epv4] = @c352,
		[epv5] = @c353,
		[epcusto] = @c354,
		[epcpond] = @c355,
		[epcult] = @c356,
		[epmvenda] = @c357,
		[epvultimo] = @c358,
		[epvcon] = @c359,
		[iva1incl] = @c360,
		[iva2incl] = @c361,
		[iva3incl] = @c362,
		[iva4incl] = @c363,
		[iva5incl] = @c364,
		[ivapcincl] = @c365,
		[stns] = @c366,
		[tipodesc] = @c367,
		[usr6] = @c368,
		[convunsup] = @c369,
		[nccod] = @c370,
		[massaliq] = @c371,
		[url] = @c372,
		[vaiwww] = @c373,
		[iectsug] = @c374,
		[iectin] = @c375,
		[eiectin] = @c376,
		[iectinii] = @c377,
		[codfiscal] = @c378,
		[iecasug] = @c379,
		[iecagrad] = @c380,
		[iecaref] = @c381,
		[iecarefnome] = @c382,
		[txieca] = @c383,
		[txiecanome] = @c384,
		[iecautt] = @c385,
		[iecamultgrad] = @c386,
		[iecaisref] = @c387,
		[site] = @c388,
		[qlook] = @c389,
		[txtqlook] = @c390,
		[imgqlook] = @c391,
		[lang1] = @c392,
		[langdes1] = @c393,
		[lang2] = @c394,
		[langdes2] = @c395,
		[lang3] = @c396,
		[langdes3] = @c397,
		[lang4] = @c398,
		[langdes4] = @c399,
		[lang5] = @c400,
		[langdes5] = @c401,
		[nexist] = @c402,
		[statuspda] = @c403,
		[qttcat] = @c404,
		[compnovo] = @c405,
		[ecovalor] = @c406,
		[eecoval] = @c407,
		[ecoval] = @c408,
		[ecopl] = @c409,
		[eecopval] = @c410,
		[ecopval] = @c411,
		[ecoel] = @c412,
		[eecoeval] = @c413,
		[ecoeval] = @c414,
		[ecorl] = @c415,
		[eecorval] = @c416,
		[ecorval] = @c417,
		[ecool] = @c418,
		[eecooval] = @c419,
		[ecooval] = @c420,
		[ecopilha] = @c421,
		[ecoacumulador] = @c422,
		[ousrinis] = @c423,
		[ousrdata] = @c424,
		[ousrhora] = @c425,
		[usrinis] = @c426,
		[usrdata] = @c427,
		[usrhora] = @c428,
		[marcada] = @c429,
		[qtttouch] = @c430,
		[semserprv] = @c431,
		[contaieo] = @c432,
		[inactivo] = @c433,
		[orcamento] = @c434,
		[refmo] = @c435,
		[descrmo] = @c436,
		[pcmo] = @c437,
		[epcmo] = @c438,
		[custo] = @c439,
		[ecusto] = @c440,
		[desci] = @c441,
		[descii] = @c442,
		[datar] = @c443,
		[pcdisp] = @c444,
		[epcdisp] = @c445,
		[pclab] = @c446,
		[epclab] = @c447,
		[custof] = @c448,
		[ecustof] = @c449,
		[ofcstamp] = @c450,
		[custog] = @c451,
		[ecustog] = @c452,
		[unidadef] = @c453,
		[codcmb] = @c454,
		[cancpos] = @c455,
		[datanovpv] = @c456,
		[horanovpv] = @c457,
		[tkhclass] = @c458,
		[stocktch] = @c459,
		[mod] = @c460,
		[restctprep] = @c461,
		[idudesign] = @c462,
		[notimpcp] = @c463,
		[impfuelpos] = @c464,
		[amostra] = @c465,
		[sujinv] = @c466,
		[u_cpautal] = @c467,
		[u_mondego] = @c468,
		[u_fermat] = @c469,
		[u_impor] = @c470,
		[u_frete] = @c471,
		[u_seguro] = @c472,
		[u_csubfam] = @c473,
		[u_subfam] = @c474,
		[u_ctpart] = @c475,
		[u_tpart] = @c476,
		[u_dcpautal] = @c477,
		[u_kit] = @c478,
		[u_local2] = @c479,
		[u_local3] = @c480,
		[u_local4] = @c481,
		[u_local5] = @c482,
		[u_local6] = @c483,
		[sujretirs] = @c484,
		[u_design] = @c485,
		[u_circuito] = @c486,
		[u_natureza] = @c487,
		[u_pais] = @c488,
		[u_nfornec1] = @c489,
		[u_fornec1] = @c490,
		[u_rfornec1] = @c491,
		[u_dtblo] = @c492,
		[u_userblo] = @c493,
		[u_dtfoblo] = @c494,
		[u_userfobl] = @c495,
		[u_userina] = @c496,
		[u_dtinact] = @c497,
		[u_ttssinc] = @c498,
		[u_lojaweb] = @c499,
		[u_ulojaweb] = @c500,
		[u_lwstock] = @c501,
		[u_prljweb] = @c502,
		[u_prcrweb] = @c503,
		[u_destaque] = @c504,
		[u_didestq] = @c505,
		[u_dfdestq] = @c506,
		[ecoequipamento] = @c507,
		[u_vimpr] = @c508
	where [ref] = @c2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ref] = ' + convert(nvarchar(100),@c2,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[st]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','st',@c1, 'ststamp', 'stid'