**********************************************
*** Cativa Stock Encomendas Web, depois de pagas                                                                            
***
*** Criado por : Rui Vale
***  Criado em : 02.09.2020
**********************************************

If Alltrim(lower(bo.tabela1))=="paga"
	Text to uSql textmerge noshow
		Update bi set cativo=1,armazem=2001
		Where bostamp='<<bo.bostamp>>'
		And ref<>''
		And qtt > qtt2
		And cativo=0
	Endtext
Else
	Text to uSql textmerge noshow
		Update bi set cativo=0,armazem=2001
		Where bostamp='<<bo.bostamp>>'
		And ref<>''
		And qtt > qtt2
		And cativo=1
	Endtext
Endif
If u_Sqlexec(uSql)
Endif