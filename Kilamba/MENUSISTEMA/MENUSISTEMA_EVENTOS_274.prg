** J�lio Ricardo
** 2018-01-26

Public p_estab,p_ccusto,p_supervisor,p_armazem,p_armtrans
Public p_dielocal,p_dieencforn,p_dietrfarm,p_dieclimist,p_diepedloj
Public p_dieentrmat,p_diequebra,p_dieconsint,p_dieenccli

p_ccusto=''
p_estab =  0
TEXT TO msel NOSHOW TEXTMERGE
	SELECT  top 1 u_estab, ccusto, u_armazem,u_armtrans FROM e1 (nolock) WHERE  estab = 0
ENDTEXT
If Not  u_sqlexec(msel,"c_e1")
	msg(msel)
	Return
Endif

p_estab =  c_e1.u_estab
p_armazem =  c_e1.u_armazem
p_armtrans =  c_e1.u_armtrans
p_ccusto =  c_e1.ccusto
p_supervisor = ""

fecha("c_e1")

p_dielocal = Tts_ValidaNdos("u_elocal")
p_dieencforn = Tts_ValidaNdos("u_eencforn")
p_dietrfarm = Tts_ValidaNdos("u_etrfarm")
p_dieclimist = Tts_ValidaNdos("u_eclimist")
p_diepedloj = Tts_ValidaNdos("u_epedloj")
p_dieentrmat = Tts_ValidaNdos("u_eentrmat")
p_diequebra = Tts_ValidaNdos("u_equebra")
p_dieconsint = Tts_ValidaNdos("u_econsint")
p_dieenccli = Tts_ValidaNdos("u_eenccli")

Function Tts_ValidaNdos
	Lparameters uCampoConfig

	Local uRetval 
	uRetval = 0
	TEXT TO msel NOSHOW TEXTMERGE
		SELECT  TOP 1 ndos
		FROM ts (nolock)
		Where <<ALLTRIM(uCampoConfig)>> = 1
		And u_estab = <<Astr(p_estab)>>
	ENDTEXT

	If u_sqlexec(msel,"c_ts") And Reccount("c_ts")>0
		uRetval = c_ts.ndos
	Endif
	Fecha("c_ts")
	Return uRetval 
Endfunc









