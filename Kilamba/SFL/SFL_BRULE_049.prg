*************************************************
*** Valida se pode apagar o fornecedor   
***
*** Criado por Rui Vale
***  Criado em 28/09/2017
*************************************************

TEXT TO uSql TEXTMERGE noshow
	Select e1.u_noflini,e1.u_noflfim
	From e1 (nolock)
	Where e1.estab = 0
	And u_noflini <> 0
	And u_noflfim <> 0
ENDTEXT

If u_sqlexec(uSql,"uCurE1") And Reccount("uCurE1")>0
	Select fl
	If ! Between(fl.no,uCurE1.u_noflini,uCurE1.u_noflfim)
		Msg("O fornecedor n�o � deste estabelecimento, n�o pode alterar!!")
		Return .F.
	Endif
Endif

Return .T.
