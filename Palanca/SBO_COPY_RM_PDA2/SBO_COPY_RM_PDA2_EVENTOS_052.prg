** Criado por: Vasco Rocha
** Data cria��o: 08.09.2015
if not (sbo.adding or sbo.editing)
	msg("O dossier tem que est�r em modo de edi��o")
	return 
endif 
if bo.no = 0 
	msg("Introduza o Fornecedor da Entrada de Material")
	return 
endif 
*!*	nrforn = '' 
*!*	nrforn =  getnome("Introduza n� da encomenda do fornecedor ou o codigo de barras",nrforn)
** Filtro da Encomenda
IF VARTYPE(p_monitor) = 'U'
	IF VARTYPE(p_mano) = 'U' &&AND VARTYPE(p_nrforn) = 'U'
		PUBLIC p_mano &&, p_nrforn 
	ENDIF
	Create Cursor xVars ( no N(5), tipo c(1), Nome c(40), Pict c(100), lOrdem N(10), nValor N(18,5), cValor c(250), lValor l, dValor d,tBval M) 
	* prencher os dados de cada variavel a pedir 
	Select xVars 
	Append Blank 
	Replace xVars.no With 1 
	Replace xVars.tipo With "N" 
	Replace xVars.Nome With "Ano da Encomenda" 
	Replace xVars.Pict With "####" 
	Replace xVars.lOrdem With 1 
	Replace xVars.nValor With year(Date())
*!*		Select xVars 
*!*		Append Blank 
*!*		Replace xVars.no With 2 
*!*		Replace xVars.tipo With "N" 
*!*		Replace xVars.Nome With "N� Encomenda ou C�d. de Barras" 
*!*		Replace xVars.Pict With "###########" 
*!*		Replace xVars.lOrdem With 2 
*!*		Replace xVars.nValor With 0 

	m.Escolheu = .F. 
	m.mCaption = "Qual a Encomenda de Fornecedor a pesquisar." 
	docomando("do form usqlvar with 'xvars',m.mCaption") 
	If Not m.Escolheu 
		mensagem("Atribui��o interrompida!","DIRECTA") 
		Return .F. 
	Else 
		Select xVars 
		Locate 
		p_mano = xVars.nValor 
*!*			Select xVars 
*!*			Skip 
*!*			p_nrforn = xVars.nValor 
	Endif 
*!*		IF empty(p_nrforn)
*!*			msg('N�o foi indicado o ano da encomenda.')
*!*			RETURN
*!*		ENDIF
	IF empty(p_mano)
		msg('N�o foi indicado o n� da encomenda.')
		RETURN
	ENDIF
ENDIF

TEXT TO  msel NOSHOW TEXTMERGE
	SELECT 	CAST(1 as bit) Sel, mastamp, bostamp, obrano, nome, no, estab, convert(char(10),dataobra,105) dataobra
	FROM 	bo (nolock)
	inner	join bo3 (nolock) on bo.bostamp = bo3.bo3stamp
	where 	ndos in (2,938) and  fechada = 0 and emconf = 0 and bo.no = <<astr(bo.no)>> 
			and bo.boano = <<p_mano>> 
			and  (bo.obrano = <<p_nrforn>> or barcode = '<<astr(p_nrforn)>>')
ENDTEXT
IF NOT u_sqlexec(msel,"c_botemp")
	msg('Erro ao consultar Encomenda a Fornecedor.')
	RETURN .f.
ENDIF
SELECT c_botemp
IF RECCOUNT("c_botemp") = 0
	msg("A encomenda n�o existe ou est� fechada")
	fecha("c_botemp")
	RETURN
ENDIF
=CURSORSETPROP('Buffering',5,"c_botemp")
m.ncol = 5
DECLARE list_tit(m.ncol),list_cam(m.ncol),list_tam(m.ncol),list_pic(m.ncol), list_ronly(m.ncol)
m.ncol = 1
list_tit(m.ncol)   = "Sel"
list_cam(m.ncol)   = "c_botemp.Sel"
list_tam(m.ncol)   = 50
list_pic(m.ncol)   = "LOGIC"
list_ronly(m.ncol) = .F.
m.ncol = m.ncol + 1
list_tit(m.ncol)   = "Cliente"
list_cam(m.ncol)   = "c_botemp.nome"
list_tam(m.ncol)   = 50*3
list_pic(m.ncol)   = ""
list_ronly(m.ncol) = .T.
m.ncol = m.ncol + 1
list_tit(m.ncol)   = "N� Cliente"
list_cam(m.ncol)   = "c_botemp.no"
list_tam(m.ncol)   = 50*2
list_pic(m.ncol)   = ""
list_ronly(m.ncol) = .T.
m.ncol = m.ncol + 1
list_tit(m.ncol)   = "Data"
list_cam(m.ncol)   = "c_botemp.dataobra"
list_tam(m.ncol)   = 50*2
list_pic(m.ncol)   = ""
list_ronly(m.ncol) = .T.
m.ncol = m.ncol + 1
list_tit(m.ncol)   = "N� Encomenda"
list_cam(m.ncol)   = "c_botemp.obrano"
list_tam(m.ncol)   = 50
list_pic(m.ncol)   = ""
list_ronly(m.ncol) = .T.	
m.Escolheu=.F.
*browlist('Listagem de documentos a emitir','c_botemp', 'c_botempTemp',.T.,.F.,.F.,.T.,.T.,'',.T.,.T.)
IF 0 = 0 && m.Escolheu
	SELECT c_botemp
	SCAN 
		IF c_botemp.sel
			TEXT TO msel NOSHOW TEXTMERGE
				select boo.nmdos, boo.obrano, boo.bistamp,boo.ref, boo.design,boo.edebito, boo.desconto,boo.desc2,  boo.qtt, boo.qtt2, boo.pendente 
				from
					(select bo.nmdos, bo.obrano, ref,design , bistamp, bi.edebito ,bi.desconto,  bi.desc2
							,qtt, qtt2 ,qtt-qtt2 pendente, bi.lordem
					from 	bo (nolock)
					inner 	join  bi (nolock) on bo.bostamp = bi.bostamp
					where 	bo.ndos in(2)  and  bo.bostamp = '<<c_botemp.bostamp>>' and ref <> '' and bo.fechada = 0
							and  bi.fechada = 0
							and  qtt-qtt2 > 0 ) boo
				order by lordem
			ENDTEXT
			IF NOT u_sqlexec(msel,"c_bitemp")
				msg('Erro ao consultar dados da Encomenda.')
				RETURN .f.
			ENDIF
			*fecha("c_botemp")
			SELECT c_bitemp
			IF RECCOUNT("c_bitemp") = 0
				msg("A encomenda n�o existe ou est� fechada")
				fecha("c_bitemp")
				RETURN
			ENDIF
			**SELECT  c_bitemp
			*mostrameisto("c_bitemp")
			select c_bitemp
			goto top 
			SELECT bi
			goto bottom
			LOCATE FOR bi.design = RTRIM(c_bitemp.nmdos) + ' n� ' +  astr(c_bitemp.obrano)
			IF NOT FOUND()
				SELECT bi
				DO Boine2in
				REPLACE bi.DESIGN  WITH  RTRIM(c_bitemp.nmdos) + ' n� ' +  astr(c_bitemp.obrano)
			ENDIF 
			SELECT  c_bitemp
			SCAN FOR  c_bitemp.pendente  > 0
				SELECT bi
				GOTO BOTTOM 
				LOCATE FOR UPPER(ALLTRIM(bi.ref)) == UPPER(ALLTRIM(c_bitemp.ref)) AND ALLTRIM(bi.oobistamp) = ALLTRIM(c_bitemp.bistamp)
				IF NOT FOUND()
					SELECT bi
					DO Boine2in
					SELECT bi
					REPLACE bi.ref WITH  c_bitemp.ref
					DO BOACTREF WITH '',.T.,'OKPRECOS','BI'
					REPLACE bi.obistamp WITH  c_bitemp.bistamp
					REPLACE bi.oobistamp WITH  c_bitemp.bistamp
					REPLACE bi.qtt WITH  c_bitemp.pendente 
					REPLACE bi.edebito WITH  c_bitemp.edebito
					REPLACE bi.desconto WITH  c_bitemp.desconto
					REPLACE bi.desc2 WITH  c_bitemp.desc2
					*REPLACE bi.series WITH  c_bitemp.u_pdarmstamp
					DO u_bottdeb WITH 'BI',.F.
				ENDIF
			ENDSCAN
		ENDIF 
	ENDSCAN 
ELSE
	msg('Opera��o Cancelada!')
	RETURN .f.
ENDIF
DO BOTOTS WITH .T.
SELECT bi
GOTO TOP
**SELECT bi 
**BROWSE 
docomando("EventosExe('sbo_confere_pda','introduzir',.f.)")
sbo.REFRESH()
IF VARTYPE(p_monitor) <> 'U'
	release('p_monitor')
ENDIF 
release('p_mano')
**release('p_nrforn')
