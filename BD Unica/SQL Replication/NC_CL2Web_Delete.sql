USE [FERMAT_LOJA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbocl2]    Script Date: 22/02/2023 18:43:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_CL2Web_Delete]
		@c1 char(25),
		@c2 varchar(2),
		@c3 varchar(55),
		@c4 bit,
		@c5 varchar(30),
		@c6 datetime,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 bit,
		@c12 bit,
		@c13 bit,
		@c14 varchar(55),
		@c15 numeric(10,0),
		@c16 numeric(3,0),
		@c17 varchar(10),
		@c18 char(25),
		@c19 varchar(10),
		@c20 char(25),
		@c21 varchar(10),
		@c22 char(25),
		@c23 varchar(10),
		@c24 char(25),
		@c25 bit,
		@c26 varchar(20),
		@c27 varchar(50),
		@c28 datetime,
		@c29 varchar(8),
		@c30 varchar(6),
		@c31 bit,
		@c32 bit,
		@c33 bit,
		@c34 numeric(10,6),
		@c35 numeric(10,6),
		@c36 varchar(6),
		@c37 varchar(40),
		@c38 varchar(15),
		@c39 varchar(5),
		@c40 bit,
		@c41 bit,
		@c42 varchar(40),
		@c43 datetime,
		@c44 bit,
		@c45 varchar(4),
		@c46 varchar(11),
		@c47 varchar(15),
		@c48 varchar(3),
		@c49 varchar(3),
		@c50 varchar(250),
		@c51 varchar(4),
		@c52 varchar(40),
		@c53 varchar(2),
		@c54 varchar(55),
		@c55 bit,
		@c56 numeric(1,0),
		@c57 numeric(10,0),
		@c58 numeric(5,2),
		@c59 numeric(6,0),
		@c60 bit,
		@c61 bit,
		@c62 bit,
		@c63 datetime,
		@c64 varchar(15),
		@c65 varchar(15),
		@c66 varchar(55),
		@c67 text,
		@c68 varchar(40),
		@c69 varchar(40),
		@c70 varchar(40),
		@c71 numeric(10,2),
		@c72 bit,
		@c73 varchar(20),
		@c74 numeric(6,0),
		@c75 varchar(3),
		@c76 varchar(30),
		@c77 varchar(30),
		@c78 bit,
		@c79 numeric(16,2),
		@c80 numeric(8,0),
		@c81 datetime,
		@c82 varchar(10),
		@c83 varchar(10),
		@c84 varchar(254)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PL].[cl2] 
	where [cl2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[cl2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PL].[cl2]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from cl2 where cl2stamp=@c1