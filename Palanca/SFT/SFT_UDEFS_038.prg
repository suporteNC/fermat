*******************************************
** Guarda estabelecimento
**
** Criado por Rui Vale
** Criado em  22/09/2017
*******************************************

Select ft

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From td(nolock)
	Where td.ndoc=<<STR(ft.ndoc)>>
ENDTEXT

If u_sqlexec(uSql,"uCurTd") And Reccount("uCurTd")>0
	Select uCurTd
	Replace ft.u_estab With uCurTd.u_estab

	fecha("uCurTd")
	
Endif


Return ft.u_estab