
if atualizarMovDossiers()

    doread("stic", "sstic")
    relancarInv()
endif 

PROCEDURE atualizarMovDossiers()

    local mSql, lcCrsSql
    lcCrsSql = "crsSql"
    text to mSql noshow textmerge
        
        update bi
        set bi.qtt=bi.qtt, bi.marcada=1
        --select * 
        from bi inner join ts on bi.ndos = ts.ndos inner join bo2 on bi.bostamp = bo2.bo2stamp 
        where 
            ts.stocks = 1 and bistamp not in (select bistamp from sl) and bi.dataobra>'20221231' and bo2.anulado=0 and bi.ref != '' 
            --((ts.trfa=1 and (bi.armazem!=bi.ar2mazem or bi.alvst) ) or )
    endtext
    
    fecha(lcCrsSql)
    if !u_sqlexec(mSql, lcCrsSql)

        msg("Erro atualizarMovDossiers")
        return .f.
    endif
    fecha(lcCrsSql)
    
ENDPROC 

PROCEDURE relancarInv()

    text to msel noshow textmerge 

        select stic.sticstamp, descricao, data, hora
        from stic 
        where year(stic.data) = 2023 and lanca = 1
        order by data, hora, descricao
    endtext 


    fecha("crstmpstic")
    if u_sqlexec(msel, "crstmpstic") and reccount("crstmpstic")>0 

        dbfusestic()
        dbfusestil()
        select crstmpstic
        go top 
        regua(0, reccount(), "")
        scan 
            regua(1, recno(), "")
            v_sticstamp = crstmpstic.sticstamp
            v_stilsticstamp = crstmpstic.sticstamp
            if u_requery("stic") and u_requery("stil")

                if anularInv() 
                    if type("sstic.sstictimer1")="U"
                        sstic.AddObject("sstictimer1","sstictimer")
                        sstic.sstictimer1.interval = 1
                        sstic.sstictimer1.enabled = .t.
                    else    
                        sstic.sstictimer1.interval = 1
                        sstic.sstictimer1.enabled = .t.
                    endif
                    doread("sticlan", "sticlan")
                endif 
            endif 
        endscan 
        regua(2)
    endif
    fecha("crstmpstic")
ENDPROC


PROCEDURE anularInv() 

    select stic
    replace stic.lanca with .f.
    REPLACE stic.usrdata with DATE()
    REPLACE stic.usrinis with m_chinis
    REPLACE stic.usrhora with TIME()
    if u_tabupdate(.t.,.t.,"STIC")
        m.msel="DELETE from SL where sl.sticstamp='"+stic.sticstamp+"' "
        if !u_sqlexec(m.msel)
            mensagem("stic.naoslapagados")	
            return .f.
        endif	
    else
        tablerevert(.f.,"STIC")
        mensagem("stic.naoslapagados")	
        return .f.
    endif
ENDPROC


DEFINE CLASS sstictimer AS timer

	PROCEDURE timer()

		if type("sticlan") == "O" 
			
            sstic.sstictimer1.interval = 999999
            sstic.sstictimer1.enabled = .f.
            sstic.removeobject("sstictimer1")

			select stic
			sticlan.adocout.value = stic.descricao
			sticlan.adocin.value 	= stic.descricao
			sticlan.descin.value  = "Dif. Invent Positiva Entrada"
			sticlan.descout.value = "Dif. Invent Negativa Saida"
			sticlan.Bok1.Okbuttomdef1.CLICK()   
		endif
	ENDPROC
ENDDEFINE