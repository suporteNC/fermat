USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_attachment]    Script Date: 06/09/2023 17:36:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_UATT_Update]
		@c1 varchar(25),
		@c2 text,
		@c3 text,
		@c4 varchar(10),
		@c5 varchar(25),
		@c6 date,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 date,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 bit,
		@c13 text,
		@c14 varchar(25),
		@c15 text,
		@c16 text,
		@c17 varchar(10),
		@c18 varchar(25),
		@c19 date,
		@c20 varchar(8),
		@c21 varchar(30),
		@c22 date,
		@c23 varchar(8),
		@c24 varchar(30),
		@c25 bit,
		@c26 text
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c14 = @c1)
begin 
update [dbo].[u_attachment] set
		[u_attachmentstamp] = @c14,
		[attachment] = @c15,
		[description] = @c16,
		[oritable] = @c17,
		[oritablestamp] = @c18,
		[ousrdata] = @c19,
		[ousrhora] = @c20,
		[ousrinis] = @c21,
		[usrdata] = @c22,
		[usrhora] = @c23,
		[usrinis] = @c24,
		[marcada] = @c25,
		[filename] = @c26
	where [u_attachmentstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_attachmentstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[u_attachment]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[u_attachment] set
		[attachment] = @c15,
		[description] = @c16,
		[oritable] = @c17,
		[oritablestamp] = @c18,
		[ousrdata] = @c19,
		[ousrhora] = @c20,
		[ousrinis] = @c21,
		[usrdata] = @c22,
		[usrhora] = @c23,
		[usrinis] = @c24,
		[marcada] = @c25,
		[filename] = @c26
	where [u_attachmentstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_attachmentstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[u_attachment]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','u_attachment',@c1, 'u_attachmentstamp', ''