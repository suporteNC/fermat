Public sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp,xcmpcl, xcanfat, xavvend, xgrvcob, xdiasf, lmoeda, lcampoMoeda, m_Attachments, l_openclient, l_quiet, c_categories, m_toerro, x_inst, x_cl

**
** Ver se � PT ou Africa
**
x_inst=0 && PT
u_sqlexec("select top 1 case when pack like '%Africa%' then '�frica' else 'PT' end pack from exev  order by ousrdata desc","curinst")
If curinst.Pack<>'PT'
	x_inst=1
Endif

**
** Ler defini��es da ficha da empresa
**
fecha("crse4")
TEXT to msel textmerge noshow
select u_cobemenv,u_cobserv,u_cobuser,u_cobpass,u_cobtlig,u_cobporta,u_cobcamcl,u_cobcancf,u_cobavven,u_cobgcob,u_cobndias,u_cobpmoe,u_cobmoe, u_emailerr
from e4 inner join e1 on e4stamp=e1stamp where e1.estab=0
ENDTEXT
u_sqlexec(msel,"crse4")

m_Attachments=""
l_openclient=.F.
l_quiet=.T.
c_categories=""
**
sFromEmail= Alltrim(crse4.u_cobemenv)
m.sSmtpServer= Alltrim(crse4.u_cobserv)
m.sUserSmtp= Alltrim(crse4.u_cobuser)
m.sPassSmtp= Alltrim(crse4.u_cobpass)
m.sPortaSmtp= Alltrim(crse4.u_cobporta)
m.sLigSmtp=Val(crse4.u_cobtlig)
xcmpcl=Alltrim(crse4.u_cobcamcl)
m_toerro=Alltrim(crse4.u_emailerr)
xcanfat=crse4.u_cobcancf
xavvend=crse4.u_cobavven
xgrvcob=crse4.u_cobgcob
xdiasf=crse4.u_cobndias
lmoeda=Alltrim(crse4.u_cobmoe)
lcampoMoeda=Alltrim(crse4.u_cobpmoe)

cobrancas()
**
** Vai cancelar Fatura��o/Encomendas se o parametro estiver a T
**
If xcanfat=.T.
	CancelaFat()
Endif
**DO CQUIT


Procedure cobrancas
	Local mensagem
	m.mensagem = .F.

	*******--------------EMAIL Aviso Vencimento--------------*******
	fecha("crsemail")
	u_sqlexec("select u_aviass, u_avibody, u_avifilt, u_aviven, u_aviemail from e4 inner join e1 on e4stamp=e1stamp where e1.estab=0", "crsemail")
	Select crsemail
	TEXT to msel noshow textmerge
		select distinct cl.clstamp, cl.no, cl.nome, cl.<<xcmpcl>> email, coalesce(cm3.email, '') emailvendedor
		from cl inner join cc on cl.no = cc.no left join cm3 on cm3.cm = cl.vendedor
		where
				cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0
			and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
			and CONVERT(date, getdate())<= CONVERT(date, dataven)
			and CONVERT(date, datalc)<> CONVERT(date, dataven)
			and u_avienvi=0
			and <<lcampoMoeda>>saldo >0
			and cl.inactivo = 0
  	ENDTEXT

	If !Empty(crsemail.u_avifilt)
		msel = msel +  ' and '  + Alltrim(crsemail.u_avifilt)
	Endif

	msel = msel +' order by cl.no'

	fecha("crsdados")
	If u_sqlexec(msel, "crsdados") And Reccount("crsdados")>0

		m.mensagem = .T.
		Select crsdados
		Go Top
		Scan
			TEXT to msel noshow textmerge
		declare @pt bit
		IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.COLUMNS
		WHERE  TABLE_NAME='cc' AND COLUMN_NAME='eivacativado' )
		 set @pt=0
		 else
		 set @pt=1
		select datalc, dataven, cmdesc, convert(varchar(10), convert(decimal(10,0), nrdoc)) nrdoc, convert(varchar(16), convert(decimal(10,2),
		case when @pt=0 then
		 (case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then (<<lcampoMoeda>>cred-<<lcampoMoeda>>credf)-((<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg)*-1) else <<lcampoMoeda>>deb-<<lcampoMoeda>>debf-(<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg) end)
		 else
		 (case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then <<lcampoMoeda>>cred-<<lcampoMoeda>>credf else <<lcampoMoeda>>deb-<<lcampoMoeda>>debf end)
		 end)) + '<<lmoeda>>'  vdivida,
		  DATEDIFF(day, convert(date, getdate()), dataven) atraso, 0 ordem
				from cc
				where
						(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
					and CONVERT(date, getdate())<= CONVERT(date, dataven)
					and u_avienvi=0
					and cc.no =<<crsdados.no>>


				union all

				select '', '', '', '<b>TOTAL</b>', '<b>' + convert(varchar(16), convert(decimal(20,2), sum(<<lcampoMoeda>>deb-<<lcampoMoeda>>debf -(<<lcampoMoeda>>cred-<<lcampoMoeda>>credf))))+ '<<lmoeda>>' + '</b>' vdivida, 0 atraso, 1 ordem
				from cc
				where
						(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
					and CONVERT(date, getdate())<= CONVERT(date, dataven)
					and u_avienvi=0
					and cc.no =<<crsdados.no>>
				order by ordem, cmdesc, dataven, datalc
			ENDTEXT

			fecha("crsdocs")
			If u_sqlexec(msel, "crsdocs") And Reccount("crsdocs")>0
				Declare list_tit(6),list_cam(6),list_pic(6), list_tam(6)

				list_tit(1)="Data Emiss�o"
				list_cam(1)="crsdocs.datalc"
				list_pic(1)=""
				list_tam(1)=8*10

				list_tit(2)="Data Vencimento"
				list_cam(2)="crsdocs.dataven"
				list_pic(2)=""
				list_tam(2)=8*10

				list_tit(3)="Documento"
				list_cam(3)="crsdocs.cmdesc"
				list_pic(3)=""
				list_tam(3)=8*20

				list_tit(4)="N�"
				list_cam(4)="crsdocs.nrdoc"
				list_pic(4)=""
				list_tam(4)=8*10

				list_tit(5)="Valor em D�vida"
				list_cam(5)="crsdocs.vdivida"
				list_pic(5)=""
				list_tam(5)=8*18

				list_tit(6)="Dias para Vencer"
				list_cam(6)="crsdocs.atraso"
				list_pic(6)="########"
				list_tam(6)=8*18

				If Empty(crsemail.u_aviemail)
					If !Empty(crsdados.email)
						m_To = crsdados.email
					Else
						m_To = m_toerro
					Endif

					If !Empty(crsdados.emailvendedor)
						m_ToVend = Alltrim(crsdados.emailvendedor)
					Else
						m_ToVend = ''
					Endif
				Else
					m_To = crsemail.u_aviemail
					m_ToVend = ''
				Endif

                                ****************************************
                                ** SOMENTE PARA A  DELTAQ
                                ****************************************
                                m_ToVend='bruno.vaz@deltaq.pt'

				m_Subject = Alltrim(crsemail.u_aviass)
				m_Body = crsemail.u_avibody
				m_Body = Strtran(m_Body, "#NDIAS#", astr(crsemail.u_aviven))
				m_Body =Strtran(m_Body, "#LISTADOC#", listtohtml("crsdocs",Alltrim(crsdados.Nome) + "<br> Documentos a Vencer nos pr�ximos  " + astr(crsemail.u_aviven) + " dias.",2,.T.))

**				m_body = strtran(m_Body, "GAP, LDA","Securnet, Lda")
**				m_body = strtran(m_Body, "Software PHC CS Advanced"," ")

				Erro=0
				Erro=u_sendmailhtml (m_To, m_Subject, m_Body, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)

				If !Empty(m_ToVend) And xavvend=.T.
					Erro=u_sendmailhtml (m_ToVend, m_Subject, m_Body, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)
				Endif

				If Erro<>0
					m_bodyerro=''
					m_bodyerro='Ocorreram erros ao enviar o email de aviso de vencimento para o cliente '+Alltrim(crsdados.Nome)
					Erro=u_sendmailhtml (m_toerro, "Erros email avisos", m_bodyerro, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)
				Else

					TEXT to msel noshow textmerge
					update cc
					set u_avienvi = 1
					where
							(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
						and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
						and CONVERT(date, getdate())<= CONVERT(date, dataven)
						and u_avienvi=0
						and cc.no =<<crsdados.no>>
					ENDTEXT
					u_sqlexec(msel)
				Endif
			Endif
		Endscan

	Endif

	*******--------------EMAIL COBRAN�AS--------------*******
	fecha("crsemail")
	u_sqlexec("select u_cobass, u_cobbody, u_cobfilt, u_cobper, u_cobven, u_cobemail, u_aviven from e4 inner join e1 on e4stamp=e1stamp where e1.estab=0", "crsemail")
	Select crsemail

	TEXT to msel noshow textmerge
		select distinct cl.clstamp, cl.no, cl.nome,  cl.<<xcmpcl>> email, coalesce(cm3.email, '') emailvendedor
		from cl (nolock) inner join cc on cl.no = cc.no inner join ft2 on ft2.ft2stamp = cc.ftstamp left join cm3 on cm3.cm = cl.vendedor
		where
				cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0
			and CONVERT(date, getdate())>= dateadd(day, <<crsemail.u_cobven>>, dataven)
			and DATEDIFF(day,cl.u_ultenv, convert(date, getdate()))>=<<crsemail.u_cobper>>
			and <<lcampoMoeda>>saldo >0
			and ft2.assinatura <> ''
			and cl.inactivo = 0
	ENDTEXT

	If !Empty(crsemail.u_cobfilt)
		msel = msel + ' and ' + Alltrim(crsemail.u_cobfilt)
	Endif

	msel = msel +' order by cl.no'

	fecha("crsdados")
	If u_sqlexec(msel, "crsdados") And Reccount("crsdados")>0
		m.mensagem = .T.
		Select crsdados
		Go Top
		Scan
			TEXT to msel noshow textmerge
			declare @pt bit
			IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME='cc' AND COLUMN_NAME='eivacativado' )
			 set @pt=0
			 else
			 set @pt=1

				select datalc, dataven, cmdesc, convert(varchar(10), convert(decimal(10,0), nrdoc)) nrdoc,
				convert(varchar(16),
				convert(decimal(10,2),
				case when @pt=0 then
				(case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then (<<lcampoMoeda>>cred-<<lcampoMoeda>>credf)-((<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg)*-1) else (<<lcampoMoeda>>deb-<<lcampoMoeda>>debf)-(<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg) end)
				else
				(case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then <<lcampoMoeda>>cred-<<lcampoMoeda>>credf else <<lcampoMoeda>>deb-<<lcampoMoeda>>debf end)
				end)) + '<<lmoeda>>'  vdivida,
				DATEDIFF(day, dataven, convert(date, getdate())) atraso, 0 ordem
				from cc
				where
					(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					and CONVERT(date, getdate())> dateadd(day, <<crsemail.u_cobper>>, dataven)
					and cc.no =<<crsdados.no>>


				union all

				select '', '', '', '<b>TOTAL</b>', '<b>' + convert(varchar(16), convert(decimal(20,2), sum(<<lcampoMoeda>>deb-<<lcampoMoeda>>debf -(<<lcampoMoeda>>cred-<<lcampoMoeda>>credf))-sum(<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg)))+ '<<lmoeda>>' + '</b>' vdivida, 0 atraso, 1 ordem
				from cc
				where
						(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					and CONVERT(date, getdate())> dateadd(day, <<crsemail.u_cobper>>, dataven)
					and cc.no =<<crsdados.no>>
				order by ordem, cmdesc, dataven, datalc
			ENDTEXT

			fecha("crsdocs")
			If u_sqlexec(msel, "crsdocs") And Reccount("crsdocs")>1
				Declare list_tit(6),list_cam(6),list_pic(6), list_tam(6)

				list_tit(1)="Data Emiss�o"
				list_cam(1)="crsdocs.datalc"
				list_pic(1)=""
				list_tam(1)=8*10

				list_tit(2)="Data Vencimento"
				list_cam(2)="crsdocs.dataven"
				list_pic(2)=""
				list_tam(2)=8*10

				list_tit(3)="Documento"
				list_cam(3)="crsdocs.cmdesc"
				list_pic(3)=""
				list_tam(3)=8*20

				list_tit(4)="N�"
				list_cam(4)="crsdocs.nrdoc"
				list_pic(4)=""
				list_tam(4)=8*10

				list_tit(5)="Valor em D�vida"
				list_cam(5)="crsdocs.vdivida"
				list_pic(5)=""
				list_tam(5)=8*25

				list_tit(6)="Dias de Atraso"
				list_cam(6)="crsdocs.atraso"
				list_pic(6)="########"
				list_tam(6)=8*18

				If Empty(crsemail.u_cobemail)
					If !Empty(crsdados.email)
						m_To = crsdados.email
					Else
						m_To = m_toerro
					Endif

					If !Empty(crsdados.emailvendedor) And xavvend=.T.
						m_ToVend = Alltrim(crsdados.emailvendedor)
					Else
						m_ToVend = ''
					Endif
				Else
					m_To = crsemail.u_cobemail
					m_ToVend = ''
				Endif

                                ****************************************
                                ** SOMENTE PARA A  DELTAQ
                                ****************************************
                                m_ToVend='bruno.vaz@deltaq.pt'


				m_Subject = Alltrim(crsemail.u_cobass)
				m_Body = crsemail.u_cobbody
				m_Body =Strtran(m_Body, "#LISTAVEN#", listtohtml("crsdocs",Alltrim(crsdados.Nome) + "<br> Documentos Vencidos em " + astr(Date()),2,.T.))

				**
				** Adiciona lista de documentos a vencer
				**
				msel1=''
				TEXT to msel1 noshow textmerge
		declare @pt bit
		IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.COLUMNS
		WHERE  TABLE_NAME='cc' AND COLUMN_NAME='eivacativado' )
		 set @pt=0
		 else
		 set @pt=1
		select datalc, dataven, cmdesc, convert(varchar(10), convert(decimal(10,0), nrdoc)) nrdoc, convert(varchar(16), convert(decimal(10,2),
		case when @pt=0 then
		 (case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then (<<lcampoMoeda>>cred-<<lcampoMoeda>>credf)-((<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg)*-1) else <<lcampoMoeda>>deb-<<lcampoMoeda>>debf-(<<lcampoMoeda>>ivacativado-<<lcampoMoeda>>ivacatreg) end)
		 else
		 (case when <<lcampoMoeda>>cred-<<lcampoMoeda>>credf>0 then <<lcampoMoeda>>cred-<<lcampoMoeda>>credf else <<lcampoMoeda>>deb-<<lcampoMoeda>>debf end)
		 end)) + '<<lmoeda>>'  vdivida, 0 ordem
				from cc
				where
						(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					--and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
					and CONVERT(date, getdate())<= CONVERT(date, dataven)
					--and u_avienvi=0
					and cc.no = <<crsdados.no>>


				union all

				select '', '', '', '<b>TOTAL</b>', '<b>' + convert(varchar(16), convert(decimal(20,2), sum(<<lcampoMoeda>>deb-<<lcampoMoeda>>debf -(<<lcampoMoeda>>cred-<<lcampoMoeda>>credf))))+ '<<lmoeda>>' + '</b>' vdivida, 1 ordem
				from cc
				where
						(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
					--and CONVERT(date, getdate())>= dateadd(day, -<<crsemail.u_aviven>>, dataven)
					and CONVERT(date, getdate())<= CONVERT(date, dataven)
					--and u_avienvi=0
					and cc.no = <<crsdados.no>>
				order by ordem, cmdesc, dataven, datalc
				ENDTEXT

				fecha("crsav")
				If u_sqlexec(msel1, "crsav") And Reccount("crsav")>1
					Declare list_tit(5),list_cam(5),list_pic(5), list_tam(5)

					list_tit(1)="Data Emiss�o"
					list_cam(1)="crsav.datalc"
					list_pic(1)=""
					list_tam(1)=8*10

					list_tit(2)="Data Vencimento"
					list_cam(2)="crsav.dataven"
					list_pic(2)=""
					list_tam(2)=8*10

					list_tit(3)="Documento"
					list_cam(3)="crsav.cmdesc"
					list_pic(3)=""
					list_tam(3)=8*20

					list_tit(4)="N�"
					list_cam(4)="crsav.nrdoc"
					list_pic(4)=""
					list_tam(4)=8*10

					list_tit(5)="Valor em D�vida"
					list_cam(5)="crsav.vdivida"
					list_pic(5)=""
					list_tam(5)=8*18

					m_Body =Strtran(m_Body, "#LISTADOC#", listtohtml("crsav",Alltrim(crsdados.Nome) + "<br> Documentos a vencer em " + astr(Date()),2,.T.))
				Endif

				**

**				m_body = strtran(m_Body, "GAP, LDA","Securnet, Lda")
**				m_body = strtran(m_Body, "Software PHC CS Advanced"," ")

				Erro=0
				Erro=u_sendmailhtml (m_To, m_Subject, m_Body, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)

				If !Empty(m_ToVend) And xavvend=.T.
					Erro=u_sendmailhtml (m_ToVend, m_Subject, m_Body, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)
				Endif

				If Erro<>0
					m_bodyerro=''
					m_bodyerro='Ocorreram erros ao enviar o email de aviso de cobran�as para o cliente '+Alltrim(crsdados.Nome)
					Erro=u_sendmailhtml (m_toerro, "Erros email avisos", m_bodyerro, m_Attachments, l_openclient, l_quiet, c_categories, sFromEmail, m.sSmtpServer, m.sUserSmtp, m.sPassSmtp, m.sPortaSmtp, m.sLigSmtp)
				Else

					**
					** Vai gravar nas cobran�as PHC se o parametro estiver a T
					**
					u_sqlexec("update cl set u_ultenv = convert(date, getdate()) where no=?crsdados.no")
					TEXT to msel noshow textmerge
					insert into co (data, hora, nome, no, realizada, resumo, ccstamps, costamp)
					select
						?date(),
						?time(),
						nome,
						no,
						1,
						'Email autom�tico de cobran�a ' + ltrim(rtrim(cmdesc)) + ' n� ' + convert(varchar(10), nrdoc) ,
						ccstamp,
						(select suser_sname()+left(newid(),5)+right(newid(),5)+ left(newid(),5)+right(newid(),5))
					from cc
					where
							(cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0 or cc.<<lcampoMoeda>>cred-cc.<<lcampoMoeda>>credf>0)
						and CONVERT(date, getdate())> dateadd(day, 0, dataven)
						and cc.no =<<crsdados.no>>
					ENDTEXT
					If xgrvcob=.T.
						u_sqlexec(msel)
					Endif
				Endif
			Endif
		Endscan
	Endif
Endproc

Procedure CancelaFat
	TEXT to msel noshow textmerge
					select cl.clstamp, cl.no, min(cc.dataven) dataven
					from cl (nolock) inner join cc on cl.no = cc.no inner join ft2 on ft2.ft2stamp = cc.ftstamp left join cm3 on cm3.cm = cl.vendedor
					where
					cc.<<lcampoMoeda>>deb-cc.<<lcampoMoeda>>debf>0
					and CONVERT(date, getdate())>= dateadd(day, <<xdiasf>>, dataven)
					and <<lcampoMoeda>>saldo >0
					and ft2.assinatura <> ''
					and cl.inactivo = 0
	ENDTEXT

	If !Empty(crsemail.u_cobfilt)
		msel = msel + ' and ' + Alltrim(crsemail.u_cobfilt)
	Endif

	msel = msel +' group by cl.clstamp, cl.no order by cl.no'

	fecha("crscancf")
	If u_sqlexec(msel, "crscancf") And Reccount("crscancf")>0
		Select crscancf
		Go Top
		Scan
			TEXT to msel noshow textmerge
				update cl
				set cl.NOCREDIT = 1, cl.NAOENCOMENDA = 1
				from cl
				where  cl.no =<<crscanf.no>>
			ENDTEXT
			u_sqlexec(msel)

			TEXT to msel noshow textmerge
				update cl2
				set cl2.u_databloq = ?crscancf.dataven
				from cl2 join cl on cl2.cl2stamp = cl.clstamp
				where   cl.no =<<crscanf.no>>
			ENDTEXT
			u_sqlexec(msel)
		Endscan
	Endif
Endproc
