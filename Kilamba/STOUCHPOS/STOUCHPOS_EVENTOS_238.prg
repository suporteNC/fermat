** ACTUALIZA DESCONTOS
**********************************************


SELECT FPOSFT3
IF !EMPTY(FPOSFT3.U_DESCDOC)
	IF PERGUNTA('Existe definido um desconto de '+ALLTRIM(STR(FPOSFT3.U_DESCDOC,6,2))+' para o documento! Deseja aplicar a esta refer�ncia?')

		MVAL = FPOSFT3.U_DESCDOC
		SELECT FPOSL
		REPLACE FPOSL.DESCONTO WITH MVAL
		DO U_FTILIQ WITH .F.,.F.,.F.,'FPOSC','FPOSL'
		DO fttots WITH .T.,'POS'

		stouchpos.zonatotaldoc.painel.tobj1.lbltotal1.CAPTION=ALLTRIM(STR(FPOSC.TOTAL,14,2))
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal1.REFRESH()
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal.CAPTION=ALLTRIM(STR(FPOSC.TOTAL,14,2))
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal.REFRESH()

	ENDIF
ENDIF



** PREENCHE QTT ENTREGUE
***********************************************

SELECT   FPOSL 
	IF   FPOSL.u_QTT<> FPOSL.qtt
*wait window  fposl.fpos + " - "+astr(fposl.qtt) +" - " +astr(fposl.u_QTTENT)
		SELECT FPOSL
		REPLACE FPOSL.u_QTTENT WITH iif( FPOSL.u_QTTENT = 0,  FPOSL.qtt,  FPOSL.u_QTTENT + 1)   && iif(fposl.promo = .t., FPOSL.QTT , FPOSL.u_QTTENT + 1) 
		REPLACE FPOSL.LOBS2 WITH 'Quantidade Entregue: '+ALLTRIM(STR(FPOSL.u_QTTENT,14,3))
		REPLACE FPOSL.u_QTT WITH FPOSL.qtt
	ENDIF

***  VERIFICA PROMOCOES
************************************************
SELECT FPOSL

M_EPROMO = FPOSL.EPROMO
M_TPPROMO = ' '

IF M_EPROMO = .F.

	IF stouchpos.posmetodos1.clivd = .F.

		SELECT FPOSL

		TEXT TO MSQL TEXTMERGE NOSHOW
		SELECT TOP 1 *
		FROM (
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN U_SPST (nolock) on U_SPST.ref = st.ref
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPST.spstamp
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and ST.REF = '<<FPOSL.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		UNION ALL
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN U_SPSTFAMI (nolock) on U_SPSTFAMI.REF = ST.FAMILIA
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPSTFAMI.spstamp
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and ST.REF = '<<FPOSL.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		UNION ALL
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN u_SPSTSUBFAM (nolock) on u_SPSTSUBFAM.subfami = ST.U_SUBFAM
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPSTSUBFAM.spstamp
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and ST.REF = '<<FPOSL.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		UNION ALL
		select '<<FPOSL.REF>>', SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM SP (nolock)
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and '<<FPOSL.REF>>' in (select ref from st (nolock))
		and SP.U_TODOSART = 1
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		) a
		ORDER BY a.ordem
		ENDTEXT
		IF !U_SQLEXEC(MSQL,'CR_MPROMO')
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			RETURN
		ENDIF

		SELECT CR_MPROMO
		IF RECCOUNT('CR_MPROMO') > 0
			M_EPROMO = .T.
			M_TPPROMO = 'em Cart�o'
		ENDIF
	ENDIF
ENDIF

IF M_EPROMO = .T.
	SELECT FPOSL
	REPLACE FPOSL.EPROMO WITH .T.
	REPLACE FPOSL.LOBS3 WITH 'Promo��o ' + M_TPPROMO

	* se for promo��o em cart�o retira o desconto
	IF M_TPPROMO = 'em Cart�o'
		SELECT FPOSL
		REPLACE FPOSL.DESCONTO WITH 0
		DO U_FTILIQ WITH .F.,.F.,.F.,'FPOSC','FPOSL'
		DO fttots WITH .T.,'POS'

		stouchpos.zonatotaldoc.painel.tobj1.lbltotal1.CAPTION=ALLTRIM(STR(FPOSC.TOTAL,14,2))
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal1.REFRESH()
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal.CAPTION=ALLTRIM(STR(FPOSC.TOTAL,14,2))
		stouchpos.zonatotaldoc.painel.tobj1.lbltotal.REFRESH()
	ENDIF
ENDIF

**************************************************************************

