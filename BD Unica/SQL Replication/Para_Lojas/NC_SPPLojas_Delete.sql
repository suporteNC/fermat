USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbospp]    Script Date: 09/02/2023 18:15:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_SPPLojas_Delete]
		@c1 char(25),
		@c2 varchar(100),
		@c3 text,
		@c4 text,
		@c5 text,
		@c6 bit,
		@c7 text,
		@c8 bit,
		@c9 bit,
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 varchar(30),
		@c14 datetime,
		@c15 varchar(8),
		@c16 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[spp] 
	where [sppstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sppstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[spp]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from spp where sppstamp=@c1