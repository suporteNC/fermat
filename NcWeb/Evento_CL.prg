Select cl2
**
** Se for revendedor
**


If cl2.u_revend=.T.

	If u_idncweb=0
		xadd=1
		xed=0
	Else
		xadd=0
		xed=1
	Endif

	u_sqlexec("select max(cm)+1 as nrv from cm3","xnrv")

	u_sqlexec("select * from cm3 (nolock) where cm3.cm=?cl.vendedor","CrsWrkcm3")
	CriaUpdateCursor("CrsWrkcm3","cm3","cm3stamp")
	=CursorSetProp("Buffering",5,"CrsWrkcm3")
	Select CrsWrkcm3

	If Reccount("CrsWrkcm3")>0
		Replace CrsWrkcm3.Nome      With cl.Nome
		Replace CrsWrkcm3.morada    With cl.morada
		Replace CrsWrkcm3.codpost   With cl.codpost
		Replace CrsWrkcm3.Local     With cl.Local
		Replace CrsWrkcm3.telefone  With cl.telefone
		Replace CrsWrkcm3.email     With cl.email
		Replace CrsWrkcm3.cmdesc    With cl.nome2
		Replace CrsWrkcm3.ousrdata  With Date()
		Replace CrsWrkcm3.usrdata   With Date()
		Replace CrsWrkcm3.ousrinis  With m_chinis
		Replace CrsWrkcm3.usrinis   With m_chinis
		Replace CrsWrkcm3.ousrhora  With Time()
		Replace CrsWrkcm3.usrhora   With Time()
	Else
		Scatter Memvar
		Append Blank
		Gather Memvar
		Replace CrsWrkcm3.cm3stamp  With u_stamp()
		Replace CrsWrkcm3.Nome      With cl.Nome
		Replace CrsWrkcm3.morada    With cl.morada
		Replace CrsWrkcm3.codpost   With cl.codpost
		Replace CrsWrkcm3.Local     With cl.Local
		Replace CrsWrkcm3.telefone  With cl.telefone
		Replace CrsWrkcm3.email     With cl.email
		Replace CrsWrkcm3.cmdesc    With cl.nome2
		Replace CrsWrkcm3.cm        With xnrv.nrv
		Replace CrsWrkcm3.ousrdata  With Date()
		Replace CrsWrkcm3.usrdata   With Date()
		Replace CrsWrkcm3.ousrinis  With m_chinis
		Replace CrsWrkcm3.usrinis   With m_chinis
		Replace CrsWrkcm3.ousrhora  With Time()
		Replace CrsWrkcm3.usrhora   With Time()
	Endif
	If !u_tabupdate(.T.,.T.,"CrsWrkcm3")

		Tablerevert(.T.,"CrsWrkcm3")

	Else

		TEXT to mupd noshow textmerge
                    update cl
                    set vendedor=<<xnrv.nrv>>, vendnm='<<cl.nome>>'
                    where clstamp='<<cl.clstamp>>'
		ENDTEXT
		u_sqlexec(mupd)

		**
		** Criar utilizador na bd NcWeb
		**
		Createuser(cl2.u_user, cl2.u_pass, cl.email, cl2.u_firstN, cl2.u_lastn, cl2.u_ncini, cl2.u_idncweb, cl.clstamp)

	Endif
	scl.refrescar()
	scl.Refresh()
Endif


Procedure Createuser
	Parameters Juser, JPass, JEmail, JFistN, JLastN, Jini, Jid, cstamp
	Local m_Url, m_UrlTest, m_UrlProd, m_UserName, m_PassWord, m_DealerID, m_Dealer, m_JsonDoc, m_MsgTipo, m_HouseNrs
	Declare Integer DeleteUrlCacheEntry In wininet String lpszUrlName
	Local loXMLHTTP As MSXML2.XMLHTTP
	#Define clAsync .F.
	#Define ccCRLF Chr(13)+Chr(10)
	#Define ccTAB Chr(9)
	m_Status = 0
	m_Response = ''

	u_sqlexec("select u_idcomp,ncont from e1 (nolock) where estab=0","xe1")
	If xe1.u_idcomp=0
		msg("O ID da empresa n�o est� definido. Verifique na Ficha Completa da Empresa")
		Return
	Endif

	**
	** Envia o token da aplica��o
	**
	m_Url="https://ncauth.novoscanais.com/api/get-token?id_company="+Alltrim(astr(xe1.u_idcomp))+"&nif="+Alltrim(astr(xe1.ncont))

	*******************************************************************************
	** Limpar Cache
	*******************************************************************************
	DeleteUrlCacheEntry(m_Url)
	**
	loXMLHTTP = Createobject( "Microsoft.XMLHTTP" )
	loXMLHTTP.Open( "GET", m_Url, clAsync )
	loXMLHTTP.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	loXMLHTTP.Send()
	Do While loXMLHTTP.readyState <>4
		DoEvents Force && when using VFP9
	Enddo
	m_Response = Alltrim(Left(loXMLHTTP.responseText,5000))
	m_Status = loXMLHTTP.Status									&& 200 means ok, 404 not found etc. http status numbers see wikipedia, w3c, google them, etc.
	**
	** Se status=200, foi aceite e envia o token de autoriza��o
	**
	FoxObj = jsonParse(m_Response)
	If FoxObj.Status="OK"
		tkn=Alltrim(FoxObj.Data.token)
	Else
		msg("N�o autorizado!")
		Return
	Endif

	**
	** Enviar user
	**
	If xadd=1
		xadd=0
		m_Url="https://ncauth.novoscanais.com/api/nc/create-user"
		loXMLHTTP = Createobject( "Microsoft.XMLHTTP" )
		loXMLHTTP.Open( "POST", m_Url, clAsync )
		loXMLHTTP.setRequestHeader("Content-Type", "application/json; charset=utf-8")
		*********************************************************
		** Construir o Json a enviar
		*********************************************************
		m_JsonDoc = "{" + ccCRLF
		m_JsonDoc = Parse_Line( m_JsonDoc, '"token": "'+ Alltrim(tkn) +'",' ,2 )						&& Token de acesso
		m_JsonDoc = Parse_Line( m_JsonDoc, '"usercode": "'+ Alltrim(Juser)  +'",' ,2) 					&& User Code
		m_JsonDoc = Parse_Line( m_JsonDoc, '"email": "'+ Alltrim(JEmail)  +'",' ,2) 					&& User Email
		m_JsonDoc = Parse_Line( m_JsonDoc, '"firstName": "'+ Alltrim(JFistN)  +'",' ,2) 				&& First Name
		m_JsonDoc = Parse_Line( m_JsonDoc, '"lastName": "'+ Alltrim(JLastN)  +'",' ,2) 					&& Last Name
		m_JsonDoc = Parse_Line( m_JsonDoc, '"id_company": "'+ Alltrim(astr(xe1.u_idcomp))  +'",' ,2) 	&& ID Company
		m_JsonDoc = Parse_Line( m_JsonDoc, '"id_customer": "'+ Alltrim(astr(cl.no))  +'",' ,2)			&& Client Number
		m_JsonDoc = Parse_Line( m_JsonDoc, '"clstamp": "'+ Alltrim(cstamp)  +'",' ,2) 					&& CLstamp
		m_JsonDoc = Parse_Line( m_JsonDoc, '"password": "'+ Alltrim(JPass)  +'",' ,2) 					&& Password
		m_JsonDoc = Parse_Line( m_JsonDoc, '"passwordConfirm": "'+ Alltrim(JPass) +'"' ,2) 				&& Password Confirmation

		m_JsonDoc = m_JsonDoc+"}" + ccCRLF
msg(m_jsondoc)
		loXMLHTTP.Send(m_JsonDoc)
		Do While loXMLHTTP.readyState <>4
			DoEvents Force && when using VFP9
		Enddo
		m_Response = Alltrim(Left(loXMLHTTP.responseText,5000))
msg(alltrim(m_response))
		m_Status = loXMLHTTP.Status									&& 200 means ok, 404 not found etc. http status numbers see wikipedia, w3c, google them, etc.
		**
		** Se status=200, foi aceite e envia o token de autoriza��o
		**

		FoxObj = jsonParse(m_Response)
		If FoxObj.Status="OK"
			nid=FoxObj.Data.insertId
			TEXT to mupd noshow textmerge
				update cl2
				set u_idncweb=<<nid>> where cl2stamp='<<cl.clstamp>>'
			ENDTEXT
			u_sqlexec(mupd)
			msg("Utilizador B2B criado com sucesso")
		Else
			msg("Erro ao criar o user no NcWeb!")
			Return
		Endif
	Endif

	If xed=1
		xed=0
		m_Url="https://ncauth.novoscanais.com/api/nc/update-user"
		loXMLHTTP = Createobject( "Microsoft.XMLHTTP" )
		loXMLHTTP.Open( "POST", m_Url, clAsync )
		loXMLHTTP.setRequestHeader("Content-Type", "application/json; charset=utf-8")
		*********************************************************
		** Construir o Json a enviar
		*********************************************************

		m_JsonDoc = "{" + ccCRLF
		m_JsonDoc = Parse_Line( m_JsonDoc, '"token": "'+ Alltrim(tkn) +'",' ,2 )						&& Token de acesso
		m_JsonDoc = Parse_Line( m_JsonDoc, '"id": "'+ astr(Jid) +'",' ,2 )								&& ID User
		m_JsonDoc = Parse_Line( m_JsonDoc, '"firstName": "'+ Alltrim(JFistN)  +'",' ,2) 				&& First Name
		m_JsonDoc = Parse_Line( m_JsonDoc, '"lastName": "'+ Alltrim(JLastN)  +'",' ,2) 					&& Last Name
		m_JsonDoc = Parse_Line( m_JsonDoc, '"usercode": "'+ Alltrim(Juser)  +'",' ,2) 				&& User Name
		m_JsonDoc = Parse_Line( m_JsonDoc, '"email": "'+ Alltrim(JEmail)  +'"' ,2) 						&& User Email

		m_JsonDoc = m_JsonDoc+"}" + ccCRLF

		loXMLHTTP.Send(m_JsonDoc)
		Do While loXMLHTTP.readyState <>4
			DoEvents Force && when using VFP9
		Enddo
		m_Response = Alltrim(Left(loXMLHTTP.responseText,5000))
		m_Status = loXMLHTTP.Status									&& 200 means ok, 404 not found etc. http status numbers see wikipedia, w3c, google them, etc.
		**
		** Se status=200, foi aceite e envia o token de autoriza��o
		**
		FoxObj = jsonParse(m_Response)
		If FoxObj.Status="OK"
			**		token=FoxObj.data.token
		Else
			msg("Erro ao atualizar o user no NcWeb!")
			Return
		Endif
	Endif
Endproc


**
** Fun��o para construir Json
**
Function Parse_Line( cExpression, cLine, nTab )
	If nTab > 0
		cExpression = cExpression + Replicate( ccTAB, nTab)
	Endif
	If  cLine <> 'SameLine'
		cExpression = cExpression + cLine + ccCRLF
	Else
		cExpression = cExpression + cLine
	Endif
	Return cExpression
Endfunc




Procedure jsonParse
	Parameters pcJSON, poTarget
	Local lcScript
	Local sc As scriptcontrol
	If Pcount() = 1
		poTarget = Newobject("Empty")
	Endif
	sc = Createobject("scriptcontrol")
	sc.Language="JScript"
	TEXT TO lcScript TEXTMERGE NOSHOW
        function parseJSON() {
        var json = << pcJSON >>;
        var str = "";
        for (var name in json) {
            var realType = RealTypeOf(json[name]);
            if ( realType == "array" ) {
            str += "ADDPROPERTY(poTarget,'"+name+"(1)',.f.)\r\n";
            for (var i = 0;i < json[name].length;i++) {
                var arrayElement = "poTarget."+name+"["+(i+1)+"]"
                str += "DIMENSION "+arrayElement+"\r\n";
                var value = json[name][i];
                var realType = RealTypeOf(value);
                if ( realType == "object") {
                str += arrayElement + " =  jsonParse(\""+toJson(value)+"\")\r\n";
                } else if ( realType == "string" ) {
                str += arrayElement + ' = "'+value+'"'+"\r\n";
                } else if ( realType == "number" ) {
                str += arrayElement + " = "+value+"\r\n";
                } else if ( realType == "date" ) {
                str += arrayElement + " = {"+value+"}\r\n";
                } else if ( realType == "boolean" ) {
                str += arrayElement + " = "+((value)?".t.":".f.")+"\r\n";
                }
            }
            } else if ( realType == "string" ) {
            str += "ADDPROPERTY(poTarget,'"+name+"',["+json[name]+"])\r\n";
            } else if ( realType == "number" ) {
            str += "ADDPROPERTY(poTarget,'"+name+"',"+json[name]+")\r\n";
            } else if ( realType == "date" ) {
            str += "ADDPROPERTY(poTarget,'"+name+"',{"+json[name]+"})\r\n";
            } else if (realType == "boolean" ) {
            str += "ADDPROPERTY(poTarget,'"+name+"',"+((json[name])?".t.":".f.")+")\r\n";
            } else if ( realType == "object") {
            str += "ADDPROPERTY(poTarget,'"+name+"',jsonParse(\""+toJson(json[name])+"\"))\r\n";
            }
        }
        return str;
        }
        function RealTypeOf(v) {
            if (typeof(v) == "object") {
                if (v == null) return "null";
                if (v.constructor == (new Array).constructor) return "array";
                if (v.constructor == (new Date).constructor) return "date";
                if (v.constructor == (new RegExp).constructor) return "regex";
                return "object";
            }
            return typeof(v);
            }
            function toJson(obj) {
            switch (typeof obj) {
                case 'object':
                if (obj) {
                    var list = [];
                    if (obj instanceof Array) {
                        for (var i=0;i < obj.length;i++) {
                                list.push(toJson(obj[i]));
                        }
                        return '[' + list.join(',') + ']';
                    } else {
                        for (var prop in obj) {
                                list.push(prop + ':' + toJson(obj[prop]));
                        }
                        return '{' + list.join(',') + '}';
                    }
                } else {
                    return 'null';
                }
                case 'string':
                return "'" + obj.replace(/(['])/g, '\\$1') + "'";
                case 'number':
                case 'boolean':
                return new String(obj);
            }
            }
	ENDTEXT
	sc.AddCode(lcScript)
	lcScript = sc.Eval("parseJSON();")
	Execscript(lcScript)
	sc = Null
	Release sc
	Return poTarget
Endproc
