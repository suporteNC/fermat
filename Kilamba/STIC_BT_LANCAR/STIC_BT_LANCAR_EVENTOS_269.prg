*********************************************************************
*** SSTIC - Bot�o de Lan�ar Invent�rio
*** Criado por TTS-RPM em 08/01/2018
*** Alterado por
**********************************************************************


Select Stic
If pergunta('Tem a certeza que quer lan�ar este Invent�rio?',1,'')

	If  Not Empty(Stic.U_TERMINAL)
		*** Vamos validar se tem linhas n�o contadas
		Select stil
		Locate For stil.u_contado = .F. And Len(Alltrim(stil.ref)) <> 0
		If Found()
			msg("Aten��o, tem linhas no invent�rio n�o contadas, n�o pode Lan�ar!")
			Return
		Endif
	Endif
	*** Vamos validar se este invent�rio tem linhas centros analiticos lan�ados
	Select stil
	Locate For Len(Alltrim(stil.ccusto)) <> 0 and  0 = 1 
	If Found()
		msg("Aten��o, tem linhas com Centro Analitico preenchido, � necess�rio retirar a agrupar o Invent�rio!")

		sstic.showsave()
		Select stil
		Replace All stil.ccusto With ''

		Select stil
		Goto Top

		Return
	Endif
           xcbpsg=alltrim(m_chinis)+"STICLANcbpsg"
           ret_defeito(xcbpsg,"L",.T.)


	*** Pode abrir o Ecran do lan�amento
	doread('STICLAN','STICLAN')


Endif
