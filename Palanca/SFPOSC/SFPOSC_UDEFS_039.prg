*******************************************
** Guarda estabelecimento
**
** Criado por Rui Vale
** Criado em  22/09/2017
*******************************************

Select fposc

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From td(nolock)
	Where td.ndoc=<<STR(fposc.ndoc)>>
ENDTEXT

If u_sqlexec(uSql,"uCurTd") And Reccount("uCurTd")>0
	Select uCurTd
	Replace fposc.u_estab With uCurTd.u_estab

	fecha("uCurTd")
	Return fposc.u_estab
Endif

Return fposc.u_estab