USE [VIANA]
GO

/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbofi2]    Script Date: 19/01/2023 19:35:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[NC_FI2VN_Delete]
		@c1 char(25),
		@c2 bit,
		@c3 varchar(60),
		@c4 datetime,
		@c5 varchar(50),
		@c6 varchar(50),
		@c7 char(25),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 datetime,
		@c13 varchar(8),
		@c14 bit,
		@c15 datetime,
		@c16 bit,
		@c17 char(25),
		@c18 varchar(10),
		@c19 varchar(5),
		@c20 varchar(115),
		@c21 varchar(10),
		@c22 varchar(115),
		@c23 numeric(6,2),
		@c24 numeric(6,2),
		@c25 numeric(5,2),
		@c26 varchar(20),
		@c27 numeric(19,6),
		@c28 numeric(18,5),
		@c29 bit,
		@c30 numeric(19,6),
		@c31 numeric(18,5),
		@c32 numeric(5,0),
		@c33 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [VN].[fi2] 
	where [fi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[fi2]', @param2=@primarykey_text, @param3=13234
		End
end  

delete from fi2 where fi2stamp=@c1
GO