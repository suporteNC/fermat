USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_caixal]    Script Date: 27/01/2023 13:18:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXALVN_Delete]
		@c1 char(25),
		@c2 varchar(100),
		@c3 varchar(50),
		@c4 numeric(15,2),
		@c5 varchar(25),
		@c6 varchar(25),
		@c7 varchar(25),
		@c8 numeric(10,0),
		@c9 varchar(25),
		@c10 numeric(15,5),
		@c11 numeric(15,5),
		@c12 numeric(15,5),
		@c13 numeric(3,0),
		@c14 char(25),
		@c15 varchar(30),
		@c16 varchar(25),
		@c17 varchar(30),
		@c18 numeric(5,0),
		@c19 varchar(20),
		@c20 varchar(30),
		@c21 varchar(15),
		@c22 bit,
		@c23 varchar(25),
		@c24 numeric(10,2),
		@c25 numeric(10,2),
		@c26 numeric(10,0),
		@c27 varchar(35),
		@c28 varchar(25),
		@c29 varchar(40),
		@c30 varchar(30),
		@c31 datetime,
		@c32 varchar(8),
		@c33 varchar(30),
		@c34 datetime,
		@c35 varchar(8),
		@c36 bit,
		@c37 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [VN].[u_caixal] 
	where [u_caixalstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixalstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[u_caixal]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_caixal where u_caixalstamp=@c1