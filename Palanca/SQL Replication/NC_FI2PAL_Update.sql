USE [FERMAT_LOJA]
GO

/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbofi2]    Script Date: 19/01/2023 19:39:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[NC_FI2PAL_Update]
		@c1 char(25),
		@c2 bit,
		@c3 varchar(60),
		@c4 datetime,
		@c5 varchar(50),
		@c6 varchar(50),
		@c7 char(25),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 datetime,
		@c13 varchar(8),
		@c14 bit,
		@c15 datetime,
		@c16 bit,
		@c17 char(25),
		@c18 varchar(10),
		@c19 varchar(5),
		@c20 varchar(115),
		@c21 varchar(10),
		@c22 varchar(115),
		@c23 numeric(6,2),
		@c24 numeric(6,2),
		@c25 numeric(5,2),
		@c26 varchar(20),
		@c27 numeric(19,6),
		@c28 numeric(18,5),
		@c29 bit,
		@c30 numeric(19,6),
		@c31 numeric(18,5),
		@c32 numeric(5,0),
		@c33 char(25),
		@c34 bit,
		@c35 varchar(60),
		@c36 datetime,
		@c37 varchar(50),
		@c38 varchar(50),
		@c39 char(25),
		@c40 varchar(30),
		@c41 datetime,
		@c42 varchar(8),
		@c43 varchar(30),
		@c44 datetime,
		@c45 varchar(8),
		@c46 bit,
		@c47 datetime,
		@c48 bit,
		@c49 char(25),
		@c50 varchar(10),
		@c51 varchar(5),
		@c52 varchar(115),
		@c53 varchar(10),
		@c54 varchar(115),
		@c55 numeric(6,2),
		@c56 numeric(6,2),
		@c57 numeric(5,2),
		@c58 varchar(20),
		@c59 numeric(19,6),
		@c60 numeric(18,5),
		@c61 bit,
		@c62 numeric(19,6),
		@c63 numeric(18,5),
		@c64 numeric(5,0)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c33 = @c1)
begin 
update [PL].[fi2] set
		[fi2stamp] = @c33,
		[prestsrv] = @c34,
		[originatingon] = @c35,
		[orderdate] = @c36,
		[refretif] = @c37,
		[motretif] = @c38,
		[ftstamp] = @c39,
		[ousrinis] = @c40,
		[ousrdata] = @c41,
		[ousrhora] = @c42,
		[usrinis] = @c43,
		[usrdata] = @c44,
		[usrhora] = @c45,
		[marcada] = @c46,
		[u_ttssinc] = @c47,
		[noupdqtt2ori] = @c48,
		[oristamp] = @c49,
		[origem] = @c50,
		[nomecmb] = @c51,
		[designcmb] = @c52,
		[codisp] = @c53,
		[designisp] = @c54,
		[pctengfssl] = @c55,
		[pctengrnv] = @c56,
		[co2emiss] = @c57,
		[unico2mdd] = @c58,
		[esbrcstincbio] = @c59,
		[sbrcstincbio] = @c60,
		[ispoveruni2] = @c61,
		[evalorisp] = @c62,
		[valorisp] = @c63,
		[u_estab] = @c64
	where [fi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PL].[fi2]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PL].[fi2] set
		[prestsrv] = @c34,
		[originatingon] = @c35,
		[orderdate] = @c36,
		[refretif] = @c37,
		[motretif] = @c38,
		[ftstamp] = @c39,
		[ousrinis] = @c40,
		[ousrdata] = @c41,
		[ousrhora] = @c42,
		[usrinis] = @c43,
		[usrdata] = @c44,
		[usrhora] = @c45,
		[marcada] = @c46,
		[u_ttssinc] = @c47,
		[noupdqtt2ori] = @c48,
		[oristamp] = @c49,
		[origem] = @c50,
		[nomecmb] = @c51,
		[designcmb] = @c52,
		[codisp] = @c53,
		[designisp] = @c54,
		[pctengfssl] = @c55,
		[pctengrnv] = @c56,
		[co2emiss] = @c57,
		[unico2mdd] = @c58,
		[esbrcstincbio] = @c59,
		[sbrcstincbio] = @c60,
		[ispoveruni2] = @c61,
		[evalorisp] = @c62,
		[valorisp] = @c63,
		[u_estab] = @c64
	where [fi2stamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[fi2stamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PL].[fi2]', @param2=@primarykey_text, @param3=13233
		End
end 
end 

EXEC dbo.NC_Sinc_Update 'dbo','PL','fi2',@c1, 'fi2stamp', ''


GO


