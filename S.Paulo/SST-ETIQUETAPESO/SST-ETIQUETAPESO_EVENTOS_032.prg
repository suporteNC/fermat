
Ccod = 'zzzz'

public p_nomeetiq
p_nomeetiq = 'Quant.:'
set point to ','
DO WHILE NOT EMPTY(Ccod)
	npeso = 0.00
	Ccod = GETNOME("Introduza a refer�ncia OU C�digo de barras","")
	IF  NOT EMPTY(Ccod)
		TEXT TO MSEL TEXTMERGE NOSHOW
			select  top 1 ST.ref, ST.stid, ST.ststamp
					,case when ST.langdes1 <> '' then ST.langdes1 else ST.design end design
					,space(18) codigo
					,convert(numeric(15,3),0) qtt, unidade, ST.conversao, ST.stock + ST.QTTREC stock  , ST.stns  
			from ST (nolock) 
			left join BC (nolock) on bc.ststamp = st.ststamp
			where (ST.codigo = '<<alltrim(Ccod)>>' or ST.ref='<<alltrim(Ccod)>>' or BC.codigo = '<<alltrim(Ccod)>>')
		ENDTEXT

		if !u_sqlexec(MSEL,'c_ref')
			msg('erro encontrado')
			msg(MSEL)
			return		
		endif

		SELECT c_ref
		IF RECCOUNT("c_ref") = 0 
			MSG('Artigo n�o encontrado!')	
			return	
		ENDIF

		cref = c_ref.ref

		IF  RECCOUNT("c_ref") > 0
			do while npeso > 999 or npeso = 0
				npeso = GETNOME("Introduza o peso",npeso, rtrim(c_ref.ref) +' - '+ rtrim(c_ref.design) + chr(13) + "Unidade: " + c_ref.unidade + chr(13) + 'Factor: ' + astr(round(1/c_ref.conversao,4)) + '                     STOCK:   ' + ltrim(str(c_ref.stock,15,4))  )
				if npeso > 999
					msg("O peso n�o pode ser superior a 999")
				endif 
				if npeso > c_ref.stock and c_ref.stns = .f.
					if pergunta("A quantidade � maior que o stock existente, quer corrigir",1)
						npeso = 0.00
					endif  
				endif  
			enddo 

			TEXT TO msel NOSHOW TEXTMERGE
				declare  @codigoNew char(18), @ref char(18)
				set @codigoNew = ''
				set @ref = '<<c_ref.ref>>'


				select @codigoNew = codigo  from bc (nolock) where ref = @ref and emb = 'PESO'
				if  @codigoNew = ''
				begin
					select @codigoNew =  right(rtrim('000000'+ convert(char(6),convert(numeric(5),isnull(max(codigo),'0'))+1)),5)
					from bc  where emb = 'PESO'  AND LEN(CODIGO) <= 5

					insert into bc
					(bcstamp,ref, design,codigo, ststamp,qtt,emb)
					values
					(	left(replace(newid(),'-',''),24)
						,@ref
						,'<<c_ref.design>>'
						,@codigoNew
						,'<<c_ref.ststamp>>'
						,0
						,'PESO'
					)
				END

				select @codigoNew codigo

			ENDTEXT

			u_sqlexec(msel,"c_codigo")
	
			Ccod  = '27'+ RTRIM(c_codigo.codigo) +  RIGHT('000000' + alltrim(STRTRAN(STRTRAN(STR(npeso,7,2),',',''),'.','')),5)
			Ccod = Ean13ckd(rtrim(Ccod))
			select c_ref
			replace c_ref.codigo with Ccod
			replace c_ref.qtt with npeso
			doread('st','SST')
			if used("stlist")
				select  stlist
				delete all 
			endif
			if reccount('st') = 0 
				navega('st',c_ref.ststamp)
			endif 

			idu_etiqmulti="qtt"
			idu_bdados = "stlbliduc"
			idu_bldados = "stlblidul"
			idu_fdados = "st"
			deftit="Etiquetas"
			idu_fldados = ""

			docomando("do form iduigen with .f.,'stcampos','',.f.,.f.,.f.,.f.,.f.,'','',''")		
			ccod = ''			
		ELSE
			msg("A refer�ncia n�o existe")
		ENDIF

		
		fecha("c_ref")
	ENDIF

ENDDO
