USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbore]    Script Date: 15/05/2023 10:06:44 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_REVN_Insert]
    @c1 char(25),
    @c2 varchar(20),
    @c3 numeric(10,0),
    @c4 datetime,
    @c5 char(55),
    @c6 numeric(18,5),
    @c7 numeric(19,6),
    @c8 numeric(3,0),
    @c9 numeric(10,0),
    @c10 varchar(55),
    @c11 varchar(43),
    @c12 varchar(45),
    @c13 varchar(20),
    @c14 numeric(4,0),
    @c15 varchar(12),
    @c16 varchar(1),
    @c17 numeric(15,2),
    @c18 varchar(11),
    @c19 varchar(100),
    @c20 varchar(50),
    @c21 varchar(20),
    @c22 varchar(20),
    @c23 varchar(20),
    @c24 numeric(4,0),
    @c25 bit,
    @c26 varchar(22),
    @c27 varchar(28),
    @c28 numeric(6,2),
    @c29 numeric(18,5),
    @c30 numeric(15,3),
    @c31 bit,
    @c32 varchar(60),
    @c33 varchar(20),
    @c34 varchar(20),
    @c35 datetime,
    @c36 datetime,
    @c37 varchar(20),
    @c38 varchar(50),
    @c39 varchar(33),
    @c40 bit,
    @c41 numeric(4,0),
    @c42 varchar(20),
    @c43 char(25),
    @c44 char(25),
    @c45 varchar(25),
    @c46 char(25),
    @c47 numeric(19,6),
    @c48 numeric(19,6),
    @c49 numeric(18,5),
    @c50 varchar(20),
    @c51 numeric(1,0),
    @c52 numeric(3,0),
    @c53 numeric(18,5),
    @c54 numeric(19,6),
    @c55 numeric(18,5),
    @c56 numeric(19,6),
    @c57 numeric(18,5),
    @c58 numeric(19,6),
    @c59 numeric(18,5),
    @c60 numeric(19,6),
    @c61 numeric(18,5),
    @c62 numeric(19,6),
    @c63 numeric(18,5),
    @c64 numeric(19,6),
    @c65 numeric(18,5),
    @c66 numeric(19,6),
    @c67 numeric(18,5),
    @c68 numeric(19,6),
    @c69 numeric(18,5),
    @c70 numeric(19,6),
    @c71 numeric(19,6),
    @c72 varchar(4),
    @c73 numeric(18,5),
    @c74 varchar(20),
    @c75 varchar(20),
    @c76 bit,
    @c77 bit,
    @c78 bit,
    @c79 bit,
    @c80 bit,
    @c81 datetime,
    @c82 varchar(10),
    @c83 varchar(55),
    @c84 bit,
    @c85 datetime,
    @c86 varchar(40),
    @c87 numeric(10,0),
    @c88 numeric(2,0),
    @c89 bit,
    @c90 char(25),
    @c91 numeric(18,5),
    @c92 numeric(19,6),
    @c93 varchar(50),
    @c94 varchar(1),
    @c95 numeric(4,0),
    @c96 numeric(18,5),
    @c97 numeric(19,6),
    @c98 varchar(11),
    @c99 numeric(15,2),
    @c100 varchar(20),
    @c101 varchar(20),
    @c102 numeric(3,0),
    @c103 char(25),
    @c104 varchar(30),
    @c105 char(25),
    @c106 varchar(30),
    @c107 numeric(18,5),
    @c108 numeric(19,6),
    @c109 numeric(19,6),
    @c110 varchar(15),
    @c111 numeric(19,6),
    @c112 numeric(18,5),
    @c113 numeric(15,3),
    @c114 numeric(19,6),
    @c115 numeric(18,5),
    @c116 numeric(15,3),
    @c117 varchar(15),
    @c118 numeric(19,6),
    @c119 numeric(18,5),
    @c120 numeric(15,3),
    @c121 numeric(19,6),
    @c122 numeric(18,5),
    @c123 numeric(15,3),
    @c124 varchar(15),
    @c125 numeric(19,6),
    @c126 numeric(18,5),
    @c127 numeric(15,3),
    @c128 numeric(19,6),
    @c129 numeric(18,5),
    @c130 numeric(15,3),
    @c131 varchar(15),
    @c132 numeric(19,6),
    @c133 numeric(18,5),
    @c134 numeric(15,3),
    @c135 numeric(19,6),
    @c136 numeric(18,5),
    @c137 numeric(15,3),
    @c138 varchar(15),
    @c139 numeric(19,6),
    @c140 numeric(18,5),
    @c141 numeric(15,3),
    @c142 numeric(19,6),
    @c143 numeric(18,5),
    @c144 numeric(15,3),
    @c145 varchar(15),
    @c146 numeric(19,6),
    @c147 numeric(18,5),
    @c148 numeric(15,3),
    @c149 numeric(19,6),
    @c150 numeric(18,5),
    @c151 numeric(15,3),
    @c152 numeric(18,5),
    @c153 numeric(19,6),
    @c154 numeric(19,6),
    @c155 bit,
    @c156 numeric(18,5),
    @c157 numeric(19,6),
    @c158 varchar(25),
    @c159 numeric(18,5),
    @c160 numeric(19,6),
    @c161 varchar(11),
    @c162 numeric(15,2),
    @c163 numeric(15,2),
    @c164 numeric(15,2),
    @c165 numeric(15,2),
    @c166 varchar(11),
    @c167 numeric(15,2),
    @c168 varchar(25),
    @c169 varchar(25),
    @c170 char(25),
    @c171 char(25),
    @c172 varchar(30),
    @c173 datetime,
    @c174 varchar(8),
    @c175 varchar(30),
    @c176 datetime,
    @c177 varchar(8),
    @c178 bit,
    @c179 numeric(18,5),
    @c180 numeric(19,6),
    @c181 numeric(10,0),
    @c182 bit,
    @c183 varchar(20),
    @c184 varchar(40),
    @c185 bit,
    @c186 numeric(18,5),
    @c187 numeric(19,6),
    @c188 varchar(35),
    @c189 varchar(35),
    @c190 bit,
    @c191 numeric(13,0),
    @c192 varchar(50),
    @c193 bit,
    @c194 varchar(60),
    @c195 varchar(1),
    @c196 bit,
    @c197 datetime,
    @c198 numeric(5,0),
    @c199 bit,
    @c200 varchar(30),
    @c201 datetime,
    @c202 varchar(8),
    @c203 numeric(19,6),
    @c204 numeric(19,6),
    @c205 numeric(18,5),
    @c206 numeric(19,6),
    @c207 numeric(10,0),
    @c208 datetime,
    @c209 varchar(100),
    @c210 numeric(1,0),
    @c211 varchar(254)
as
begin  
	insert into [VN].[re] (
		[restamp],
		[nmdoc],
		[rno],
		[rdata],
		[nome],
		[total],
		[etotal],
		[ndoc],
		[no],
		[morada],
		[local],
		[codpost],
		[ncont],
		[reano],
		[olcodigo],
		[telocal],
		[totalmoeda],
		[moeda],
		[desc1],
		[desc2],
		[fref],
		[ccusto],
		[ncusto],
		[contado],
		[process],
		[cobranca],
		[nib],
		[fin],
		[finv],
		[finvmoeda],
		[impresso],
		[userimpresso],
		[clbanco],
		[clcheque],
		[procdata],
		[vdata],
		[zona],
		[ollocal],
		[descba],
		[plano],
		[vendedor],
		[vendnm],
		[olstamp],
		[ccstamp],
		[segmento],
		[cc2stamp],
		[efinv],
		[edifcambio],
		[difcambio],
		[tipo],
		[pais],
		[estab],
		[ivav1],
		[eivav1],
		[ivav2],
		[eivav2],
		[ivav3],
		[eivav3],
		[ivav4],
		[eivav4],
		[ivav5],
		[eivav5],
		[ivav6],
		[eivav6],
		[ivav7],
		[eivav7],
		[ivav8],
		[eivav8],
		[ivav9],
		[eivav9],
		[earred],
		[memissao],
		[arred],
		[cobrador],
		[rota],
		[introfin],
		[tbok],
		[faztrf],
		[procomss],
		[cheque],
		[chdata],
		[intid],
		[nome2],
		[regiva],
		[dplano],
		[dinoplano],
		[dilnoplano],
		[diaplano],
		[planoonline],
		[dostamp],
		[totol2],
		[etotol2],
		[ollocal2],
		[telocal2],
		[contado2],
		[chtotal],
		[echtotal],
		[chmoeda],
		[chtotalm],
		[site],
		[pnome],
		[pno],
		[cxstamp],
		[cxusername],
		[ssstamp],
		[ssusername],
		[vdinheiro],
		[evdinheiro],
		[mvdinheiro],
		[modop1],
		[epaga1],
		[paga1],
		[mpaga1],
		[ecompaga1],
		[compaga1],
		[mcompaga1],
		[modop2],
		[epaga2],
		[paga2],
		[mpaga2],
		[ecompaga2],
		[compaga2],
		[mcompaga2],
		[modop3],
		[epaga3],
		[paga3],
		[mpaga3],
		[ecompaga3],
		[compaga3],
		[mcompaga3],
		[modop4],
		[epaga4],
		[paga4],
		[mpaga4],
		[ecompaga4],
		[compaga4],
		[mcompaga4],
		[modop5],
		[epaga5],
		[paga5],
		[mpaga5],
		[ecompaga5],
		[compaga5],
		[mcompaga5],
		[modop6],
		[epaga6],
		[paga6],
		[mpaga6],
		[ecompaga6],
		[compaga6],
		[mcompaga6],
		[acerto],
		[eacerto],
		[macerto],
		[exportado],
		[totow],
		[etotow],
		[tptit],
		[virs],
		[evirs],
		[moeda2],
		[valorowm2],
		[valorm2],
		[valor2m2],
		[earredm2],
		[moeda3],
		[valor2m3],
		[contrato],
		[cessao],
		[facstamp],
		[faccstamp],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[totoladi],
		[etotoladi],
		[cbbno],
		[luserfin],
		[bic],
		[iban],
		[processsepa],
		[totalimp],
		[etotalimp],
		[sepagh],
		[sepapi],
		[operext],
		[npedido],
		[tiporeg],
		[temch],
		[paymentrefnoori],
		[upddespacho],
		[revogado],
		[revdata],
		[u_estab],
		[anulado],
		[anulinis],
		[anuldata],
		[anulhora],
		[esirca],
		[ecativa],
		[cativa],
		[mcativa],
		[noimps],
		[u_ttssinc],
		[atcud],
		[contingencia],
		[nomeat]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57,
		@c58,
		@c59,
		@c60,
		@c61,
		@c62,
		@c63,
		@c64,
		@c65,
		@c66,
		@c67,
		@c68,
		@c69,
		@c70,
		@c71,
		@c72,
		@c73,
		@c74,
		@c75,
		@c76,
		@c77,
		@c78,
		@c79,
		@c80,
		@c81,
		@c82,
		@c83,
		@c84,
		@c85,
		@c86,
		@c87,
		@c88,
		@c89,
		@c90,
		@c91,
		@c92,
		@c93,
		@c94,
		@c95,
		@c96,
		@c97,
		@c98,
		@c99,
		@c100,
		@c101,
		@c102,
		@c103,
		@c104,
		@c105,
		@c106,
		@c107,
		@c108,
		@c109,
		@c110,
		@c111,
		@c112,
		@c113,
		@c114,
		@c115,
		@c116,
		@c117,
		@c118,
		@c119,
		@c120,
		@c121,
		@c122,
		@c123,
		@c124,
		@c125,
		@c126,
		@c127,
		@c128,
		@c129,
		@c130,
		@c131,
		@c132,
		@c133,
		@c134,
		@c135,
		@c136,
		@c137,
		@c138,
		@c139,
		@c140,
		@c141,
		@c142,
		@c143,
		@c144,
		@c145,
		@c146,
		@c147,
		@c148,
		@c149,
		@c150,
		@c151,
		@c152,
		@c153,
		@c154,
		@c155,
		@c156,
		@c157,
		@c158,
		@c159,
		@c160,
		@c161,
		@c162,
		@c163,
		@c164,
		@c165,
		@c166,
		@c167,
		@c168,
		@c169,
		@c170,
		@c171,
		@c172,
		@c173,
		@c174,
		@c175,
		@c176,
		@c177,
		@c178,
		@c179,
		@c180,
		@c181,
		@c182,
		@c183,
		@c184,
		@c185,
		@c186,
		@c187,
		@c188,
		@c189,
		@c190,
		@c191,
		@c192,
		@c193,
		@c194,
		@c195,
		@c196,
		@c197,
		@c198,
		@c199,
		@c200,
		@c201,
		@c202,
		@c203,
		@c204,
		@c205,
		@c206,
		@c207,
		@c208,
		@c209,
		@c210,
		@c211	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','VN','re',@c1, 'restamp'

end  
