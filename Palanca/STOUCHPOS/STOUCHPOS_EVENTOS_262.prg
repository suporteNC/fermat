** Criado por: TOTALSOFT | Paulo Ricardo Martins
** Data 09/10/2017
** Aplicar Promo��o
***********************************************************


Local MTOTPROMO

MTOTPROMO = 0

If stouchpos.posmetodos1.clivd = .F.

	Select FI
	Go Top
	Scan

		IF FI.LOBS3 = 'Promo��o em Cart�o'

		TEXT TO MSQL TEXTMERGE NOSHOW
		SELECT TOP 1 *
		FROM (
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN U_SPST (nolock) on U_SPST.ref = st.ref
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPST.spstamp
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and ST.REF = '<<FI.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		UNION ALL
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN U_SPSTFAMI (nolock) on U_SPSTFAMI.REF = ST.FAMILIA
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPSTFAMI.spstamp
		WHERE convert(varchar(8),getdate(),112) between SP.datai and SP.dataf
		and ST.REF = '<<FI.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 ) 
		UNION ALL
		select ST.REF, SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM ST (nolock)
		INNER JOIN u_SPSTSUBFAM (nolock) on u_SPSTSUBFAM.subfami = ST.U_SUBFAM
		INNER JOIN SP (nolock) on SP.SPSTAMP = u_SPSTSUBFAM.spstamp
		WHERE convert(varchar(8),getdate(),112)  between SP.datai and SP.dataf
		and ST.REF = '<<FI.REF>>'
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		UNION ALL
		select '<<FI.REF>>', SP.u_desconto, SP.u_minvenda, SP.ordem
		FROM SP (nolock) 
		WHERE convert(varchar(8),getdate(),112)  between SP.datai and SP.dataf
		and '<<FI.REF>>' in (select ref from st (nolock))
		and SP.U_TODOSART = 1
		and SP.u_CARTAO = 1
		and (SP.site = '<<m.pos_site>>' or SP.sites = 1 )
		) a
		ORDER BY a.ordem
		ENDTEXT
		If !U_SQLEXEC(MSQL,'CR_MPROMO')
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			Return
		Endif

		Select CR_MPROMO
		If Reccount('CR_MPROMO') > 0

			Select FT
			If FT.ETOTAL >= CR_MPROMO.u_minvenda
				MTOTPROMO = FI.ETILIQUIDO*(CR_MPROMO.u_DESCONTO/100)
			Endif

		Endif

		Endif

	Endscan


	IF MTOTPROMO > 0

		**************************************************************************
		Select FT
		TEXT TO MSQL TEXTMERGE NOSHOW
		SELECT TOP 1 CL.NOME, CL.NO, CL.ESTAB, CL.U_PONTOS, CL2.u_ncartao
		FROM CL (nolock)
		INNER JOIN CL2 (NOLOCK) ON CL2.CL2STAMP = CL.CLSTAMP
		WHERE CL.NO=<<ASTR(FT.NO)>> and CL.ESTAB=<<ASTR(FT.ESTAB)>>
		ORDER BY CL.NO
		ENDTEXT

		If !U_SQLEXEC(MSQL,'CR_CLTMP')
			MSG('ERRO ENCONTRADO')
			MSG(MSQL)
			Return
		Endif

		Select CR_CLTMP

		MVAL = MTOTPROMO

		If MVAL > 0

			U_SQLEXEC('BEGIN TRANSACTION')

			TEXT TO MSQL TEXTMERGE NOSHOW
		INSERT INTO CC (CCSTAMP, DEB, EDEB, DEBF, EDEBF, CRED, ECRED, CREDF, ECREDF, DATALC, CMDESC, CM , MOEDA, NOME, NO, ESTAB, ORIGEM, nrdoc)
			VALUES ('<<U_STAMP()>>',0,0,0,0, <<STRTRAN(STR(MVAL,14,2),',','.')>>, <<STRTRAN(STR(MVAL,14,2),',','.')>>,0,0, GETDATE(), 'Vale em Cart�o', 122, 'AKZ', '<<CR_CLTMP.NOME>>',<<ASTR(CR_CLTMP.NO)>>,<<ASTR(CR_CLTMP.ESTAB)>>,'CC'
			, (select isnull(max(nrdoc),0) + 1   from cc  (nolock) where cmdesc = 'Vale em Cart�o' ) 
			)
			ENDTEXT
			If !U_SQLEXEC(MSQL)
				MSG('ERRO ENCONTRADO')
				MSG(MSQL)
				U_SQLEXEC('ROLLBACK')
				Return
			Endif

			U_SQLEXEC('COMMIT TRANSACTION')

		Endif
	Endif
Endif

*** CONFIRMAR CUP�ES UTILIZADOS
***********************************

SELECT FPOSC
MSTAMP = FPOSC.FPOSCSTAMP

TEXT TO usql TEXTMERGE NOSHOW
UPDATE U_CUPOM
SET confirmado = 1
WHERE u_CUPOM.ORISTAMP = '<<MSTAMP>>'
ENDTEXT
IF !u_sqlexec(usql)
	msg('Erro a confirmar cup�es')
	msg(usql)
	return
ENDIF

TEXT TO usql TEXTMERGE NOSHOW
UPDATE U_CUPO
SET JAUTILIZADO = 1
WHERE U_CUPO.NCUPAO in (select NCUPAO from u_CUPOM (nolock) where u_CUPOM.ORISTAMP = '<<MSTAMP>>' and u_CUPOM.CONFIRMADO = 1)
ENDTEXT

IF !u_sqlexec(usql)
	msg('Erro a colucar cup�es como utilizados')
	msg(usql)
	return
ENDIF
*************************************