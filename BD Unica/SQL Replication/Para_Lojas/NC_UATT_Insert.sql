USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_attachment]    Script Date: 06/09/2023 17:36:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_UATT_Insert]
    @c1 varchar(25),
    @c2 text,
    @c3 text,
    @c4 varchar(10),
    @c5 varchar(25),
    @c6 date,
    @c7 varchar(8),
    @c8 varchar(30),
    @c9 date,
    @c10 varchar(8),
    @c11 varchar(30),
    @c12 bit,
    @c13 text
as
begin  
	insert into [dbo].[u_attachment] (
		[u_attachmentstamp],
		[attachment],
		[description],
		[oritable],
		[oritablestamp],
		[ousrdata],
		[ousrhora],
		[ousrinis],
		[usrdata],
		[usrhora],
		[usrinis],
		[marcada],
		[filename]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13	) 

        EXEC dbo.NC_Sinc_Insert 'dbo','PG','u_attachment',@c1, 'u_attachmentstamp'

end  
