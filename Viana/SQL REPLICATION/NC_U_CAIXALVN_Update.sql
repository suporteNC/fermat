USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_caixal]    Script Date: 27/01/2023 16:44:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXALVN_Update]
		@c1 char(25),
		@c2 varchar(100),
		@c3 varchar(50),
		@c4 numeric(15,2),
		@c5 varchar(25),
		@c6 varchar(25),
		@c7 varchar(25),
		@c8 numeric(10,0),
		@c9 varchar(25),
		@c10 numeric(15,5),
		@c11 numeric(15,5),
		@c12 numeric(15,5),
		@c13 numeric(3,0),
		@c14 char(25),
		@c15 varchar(30),
		@c16 varchar(25),
		@c17 varchar(30),
		@c18 numeric(5,0),
		@c19 varchar(20),
		@c20 varchar(30),
		@c21 varchar(15),
		@c22 bit,
		@c23 varchar(25),
		@c24 numeric(10,2),
		@c25 numeric(10,2),
		@c26 numeric(10,0),
		@c27 varchar(35),
		@c28 varchar(25),
		@c29 varchar(40),
		@c30 varchar(30),
		@c31 datetime,
		@c32 varchar(8),
		@c33 varchar(30),
		@c34 datetime,
		@c35 varchar(8),
		@c36 bit,
		@c37 datetime,
		@c38 char(25),
		@c39 varchar(100),
		@c40 varchar(50),
		@c41 numeric(15,2),
		@c42 varchar(25),
		@c43 varchar(25),
		@c44 varchar(25),
		@c45 numeric(10,0),
		@c46 varchar(25),
		@c47 numeric(15,5),
		@c48 numeric(15,5),
		@c49 numeric(15,5),
		@c50 numeric(3,0),
		@c51 char(25),
		@c52 varchar(30),
		@c53 varchar(25),
		@c54 varchar(30),
		@c55 numeric(5,0),
		@c56 varchar(20),
		@c57 varchar(30),
		@c58 varchar(15),
		@c59 bit,
		@c60 varchar(25),
		@c61 numeric(10,2),
		@c62 numeric(10,2),
		@c63 numeric(10,0),
		@c64 varchar(35),
		@c65 varchar(25),
		@c66 varchar(40),
		@c67 varchar(30),
		@c68 datetime,
		@c69 varchar(8),
		@c70 varchar(30),
		@c71 datetime,
		@c72 varchar(8),
		@c73 bit,
		@c74 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c38 = @c1)
begin 
update [VN].[u_caixal] set
		[u_caixalstamp] = @c38,
		[obs] = @c39,
		[rubrica] = @c40,
		[valorreg] = @c41,
		[ou_caixalstamp] = @c42,
		[ftstamp] = @c43,
		[nmdoc] = @c44,
		[fno] = @c45,
		[o_ucaixalstamp] = @c46,
		[compra] = @c47,
		[valdevol] = @c48,
		[creditar] = @c49,
		[estab] = @c50,
		[u_caixastamp] = @c51,
		[site] = @c52,
		[oristamp] = @c53,
		[cxusername] = @c54,
		[pno] = @c55,
		[pnome] = @c56,
		[multibanco] = @c57,
		[piso] = @c58,
		[alterado] = @c59,
		[stamp] = @c60,
		[valor] = @c61,
		[conta] = @c62,
		[linha] = @c63,
		[item] = @c64,
		[idcx] = @c65,
		[tipo] = @c66,
		[ousrinis] = @c67,
		[ousrdata] = @c68,
		[ousrhora] = @c69,
		[usrinis] = @c70,
		[usrdata] = @c71,
		[usrhora] = @c72,
		[marcada] = @c73,
		[u_ttssinc] = @c74
	where [u_caixalstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixalstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[u_caixal]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [VN].[u_caixal] set
		[obs] = @c39,
		[rubrica] = @c40,
		[valorreg] = @c41,
		[ou_caixalstamp] = @c42,
		[ftstamp] = @c43,
		[nmdoc] = @c44,
		[fno] = @c45,
		[o_ucaixalstamp] = @c46,
		[compra] = @c47,
		[valdevol] = @c48,
		[creditar] = @c49,
		[estab] = @c50,
		[u_caixastamp] = @c51,
		[site] = @c52,
		[oristamp] = @c53,
		[cxusername] = @c54,
		[pno] = @c55,
		[pnome] = @c56,
		[multibanco] = @c57,
		[piso] = @c58,
		[alterado] = @c59,
		[stamp] = @c60,
		[valor] = @c61,
		[conta] = @c62,
		[linha] = @c63,
		[item] = @c64,
		[idcx] = @c65,
		[tipo] = @c66,
		[ousrinis] = @c67,
		[ousrdata] = @c68,
		[ousrhora] = @c69,
		[usrinis] = @c70,
		[usrdata] = @c71,
		[usrhora] = @c72,
		[marcada] = @c73,
		[u_ttssinc] = @c74
	where [u_caixalstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixalstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[u_caixal]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','VN','u_caixal',@c1, 'u_caixalstamp', ''