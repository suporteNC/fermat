USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_stsubfam]    Script Date: 24/01/2023 17:59:58 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_STSUBFAM_Update]
		@c1 char(25),
		@c2 varchar(10),
		@c3 varchar(30),
		@c4 varchar(30),
		@c5 datetime,
		@c6 varchar(8),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 bit,
		@c11 numeric(16,2),
		@c12 bit,
		@c13 datetime,
		@c14 char(25),
		@c15 varchar(10),
		@c16 varchar(30),
		@c17 varchar(30),
		@c18 datetime,
		@c19 varchar(8),
		@c20 varchar(30),
		@c21 datetime,
		@c22 varchar(8),
		@c23 bit,
		@c24 numeric(16,2),
		@c25 bit,
		@c26 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c14 = @c1)
begin 
update [PG].[u_stsubfam] set
		[u_stsubfamstamp] = @c14,
		[csubfam] = @c15,
		[subfam] = @c16,
		[ousrinis] = @c17,
		[ousrdata] = @c18,
		[ousrhora] = @c19,
		[usrinis] = @c20,
		[usrdata] = @c21,
		[usrhora] = @c22,
		[marcada] = @c23,
		[margem] = @c24,
		[pea] = @c25,
		[u_ttssinc] = @c26
	where [u_stsubfamstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_stsubfamstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_stsubfam]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[u_stsubfam] set
		[csubfam] = @c15,
		[subfam] = @c16,
		[ousrinis] = @c17,
		[ousrdata] = @c18,
		[ousrhora] = @c19,
		[usrinis] = @c20,
		[usrdata] = @c21,
		[usrhora] = @c22,
		[marcada] = @c23,
		[margem] = @c24,
		[pea] = @c25,
		[u_ttssinc] = @c26
	where [u_stsubfamstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_stsubfamstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_stsubfam]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','u_stsubfam',@c1, 'u_stsubfamstamp', ''