 Xexpressao
"** Criado por:J�lio Ricardo
** Data cria��o: 11..04.2014

LOCAL cXpdstamp

TEXT TO m.Msel TEXTMERGE NOSHOW PRETEXT 7
	SELECT	convert(varchar(10), bo.dataobra, 121) dataobra, 
			bo.nmdos, bo.obrano, bo.no, bo.estab, bo.nome, 
			bo.bostamp,  rtrim(bo.nmdos) + ' N� ' + convert(varchar(10), bo.obrano) as dossier, 
             bo.obs,
			 bo.obranome
			, case when bo.datafinal = '19000101' then '' else convert(char(10), bo.datafinal, 105) end as Datafinal
			, bo.u_hrentr Hora_Entrega
                    , bo.ousrinis
	from	bo (nolock)
	INNER	JOIN bo2 (nolock) on bo2.bo2stamp = bo.bostamp 
	WHERE	 bo.fechada = 0 and bo2.anulado = 0 and bo.ndos = 7 
	group	by bo.dataobra, bo.nmdos, bo.obrano, bo.no, bo.estab, bo.nome, bo.bostamp, bo.obs, bo.obranome, bo.datafinal, bo.u_hrentr
                     ,  bo.ousrinis
	order	by bo.datafinal
	
ENDTEXT

u_sqlexec(m.Msel, "c_boRptemp")
SELECT c_boRptemp
GOTO top"
