USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbostic]    Script Date: 30/03/2023 15:00:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STIC_Insert]
    @c1 char(25),
    @c2 datetime,
    @c3 varchar(60),
    @c4 bit,
    @c5 char(25),
    @c6 varchar(20),
    @c7 varchar(30),
    @c8 datetime,
    @c9 varchar(8),
    @c10 varchar(30),
    @c11 datetime,
    @c12 varchar(8),
    @c13 bit,
    @c14 bit,
    @c15 bit,
    @c16 varchar(60),
    @c17 varchar(8),
    @c18 bit,
    @c19 varchar(30),
    @c20 varchar(30),
    @c21 varchar(20),
    @c22 varchar(40),
    @c23 varchar(6),
    @c24 bit,
    @c25 varchar(40),
    @c26 numeric(1,0),
    @c27 bit,
    @c28 numeric(10,0),
    @c29 numeric(10,0),
    @c30 varchar(55),
    @c31 varchar(16)
as
begin  
	insert into [PG].[stic] (
		[sticstamp],
		[data],
		[descricao],
		[lanca],
		[stamp],
		[ccusto],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[exportado],
		[impresso],
		[userimpresso],
		[hora],
		[zera],
		[u_ffim],
		[u_fini],
		[u_status],
		[u_sticquem],
		[u_terminal],
		[u_esticg],
		[u_codebar],
		[u_tipostic],
		[u_updst],
		[u_diquebra],
		[u_disobra],
		[u_identif],
		[u_loja]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','stic',@c1, 'sticstamp'

end  
