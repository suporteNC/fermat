** Criado por: TOTALSOFT | Paulo Ricardo Martins 
** Data: 26/09/2017
** Valida��o de cria��o de cart�es de cliente


select U_CT

**** CARTOES DUPLICADOS *************
TEXT TO MSQL TEXTMERGE NOSHOW
SELECT count(U_CTSTAMP) as QTT
FROM U_CT
WHERE NCARTAO = '<<U_CT.NCARTAO>>'
and U_CTSTAMP <> '<<u_CT.U_CTSTAMP>>'
ENDTEXT

If !U_SQLEXEC(MSQL,'CR_CARTAO')
	MSG('Erro Encontrado')
	MSG(MSQL)
	RETURN
ENDIF
SELECT CR_CARTAO
IF CR_CARTAO.QTT > 0
	MSG('Cart�o j� existente!')
	FECHA("CR_CARTAO")
	Return .F.
ENDIF
FECHA("CR_CARTAO")
*************************************


**** S� 1 CART�O ACTIVO POR CLIENTE ***********
SELECT u_CT
IF U_CT.INACTIVO = .f.

	TEXT TO MSQL TEXTMERGE NOSHOW
	SELECT count(U_CTSTAMP) as QTT
	FROM U_CT
	WHERE NO = '<<U_CT.NO>>'
	and inactivo = 0
	and U_CTSTAMP <> '<<U_CT.U_CTSTAMP>>'
	ENDTEXT

	If !U_SQLEXEC(MSQL,'CR_CARTAO')
		MSG('Erro Encontrado')
		MSG(MSQL)
		RETURN
	ENDIF
	SELECT CR_CARTAO
	IF CR_CARTAO.QTT > 0
		IF PERGUNTA('Existem outros cart�es activos para este cliente! Deseja inactiva-los')
			TEXT TO MSQL TEXTMERGE NOSHOW
			UPDATE u_CT 
			SET inactivo = 1
				,DTINACTIVO  = getdate()
			FROM U_CT
			WHERE NO = '<<U_CT.NO>>'
			and U_CTSTAMP <> '<<U_CT.U_CTSTAMP>>'
			ENDTEXT
			If !U_SQLEXEC(MSQL)
				MSG('Erro Encontrado')
				MSG(MSQL)
				RETURN
			ENDIF
			MSG('Actualiza��o de cart�es terminada com sucesso')
		ELSE
			MSG('S� pode existir um cart�o activo por cliente! Acc��o cancelada')
			FECHA("CR_CARTAO")
			Return .F.
		ENDIF
		FECHA("CR_CARTAO")
		Return .F.
	ENDIF

	TEXT TO MSQL TEXTMERGE NOSHOW
	UPDATE CL2 
	SET CL2.U_NCARTAO = '<<U_CT.NCARTAO>>'
	FROM CL2 INNER JOIN CL on CL.CLSTAMP=CL2.CL2STAMP
	WHERE CL.NO = <<ASTR(U_CT.NO)>>
	ENDTEXT

	If !U_SQLEXEC(MSQL)
		MSG('Erro Encontrado')
		MSG(MSQL)
		RETURN
	ENDIF

	FECHA("CR_CARTAO")
ENDIF

***********************************************


select U_CT
IF U_CT.INACTIVO = .t. and changed("U_CT.INACTIVO")

	replace DTINACTIVO with date()


ENDIF