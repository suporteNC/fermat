*******************************************
** Guarda estabelecimento
**
** Criado por Rui Vale
** Criado em  26/01/2018
*******************************************

Select cc

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From e1(nolock)
	Where estab = 0
ENDTEXT

If u_sqlexec(uSql,"uCurE1") And Reccount("uCurE1")>0
	Select fc
	Replace fc.u_estab With uCurE1.u_estab

	fecha("uCurE1")
Endif

Return fc.zona