USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbospp]    Script Date: 09/02/2023 18:17:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_SPPLojas_Update]
		@c1 char(25),
		@c2 varchar(100),
		@c3 text,
		@c4 text,
		@c5 text,
		@c6 bit,
		@c7 text,
		@c8 bit,
		@c9 bit,
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 varchar(30),
		@c14 datetime,
		@c15 varchar(8),
		@c16 bit,
		@c17 char(25),
		@c18 varchar(100),
		@c19 text,
		@c20 text,
		@c21 text,
		@c22 bit,
		@c23 text,
		@c24 bit,
		@c25 bit,
		@c26 varchar(30),
		@c27 datetime,
		@c28 varchar(8),
		@c29 varchar(30),
		@c30 datetime,
		@c31 varchar(8),
		@c32 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c17 = @c1)
begin 
update [PG].[spp] set
		[sppstamp] = @c17,
		[resumo] = @c18,
		[obs] = @c19,
		[descricao] = @c20,
		[critexpr] = @c21,
		[criteprg] = @c22,
		[porspexpr] = @c23,
		[porspeprg] = @c24,
		[inactivo] = @c25,
		[ousrinis] = @c26,
		[ousrdata] = @c27,
		[ousrhora] = @c28,
		[usrinis] = @c29,
		[usrdata] = @c30,
		[usrhora] = @c31,
		[marcada] = @c32
	where [sppstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sppstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[spp]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[spp] set
		[resumo] = @c18,
		[obs] = @c19,
		[descricao] = @c20,
		[critexpr] = @c21,
		[criteprg] = @c22,
		[porspexpr] = @c23,
		[porspeprg] = @c24,
		[inactivo] = @c25,
		[ousrinis] = @c26,
		[ousrdata] = @c27,
		[ousrhora] = @c28,
		[usrinis] = @c29,
		[usrdata] = @c30,
		[usrhora] = @c31,
		[marcada] = @c32
	where [sppstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sppstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[spp]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','spp',@c1, 'sppstamp', ''