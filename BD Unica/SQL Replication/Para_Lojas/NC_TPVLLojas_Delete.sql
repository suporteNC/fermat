USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbotpvl]    Script Date: 09/02/2023 18:19:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_TPVLLojas_Delete]
		@c1 char(25),
		@c2 varchar(200),
		@c3 numeric(3,0),
		@c4 bit,
		@c5 numeric(1,0),
		@c6 numeric(1,0),
		@c7 numeric(5,2),
		@c8 numeric(18,5),
		@c9 numeric(19,6),
		@c10 numeric(1,0),
		@c11 varchar(18),
		@c12 varchar(18),
		@c13 varchar(18),
		@c14 numeric(18,5),
		@c15 numeric(19,6),
		@c16 bit,
		@c17 numeric(10,0),
		@c18 bit,
		@c19 bit,
		@c20 text,
		@c21 bit,
		@c22 bit,
		@c23 varchar(30),
		@c24 datetime,
		@c25 varchar(8),
		@c26 varchar(30),
		@c27 datetime,
		@c28 varchar(8),
		@c29 bit,
		@c30 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[tpvl] 
	where [tpvlno] = @c3
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpvlno] = ' + convert(nvarchar(100),@c3,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tpvl]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from tpvl where tpvlstamp=@c1