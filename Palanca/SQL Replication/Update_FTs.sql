update ft2
set f2.u_estab=3 from ft2 inner join ft on ft.ftstamp=ft2.ft2stamp where ft.u_estab=3 and ft.ousrdata>='2023.01.18'
update ft3
set f3.u_estab=3 from ft3 inner join ft on ft.ftstamp=ft3.ft3stamp where ft.u_estab=3 and ft.ousrdata>='2023.01.18'

update fi
set fi.u_estab=3 from fi inner join ft on ft.ftstamp=fi.ftstamp where ft.u_estab=3 and ft.ousrdata>='2023.01.18'
update fi2
set fi2.u_estab=3 from fi2 inner join ft on ft.ftstamp=fi2.ftstamp where ft.u_estab=3 and ft.ousrdata>='2023.01.18'