Create Cursor uDadosIf (Tipo c(30) ,  descricao c (50),  datai d(8),dataf d(8),ref c(18),Design c(65),epfixo N(15,2), desconto  N(15,2))

gcTable=GETFILE('XLS', 'Procurar ficheiro com a extens�o .XLS:', 'Procurar', 0, 'Procurar')
if  'Untitled' $ gcTable or   EMPTY(gcTable)
	msg("Ficheiro Invalido.")
	fecha("sqltmp")
	return  
endif   

SELECT uDadosIf 
APPEND FROM (gcTable) XL5
SELECT uDadosIf 
GOTO TOP 
DELETE  



Create Cursor uCurErros ( Erro c(254) )
**Do  xlx_imp

Select uDadosIf
m.escolheu =  .F.
mostrameisto("uDadosIf")

If  m.escolheu =  .F.
	msg("Opera��o cancelada.")
	Return
Endif


Select uDadosIf

Scan
	If Empty(uDadosIf.ref)
		msg("A linha com a designa��o " + Alltrim(uDadosIf.Design) + " n�o tem referencia.")
		Return
	Endif

	If uDadosIf.epfixo = 0 And uDadosIf.desconto = 0
		msg("A linha com a ref " + Alltrim(uDadosIf.ref) + " n�o tem pre�o e desconto.")
		Return
	Endif
	If uDadosIf.desconto <>  0 And  uDadosIf.epfixo <> 0
		msg("A linha com a ref " + Alltrim(uDadosIf.ref) + " tem pre�o e desconto.")
		Return
	Endif

**  valida se o cliente existe
	TEXT TO msel NOSHOW TEXTMERGE
		SELECT design   FROM st (nolock)
		where ref = '<<uDadosIf.ref>>' and inactivo = 0
	ENDTEXT
	If Not u_sqlexec(msel,"c_Temp")
		msg(msel)
		Return
	Endif
	If  Reccount("c_temp") = 0
		msg("A linha com a ref " + Alltrim(uDadosIf.ref) + " tem um artigo que n�o existe.")
		fecha("c_Temp")
		Return
	Else
		Select uDadosIf
		Replace uDadosIf.Design  With  c_temp.Design
	Endif
	fecha("c_Temp")

*valida se o tipo  existe
	If  Not Empty(uDadosIf.tipo)
		TEXT TO msel NOSHOW TEXTMERGE
			select  campo  from dytable (nolock) where campo = '<<uDadosIf.tipo>>' and  entityname = 'a_cltipo'
		ENDTEXT
		If Not u_sqlexec(msel,"c_Temp")
			msg(msel)
			Return
		Endif
		If  Reccount("c_temp") = 0
			msg("A linha com a ref " + Alltrim(uDadosIf.ref) + " tem um tipo que n�o existe.")
			fecha("c_Temp")
			Return
		Endif
		fecha("c_Temp")
	Endif
Endscan


Select uDadosIf
Scan For  Upper(descricao) <> 'DESCRI��O'
	TEXT TO msel NOSHOW TEXTMERGE
			INSERT INTO sp
			(spstamp, descricao,tipo,  datai, dataf, refi, STDESIGN  , pfixo, epfixo,desc1 ,
 			porcl, porst, porsp, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,sites
			)
			values
			(LEFT(replace(newid(),'-','.'),24)
			,'<<uDadosIf.descricao>>', '<<upper(uDadosIf.tipo)>>', '<<DTOS(uDadosIf.datai)>>', '<<DTOS(uDadosIf.dataf)>>','<<astr(uDadosIf.ref)>>', '<<astr(uDadosIf.design)>>'
			,replace('<<astr(uDadosIf.epfixo)>>',',','.') ,replace('<<astr(uDadosIf.epfixo)>>',',','.')
			, replace('<<astr(uDadosIf.desconto)>>',',','.')
			,<<IIF(EMPTY(uDadosIf.tipo),1,2)>>,3, <<IIF(uDadosIf.desconto <> 0,2,4)>>
			, '<<m.m_chinis>>', convert(char(8),getdate(),112), convert(char(10),getdate(),108)
			, '<<m.m_chinis>>', convert(char(8),getdate(),112), convert(char(10),getdate(),108)
			,1
			)
	ENDTEXT
	If Not u_sqlexec(msel)
		msg(msel)
		Return
	Endif
Endscan


msg("Importa��o concluida")



Procedure xlx_imp

Create Cursor uCurErros ( Erro c(254) )
** Vou pedir o ficheiro para importar
uNomeFicheiroZip = Getfile("","Ficheiro a abrir","Abrir",2,"Abrir")
uNomeDoFicheiro = Justfname(uNomeFicheiroZip)

If Empty(uNomeFicheiroZip)
	msg("Cancelado pelo utilizador!!!")
	Return
Endif

uFolhas = ""

uExtFile = Upper(Justext(uNomeFicheiroZip))

If !Alltrim(Upper(uExtFile))=="XLS" And !Alltrim(Upper(uExtFile))=="XLSX"
	msg("Tipo de ficheiro inv�lido!!!")
	Return
Endif

** Se � um XLSX, transformo em XLS
If uExtFile = "XLSX"

	u_nficheiro = Justpath(uNomeFicheiroZip)+"\"+Strtran(Justfname(uNomeFicheiroZip),".XLSX","_XLS.XLS")

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.Visible = .T.
	oExcel.DisplayAlerts = .F.
	oExcel.ActiveWorkbook.SaveAs(u_nficheiro, 39)
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

	uNomeFicheiroZip = u_nficheiro
Else

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

Endif
**************

Create Cursor xVars ( No N(5), Tipo c(1), Nome c(40), Pict c(100), lOrdem N(10), nValor N(18,5), cValor c(250), lValor l, dValor d, tbval M )

Select xVars
Append Blank
Replace xVars.No With 1
Replace xVars.Tipo With "T"
Replace xVars.Nome With "Folha"
Replace xVars.Pict With ""
Replace xVars.lOrdem With 1
Replace xVars.tbval With uFolhas

m.mCaption = "Introduza os dados"
m.escolheu=.F.
docomando("do form usqlvar with 'xvars',m.mCaption")

If ! m.escolheu
	msg("Cancelado pelo utilizador")
	Return
Else
	Select xVars
	Locate For No=1
	uNomeFolha = xVars.cValor
	Locate For No=2
	uNo = xVars.nValor
Endif

uFechar = .F.

uNomeFicheiroZip = ["] + uNomeFicheiroZip + ["]
uNomeFolha = ["] + Alltrim(uNomeFolha) + ["]


Try
	Select uDadosIf
	Append From &uNomeFicheiroZip Type Xl5 Sheet &uNomeFolha
Catch
	msg("Os dados a importar n�o est�o de acordo com o esperado!!!"+Chr(13)+Chr(10)+"Verifique se o documento est� aberto, e se est� na vers�o correta.")
	uFechar = .T.
Endtry
Delete File &uNomeFicheiroZip
If uFechar
	Return
Endif
Select uDadosIf
Goto Top
Delete

Endproc








