*******************************************
** Guarda estabelecimento
**
** Criado por J�lio Ricardo
** Criado em  25/01/2018
*******************************************

Select rd

TEXT TO uSql TEXTMERGE NOSHOW
	Select u_estab
	From tsrd(nolock)
	Where ndoc=<<STR(rd.ndoc)>>
ENDTEXT

If u_sqlexec(uSql,"uCurTs") And Reccount("uCurTs")>0
	Select uCurTs
	Replace rd.u_estab With uCurTs.u_estab

	fecha("uCurTs")
	Return rd.u_estab
Endif

Return rd.u_estab