*************************************
*** Importa��o inicial de artigos
*** Criado em 13/07/2017
*** Criado por Rui Vale
*************************************

Msg("Aten��o, a estrutura do ficheiro tem de ser :" + Chr(13) + Chr(10);
	+ "Refer�ncia C(18), Designa��o C(60), Cria Mondego L (Sim,N�o), Cria Import L (Sim,N�o), Cria Fermat L (Sim,N�o), Tipo Artigo C (20)" + Chr(13) + Chr(10) ;
	+ ", Fam�lia C(20), Sub Fam�lia C(20), Marca C(20), Modelo C(20), Inactivo L (Sim,N�o), Unidade C(4), Unid. Alt. C(4)" + Chr(13) + Chr(10) ;
	+ ", Fator Convers�o N(16,6), Cod Barras C(20), N� Fornecedor N(10), Fornecedor C(55), Ref.Fornecedor C(18), Cod Pautal C(20)" + Chr(13) + Chr(10) ;
	+ ", Pre�o de custo N(16,2), Pre�o de tabela N(16,2), Margem 1 N(10,3), Pre�o de venda N(16,2), Stock min N(16,3), Stock maxN(16,3)" + Chr(13) + Chr(10) ;
	+ ", Tabela iva N(2)")

Create Cursor uCurErros ( Erro C(254) )
Create Cursor uDadosSt (Ref C(18), Design C(60), Mondego C(3), Import C(3), Fermat C(3), TipoArtigo C (20), Familia C(20), Sub_Familia C(20) ;
	, Marca C(20), Modelo C(20), Inactivo C(3), Unidade C(4), Unid_Alt C(4), Conversao N(16,6), Codigo C(20), Fornec N(10), Fornecedor C(55) ;
	, RefFor C(18), CodigoPautal C(20), epcult N(16,3), epcusto N(16,3), Margem1 N(16,3), epv1 N(16,3), Stmin N(16,3), Stmax N(16,3), Tabiva N(2))

** Vou pedir o ficheiro para importar
uNomeFicheiroZip = Getfile("","Ficheiro a abrir","Abrir",2,"Abrir")

If Empty(uNomeFicheiroZip)
	Msg("Cancelado pelo utilizador!!!")
	Return
Endif

uFolhas = ""

uExtFile = Upper(Justext(uNomeFicheiroZip))

If !Alltrim(Upper(uExtFile))=="XLS" And !Alltrim(Upper(uExtFile))=="XLSX"
	Msg("Tipo de ficheiro inv�lido!!!")
	Return
Endif

** Se � um XLSX, transformo em XLS
If uExtFile = "XLSX"

	u_nficheiro = Justpath(uNomeFicheiroZip)+"\"+Strtran(Justfname(uNomeFicheiroZip),".XLSX","_XLS.XLS")

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.Visible = .T.
	oExcel.DisplayAlerts = .F.
	oExcel.ActiveWorkbook.SaveAs(u_nficheiro, 39)
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

	uNomeFicheiroZip = u_nficheiro
Else

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

Endif
**************

Create Cursor xVars ( No N(5), tipo C(1), Nome C(40), Pict C(100), lOrdem N(10), nValor N(18,5), cValor C(250), lValor l, dValor d, tbval M )

Select xVars
Append Blank
Replace xVars.No With 1
Replace xVars.tipo With "T"
Replace xVars.Nome With "Folha"
Replace xVars.Pict With ""
Replace xVars.lOrdem With 1
Replace xVars.tbval With uFolhas

m.mCaption = "Qual a Folha a importar"
m.escolheu=.F.
docomando("do form usqlvar with 'xvars',m.mCaption")

If ! m.escolheu
	Msg("Cancelado pelo utilizador")
	Return
Else
	Select xVars
	Locate For No=1
	uNomeFolha = xVars.cValor
Endif

uFechar = .F.

uNomeFicheiroZip = ["] + uNomeFicheiroZip + ["]
uNomeFolha = ["] + Alltrim(uNomeFolha) + ["]

Try
	Select uDadosSt
	Append From &uNomeFicheiroZip Type Xl5 Sheet &uNomeFolha
Catch
	Msg("Os dados a importar n�o est�o de acordo com o esperado!!!")
	uFechar = .T.
Endtry

select  uDadosSt
scan for  empty(uDadosSt.ref) and   empty(uDadosSt.design)
	delete 
endscan 

mostrameisto("uDadosSt")

If ! Pergunta("Quer mesmo importar")
	uFechar = .T.
Endif

If uFechar
	Return
Endif

Select uDadosSt
ntotal=Reccount()

regua(0,ntotal,"A importar os artigos!!!")


Select uDadosSt
Go Top
Skip
Do While !Eof()

	regua[1,recno("uDadosSt"),"A importar o artigo Refer�ncia : "+Alltrim(uDadosSt.Ref)]

	If Empty(uDadosSt.Ref)
		uCriar=.T.
	Else
		TEXT TO uSql TEXTMERGE noshow
			SELECT *
			From st (nolock)
			Where ref = '<<uDadosSt.ref>>'
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			If Reccount("uCurArtigos")<=0
				uCriar=.T.
			Else
				uCriar=.F.
			Endif
		Endif
	Endif

	If uCriar
		

		TEXT TO uSql TEXTMERGE noshow
			SELECT *
			From st (nolock)
			Where 1=2
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			Select uCurArtigos
			Append Blank
			Replace uCurArtigos.ststamp With u_Stamp()
			If Empty(uDadosSt.Ref)
				TEXT TO uSql TEXTMERGE noshow
					select isnull(max(cast(ref as numeric(14)))+1,1) as ref
					from st (nolock)
					where ref<>''
					and isnumeric(ref)=1
				ENDTEXT

				If u_Sqlexec(uSql,"uCurRefArtigos") And Reccount("uCurRefArtigos")>0
					uRef = uCurRefArtigos.Ref
				Endif
			Else
				uRef = uDadosSt.Ref
			Endif
			select uCurArtigos
			Replace uCurArtigos.Ref With astr(uRef)
			Replace uCurArtigos.Design With uDadosSt.Design
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.u_ctpart With Alltrim(uDadosSt.TipoArtigo)
			Replace uCurArtigos.u_csubfam With Alltrim(uDadosSt.Sub_Familia)
			Replace uCurArtigos.u_mondego With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Inactivo))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.F.,.T.)
			Replace uCurArtigos.u_impor With Iif(Alltrim(Upper(uDadosSt.Import))=="SIM",.T.,.F.)
			Replace uCurArtigos.u_fermat With Iif(Alltrim(Upper(uDadosSt.Fermat))=="SIM",.T.,.F.)
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.usr1 With Alltrim(uDadosSt.Marca)
			Replace uCurArtigos.usr2 With Alltrim(uDadosSt.Modelo)
			Replace uCurArtigos.Unidade With Alltrim(uDadosSt.Unidade)
			Replace uCurArtigos.Uni2 With Alltrim(uDadosSt.Unid_Alt)
			Replace uCurArtigos.Conversao With uDadosSt.Conversao
			Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
			Replace uCurArtigos.Fornec With uDadosSt.Fornec
			Replace uCurArtigos.cpoc With 1
			Replace uCurArtigos.containv With '321'
			Replace uCurArtigos.contacev With '611'
			replace st.imagem with "\\192.168.1.200\Mondego\Artigos_fotos\"+alltrim(astr(uRef))+".jpg"			

			select uCurArtigos 
			Replace uCurArtigos.Fornecedor With Alltrim(uDadosSt.Fornecedor)
			Replace uCurArtigos.ForRef With Alltrim(uDadosSt.RefFor)
			Replace uCurArtigos.u_cpautal With Strtran(uDadosSt.CodigoPautal,".","")
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

			Select uCurArtigos
			If !Empty(uDadosSt.Codigo)
				uCodEan13 = Ean13ckd(Left(Alltrim(uDadosSt.Codigo),12))
				If uCodEan13 == Alltrim(uDadosSt.Codigo)
					Select uCurArtigos
					Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
				Else
					Select uCurErros
					Append Blank
					Replace uCurErros.Erro With "O EAN " + Alltrim(uDadosSt.Codigo) + " do artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + ", � inv�lido !!"
				Endif
			Endif

			Select uCurArtigos
			Replace uCurArtigos.Marg1 With uDadosSt.Margem1
			Replace uCurArtigos.epcpond With uDadosSt.epcult
			Replace uCurArtigos.epcusto With uDadosSt.epcusto
			Replace uCurArtigos.epcult With uDadosSt.epcult
			Replace uCurArtigos.epv1 With uDadosSt.epv1
			Replace uCurArtigos.Stmin With uDadosSt.Stmin
			Replace uCurArtigos.Stmax With uDadosSt.Stmax
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

			** Valida e l� familia
			TEXT TO uSql TEXTMERGE noshow
				SELECT nome
				From stfami (nolock)
				Where ref = '<<Alltrim(uDadosSt.Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurFami") And Reccount("uCurFami")>0
				Select uCurArtigos
				Replace uCurArtigos.faminome With uCurFami.Nome
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A familia " + Left(Alltrim(uDadosSt.Sub_Familia),3) + ", n�o existe !!"
			Endif

			** Valida Tipo Artigo
			TEXT TO uSql TEXTMERGE noshow
				SELECT ctpart,tpart
				From u_sttpart (nolock)
				Where ctpart = '<<Alltrim(uDadosSt.TipoArtigo)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurTpArt") And Reccount("uCurTpArt")>0
				Select uCurArtigos
				Replace uCurArtigos.u_tpart With uCurTpArt.tpart
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "O Tipo Artigo " + Left(Alltrim(uDadosSt.Sub_Familia),1) + ", n�o existe !!"
			Endif

			** Valida a Sub-fam�lia
			TEXT TO uSql TEXTMERGE noshow
				SELECT csubfam,subfam
				From u_stsubfam (nolock)
				Where csubfam = '<<Alltrim(uDadosSt.Sub_Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCursubfam") And Reccount("uCursubfam")>0
				Select uCurArtigos
				Replace uCurArtigos.u_subfam With uCursubfam.subfam
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A Sub-Fam�lia " + Alltrim(uDadosSt.Sub_Familia) + ", n�o existe !!"
			Endif

			Select uCurArtigos
			Replace uCurArtigos.ousrdata With Date()
			Replace uCurArtigos.ousrhora With Time()
			Replace uCurArtigos.ousrinis With m_chinis
			Replace uCurArtigos.usrdata With Date()
			Replace uCurArtigos.usrhora With Time()
			Replace uCurArtigos.usrinis With m_chinis

			If !Tts_GuardaSql("uCurArtigos","ST")
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "N�o foi poss�vel guardar o artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + " !!"
			Endif
			select  uCurArtigos
			TEXT TO uSql TEXTMERGE noshow
				SELECT *
				From stobs (nolock) 
				Where ref = '<<uCurArtigos.Ref>>'
			ENDTEXT
			If u_Sqlexec(uSql,"uCurStobs")
				If Reccount("uCurStobs")<=0
					Select uCurStobs
					Append Blank
					Replace uCurStobs.stobsstamp With u_Stamp()
					Replace uCurStobs.Ref With uCurArtigos.Ref
					Replace uCurStobs.tipoprod With "M"

					Replace uCurStobs.ousrdata With Date()
					Replace uCurStobs.ousrhora With Time()
					Replace uCurStobs.ousrinis With m_chinis
					Replace uCurStobs.usrdata With Date()
					Replace uCurStobs.usrhora With Time()
					Replace uCurStobs.usrinis With m_chinis  
					If !Tts_GuardaSql("uCurStobs","STOBS")
						Select uCurErros
						Append Blank
						Replace uCurErros.Erro With "N�o foi poss�vel guardar o artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + " !!"
					Endif
				Endif
			Endif
		Endif
	Else
		*Fecha("uCurArtigos")

		TEXT TO uSql TEXTMERGE noshow
			SELECT *
			From st (nolock)
			Where ref = '<<uDadosSt.Ref>>'
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			TEXT TO msel NOSHOW TEXTMERGE
				SELECT  nome FROM fl(nolock) WHERE no = <<adec_tr(uDadosSt.Fornec)>> and  estab = 0 
			ENDTEXT
			IF  NOT u_sqlexec(msel,"c_tempfl")
				msg(msel)
				RETURN 
			ENDIF 
			IF RECCOUNT("c_tempfl")= 0 
				msg("O fornecedor "+astr(uDadosSt.Fornec)+" n�o existe")
				RETURN  
			ENDIF   
			cfornecedor = c_tempfl.nome  
			fecha("c_tempfl")
			
			Select uCurArtigos
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.u_ctpart With Alltrim(uDadosSt.TipoArtigo)
			Replace uCurArtigos.u_csubfam With Alltrim(uDadosSt.Sub_Familia)
			Replace uCurArtigos.u_mondego With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Inactivo))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.F.,.T.)
			Replace uCurArtigos.u_impor With Iif(Alltrim(Upper(uDadosSt.Import))=="SIM",.T.,.F.)
			Replace uCurArtigos.u_fermat With Iif(Alltrim(Upper(uDadosSt.Fermat))=="SIM",.T.,.F.)
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.usr1 With Alltrim(uDadosSt.Marca)
			Replace uCurArtigos.usr2 With Alltrim(uDadosSt.Modelo)
			Replace uCurArtigos.Unidade With Alltrim(uDadosSt.Unidade)
			Replace uCurArtigos.Uni2 With Alltrim(uDadosSt.Unid_Alt)
			Replace uCurArtigos.Conversao With uDadosSt.Conversao
			Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
			Replace uCurArtigos.Fornec With uDadosSt.Fornec
			Replace uCurArtigos.Fornecedor With ALLTRIM(cfornecedor )
			Replace uCurArtigos.ForRef With Alltrim(uDadosSt.RefFor)
			Replace uCurArtigos.u_cpautal With Strtran(uDadosSt.CodigoPautal,".","")
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

			Select uCurArtigos
			If !Empty(uDadosSt.Codigo)
				uCodEan13 = Ean13ckd(Left(Alltrim(uDadosSt.Codigo),12))
				If uCodEan13 == Alltrim(uDadosSt.Codigo)
					Select uCurArtigos
					Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
				Else
					Select uCurErros
					Append Blank
					Replace uCurErros.Erro With "O EAN " + Alltrim(uDadosSt.Codigo) + " do artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + ", � inv�lido !!"
				Endif
			Endif

			Select uCurArtigos
			Replace uCurArtigos.Marg1 With uDadosSt.Margem1
			Replace uCurArtigos.epcpond With uDadosSt.epcult
			Replace uCurArtigos.epcusto With uDadosSt.epcusto
			Replace uCurArtigos.epcult With uDadosSt.epcult
			Replace uCurArtigos.epv1 With uDadosSt.epv1
			Replace uCurArtigos.Stmin With uDadosSt.Stmin
			Replace uCurArtigos.Stmax With uDadosSt.Stmax
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

			** Valida e l� familia
			TEXT TO uSql TEXTMERGE noshow
				SELECT nome
				From stfami (nolock)
				Where ref = '<<Alltrim(uDadosSt.Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurFami") And Reccount("uCurFami")>0
				Select uCurArtigos
				Replace uCurArtigos.faminome With uCurFami.Nome
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A familia " + Left(Alltrim(uDadosSt.Sub_Familia),3) + ", n�o existe !!"
			Endif

			** Valida Tipo Artigo
			TEXT TO uSql TEXTMERGE noshow
				SELECT ctpart,tpart
				From u_sttpart (nolock)
				Where ctpart = '<<Alltrim(uDadosSt.TipoArtigo)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurTpArt") And Reccount("uCurTpArt")>0
				Select uCurArtigos
				Replace uCurArtigos.u_tpart With uCurTpArt.tpart
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "O Tipo Artigo " + Left(Alltrim(uDadosSt.Sub_Familia),1) + ", n�o existe !!"
			Endif

			** Valida a Sub-fam�lia
			TEXT TO uSql TEXTMERGE noshow
				SELECT csubfam,subfam
				From u_stsubfam (nolock)
				Where csubfam = '<<Alltrim(uDadosSt.Sub_Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCursubfam") And Reccount("uCursubfam")>0
				Select uCurArtigos
				Replace uCurArtigos.u_subfam With uCursubfam.subfam
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A Sub-Fam�lia " + Alltrim(uDadosSt.Sub_Familia) + ", n�o existe !!"
			Endif

			Select uCurArtigos
			Replace uCurArtigos.usrdata With Date()
			Replace uCurArtigos.usrhora With Time()
			Replace uCurArtigos.usrinis With m_chinis

			If !Tts_AlteraReg("uCurArtigos","ST","st.ref='"+uDadosSt.Ref+"'")
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "N�o foi poss�vel guardar o artigo com refer�ncia " + Alltrim(uDadosSt.Ref) + " !!"
			Endif

		Endif
	Endif

	Select uDadosSt
	Skip
Enddo

regua(2)

If Reccount("uCurErros")>0
	mostrameisto("uCurErros")
Endif

Fecha("uCurArtigos")

Function Tts_GuardaSql
	Parameter meucursor,uMinhaTabela
	************************************************
	** Fun��o para guardar cursor numa tabela SQL **
	************************************************

	Local uMENCOUTROU,uNREG,uNREG1,uNRTENTATIVAS

	merro=.F.

	Select &meucursor

	gnFieldcount = Afields(gaMyArray)  && Create array
	Clear
	Dimension aCampos  [gnFieldcount]
	Dimension aTipos   [gnFieldcount]
	Dimension aTamanho [gnFieldcount]
	Dimension aCasas   [gnFieldcount]

	For nCount = 1 To gnFieldcount
		aCampos [nCount] = gaMyArray[nCount,1]
		aTipos  [nCount] = gaMyArray[nCount,2]
		aTamanho[nCount] = gaMyArray[nCount,3]
		aCasas  [nCount] = gaMyArray[nCount,4]
	Endfor

	sSQL=""
	cSQL=""
	inicSQL=""

	sSQLString = "select * from "+Alltrim(uMinhaTabela)+Chr(13)
	sSQLString =sSQLString +" (nolock) where 1=2"+Chr(13)

	If u_Sqlexec(sSQLString,"uCURTEMP")

		Select uCURTEMP

		gnFieldcount1 = Afields(uCURTEMP)  && Create array
		Clear
		Dimension aCampos1  [gnFieldcount1]
		Dimension aTipos1   [gnFieldcount1]
		Dimension aTamanho1 [gnFieldcount1]
		Dimension aCasas1   [gnFieldcount1]

		For nCount = 1 To gnFieldcount1
			aCampos1 [nCount] = uCURTEMP[nCount,1]
			aTipos1  [nCount] = uCURTEMP[nCount,2]
			aTamanho1[nCount] = uCURTEMP[nCount,3]
			aCasas1  [nCount] = uCURTEMP[nCount,4]
		Endfor


		** verifica quantos registos s�o iguais
		uNREG=0
		For nCount = 1 To gnFieldcount

			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uNREG=uNREG+1
				Endif

			Endfor
		Endfor

		uNREG1=0
		For nCount = 1 To gnFieldcount

			uMENCOUTROU=.F.
			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uMENCOUTROU=.T.
				Endif

			Endfor

			If uMENCOUTROU And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))

				uNREG1=uNREG1+1

				Select &meucursor

				cDado = Evaluate(aCampos[nCount])

				cTipo = aTipos[nCount]

				ucCampo = aCampos[nCount]


				If aTipos[nCount] = "N" Or aTipos[nCount] = "Y"
					cDado = Alltrim(Str(cDado,aTamanho[nCount],aCasas[nCount]))
				Endif

				If aTipos[nCount] = "I"
					cDado = Alltrim(Str(cDado))
				Endif

				If aTipos[nCount] = "T" Or aTipos[nCount] = "D"
					If Empty(cDado)
						cDado = "1900-01-01"
					Else
						cData = Alltrim(Str(Year(cDado)))+"-"+Padl(Alltrim(Str(Month(cDado))),2,"0")+"-"+Padl(Alltrim(Str(Day(cDado))),2,"0")
						cDado = cData
					Endif
				Endif

				If aTipos[nCount] = "L"
					If cDado = .T.
						cDado = Alltrim(Str(1))
					Else
						cDado = Alltrim(Str(0))
					Endif
				Endif

				If aTipos[nCount] = "N" Or aTipos[nCount] = "I" Or ;
						aTipos[nCount] = "Y" Or aTipos[nCount] = "B" Or ;
						aTipos[nCount] = "L"
					sSQL = sSQL + Strtran(Alltrim(cDado),",",".")
				Else
					sSQL = sSQL + "'" + Strtran(Alltrim(cDado),"'","'+char(39)+'")+ "'"
				Endif

				cSQL = cSQL + Alltrim(ucCampo)

				If uNREG1 < uNREG
					sSQL = sSQL + "," +Chr(13)
					cSQL = cSQL + ","+Chr(13)
				Endif

			Endif

		Endfor

		sSQLString = "Insert Into "+Alltrim(uMinhaTabela)+Chr(13)
		sSQLString =sSQLString +" ("+cSQL
		sSQLString =sSQLString +") Values ("
		sSQLString =sSQLString + sSQL + ")"
		If !Empty(inicSQL)
			sSQLString = sSQLString + inicSQL+Chr(13)
		Endif
		*msg(sSQLString)
		uNRTENTATIVAS=0
		merro=.T.
		Do While uNRTENTATIVAS<10 And merro

			If u_Sqlexec(sSQLString,"")
				merro=.F.
			Else
				merro=.T.
			Endif

			uNRTENTATIVAS=uNRTENTATIVAS+1

			Wait Window "Estou a Guardar!!" Nowait Timeout 100

		Enddo

		If merro
			_Cliptext=sSQLString
			= Aerror(aErrorArray)
			msgerro=aErrorArray(2)
			erros=.T.
			*!*				msg="O Registo N�o foi gravado!!!"
		Endif

	Endif

	*If Used(meucursor)
		*Fecha(meucursor)
	*Endif

	Return !merro
Endfunc

Function Tts_AlteraReg
	Lparameter meucursor,uMinhaTabela,uMinhaCondicao,uBaseDados,uServer

	***************************************************
	** Fun��o que altera um registo da Base de dados **
	***************************************************

	**************** PAR�METROS *******************
	** meucursor     : Nome do cursor a guardar
	** uMinhaTabela  : Nome da tabela para alterar
	** uMinhaCondicao: Condi��o para alterar
	** uBDados       : Base de Dados onde vai guardar
	***********************************************

	Local uMENCOUTROU,uNREG,uNREG1,uNRTENTATIVAS

	If Empty(uBaseDados)
		uBaseDados = ""
	Endif
	If Empty(uServer)
		uServer = ""
	Endif

	merro=.F.

	Select &meucursor

	gnFieldcount = Afields(gaMyArray)  && Create array
	Clear
	Dimension aCampos  [gnFieldcount]
	Dimension aTipos   [gnFieldcount]
	Dimension aTamanho [gnFieldcount]
	Dimension aCasas   [gnFieldcount]

	For nCount = 1 To gnFieldcount
		aCampos [nCount] = gaMyArray[nCount,1]
		aTipos  [nCount] = gaMyArray[nCount,2]
		aTamanho[nCount] = gaMyArray[nCount,3]
		aCasas  [nCount] = gaMyArray[nCount,4]
	Endfor

	sSQL=""
	cSQL=""

	If Empty(uBaseDados)
		sSQLString = "select * from " + Alltrim(uMinhaTabela) + Chr(13)
	Else
		If Empty(uServer)
			sSQLString = "select * from " + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + Chr(13)
		Else
			sSQLString = "select * from [" + Alltrim(uServer) + "]." + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + Chr(13)
		Endif
	Endif
	sSQLString = sSQLString + Alltrim(uMinhaTabela) + " (nolock) where 1=2"+Chr(13)

	If u_Sqlexec(sSQLString,"uCURTEMP")

		Select uCURTEMP
		gnFieldcount1 = Afields(uCURTEMP)  && Create array
		Clear
		Dimension aCampos1  [gnFieldcount1]
		Dimension aTipos1   [gnFieldcount1]
		Dimension aTamanho1 [gnFieldcount1]
		Dimension aCasas1   [gnFieldcount1]

		For nCount = 1 To gnFieldcount1
			aCampos1 [nCount] = uCURTEMP[nCount,1]
			aTipos1  [nCount] = uCURTEMP[nCount,2]
			aTamanho1[nCount] = uCURTEMP[nCount,3]
			aCasas1  [nCount] = uCURTEMP[nCount,4]
		Endfor

		** verifica quantos registos s�o iguais
		uNREG=0
		For nCount = 1 To gnFieldcount

			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uNREG=uNREG+1
				Endif

			Endfor
		Endfor

		uNREG1=0
		For nCount = 1 To gnFieldcount

			uMENCOUTROU=.F.
			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uMENCOUTROU=.T.
				Endif

			Endfor

			If uMENCOUTROU And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))

				uNREG1=uNREG1+1

				Select &meucursor

				cDado = Evaluate(aCampos[nCount])

				cTipo = aTipos[nCount]

				ucCampo = aCampos[nCount]

				If Alltrim(Upper(ucCampo))<>'USTRID' And Alltrim(Upper(ucCampo))<>'USTRID'

					If aTipos[nCount] = "N" Or aTipos[nCount] = "Y"
						cDado = Alltrim(Str(cDado,aTamanho[nCount],aCasas[nCount]))
					Endif

					If aTipos[nCount] = "I"
						cDado = Alltrim(Str(cDado))
					Endif

					If aTipos[nCount] = "T" Or aTipos[nCount] = "D"
						If Empty(cDado)
							cDado = "19000101"
						Else
							cData = Alltrim(Dtos(cDado))
							cDado = cData
						Endif
					Endif

					If aTipos[nCount] = "L"
						If cDado = .T.
							cDado = Alltrim(Str(1))
						Else
							cDado = Alltrim(Str(0))
						Endif
					Endif

					If aTipos[nCount] = "N" Or aTipos[nCount] = "I" Or ;
							aTipos[nCount] = "Y" Or aTipos[nCount] = "B" Or ;
							aTipos[nCount] = "L"
						sSQL = sSQL + aCampos[nCount] + "=" + Strtran(Alltrim(cDado),",",".")
					Else
						sSQL = sSQL + aCampos[nCount] + "=" + "'" +  Strtran(Alltrim(cDado),"'","''") + "'"
					Endif

					cSQL = cSQL + Alltrim(ucCampo)

				Endif
				If uNREG1 < uNREG
					sSQL = sSQL + "," +Chr(13)
					cSQL = cSQL + ","+Chr(13)
				Endif

			Endif

		Endfor

		If Empty(uBaseDados)
			sSQLString = "update " + Alltrim(uMinhaTabela) + " set "
		Else
			If Empty(uServer)
				sSQLString = "update " + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + " set "
			Else
				sSQLString = "update [" + Alltrim(uServer) + "]." + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + " set "
			Endif
		Endif

		sSQLString = sSQLString + sSQL
		sSQLString = sSQLString + " where " + Alltrim(uMinhaCondicao)

		uNRTENTATIVAS=0
		merro=.T.
		Do While uNRTENTATIVAS<100 And merro

			If u_Sqlexec(sSQLString)
				merro=.F.
			Else
				merro=.T.
			Endif

			uNRTENTATIVAS=uNRTENTATIVAS+1

			Wait Window "Estou a Guardar!!" Nowait Timeout 100

		Enddo

		If merro
			= Aerror(aErrorArray)
			_Cliptext=sSQLString
			Msg(sSQLString)
			*MostraErros(aErrorArray(1),aErrorArray(2),aErrorArray(3),aErrorArray(4))

			*mostraerros()

			*MostraErros(aErrorArray(1),aErrorArray(2),aErrorArray(3),aErrorArray(4))
			Return .F.
		Endif

	Endif

	*If Used(meucursor)
	*	fecha(meucursor)
	*Endif

	Return .T.

Endfunc