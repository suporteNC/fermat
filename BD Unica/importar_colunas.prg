Tab=getnome("Introduza o nome da tabela:")
If !Empty(Tab)
	Select fasc
	fascst=fasc.fascstamp
	If u_sqlexec("select COLUMN_NAME from information_schema.columns where table_name = ?tab order by column_name","curcol") And Reccount("curcol")>0
		Select curcol
		Go Top
		Scan
			Select fascl
			Append Blank
			Replace fascl.fascstamp With fascst
			Replace fascl.fasclstamp With u_stamp()
			Replace fascl.Nome With curcol.column_name
			Replace fascl.nomedsn With curcol.column_name
			Replace fascl.ord With 0
			Replace fascl.ini With 0
			Replace fascl.Len With 0
			Replace fascl.fixo With .f.
			Replace fascl.ndecs With 0
			Replace fascl.ousrinis With m_chinis
			Replace fascl.ousrdata With Date()
		Endscan
	Endif
Endif
