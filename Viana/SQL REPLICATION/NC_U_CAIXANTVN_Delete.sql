USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_caixant]    Script Date: 27/01/2023 13:18:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXANTVN_Delete]
		@c1 char(25),
		@c2 varchar(10),
		@c3 numeric(15,2),
		@c4 char(25),
		@c5 varchar(35),
		@c6 numeric(5,0),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 bit,
		@c14 numeric(15,2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [VN].[u_caixant] 
	where [u_caixantstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixantstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[u_caixant]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_caixant where u_caixantstamp=@c1