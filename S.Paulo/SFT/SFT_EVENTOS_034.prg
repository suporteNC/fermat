** Criado por: Am�ndio Pacheco
** Data cria��o: 14.03.2014
select ft
create cursor cur_ftclose (bostamp C(25), marcada L, ndos N(3), nmdos c(50), dataobra D, obrano N(10))
select fi
goto top
scan for not empty(fi.autostamp)
	select cur_ftclose 
	GOTO top
	LOCATE FOR ALLTRIM(cur_ftclose.bostamp) == ALLTRIM(fi.autostamp)
	IF NOT FOUND()
		append blank
		replace cur_ftclose.bostamp with fi.autostamp 
		replace cur_ftclose.marcada with .t.
		replace cur_ftclose.ndos with fi.ndocft
	ENDIF
endscan
if reccount("cur_ftclose") > 0
	select cur_ftclose
	scan 
		if u_sqlexec("select dataobra, nmdos, obrano from bo (nolock) where bostamp = '" + alltrim(cur_ftclose.bostamp) + "'", "bopvtemp") and reccount("bopvtemp") > 0
			select bopvtemp
			replace cur_ftclose.dataobra WITH bopvtemp.dataobra
			replace cur_ftclose.nmdos WITH bopvtemp.nmdos
			replace cur_ftclose.obrano WITH bopvtemp.obrano
		endif
	ENDSCAN
	
	var_i = 4
	DECLARE list_tit(var_i),list_cam(var_i),list_pic(var_i),list_tam(var_i),list_ronly(var_i), list_pic(var_i), list_combo(var_i) ,list_rot(var_i)
	SELECT cur_ftclose
	var_i = 1
	list_tit(var_i)="Fechar?"
	list_cam(var_i)="cur_ftclose.marcada"
	list_pic(var_i)="LOGIC"
	list_tam(var_i)=8*8
	list_ronly(var_i)=.F.
	var_i = var_i +1
	list_tit(var_i)="Data"
	list_cam(var_i)="cur_ftclose.dataobra"
	list_pic(var_i)=""
	list_tam(var_i)=8*8
	list_ronly(var_i)=.T.
	var_i = var_i +1
	list_tit(var_i)="Dossier"
	list_cam(var_i)="cur_ftclose.nmdos"
	list_pic(var_i)=""
	list_tam(var_i)=8*50
	list_ronly(var_i)=.T.
	var_i = var_i +1
	list_tit(var_i)="N� Dossier"
	list_cam(var_i)="cur_ftclose.obrano"
	list_pic(var_i)=""
	list_tam(var_i)=8*50
	list_ronly(var_i)=.T.
	m.escolheu = .F.
	=CURSORSETPROP('Buffering',5,"cur_ftclose")
	browlist('Escolha','cur_ftclose', 'cur_ftclose',.T.,.T.,.F.,.F.,.T.,'',.T.)
	IF m.escolheu = .F.
		msg("Opera��o cancelada")
		RETURN
	ELSE
		SELECT cur_ftclose
		SCAN FOR cur_ftclose.marcada
			u_sqlexec("update bo set fechada = 1, datafecho = convert(char(8), getdate(), 112) where bostamp = '" + ALLTRIM(cur_ftclose.bostamp) + "'")
		ENDSCAN
	ENDIF
ENDIF
fecha("cur_ftclose")