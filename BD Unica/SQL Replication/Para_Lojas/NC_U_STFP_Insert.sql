USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_stfp]    Script Date: 07/02/2023 19:19:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_STFP_Insert]
    @c1 char(25),
    @c2 char(25),
    @c3 varchar(50),
    @c4 text,
    @c5 varchar(4),
    @c6 varchar(50),
    @c7 bit,
    @c8 varchar(254),
    @c9 text,
    @c10 varchar(50),
    @c11 varchar(30),
    @c12 datetime,
    @c13 varchar(8),
    @c14 varchar(30),
    @c15 datetime,
    @c16 varchar(8),
    @c17 bit
as
begin  
	insert into [PG].[u_stfp] (
		[u_stfpstamp],
		[ststamp],
		[nomef],
		[dados],
		[exti],
		[descricao],
		[ntratado],
		[caminho],
		[foto],
		[nomei],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','u_stfp',@c1, 'u_stfpstamp'

end  
