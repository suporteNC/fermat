USE [SPAULO]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_caixant]    Script Date: 27/01/2023 16:44:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXANTSP_Update]
		@c1 char(25),
		@c2 varchar(10),
		@c3 numeric(15,2),
		@c4 char(25),
		@c5 varchar(35),
		@c6 numeric(5,0),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 bit,
		@c14 numeric(15,2),
		@c15 char(25),
		@c16 varchar(10),
		@c17 numeric(15,2),
		@c18 char(25),
		@c19 varchar(35),
		@c20 numeric(5,0),
		@c21 varchar(30),
		@c22 datetime,
		@c23 varchar(8),
		@c24 varchar(30),
		@c25 datetime,
		@c26 varchar(8),
		@c27 bit,
		@c28 numeric(15,2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c15 = @c1)
begin 
update [SP].[u_caixant] set
		[u_caixantstamp] = @c15,
		[tipo] = @c16,
		[valor] = @c17,
		[u_caixastamp] = @c18,
		[descricao] = @c19,
		[qtt] = @c20,
		[ousrinis] = @c21,
		[ousrdata] = @c22,
		[ousrhora] = @c23,
		[usrinis] = @c24,
		[usrdata] = @c25,
		[usrhora] = @c26,
		[marcada] = @c27,
		[total] = @c28
	where [u_caixantstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixantstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[SP].[u_caixant]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [SP].[u_caixant] set
		[tipo] = @c16,
		[valor] = @c17,
		[u_caixastamp] = @c18,
		[descricao] = @c19,
		[qtt] = @c20,
		[ousrinis] = @c21,
		[ousrdata] = @c22,
		[ousrhora] = @c23,
		[usrinis] = @c24,
		[usrdata] = @c25,
		[usrhora] = @c26,
		[marcada] = @c27,
		[total] = @c28
	where [u_caixantstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_caixantstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[SP].[u_caixant]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','SP','u_caixant',@c1, 'u_caixantstamp', ''