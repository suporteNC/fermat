USE [VIANA]
GO

/****** Object:  StoredProcedure [dbo].[sp_MSins_dbofi2]    Script Date: 19/01/2023 19:33:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[NC_FI2VN_Insert]
    @c1 char(25),
    @c2 bit,
    @c3 varchar(60),
    @c4 datetime,
    @c5 varchar(50),
    @c6 varchar(50),
    @c7 char(25),
    @c8 varchar(30),
    @c9 datetime,
    @c10 varchar(8),
    @c11 varchar(30),
    @c12 datetime,
    @c13 varchar(8),
    @c14 bit,
    @c15 datetime,
    @c16 bit,
    @c17 char(25),
    @c18 varchar(10),
    @c19 varchar(5),
    @c20 varchar(115),
    @c21 varchar(10),
    @c22 varchar(115),
    @c23 numeric(6,2),
    @c24 numeric(6,2),
    @c25 numeric(5,2),
    @c26 varchar(20),
    @c27 numeric(19,6),
    @c28 numeric(18,5),
    @c29 bit,
    @c30 numeric(19,6),
    @c31 numeric(18,5),
    @c32 numeric(5,0),
    @c33 bit
as
begin  
	insert into [VN].[fi2] (
		[fi2stamp],
		[prestsrv],
		[originatingon],
		[orderdate],
		[refretif],
		[motretif],
		[ftstamp],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[u_ttssinc],
		[noupdqtt2ori],
		[oristamp],
		[origem],
		[nomecmb],
		[designcmb],
		[codisp],
		[designisp],
		[pctengfssl],
		[pctengrnv],
		[co2emiss],
		[unico2mdd],
		[esbrcstincbio],
		[sbrcstincbio],
		[ispoveruni2],
		[evalorisp],
		[valorisp],
		[u_estab],
		[fechabosatisteito]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33	) 


		EXEC dbo.NC_Sinc_Insert 'dbo','VN','fi2',@c1, 'fi2stamp'

end  
GO


