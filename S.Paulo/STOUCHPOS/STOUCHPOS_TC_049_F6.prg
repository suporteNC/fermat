** Criado por : TOTALSOFT | Paulo Martins  		Data de Cria��o : 21/09/2017
** Objectivo : Pede Password para autoriza��o de descontos nas linhas

Local m_USER, m_FUNCAO, m_CAMPO, m_TABELA, m_STAMP, m_OBSERV

********************VARIAVEIS*******************************************
M_TABELA = 'FPOSL'
M_CAMPO = 'u_FISTAMP'

SELECT &M_TABELA
M_STAMP = &M_TABELA..&M_CAMPO

M_FUNCAO = 'DESCONTOS'

*************************************************************************

SELECT FPOSL
IF EMPTY(FPOSL.REF)
	RETURN
ENDIF

m_PASS = GETNOME('Introduza PASSWORD para efectuar '+astr(M_FUNCAO)+'! ','','','',1,.t.)

IF EMPTY(m_PASS)
	msg('Ac��o cancelada pelo Utilizador')
	fecha('CR_ACESSO')
	RETURN
ENDIF


TEXT TO MSQL TEXTMERGE NOSHOW
	SELECT U_ACESSOS.PASS, US.USERNO, US.USERNAME, US.USRINIS, US.USSTAMP
	FROM U_ACESSOS (NOLOCK)
	INNER JOIN US (NOLOCK) ON US.USSTAMP=U_ACESSOS.USSTAMP
	WHERE US.PWPOS = '<<m_PASS>>'
		and U_ACESSOS.FUNCAO = '<<M_FUNCAO>>'
ENDTEXT

If !U_SQLEXEC(MSQL,'CR_ACESSO')
	msg('Erro Encontrado')
	msg(MSQL)
	Return
ENDIF

Select CR_ACESSO
If Reccount() = 0
	msg('O utilizador digitado n�o tem permissao para terminar a ac��o '+astr(M_FUNCAO)+'! Por favor contacte o supervisor!')
	fecha('CR_ACESSO')	
	return
ENDIF

Select CR_ACESSO

M_OBSERV = 'DESCONTOS - Introdu��o de descontos permitida por utilizador: '+ALLTRIM(CR_ACESSO.USERNAME) 

	
	TEXT TO MINS TEXTMERGE NOSHOW
	INSERT INTO u_HISTALT
		   ([u_histaltstamp]
           ,[userno]
           ,[username]
           ,[usstamp]
           ,[otabela]
           ,[oregstamp]
           ,[observ]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[marcada])
     SELECT '<<u_STAMP()>>',<<CR_ACESSO.USERNO>>,'<<CR_ACESSO.USERNAME>>','<<CR_ACESSO.USSTAMP>>','<<M_TABELA>>','<<M_STAMP>>','<<M_OBSERV>>',
     	'<<CR_ACESSO.USRINIS>>',getdate(),convert(char(10),getdate(),108),
     	'<<CR_ACESSO.USRINIS>>',getdate(),convert(char(10),getdate(),108),0
	ENDTEXT
	IF !U_SQLEXEC(MINS)
		msg('ERRO ENCONTRADO')
		msg(MINS)
		RETURN
	ENDIF

	fecha('CR_ACESSO')

SELECT FPOSL
MVAL = getnome("DESCONTO",FPOSL.DESCONTO)
REPLACE FPOSL.DESCONTO with MVAL
do fttots with .t.,'POS'
do U_FTILIQ with .f.,.f.,.f.,'FPOSC','FPOSL'
