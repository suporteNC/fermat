USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbotp]    Script Date: 31/01/2023 16:46:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_TPLojas_Delete]
		@c1 char(25),
		@c2 numeric(1,0),
		@c3 varchar(55),
		@c4 numeric(3,0),
		@c5 numeric(3,0),
		@c6 numeric(3,0),
		@c7 numeric(6,2),
		@c8 numeric(6,2),
		@c9 numeric(6,2),
		@c10 numeric(6,2),
		@c11 numeric(3,0),
		@c12 numeric(1,0),
		@c13 varchar(20),
		@c14 varchar(30),
		@c15 datetime,
		@c16 varchar(8),
		@c17 varchar(30),
		@c18 datetime,
		@c19 varchar(8),
		@c20 bit,
		@c21 bit,
		@c22 bit,
		@c23 varchar(20),
		@c24 varchar(3),
		@c25 varchar(4),
		@c26 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[tp] 
	where [tpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tp]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from tp where tpstamp=@c1