USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbostic]    Script Date: 30/03/2023 15:00:59 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STIC_Update]
		@c1 char(25),
		@c2 datetime,
		@c3 varchar(60),
		@c4 bit,
		@c5 char(25),
		@c6 varchar(20),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 bit,
		@c14 bit,
		@c15 bit,
		@c16 varchar(60),
		@c17 varchar(8),
		@c18 bit,
		@c19 varchar(30),
		@c20 varchar(30),
		@c21 varchar(20),
		@c22 varchar(40),
		@c23 varchar(6),
		@c24 bit,
		@c25 varchar(40),
		@c26 numeric(1,0),
		@c27 bit,
		@c28 numeric(10,0),
		@c29 numeric(10,0),
		@c30 varchar(55),
		@c31 varchar(16),
		@c32 char(25),
		@c33 datetime,
		@c34 varchar(60),
		@c35 bit,
		@c36 char(25),
		@c37 varchar(20),
		@c38 varchar(30),
		@c39 datetime,
		@c40 varchar(8),
		@c41 varchar(30),
		@c42 datetime,
		@c43 varchar(8),
		@c44 bit,
		@c45 bit,
		@c46 bit,
		@c47 varchar(60),
		@c48 varchar(8),
		@c49 bit,
		@c50 varchar(30),
		@c51 varchar(30),
		@c52 varchar(20),
		@c53 varchar(40),
		@c54 varchar(6),
		@c55 bit,
		@c56 varchar(40),
		@c57 numeric(1,0),
		@c58 bit,
		@c59 numeric(10,0),
		@c60 numeric(10,0),
		@c61 varchar(55),
		@c62 varchar(16)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c32 = @c1)
begin 
update [PG].[stic] set
		[sticstamp] = @c32,
		[data] = @c33,
		[descricao] = @c34,
		[lanca] = @c35,
		[stamp] = @c36,
		[ccusto] = @c37,
		[ousrinis] = @c38,
		[ousrdata] = @c39,
		[ousrhora] = @c40,
		[usrinis] = @c41,
		[usrdata] = @c42,
		[usrhora] = @c43,
		[marcada] = @c44,
		[exportado] = @c45,
		[impresso] = @c46,
		[userimpresso] = @c47,
		[hora] = @c48,
		[zera] = @c49,
		[u_ffim] = @c50,
		[u_fini] = @c51,
		[u_status] = @c52,
		[u_sticquem] = @c53,
		[u_terminal] = @c54,
		[u_esticg] = @c55,
		[u_codebar] = @c56,
		[u_tipostic] = @c57,
		[u_updst] = @c58,
		[u_diquebra] = @c59,
		[u_disobra] = @c60,
		[u_identif] = @c61,
		[u_loja] = @c62
	where [sticstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sticstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stic]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[stic] set
		[data] = @c33,
		[descricao] = @c34,
		[lanca] = @c35,
		[stamp] = @c36,
		[ccusto] = @c37,
		[ousrinis] = @c38,
		[ousrdata] = @c39,
		[ousrhora] = @c40,
		[usrinis] = @c41,
		[usrdata] = @c42,
		[usrhora] = @c43,
		[marcada] = @c44,
		[exportado] = @c45,
		[impresso] = @c46,
		[userimpresso] = @c47,
		[hora] = @c48,
		[zera] = @c49,
		[u_ffim] = @c50,
		[u_fini] = @c51,
		[u_status] = @c52,
		[u_sticquem] = @c53,
		[u_terminal] = @c54,
		[u_esticg] = @c55,
		[u_codebar] = @c56,
		[u_tipostic] = @c57,
		[u_updst] = @c58,
		[u_diquebra] = @c59,
		[u_disobra] = @c60,
		[u_identif] = @c61,
		[u_loja] = @c62
	where [sticstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sticstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stic]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','stic',@c1, 'sticstamp', ''