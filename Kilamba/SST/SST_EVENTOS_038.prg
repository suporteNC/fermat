*** Criado por: TOTALSOFT | Paulo Ricardo Martins
*** Data Cria��o: 31/08/2017

select ST

TEXT TO MSEL TEXTMERGE NOSHOW
SELECT * 
FROM US (NOLOCK)
WHERE USERNO = <<ASTR(m.ch_userno)>> 
ENDTEXT

IF !U_SQLEXEC(MSEL,'CR_USTMP')
	MSG('ERRO AO CARREGAR UTILIZADOR')
	MSG(MSEL)
	RETURN
ENDIF

select CR_USTMP

TEXT TO MSEL TEXTMERGE NOSHOW

INSERT INTO u_HISTALT
           ([u_histaltstamp]
           ,[userno]
           ,[username]
           ,[usstamp]
           ,[otabela]
           ,[oregstamp]
           ,[observ]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[marcada])
           
SELECT '<<u_STAMP()>>',
		<<astr(CR_USTMP.USERNO)>>,
		'<<astr(CR_USTMP.USERNAME)>>',
		'<<CR_USTMP.USSTAMP>>',
		'ST',
		'<<ST.STSTAMP>>',
		'ATERA��O DE PRE�O - Artigo: <<ALLTRIM(ST.REF)>> Pre�o de Venda 1 para <<ASTR(ST.EPV1)>>',
		'<<ST.ousrinis>>',
		'<<DTOS(ST.ousrdata)>>',
        '<<ST.ousrhora>>',
   		'<<ST.ousrinis>>',
		'<<DTOS(ST.ousrdata)>>',
        '<<ST.ousrhora>>',
		0
ENDTEXT


IF !U_SQLEXEC(MSEL)
	MSG('Erro a registar altera��o de pre�o')
	msg(msel)
	return
ENDIF


