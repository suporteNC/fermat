 Xexpressao
"TEXT TO m.msel NOSHOW TEXTMERGE
	SELECT	rm.u_pdarmstamp, rm.ref, rm.design, rm.qtt, convert(char(10), rm.ousrdata, 120) ousrdata
			, rm.ousrhora, rm.lordem, rm.nmdos + ' N� ' + convert(varchar(10), rm.obrano) as encomenda, bo.nome, rm.pda, bo.estab, bo.no
			, rm.obrano, rm.boano, bo.bostamp, bo.ndos 
			, qtt_ori = isnull((select  sum(qtt) from bi (nolock) where bi.bostamp = bo.bostamp and  bi.ref = rm.ref ),0) 
	FROM 	u_pdarm rm (NOLOCK) 
	inner   join bo (nolock) on bo.obrano = rm.obrano and bo.boano = rm.boano and bo.fechada = 0  and  bo.ndos =  rm.ndos  
	where 	rm.u_pdarmstamp not in (select optstamp from bi (nolock) where optstamp <> '') 
			--and YEAR(rm.ousrdata) >= (2015)
			and rm.pda like case when '<<ALLTRIM(m.ObjRecebido.Janela.pageframe1.udcpage2.obj4.value)>>' = '' then '%%' else '<<ALLTRIM(m.ObjRecebido.Janela.pageframe1.udcpage2.obj4.value)>>' end 
			and bo.ndos in  (2,105,938,5,103, 203)
		        and rm.marcada = 0
			and bo.bostamp not in (select pastamp from bo (nolock) where ndos in (5,954,938,111, 954, 211))
	ORDER 	BY rm.obrano, rm.lordem desc
ENDTEXT
IF NOT u_sqlexec(m.msel, 'pdarm')
	msg(msel)
	RETURN .f.
ENDIF
*msg(m.msel)
IF RECCOUNT('pdarm') = 0
	CREATE CURSOR pdarm (encomenda varchar(100), nome varchar(65), ref varchar(18), design varchar(65), qtt numeric(18,3), ousrdata varchar(10), ousrhora varchar(10), pda varchar(10))
	SELECT pdarm 
	APPEND BLANK 
	REPLACE pdarm.encomenda WITH "Sem Dados" 	
ENDIF

SELECT pdarm"
