USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_spstsubfam]    Script Date: 09/02/2023 18:35:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_SPSTSUBFAMILojas_Insert]
    @c1 char(25),
    @c2 char(25),
    @c3 varchar(18),
    @c4 varchar(55),
    @c5 varchar(30),
    @c6 datetime,
    @c7 varchar(8),
    @c8 varchar(30),
    @c9 datetime,
    @c10 varchar(8),
    @c11 bit
as
begin  
	insert into [PG].[u_spstsubfam] (
		[u_spstsubfamstamp],
		[spstamp],
		[subfami],
		[nome],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','u_spstsubfam',@c1, 'u_spstsubfamstamp'
end  
