 Xexpressao
"
*** Criado por Paulo Ricardo Martins em 06/11/2020
*** Criar e Utilizar cliente
*****************************************************

**IF cr_seekcltmp.telefone = '' or EMPTY(cr_seekcltmp.telefone)
	**MSG("Por favor preencha o telefone do cliente")
	**return
**ENDIF

mconsfinal = .f.

select cr_seekcltmp

IF EMPTY(cr_seekcltmp.clstamp) AND mconsfinal = .f.

	if  empty(cr_seekcltmp.telefone)
		msg("O telefone � de preenchimento obrigat�rio.")
		return  
	endif 

	doread("CL","SCL")
	scl.dointroduzir

	Select cl
	Replace cl.ncont With cr_seekcltmp.ncont
	Replace cl.Nome With cr_seekcltmp.Nome
	Replace cl.morada With cr_seekcltmp.morada
	Replace cl.Local With cr_seekcltmp.Local
	if Empty(cr_seekcltmp.codpost)
		Replace cl.codpost With "0000-000"
	Else
		Replace cl.codpost With cr_seekcltmp.codpost
	Endif
	Replace cl.telefone With cr_seekcltmp.telefone
	Replace cl.email With cr_seekcltmp.email
	Replace cl.zona With cr_seekcltmp.zona
	Replace cl.vendedor With cr_seekcltmp.vendedor
	Replace cl.vendnm With cr_seekcltmp.vendnm
	Replace cl.PAIS With 1
	Replace cl.PNCONT With 'AO'


	Select cl2
	Replace cl2.CODPAIS With 'AO'
	Replace cl2.DESCPAIS With 'Angola'
	Replace cl2.U_TIPOCL With 'PARTICULAR'

	scl.Refresh()
	scl.Gravar.Nossobutton1.Click()
	scl.Hide()
	scl.Release()

	Select cr_seekcltmp
	Replace cr_seekcltmp.no With cl.no
	Replace cr_seekcltmp.estab With cl.estab

ENDIF

select FPOSC
Replace fposc.ncont with cr_seekcltmp.ncont
Replace fposc.Nome with cr_seekcltmp.nome
Replace fposc.morada with cr_seekcltmp.morada
Replace fposc.Local with cr_seekcltmp.local
if Empty(cr_seekcltmp.codpost)
	Replace fposc.codpost With "0000-000"
Else
	Replace fposc.codpost with cr_seekcltmp.codpost
Endif
Replace fposc.telefone with cr_seekcltmp.telefone
Replace fposc.zona with cr_seekcltmp.zona
Replace fposc.vendedor with cr_seekcltmp.vendedor
Replace fposc.vendnm with cr_seekcltmp.vendnm
Replace fposc.PAIS With 1
Replace fposc.no With cr_seekcltmp.no
Replace fposc.estab With cr_seekcltmp.estab

select FPOSFT2
Replace fposft2.PNCONT With 'AO'
Replace fposft2.email with cr_seekcltmp.email
	

FTCLACT("MAIN","FPOSC","FPOSL")
stouchpos.posmetodos1.clivd = mconsfinal	
	
**STOUCHPOS.Executar('DOC_CLIDADOS')

PDU_5WJ0QSIUW.Hide()
PDU_5WJ0QSIUW.release()

**FPOSCDADOS.Hide()
**FPOSCDADOS.Release()

STOUCHPOS.zonafposcinfo.painel.tobj1.lblinfo.Caption = Alltrim(fposc.Nome) + " | ID " + Alltrim(m_chnome)
STOUCHPOS.Refresh()

"
