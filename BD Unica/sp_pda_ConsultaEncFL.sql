USE [SINCPHC]
GO
/****** Object:  StoredProcedure [dbo].[sp_pda_ConsultaEncFL]    Script Date: 28/11/2022 13:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_pda_ConsultaEncFL]
	@codigo as varchar(40)
AS
BEGIN

	declare @ip_address varchar(255);

	select @ip_address = CAST(CONNECTIONPROPERTY('client_net_address') as varchar(200))

if (charindex('192.168.29.',@ip_address)>0)
        /* PG */
        begin

            if (select left(substring(@codigo,5,len(@codigo)),1)) <> '*'
            begin
                SELECT 	top 1 bo.nmdos, bo.obrano, bo.bostamp, bo.boano, bo.ndos, barcode, nome, no, estab
                FROM	bo (nolock)
                inner	join bo2 (nolock) on bo.bostamp = bo2.bo2stamp
                inner	join bo3 (nolock) on bo.bostamp = bo3.bo3stamp
                INNER	JOIN ts (nolock) on ts.ndos = bo.ndos
                WHERE	bo3.barcode = @codigo
                        and ts.u_esujrec = 1 --and ts.resfor = 1
                        and bo2.anulado = 0 and bo.fechada = 0
                        and bo.logi7 = 0 -- Exclui PDA
            end
            else
            begin
                SELECT 	top 1 bo.nmdos, bo.obrano, bo.bostamp, bo.boano, bo.ndos, barcode, nome, no, estab
                FROM	bo (nolock)
                inner	join bo2 (nolock) on bo.bostamp = bo2.bo2stamp
                inner	join bo3 (nolock) on bo.bostamp = bo3.bo3stamp
                INNER	JOIN ts (nolock) on ts.ndos = bo.ndos
                WHERE	bo.boano = left(@codigo,4) and bo.obrano = SUBSTRING(@codigo,6,len(@codigo))
                        and ts.u_esujrec = 1 --and ts.resfor = 1
                        and bo2.anulado = 0 and bo.fechada = 0
                        and bo.logi7 = 0 -- Exclui PDA
            end

            RETURN 0

        end
else
        /* Spaulo e Palanca */
    begin

        if (select left(substring(@codigo,5,len(@codigo)),1)) <> '*'  
        begin 
            

            if( SELECT count(*)
            FROM bo (nolock)  
            inner join bo3 (nolock) on bo.bostamp = bo3.bo3stamp  
            INNER JOIN ts (nolock) on ts.ndos = bo.ndos  
            WHERE bo3.barcode = @codigo --and ts.resfor = 1 
                ) > 0 
            begin  
                SELECT  top 1 bo.nmdos, bo.obrano, bo.bostamp, bo.boano, bo.ndos, barcode  
                FROM bo (nolock)  
                inner join bo3 (nolock) on bo.bostamp = bo3.bo3stamp  
                INNER JOIN ts (nolock) on ts.ndos = bo.ndos  
                WHERE bo3.barcode = @codigo --and ts.resfor = 1  
            end 
            else   --  vai busca o campo mastamp  que tem o código do dossier de palete que deu origem à transferencia  
            begin  
                SELECT  top 1 bo.nmdos, bo.obrano, bo.bostamp, bo.boano, bo.ndos, barcode  
                FROM bo (nolock)  
                inner join bo3 (nolock) on bo.bostamp = bo3.bo3stamp  
                INNER JOIN ts (nolock) on ts.ndos = bo.ndos  
                WHERE bo.mastamp  = @codigo --and ts.resfor = 1  
            end 
        end  
        else  
        begin  
        SELECT  top 1 bo.nmdos, bo.obrano, bo.bostamp, bo.boano, bo.ndos, barcode  
        FROM bo (nolock)  
        inner join bo3 (nolock) on bo.bostamp = bo3.bo3stamp  
        INNER JOIN ts (nolock) on ts.ndos = bo.ndos  
        WHERE bo.boano = left(@codigo,4) and bo.obrano = SUBSTRING(@codigo,6,len(@codigo))  
            --and ts.resfor = 1  
        end  
        
        RETURN 0

    END
END