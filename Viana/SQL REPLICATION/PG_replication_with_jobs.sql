/****** Scripting replication configuration. Script Date: 08/09/2023 09:03:12 ******/
/****** Please Note: For security reasons, all password parameters were scripted with either NULL or an empty string. ******/

/****** Begin: Script to be run at Publisher ******/

/****** Installing the server as a Distributor. Script Date: 08/09/2023 09:03:12 ******/
use master
exec VN_adddistributor @distributor = N'FERMAT-SPAULO\SQLPHC16', @password = N'', @from_scripting = 1
GO

-- Adding the agent profiles
-- Updating the agent profile defaults
exec sp_MSupdate_agenttype_default @profile_id = 1
GO
exec sp_MSupdate_agenttype_default @profile_id = 2
GO
exec sp_MSupdate_agenttype_default @profile_id = 4
GO
exec sp_MSupdate_agenttype_default @profile_id = 6
GO
exec sp_MSupdate_agenttype_default @profile_id = 11
GO

-- Adding the distribution databases
use master
exec VN_adddistributiondb @database = N'distribution', @data_folder = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLPHC16\MSSQL\Data', @data_file = N'distribution.MDF', @data_file_size = 141, @log_folder = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLPHC16\MSSQL\Data', @log_file = N'distribution.LDF', @log_file_size = 137, @min_distretention = 0, @max_distretention = 72, @history_retention = 48, @security_mode = 1, @from_scripting = 1
GO

------ Script Date: Replication agents checkup ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Checkup') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Checkup'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Replication agents checkup')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Replication agents checkup', @enabled = 1, @description = N'Detects replication agents that are not logging history actively.', @start_step_id = 1, @category_name = N'REPL-Checkup', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 2, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'sys.VN_replication_agent_checkup @heartbeat_interval = 10', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @database_name = N'master', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 4, @freq_interval = 1, @freq_subday_type = 4, @freq_subday_interval = 10, @freq_relative_interval = 1, @freq_recurrence_factor = 0, @active_start_date = 20230124, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: Replication monitoring refresher for distribution. ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Alert Response') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Alert Response'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Replication monitoring refresher for distribution.')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Replication monitoring refresher for distribution.', @enabled = 0, @description = N'Replication monitoring refresher for distribution.', @start_step_id = 1, @category_name = N'REPL-Alert Response', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'exec dbo.VN_replmonitorrefreshjob  ', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230124, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: Reinitialize subscriptions having data validation failures ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Alert Response') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Alert Response'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Reinitialize subscriptions having data validation failures')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Reinitialize subscriptions having data validation failures', @enabled = 1, @description = N'Reinitializes all subscriptions that have data validation failures.', @start_step_id = 1, @category_name = N'REPL-Alert Response', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'exec sys.sp_MSreinit_failed_subscriptions @failure_level = 1', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'master', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: Agent history clean up: distribution ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-History Cleanup') < 1 
  execute msdb.dbo.VN_add_category N'REPL-History Cleanup'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Agent history clean up: distribution')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Agent history clean up: distribution', @enabled = 1, @description = N'Removes replication agent history from the distribution database.', @start_step_id = 1, @category_name = N'REPL-History Cleanup', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'EXEC dbo.sp_MShistory_cleanup @history_retention = 48', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 4, @freq_interval = 1, @freq_subday_type = 4, @freq_subday_interval = 10, @freq_relative_interval = 1, @freq_recurrence_factor = 0, @active_start_date = 20230124, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: Distribution clean up: distribution ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Distribution Cleanup') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Distribution Cleanup'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Distribution clean up: distribution')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Distribution clean up: distribution', @enabled = 1, @description = N'Removes replicated transactions from the distribution database.', @start_step_id = 1, @category_name = N'REPL-Distribution Cleanup', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'EXEC dbo.sp_MSdistribution_cleanup @min_distretention = 0, @max_distretention = 72', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 4, @freq_interval = 1, @freq_subday_type = 4, @freq_subday_interval = 10, @freq_relative_interval = 1, @freq_recurrence_factor = 0, @active_start_date = 20230124, @active_end_date = 99991231, @active_start_time = 500, @active_end_time = 459
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

-- Adding the distribution publishers
exec VN_adddistpublisher @publisher = N'FERMAT-SPAULO\SQLPHC16', @distribution_db = N'distribution', @security_mode = 1, @working_directory = N'C:\Replication', @trusted = N'false', @thirdparty_flag = 0, @publisher_type = N'MSSQLSERVER'
GO

------ Script Date: FERMAT-SPAULO\SQLPHC16-SPaulo-PB_CAIXA_SP-3 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Snapshot') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Snapshot'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_CAIXA_SP-3')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_CAIXA_SP-3', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Snapshot', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Snapshot Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_snapshot_history @perfmon_increment = 0,  @agent_id = 3, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Snapshot', @command = N'-Publisher [FERMAT-SPAULO\SQLPHC16] -PublisherDB [SPaulo] -Distributor [FERMAT-SPAULO\SQLPHC16] -Publication [PB_CAIXA_SP] -DistributorSecurityMode 1 ', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 10, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Snapshot'', @agent_id = 3', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 0, @freq_type = 1, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230130, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC16-SPaulo-PB_RE_SP-8 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Snapshot') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Snapshot'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_RE_SP-8')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_RE_SP-8', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Snapshot', @owner_login_name = N'sa', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Snapshot Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_snapshot_history @perfmon_increment = 0,  @agent_id = 8, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Snapshot', @command = N'-Publisher [FERMAT-SPAULO\SQLPHC16] -PublisherDB [SPaulo] -Distributor [FERMAT-SPAULO\SQLPHC16] -Publication [PB_RE_SP] -DistributorSecurityMode 1 ', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 10, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Snapshot'', @agent_id = 8', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 0, @freq_type = 1, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230512, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC16-SPaulo-PB_BO_SP-7 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Snapshot') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Snapshot'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_BO_SP-7')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_BO_SP-7', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Snapshot', @owner_login_name = N'sa', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Snapshot Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_snapshot_history @perfmon_increment = 0,  @agent_id = 7, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Snapshot', @command = N'-Publisher [FERMAT-SPAULO\SQLPHC16] -PublisherDB [SPaulo] -Distributor [FERMAT-SPAULO\SQLPHC16] -Publication [PB_BO_SP] -DistributorSecurityMode 1 ', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 10, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Snapshot'', @agent_id = 7', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 0, @freq_type = 1, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230315, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC16-SPaulo-PB_FAT_SP-2 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Snapshot') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Snapshot'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_FAT_SP-2')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_FAT_SP-2', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Snapshot', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Snapshot Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_snapshot_history @perfmon_increment = 0,  @agent_id = 2, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Snapshot', @command = N'-Publisher [FERMAT-SPAULO\SQLPHC16] -PublisherDB [SPaulo] -Distributor [FERMAT-SPAULO\SQLPHC16] -Publication [PB_FAT_SP] -DistributorSecurityMode 1 ', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 10, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Snapshot'', @agent_id = 2', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 0, @freq_type = 1, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230130, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC16-SPaulo-2 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-LogReader') < 1 
  execute msdb.dbo.VN_add_category N'REPL-LogReader'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-2')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-2', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-LogReader', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Log Reader Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_logreader_history @perfmon_increment = 0, @agent_id = 2, @runstatus = 1, 
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'LogReader', @command = N'-Publisher [FERMAT-SPAULO\SQLPHC16] -PublisherDB [SPaulo] -Distributor [FERMAT-SPAULO\SQLPHC16] -DistributorSecurityMode 1  -Continuous', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''LogReader'', @agent_id = 2', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230130, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC1-SPaulo-PB_CAIXA_SP-SRVBD-9 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Distribution') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Distribution'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_CAIXA_SP-SRVBD-9')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_CAIXA_SP-SRVBD-9', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Distribution', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Distribution Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_distribution_history @perfmon_increment = 0, @agent_id = 9, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Distribution', @command = N'-Subscriber [SRVBD] -SubscriberDB [FERMAT] -Publisher [FERMAT-SPAULO\SQLPHC16] -Distributor [FERMAT-SPAULO\SQLPHC16] -DistributorSecurityMode 1 -Publication [PB_CAIXA_SP] -PublisherDB [SPaulo]    -Continuous', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Distribution'', @agent_id = 9', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230130, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC1-SPaulo-PB_BO_SP-SRVBD-21 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Distribution') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Distribution'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_BO_SP-SRVBD-21')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_BO_SP-SRVBD-21', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Distribution', @owner_login_name = N'sa', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Distribution Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_distribution_history @perfmon_increment = 0, @agent_id = 21, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Distribution', @command = N'-Subscriber [SRVBD] -SubscriberDB [FERMAT] -Publisher [FERMAT-SPAULO\SQLPHC16] -Distributor [FERMAT-SPAULO\SQLPHC16] -DistributorSecurityMode 1 -Publication [PB_BO_SP] -PublisherDB [SPaulo]    -Continuous', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Distribution'', @agent_id = 21', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230315, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC1-SPaulo-PB_RE_SP-SRVBD-25 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Distribution') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Distribution'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_RE_SP-SRVBD-25')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_RE_SP-SRVBD-25', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Distribution', @owner_login_name = N'sa', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Distribution Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_distribution_history @perfmon_increment = 0, @agent_id = 25, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Distribution', @command = N'-Subscriber [SRVBD] -SubscriberDB [FERMAT] -Publisher [FERMAT-SPAULO\SQLPHC16] -Distributor [FERMAT-SPAULO\SQLPHC16] -DistributorSecurityMode 1 -Publication [PB_RE_SP] -PublisherDB [SPaulo]    -Continuous', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Distribution'', @agent_id = 25', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230515, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

------ Script Date: FERMAT-SPAULO\SQLPHC1-SPaulo-PB_FAT_SP-SRVBD-6 ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Distribution') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Distribution'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_FAT_SP-SRVBD-6')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_FAT_SP-SRVBD-6', @enabled = 1, @description = N'No description available.', @start_step_id = 1, @category_name = N'REPL-Distribution', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Distribution Agent startup message.', @subsystem = N'TSQL', @command = N'sp_MSadd_distribution_history @perfmon_increment = 0, @agent_id = 6, @runstatus = 1,  
                    @comments = N''Starting agent.''', @cmdexec_success_code = 0, @on_success_action = 3, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Run agent.', @subsystem = N'Distribution', @command = N'-Subscriber [SRVBD] -SubscriberDB [FERMAT] -Publisher [FERMAT-SPAULO\SQLPHC16] -Distributor [FERMAT-SPAULO\SQLPHC16] -DistributorSecurityMode 1 -Publication [PB_FAT_SP] -PublisherDB [SPaulo]    -Continuous', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 3, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 2147483647, @retry_interval = 1, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Detect nonlogged agent shutdown.', @subsystem = N'TSQL', @command = N'sp_MSdetect_nonlogged_shutdown @subsystem = ''Distribution'', @agent_id = 6', @cmdexec_success_code = 0, @on_success_action = 2, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'distribution', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 64, @freq_interval = 0, @freq_subday_type = 0, @freq_subday_interval = 0, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_start_date = 20230130, @active_end_date = 99991231, @active_start_time = 0, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO

exec VN_addsubscriber @subscriber = N'SRVBD', @type = 0, @description = N''
GO

------ Script Date: Expired subscription clean up ------
begin transaction 
  DECLARE @JobID BINARY(16)
  DECLARE @ReturnCode INT
  SELECT @ReturnCode = 0
if (select count(*) from msdb.dbo.syscategories where name = N'REPL-Subscription Cleanup') < 1 
  execute msdb.dbo.VN_add_category N'REPL-Subscription Cleanup'

select @JobID = job_id from msdb.dbo.sysjobs where (name = N'Expired subscription clean up')
if (@JobID is NULL)
BEGIN
  execute @ReturnCode = msdb.dbo.VN_add_job @job_id = @JobID OUTPUT, @job_name = N'Expired subscription clean up', @enabled = 1, @description = N'Detects and removes expired subscriptions from published databases.', @start_step_id = 1, @category_name = N'REPL-Subscription Cleanup', @owner_login_name = N'FERMATSPLO\Administrator', @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Run agent.', @subsystem = N'TSQL', @command = N'EXEC sys.VN_expired_subscription_cleanup', @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @server = N'FERMAT-SPAULO\SQLPHC16', @database_name = N'master', @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @flags = 0
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_update_job @job_id = @JobID, @start_step_id = 1
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobschedule @job_id = @JobID, @name = N'Replication agent schedule.', @enabled = 1, @freq_type = 4, @freq_interval = 1, @freq_subday_type = 1, @freq_subday_interval = 1, @freq_relative_interval = 1, @freq_recurrence_factor = 0, @active_start_date = 20230124, @active_end_date = 99991231, @active_start_time = 10000, @active_end_time = 235959
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

  execute @ReturnCode = msdb.dbo.VN_add_jobserver @job_id = @JobID, @server_name = N'FERMAT-SPAULO\SQLPHC16'
  if (@@ERROR <> 0 OR @ReturnCode <> 0) goto QuitWithRollback

END

commit transaction 
goto EndSave 
QuitWithRollback: 
  if (@@TRANCOUNT > 0) rollback transaction 
EndSave:
GO


/****** End: Script to be run at Publisher ******/


-- Enabling the replication database
use master
exec VN_replicationdboption @dbname = N'SPaulo', @optname = N'publish', @value = N'true'
GO

exec [SPaulo].sys.VN_addlogreader_agent @publisher_security_mode = 1, @job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-2', @job_login = null, @job_password = null
GO
exec [SPaulo].sys.VN_addqreader_agent @job_name = null, @frompublisher = 1, @job_login = null, @job_password = null
GO
-- Adding the transactional publication
USE [VIANA]
exec VN_addpublication @publication = N'PB_BO_SP', @description = N'Transactional publication of database ''SPaulo'' from Publisher ''FERMAT-SPAULO\SQLPHC16''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec VN_addpublication_snapshot @publication = N'PB_BO_SP', @snapshot_job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_BO_SP-7', @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'sa'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'FERMATSPLO\Administrator'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'NT SERVICE\SQLAgent$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'NT SERVICE\Winmgmt'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'NT SERVICE\SQLWriter'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'NT Service\MSSQL$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'gr'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'emajor'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'bcosta'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'Leopoldina'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'filipe'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'asilva'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'alerta'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'amalua'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'tts'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'rita'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'cchico'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'npio'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'scosta'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'nelson'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'distributor_admin'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'cmoura'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'edbrande'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'Miguel'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'apascoal'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'sincro'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'nn'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'jjj'
GO
exec VN_grant_publication_access @publication = N'PB_BO_SP', @login = N'Jorge'
GO

-- Adding the transactional articles
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'bi', @source_owner = N'dbo', @source_object = N'bi', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bi', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BIVN_Insert]', @del_cmd = N'XCALL [NC_BIVN_Delete]', @upd_cmd = N'XCALL [NC_BIVN_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_BO_SP', @article = N'bi', @filter_name = N'FLTR_bi_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_BO_SP', @article = N'bi', @view_name = N'SYNC_bi_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'bi2', @source_owner = N'dbo', @source_object = N'bi2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bi2', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BI2VN_Insert]', @del_cmd = N'XCALL [NC_BI2VN_Delete]', @upd_cmd = N'XCALL [NC_BI2VN_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_BO_SP', @article = N'bi2', @filter_name = N'FLTR_bi2_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_BO_SP', @article = N'bi2', @view_name = N'SYNC_bi2_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'bo', @source_owner = N'dbo', @source_object = N'bo', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'bo', @destination_owner = N'VN', @status = 24, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_BOVN_Insert]', @del_cmd = N'XCALL [NC_BOVN_Delete]', @upd_cmd = N'XCALL [NC_BOVN_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article's partition column(s)
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'nmdos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'obrano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'dataobra', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'nome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'totaldeb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'etotaldeb', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tipo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'datafinal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'smoe4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'smoe3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'smoe2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'smoe1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'moetotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sdeb2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sdeb1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sdeb4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sdeb3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt14', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt13', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt12', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt11', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'sqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vendedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'vendnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'stot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'stot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'stot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'stot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'no', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'obranome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'boano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'dataopen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'datafecho', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'fechada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'nopat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'total', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tecnico', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tecnnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'nomquina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'maquina', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'marca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'serie', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'zona', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'obs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ndos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'moeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'morada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'local', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'codpost', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ultfact', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'period', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ncont', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'segmento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'impresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'userimpresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'cobranca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ecusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'trab1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'trab2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'trab3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'trab4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'trab5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'custo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tabela1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'logi8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'fref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ccusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ncusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'infref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'lifref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'esdeb1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'esdeb2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'esdeb3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'esdeb4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'evqtt21', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'evqtt22', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'evqtt23', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'evqtt24', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'estot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'estot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'estot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'estot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'etotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_2tdesc1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_2tdesc2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_2tdes1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_2tdes2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'descc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'edescc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_1tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_2tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_1tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_2tvall', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo11_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo11_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo11_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo11_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo21_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo21_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo21_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo21_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo31_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo31_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo31_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo31_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo41_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo41_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo41_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo41_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo51_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo51_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo51_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo51_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo61_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo61_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo61_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo61_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo12_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo12_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo12_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo12_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo22_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo22_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo22_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo22_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo32_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo32_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo32_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo32_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo42_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo42_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo42_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo42_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo52_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo52_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo52_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo52_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo62_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo62_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo62_bins', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo62_iva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_totp1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'bo_totp2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_totp1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ebo_totp2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'edi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'memissao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'nome2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'iiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'iunit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'itotais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'iunitiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'itotaisiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'pastamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'snstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'mastamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'origem', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'orinopat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'pnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'pno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'cxstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'cxusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ssstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ssusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'alldescli', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'alldesfor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'series', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'series2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'quarto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ocupacao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tabela2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'obstab2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'situacao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'lang', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'iemail', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'inome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ean', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'iecacodisen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'boclose', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'dtclose', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tpstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'tpdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'emconf', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'statuspda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'aprovado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_guiatran', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_fltransp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_pesotota', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_tvolume', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_naltqtt2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_utilizad', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_packn', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_estabd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_BO_SP', @article = N'bo', @column = N'u_serv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_BO_SP', @article = N'bo', @filter_name = N'FLTR_bo_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_BO_SP', @article = N'bo', @view_name = N'SYNC_bo_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'bo2', @source_owner = N'dbo', @source_object = N'bo2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bo2', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BO2VN_Insert]', @del_cmd = N'XCALL [NC_BO2VN_Delete]', @upd_cmd = N'XCALL [NC_BO2VN_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_BO_SP', @article = N'bo2', @filter_name = N'FLTR_bo2_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_BO_SP', @article = N'bo2', @view_name = N'SYNC_bo2_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'bo3', @source_owner = N'dbo', @source_object = N'bo3', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'bo3', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_BO3VN_Insert]', @del_cmd = N'XCALL [NC_BO3VN_Delete]', @upd_cmd = N'XCALL [NC_BO3VN_Update]', @filter_clause = N'[u_serv] = HOST_NAME()'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_BO_SP', @article = N'bo3', @filter_name = N'FLTR_bo3_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_BO_SP', @article = N'bo3', @view_name = N'SYNC_bo3_1__60', @filter_clause = N'[u_serv] = HOST_NAME()', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BI2VN_Delete', @source_owner = N'dbo', @source_object = N'NC_BI2VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BI2VN_Insert', @source_owner = N'dbo', @source_object = N'NC_BI2VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BI2VN_Update', @source_owner = N'dbo', @source_object = N'NC_BI2VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BI2VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BIVN_Delete', @source_owner = N'dbo', @source_object = N'NC_BIVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BIVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BIVN_Insert', @source_owner = N'dbo', @source_object = N'NC_BIVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BIVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BIVN_Update', @source_owner = N'dbo', @source_object = N'NC_BIVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BIVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO2VN_Delete', @source_owner = N'dbo', @source_object = N'NC_BO2VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO2VN_Insert', @source_owner = N'dbo', @source_object = N'NC_BO2VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO2VN_Update', @source_owner = N'dbo', @source_object = N'NC_BO2VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO2VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO3VN_Delete', @source_owner = N'dbo', @source_object = N'NC_BO3VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO3VN_Insert', @source_owner = N'dbo', @source_object = N'NC_BO3VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BO3VN_Update', @source_owner = N'dbo', @source_object = N'NC_BO3VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BO3VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BOVN_Delete', @source_owner = N'dbo', @source_object = N'NC_BOVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BOVN_Insert', @source_owner = N'dbo', @source_object = N'NC_BOVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_BO_SP', @article = N'NC_BOVN_Update', @source_owner = N'dbo', @source_object = N'NC_BOVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_BOVN_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional subscriptions
USE [VIANA]
exec VN_addsubscription @publication = N'PB_BO_SP', @subscriber = N'SRVBD', @destination_db = N'FERMAT', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec VN_addpushsubscription_agent @publication = N'PB_BO_SP', @subscriber = N'SRVBD', @subscriber_db = N'FERMAT', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_BO_SP-SRVBD-21', @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
USE [VIANA]
exec VN_addpublication @publication = N'PB_CAIXA_SP', @description = N'Transactional publication of database ''SPaulo'' from Publisher ''FERMAT-SPAULO\SQLPHC16''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec VN_addpublication_snapshot @publication = N'PB_CAIXA_SP', @snapshot_job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_CAIXA_SP-3', @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'sa'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'FERMATSPLO\Administrator'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'NT SERVICE\SQLAgent$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'NT SERVICE\Winmgmt'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'NT SERVICE\SQLWriter'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'NT Service\MSSQL$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'emajor'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'bcosta'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'Leopoldina'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'filipe'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'asilva'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'alerta'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'amalua'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'tts'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'rita'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'cchico'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'npio'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'scosta'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'nelson'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'distributor_admin'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'cmoura'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'edbrande'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'Miguel'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'apascoal'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'sincro'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'nn'
GO
exec VN_grant_publication_access @publication = N'PB_CAIXA_SP', @login = N'Jorge'
GO

-- Adding the transactional articles
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'cx', @source_owner = N'dbo', @source_object = N'cx', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'cx', @destination_owner = N'VN', @status = 24, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_CXVN_Insert]', @del_cmd = N'XCALL [NC_CXVN_Delete]', @upd_cmd = N'XCALL [NC_CXVN_Update]'

-- Adding the article's partition column(s)
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'cxstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'pnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'pno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'cxno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'cxano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'ausername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'auserno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'dabrir', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'habrir', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'fusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'fuserno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'dfechar', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'hfechar', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'fechada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'causa', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_CAIXA_SP', @article = N'cx', @column = N'exportado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_CAIXA_SP', @article = N'cx', @view_name = N'SYNC_cx_1__67', @filter_clause = N'', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'fcx', @source_owner = N'dbo', @source_object = N'fcx', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'fcx', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FCXVN_Insert]', @del_cmd = N'XCALL [NC_FCXVN_Delete]', @upd_cmd = N'XCALL [NC_FCXVN_Update]'
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_CXVN_Delete', @source_owner = N'dbo', @source_object = N'NC_CXVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CXVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_CXVN_Insert', @source_owner = N'dbo', @source_object = N'NC_CXVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CXVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_CXVN_Update', @source_owner = N'dbo', @source_object = N'NC_CXVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_CXVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_FCXVN_Delete', @source_owner = N'dbo', @source_object = N'NC_FCXVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FCXVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_FCXVN_Insert', @source_owner = N'dbo', @source_object = N'NC_FCXVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FCXVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_FCXVN_Update', @source_owner = N'dbo', @source_object = N'NC_FCXVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FCXVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXALVN_Delete', @source_owner = N'dbo', @source_object = N'NC_U_CAIXALVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXALVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXALVN_Insert', @source_owner = N'dbo', @source_object = N'NC_U_CAIXALVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXALVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXALVN_Update', @source_owner = N'dbo', @source_object = N'NC_U_CAIXALVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXALVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXANTVN_Delete', @source_owner = N'dbo', @source_object = N'NC_U_CAIXANTVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXANTVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXANTVN_Insert', @source_owner = N'dbo', @source_object = N'NC_U_CAIXANTVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXANTVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXANTVN_Update', @source_owner = N'dbo', @source_object = N'NC_U_CAIXANTVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXANTVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXAVN_Delete', @source_owner = N'dbo', @source_object = N'NC_U_CAIXAVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXAVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXAVN_Insert', @source_owner = N'dbo', @source_object = N'NC_U_CAIXAVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXAVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXAVN_Update', @source_owner = N'dbo', @source_object = N'NC_U_CAIXAVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXAVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXATMVN_Delete', @source_owner = N'dbo', @source_object = N'NC_U_CAIXATMVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXATMVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXATMVN_Insert', @source_owner = N'dbo', @source_object = N'NC_U_CAIXATMVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXATMVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'NC_U_CAIXATMVN_Update', @source_owner = N'dbo', @source_object = N'NC_U_CAIXATMVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_U_CAIXATMVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'u_caixa', @source_owner = N'dbo', @source_object = N'u_caixa', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_caixa', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_CAIXAVN_Insert]', @del_cmd = N'XCALL [NC_U_CAIXAVN_Delete]', @upd_cmd = N'XCALL [NC_U_CAIXAVN_Update]'
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'u_caixal', @source_owner = N'dbo', @source_object = N'u_caixal', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_caixal', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_CAIXALVN_Insert]', @del_cmd = N'XCALL [NC_U_CAIXALVN_Delete]', @upd_cmd = N'XCALL [NC_U_CAIXALVN_Update]'
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'u_caixant', @source_owner = N'dbo', @source_object = N'u_caixant', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_caixant', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_CAIXANTVN_Insert]', @del_cmd = N'XCALL [NC_U_CAIXANTVN_Delete]', @upd_cmd = N'XCALL [NC_U_CAIXANTVN_Update]'
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_CAIXA_SP', @article = N'u_caixatm', @source_owner = N'dbo', @source_object = N'u_caixatm', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'u_caixatm', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_U_CAIXATMVN_Insert]', @del_cmd = N'XCALL [NC_U_CAIXATMVN_Delete]', @upd_cmd = N'XCALL [NC_U_CAIXATMVN_Update]'
GO

-- Adding the transactional subscriptions
USE [VIANA]
exec VN_addsubscription @publication = N'PB_CAIXA_SP', @subscriber = N'SRVBD', @destination_db = N'FERMAT', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec VN_addpushsubscription_agent @publication = N'PB_CAIXA_SP', @subscriber = N'SRVBD', @subscriber_db = N'FERMAT', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_CAIXA_SP-SRVBD-9', @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
USE [VIANA]
exec VN_addpublication @publication = N'PB_FAT_SP', @description = N'Transactional publication of database ''SPaulo'' from Publisher ''FERMAT-SPAULO\SQLPHC16''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec VN_addpublication_snapshot @publication = N'PB_FAT_SP', @snapshot_job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_FAT_SP-2', @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'sa'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'FERMATSPLO\Administrator'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'NT SERVICE\SQLAgent$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'NT SERVICE\Winmgmt'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'NT SERVICE\SQLWriter'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'NT Service\MSSQL$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'emajor'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'bcosta'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'Leopoldina'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'filipe'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'asilva'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'alerta'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'amalua'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'tts'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'rita'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'cchico'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'npio'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'scosta'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'nelson'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'distributor_admin'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'cmoura'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'edbrande'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'Miguel'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'apascoal'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'sincro'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'nn'
GO
exec VN_grant_publication_access @publication = N'PB_FAT_SP', @login = N'Jorge'
GO

-- Adding the transactional articles
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'fi', @source_owner = N'dbo', @source_object = N'fi', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'fi', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FIVN_Insert]', @del_cmd = N'XCALL [NC_FIVN_Delete]', @upd_cmd = N'XCALL [NC_FIVN_Update]', @filter_clause = N'[ousrdata] >''2022.11.20'''

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_FAT_SP', @article = N'fi', @filter_name = N'FLTR_fi_1__64', @filter_clause = N'[ousrdata] >''2022.11.20''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_FAT_SP', @article = N'fi', @view_name = N'SYNC_fi_1__64', @filter_clause = N'[ousrdata] >''2022.11.20''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'fi2', @source_owner = N'dbo', @source_object = N'fi2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'fi2', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FI2VN_Insert]', @del_cmd = N'XCALL [NC_FI2VN_Delete]', @upd_cmd = N'XCALL [NC_FI2VN_Update]', @filter_clause = N'[u_estab] =5'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_FAT_SP', @article = N'fi2', @filter_name = N'FLTR_fi2_1__64', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_FAT_SP', @article = N'fi2', @view_name = N'SYNC_fi2_1__64', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'ft', @source_owner = N'dbo', @source_object = N'ft', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N'ft', @destination_owner = N'VN', @status = 24, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_FTVN_Insert]', @del_cmd = N'XCALL [NC_FTVN_Delete]', @upd_cmd = N'XCALL [NC_FTVN_Update]', @filter_clause = N'[u_estab] =5 and [ousrdata] >''2022.11.20'''

-- Adding the article's partition column(s)
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ftstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'nmdoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'fno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'no', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'nome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'morada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'local', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'codpost', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ncont', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'bino', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'bidata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'bilocal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'telefone', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'zona', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'vendedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'vendnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'fdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ftano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'encomenda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pagamento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'expedicao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'carga', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'descar', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'saida', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'fin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'final', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ndoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ftpos', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'moeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'fref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ccusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ncusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'facturada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'fnoft', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'nmdocft', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'peso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'plano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'segmento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'totqtt', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'qtt1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'qtt2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'qtt3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'qtt4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tipo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'impresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'userimpresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cobrado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cobranca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'lifref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tipodoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'matricula', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'chora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivatx9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cambiofixo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'memissao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cobrador', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rota', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'multi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'introfin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cheque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'clbanco', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'clcheque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'chtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'echtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'chtmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'chmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'jaexpedi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'portes', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'custo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'diferido', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ettiliq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'edescc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ettiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'etotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ediferido', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'etot1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'etot2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'etot3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'etot4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'efinv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eportes', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ecusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'edebreg', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivain9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eivav9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'total', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'totalmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'finv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'finvm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivain9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamin9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivav9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ivamv9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ttiliq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tmiliq', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ttiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tmiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'descc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'descm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'debreg', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'debregm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'intid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'nome2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'lrstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'lpstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tpstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tpdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'snstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'erdtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rdtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rdtotalm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'mhstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'dplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'dinoplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'dilnoplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'diaplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'planoonline', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'dostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'optri', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'meiost', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'chdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pscm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'zncm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'excm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ptcm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'encm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ntcm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pscmdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'znregiao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'excmdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ptcmdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'encmdesc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ncin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ncout', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'usaintra', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'iectisento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'series', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'series2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cambio', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'pno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cxstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'cxusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ssstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ssusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'anulado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rpclstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rpclnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rpcldini', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'rpcldfim', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'classe', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'procomss', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eanft', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'eancl', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'lang', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'tptit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'virs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'evirs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'valorm2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'arstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'arno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'iecacodisen', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'niec', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'iecadoccod', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'aprovado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'nprotri', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_ftstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_vlstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_docvdsap', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_fnosap', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_norefsap', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_FAT_SP', @article = N'ft', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_FAT_SP', @article = N'ft', @filter_name = N'FLTR_ft_1__64', @filter_clause = N'[u_estab] =5 and [ousrdata] >''2022.11.20''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_FAT_SP', @article = N'ft', @view_name = N'SYNC_ft_1__64', @filter_clause = N'[u_estab] =5 and [ousrdata] >''2022.11.20''', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'ft2', @source_owner = N'dbo', @source_object = N'ft2', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'ft2', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FT2VN_Insert]', @del_cmd = N'XCALL [NC_FT2VN_Delete]', @upd_cmd = N'XCALL [NC_FT2VN_Update]', @filter_clause = N'[u_estab] =5'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_FAT_SP', @article = N'ft2', @filter_name = N'FLTR_ft2_1__64', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_FAT_SP', @article = N'ft2', @view_name = N'SYNC_ft2_1__64', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'ft3', @source_owner = N'dbo', @source_object = N'ft3', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'ft3', @destination_owner = N'VN', @status = 24, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_FT3VN_Insert]', @del_cmd = N'XCALL [NC_FT3VN_Delete]', @upd_cmd = N'XCALL [NC_FT3VN_Update]', @filter_clause = N'[u_estab] =5'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_FAT_SP', @article = N'ft3', @filter_name = N'FLTR_ft3_1__82', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_FAT_SP', @article = N'ft3', @view_name = N'syncobj_0x4232413632464145', @filter_clause = N'[u_estab] =5', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FI2VN_Delete', @source_owner = N'dbo', @source_object = N'NC_FI2VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FI2VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FI2VN_Insert', @source_owner = N'dbo', @source_object = N'NC_FI2VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FI2VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FI2VN_Update', @source_owner = N'dbo', @source_object = N'NC_FI2VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FI2VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FIVN_Delete', @source_owner = N'dbo', @source_object = N'NC_FIVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FIVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FIVN_Insert', @source_owner = N'dbo', @source_object = N'NC_FIVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FIVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FIVN_Update', @source_owner = N'dbo', @source_object = N'NC_FIVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FIVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT2VN_Delete', @source_owner = N'dbo', @source_object = N'NC_FT2VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT2VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT2VN_Insert', @source_owner = N'dbo', @source_object = N'NC_FT2VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT2VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT2VN_Update', @source_owner = N'dbo', @source_object = N'NC_FT2VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT2VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT3VN_Delete', @source_owner = N'dbo', @source_object = N'NC_FT3VN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT3VN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT3VN_Insert', @source_owner = N'dbo', @source_object = N'NC_FT3VN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT3VN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FT3VN_Update', @source_owner = N'dbo', @source_object = N'NC_FT3VN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FT3VN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FTVN_Delete', @source_owner = N'dbo', @source_object = N'NC_FTVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FTVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FTVN_Insert', @source_owner = N'dbo', @source_object = N'NC_FTVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FTVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_FAT_SP', @article = N'NC_FTVN_Update', @source_owner = N'dbo', @source_object = N'NC_FTVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_FTVN_Update', @destination_owner = N'dbo', @status = 16
GO

-- Adding the transactional subscriptions
USE [VIANA]
exec VN_addsubscription @publication = N'PB_FAT_SP', @subscriber = N'SRVBD', @destination_db = N'FERMAT', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec VN_addpushsubscription_agent @publication = N'PB_FAT_SP', @subscriber = N'SRVBD', @subscriber_db = N'FERMAT', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_FAT_SP-SRVBD-6', @dts_package_location = N'Distributor'
GO

-- Adding the transactional publication
USE [VIANA]
exec VN_addpublication @publication = N'PB_RE_SP', @description = N'Transactional publication of database ''SPaulo'' from Publisher ''FERMAT-SPAULO\SQLPHC16''.', @sync_method = N'concurrent', @retention = 0, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @repl_freq = N'continuous', @status = N'active', @independent_agent = N'true', @immediate_sync = N'true', @allow_sync_tran = N'false', @autogen_sync_procs = N'false', @allow_queued_tran = N'false', @allow_dts = N'false', @replicate_ddl = 1, @allow_initialize_from_backup = N'false', @enabled_for_p2p = N'false', @enabled_for_het_sub = N'false'
GO


exec VN_addpublication_snapshot @publication = N'PB_RE_SP', @snapshot_job_name = N'FERMAT-SPAULO\SQLPHC16-SPaulo-PB_RE_SP-8', @job_login = null, @job_password = null, @publisher_security_mode = 0, @publisher_login = N'sa', @publisher_password = N''
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'sa'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'FERMATSPLO\Administrator'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'NT SERVICE\SQLAgent$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'NT SERVICE\Winmgmt'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'NT SERVICE\SQLWriter'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'NT Service\MSSQL$SQLPHC16'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'gr'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'emajor'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'bcosta'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'Leopoldina'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'filipe'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'asilva'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'alerta'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'amalua'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'tts'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'rita'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'cchico'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'npio'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'scosta'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'nelson'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'distributor_admin'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'cmoura'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'edbrande'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'Miguel'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'apascoal'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'sincro'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'nn'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'jjj'
GO
exec VN_grant_publication_access @publication = N'PB_RE_SP', @login = N'Jorge'
GO

-- Adding the transactional articles
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_REVN_Delete', @source_owner = N'dbo', @source_object = N'NC_REVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_REVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_REVN_Insert', @source_owner = N'dbo', @source_object = N'NC_REVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_REVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_REVN_Update', @source_owner = N'dbo', @source_object = N'NC_REVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_REVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_RLVN_Delete', @source_owner = N'dbo', @source_object = N'NC_RLVN_Delete', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_RLVN_Delete', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_RLVN_Insert', @source_owner = N'dbo', @source_object = N'NC_RLVN_Insert', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_RLVN_Insert', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'NC_RLVN_Update', @source_owner = N'dbo', @source_object = N'NC_RLVN_Update', @type = N'proc schema only', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x0000000008000001, @destination_table = N'NC_RLVN_Update', @destination_owner = N'dbo', @status = 16
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N're', @source_owner = N'dbo', @source_object = N're', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'manual', @destination_table = N're', @destination_owner = N'VN', @status = 16, @vertical_partition = N'true', @ins_cmd = N'CALL [NC_REVN_Insert]', @del_cmd = N'XCALL [NC_REVN_Delete]', @upd_cmd = N'XCALL [NC_REVN_Update]', @filter_clause = N'[ndoc] =9'

-- Adding the article's partition column(s)
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'restamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'nmdoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'rno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'rdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'nome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'total', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'etotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ndoc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'no', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'morada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'local', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'codpost', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ncont', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'reano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'olcodigo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'telocal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'totalmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'moeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'desc1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'desc2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'fref', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ccusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ncusto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'contado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'process', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cobranca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'nib', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'fin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'finv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'finvmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'impresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'userimpresso', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'clbanco', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'clcheque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'procdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'vdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'zona', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ollocal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'descba', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'plano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'vendedor', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'vendnm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'olstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ccstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'segmento', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cc2stamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'efinv', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'edifcambio', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'difcambio', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'tipo', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'pais', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav7', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav8', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ivav9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eivav9', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'earred', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'memissao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'arred', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cobrador', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'rota', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'introfin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'tbok', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'faztrf', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'procomss', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cheque', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'chdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'intid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'nome2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'regiva', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'dplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'dinoplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'dilnoplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'diaplano', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'planoonline', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'dostamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'totol2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'etotol2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ollocal2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'telocal2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'contado2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'chtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'echtotal', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'chmoeda', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'chtotalm', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'site', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'pnome', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'pno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cxstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cxusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ssstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ssusername', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'vdinheiro', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'evdinheiro', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mvdinheiro', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga1', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga4', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga5', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'modop6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'epaga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mpaga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecompaga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'compaga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcompaga6', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'acerto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'eacerto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'macerto', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'exportado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'totow', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'etotow', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'tptit', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'virs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'evirs', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'moeda2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'valorowm2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'valorm2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'valor2m2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'earredm2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'moeda3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'valor2m3', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'contrato', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cessao', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'facstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'faccstamp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ousrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ousrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ousrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'usrinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'usrdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'usrhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'marcada', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'totoladi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'etotoladi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cbbno', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'luserfin', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'bic', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'iban', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'processsepa', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'totalimp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'etotalimp', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'sepagh', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'sepapi', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'operext', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'npedido', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'tiporeg', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'temch', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'paymentrefnoori', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'upddespacho', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'revogado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'revdata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'u_estab', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'anulado', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'anulinis', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'anuldata', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'anulhora', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'esirca', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'ecativa', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'cativa', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'mcativa', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'noimps', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'u_ttssinc', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'atcud', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'contingencia', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec VN_articlecolumn @publication = N'PB_RE_SP', @article = N're', @column = N'nomeat', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_RE_SP', @article = N're', @filter_name = N'FLTR_re_1__87', @filter_clause = N'[ndoc] =9', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_RE_SP', @article = N're', @view_name = N'SYNC_re_1__87', @filter_clause = N'[ndoc] =9', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
USE [VIANA]
exec VN_addarticle @publication = N'PB_RE_SP', @article = N'rl', @source_owner = N'dbo', @source_object = N'rl', @type = N'logbased', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000000803509F, @identityrangemanagementoption = N'none', @destination_table = N'rl', @destination_owner = N'VN', @status = 16, @vertical_partition = N'false', @ins_cmd = N'CALL [NC_RLVN_Insert]', @del_cmd = N'XCALL [NC_RLVN_Delete]', @upd_cmd = N'XCALL [NC_RLVN_Update]', @filter_clause = N'[ndoc] =9'

-- Adding the article filter
exec VN_articlefilter @publication = N'PB_RE_SP', @article = N'rl', @filter_name = N'FLTR_rl_1__87', @filter_clause = N'[ndoc] =9', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1

-- Adding the article synchronization object
exec VN_articleview @publication = N'PB_RE_SP', @article = N'rl', @view_name = N'SYNC_rl_1__87', @filter_clause = N'[ndoc] =9', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO

-- Adding the transactional subscriptions
USE [VIANA]
exec VN_addsubscription @publication = N'PB_RE_SP', @subscriber = N'SRVBD', @destination_db = N'FERMAT', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec VN_addpushsubscription_agent @publication = N'PB_RE_SP', @subscriber = N'SRVBD', @subscriber_db = N'FERMAT', @job_login = null, @job_password = null, @subscriber_security_mode = 0, @subscriber_login = N'sa', @subscriber_password = null, @job_name = N'FERMAT-SPAULO\SQLPHC1-SPaulo-PB_RE_SP-SRVBD-25', @dts_package_location = N'Distributor'
GO



