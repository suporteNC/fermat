*******************************************
** Valida estabelecimento
**
** Criado por Rui Vale
** Criado em  22/09/2017
*******************************************

TEXT TO uSql TEXTMERGE NOSHOW
	Select estab, u_estab
	From e1(nolock)
	Where e1.estab=0
ENDTEXT

If u_sqlexec(m.uSql,"uCurE1") And Reccount("uCurE1")>0
	Select bo
	If bo.u_estab <> uCurE1.u_estab
		Msg("N�o pode criar/alterar o registo!"+Chr(13)+"O documento � de um estabelecimento diferente.")
		fecha("uCurE1")
		Return .F.
	Endif

	fecha("uCurE1")
Endif

Return .T.
