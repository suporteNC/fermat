USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbobc]    Script Date: 02/02/2023 17:26:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_BC_Update]
		@c1 char(25),
		@c2 varchar(18),
		@c3 varchar(60),
		@c4 char(40),
		@c5 char(25),
		@c6 bit,
		@c7 varchar(60),
		@c8 numeric(13,3),
		@c9 varchar(30),
		@c10 datetime,
		@c11 varchar(8),
		@c12 varchar(30),
		@c13 datetime,
		@c14 varchar(8),
		@c15 bit,
		@c16 datetime,
		@c17 char(25),
		@c18 varchar(18),
		@c19 varchar(60),
		@c20 char(40),
		@c21 char(25),
		@c22 bit,
		@c23 varchar(60),
		@c24 numeric(13,3),
		@c25 varchar(30),
		@c26 datetime,
		@c27 varchar(8),
		@c28 varchar(30),
		@c29 datetime,
		@c30 varchar(8),
		@c31 bit,
		@c32 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c17 = @c1)
begin 
update [PG].[bc] set
		[bcstamp] = @c17,
		[ref] = @c18,
		[design] = @c19,
		[codigo] = @c20,
		[ststamp] = @c21,
		[defeito] = @c22,
		[emb] = @c23,
		[qtt] = @c24,
		[ousrinis] = @c25,
		[ousrdata] = @c26,
		[ousrhora] = @c27,
		[usrinis] = @c28,
		[usrdata] = @c29,
		[usrhora] = @c30,
		[marcada] = @c31,
		[u_ttssinc] = @c32
	where [bcstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[bcstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[bc]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[bc] set
		[ref] = @c18,
		[design] = @c19,
		[codigo] = @c20,
		[ststamp] = @c21,
		[defeito] = @c22,
		[emb] = @c23,
		[qtt] = @c24,
		[ousrinis] = @c25,
		[ousrdata] = @c26,
		[ousrhora] = @c27,
		[usrinis] = @c28,
		[usrdata] = @c29,
		[usrhora] = @c30,
		[marcada] = @c31,
		[u_ttssinc] = @c32
	where [bcstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[bcstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[bc]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','bc',@c1, 'bcstamp', ''