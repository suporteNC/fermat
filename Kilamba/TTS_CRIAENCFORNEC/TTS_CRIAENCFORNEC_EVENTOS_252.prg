**********************************************************
*** Criar Encomendas a Fornecedor pelas Previs�es de Encomendas
***
*** Criado em : 10/04/2017
*** Criado por: Rui Vale
**********************************************************
Select uCurPrevEncBo
Go Top
Do While !Eof()
	If uCurPrevEncBo.ok
		Select uCurPrevEncBi
		Set Filter To uCurPrevEncBi.no = uCurPrevEncBo.no In uCurPrevEncBi
		Locate For uCurPrevEncBi.no = uCurPrevEncBo.no And uCurPrevEncBi.ok
		If Found()
			Tts_CriaBoEncFornecedor(uCurPrevEncBo.no)
		Endif
		Select uCurPrevEncBi
		Set Filter To uCurPrevEncBi.no = uCurPrevEncBo.no In uCurPrevEncBi
		Select uCurPrevEncBi
		Go Top
		Do While !Eof()
			If uCurPrevEncBi.ok
				Select uCurPrevPromo
				Set Filter To uCurPrevPromo.no = uCurPrevEncBo.no And uCurPrevPromo.ref = uCurPrevEncBi.ref In uCurPrevPromo
				Select uCurPrevPromo
				Go Top
				Do While !Eof()
					If uCurPrevPromo.ok
						TEXT TO uSql TEXTMERGE noshow
							Update bi Set qtt2=qtt
							Where bistamp='<<uCurPrevPromo.bistamp>>'
						ENDTEXT
						If u_Sqlexec(uSql)
						Endif
					Endif
					Select uCurPrevPromo
					Skip
				Enddo
			Endif
			Select uCurPrevEncBi
			Skip
		Enddo
	Endif
	Select uCurPrevEncBo
	Skip
Enddo
Msg("Encomendas guardadas!!")
PDU_4W30WE4J9.Hide()
PDU_4W30WE4J9.Release()
Function Tts_CriaBoEncFornecedor
	Lparameters uNo
	fecha("BO")
	fecha("Bo2")
	fecha("Bo3")
	fecha("Bot")
	fecha("Bi")
	fecha("Bi2")
	Do dbfuseboall
	Do tsread With "",2
	fecha("uCurTmpBo")
	fecha("uCurTmpBo2")
	fecha("uCurTmpBo3")
	fecha("uCurTmpBi")
	fecha("uCurTmpBi2")
	* criar os cursores vazios
	Create Cursor uCurTmpBo (no N(10), estab N(3), memissao c(10),fref c(20))
	u_Sqlexec([select * from bo2 (nolock) where 1=0],[uCurTmpBo2])
	u_Sqlexec([select * from bo3 (nolock) where 1=0],[uCurTmpBo3])
	u_Sqlexec([select * from bi (nolock) where 1=0],[uCurTmpBi])
	u_Sqlexec([select * from bi2 (nolock) where 1=0],[uCurTmpBi2])
	Select uCurTmpBo
	Append Blank
	Select uCurTmpBo
	Replace uCurTmpBo.no With uNo
	Replace uCurTmpBo.estab With 0
	Replace uCurTmpBo.memissao With 'EURO'
	Select uCurPrevEncBi
	Set Filter To uCurPrevEncBi.no = uNo In uCurPrevEncBi
	Go Top
	Do While !Eof()
		If uCurPrevEncBi.ok
			Select uCurTmpBi
			Append Blank
			Replace uCurTmpBi.bistamp With u_stamp()
			Replace uCurTmpBi.ref With uCurPrevEncBi.ref
			Do BOACTREF With '',.T.,'OKPRECOS','uCurTmpBi'
			Replace uCurTmpBi.edebito With uCurPrevEncBi.edebito
			Replace uCurTmpBi.qtt With uCurPrevEncBi.qtt
			Replace uCurTmpBi.stipo With 1
			Do u_bottdeb With 'uCurTmpBi'
		Endif
		Select uCurPrevEncBi
		Skip
	Enddo
	If Not criabobi(2,'uCurTmpBi','uCurTmpBo','uCurTmpBo2',.F.,.F.,Date(),.F.,'uCurTmpBi2','','uCurTmpBo3')
		Msg('Erro a gravar dossier')
		Return
	Endif
	Do eventosexe With "SBO","INTRODUZIR"
Endfunc
