
** Carrega a equivalencia de PONTOS
**************************************************

IF TYPE("p_valpontos")='U'    && (X Akz / 1 Pts)
	public p_valpontos 
	p_valpontos = 0
endif


IF TYPE("p_pontosval")='U'    && (Limite minimo pontos / X Akz)
	public p_pontosval 
	p_pontosval = 0
endif


IF TYPE("p_limitepts")='U'    && (Limite minimo para troca de pontos)
	public p_limitepts 
	p_limitepts = 0
endif


** p_VALPONTOS 
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_valpontos'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_valpontos = cr_para1.valor
endif

fecha("CR_PARA1")



** p_PONTOSVAL
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_pontosval'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_pontosval = cr_para1.valor
endif

fecha("CR_PARA1")


** p_LIMITEPTS
**************************************************

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_LMINPONTOS'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	p_limitepts = cr_para1.valor
endif

fecha("CR_PARA1")


