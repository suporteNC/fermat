** Criado por: Vasco Rocha

select bo
local lresult, dellocal, auxlocal, contador

lresult = .t.

if bo.ndos = p_dielocal && Localiza��es
	create cursor c_bilocal(Gondola C(40), gondLocal C(40), Tipo C(60), Obs C(50), Notas C(50))
	*!* validar c�digos inv�lidos
	select bi
	SCAN FOR NOT EMPTY(bi.lobs)
		select c_bilocal
		append blank
		replace c_bilocal.gondola with bi.lobs3
		replace c_bilocal.gondlocal with bi.litem
		replace c_bilocal.tipo with bi.litem2
		replace c_bilocal.obs with bi.lobs
		lresult = .f.
	ENDSCAN
	
	if lresult = .f.
		msg("Existem erros no preenchimento das localiza��es, verifique!")
		mostrameisto("c_bilocal")
	else
		*!* validar c�digos alterados
		select bi
		scan
			if empty(bi.lobs2)
				replace bi.lobs2 with bi.u_locais
			else
				if alltrim(bi.lobs2) <> alltrim(bi.u_locais)
					if u_sqlexec("select count(ref) quantos from st (nolock) where local = '" + alltrim(bi.lobs2) + "'", "c_stlocaltemp")
						select c_stlocaltemp
						if c_stlocaltemp.quantos > 0
							select c_bilocal
							append blank
							replace c_bilocal.gondola with bi.lobs3
							replace c_bilocal.gondlocal with bi.litem
							replace c_bilocal.tipo with bi.litem2
							replace c_bilocal.local with bi.u_locais
							replace c_bilocal.obs with "<N�o pode alterar o local " + alltrim(bi.lobs2) + ">"
							replace c_bilocal.notas with "<Existem " + astr(c_stlocaltemp.quantos) + " artigos com esse local>"
							replace bi.lobs with c_bilocal.obs
							lresult = .f.
						else
							replace bi.lobs2 with bi.u_locais
						endif
					endif
					fecha("c_stlocaltemp")
				endif							
			endif
		endscan
		if lresult = .f.
			msg("Existem erros no preenchimento das localiza��es, verifique!")
			mostrameisto("c_bilocal")
		else
			*!* validar linhas eliminadas
			select delbi
			scan
				if empty(delbi.lobs2)
					dellocal = delbi.u_locais
				else
					dellocal = delbi.lobs2
				endif
				if u_sqlexec("select bistamp from bi (nolock) where bistamp = '" + alltrim(delbi.bistamp) + "'", "c_bilocaltemp") and reccount("c_bilocaltemp") > 0
					if u_sqlexec("select count(ref) quantos from st (nolock) where local = '" + alltrim(dellocal) + "'", "c_stlocaltemp")
						select c_stlocaltemp
						if c_stlocaltemp.quantos > 0
							select c_bilocal
							append blank
							replace c_bilocal.gondola with delbi.lobs3
							replace c_bilocal.gondlocal with delbi.litem
							replace c_bilocal.tipo with delbi.litem2
							replace c_bilocal.local with delbi.u_locais
							replace c_bilocal.obs with "<N�o pode apagar o local " + alltrim(dellocal) + ">"
							replace c_bilocal.notas with "<Existem " + astr(c_stlocaltemp.quantos) + " artigos com esse local>"
							lresult = .f.
						endif
					endif
					fecha("c_stlocaltemp")
				endif
				fecha("c_bilocaltemp")
			endscan
			if lresult = .f.
				msg("Apagou localiza��es j� associadas a artigos, cancele todas as altera��es!")
				mostrameisto("c_bilocal")
			else
				*!* validar c�digos duplicados
				*!* 
				*!*
			endif
		endif
	endif
endif


return lresult

