USE [SPAULO]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbou_caixant]    Script Date: 27/01/2023 13:31:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_CAIXANTSP_Insert]
    @c1 char(25),
    @c2 varchar(10),
    @c3 numeric(15,2),
    @c4 char(25),
    @c5 varchar(35),
    @c6 numeric(5,0),
    @c7 varchar(30),
    @c8 datetime,
    @c9 varchar(8),
    @c10 varchar(30),
    @c11 datetime,
    @c12 varchar(8),
    @c13 bit,
    @c14 numeric(15,2)
as
begin  
	insert into [SP].[u_caixant] (
		[u_caixantstamp],
		[tipo],
		[valor],
		[u_caixastamp],
		[descricao],
		[qtt],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[total]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14	) 
end  
		EXEC dbo.NC_Sinc_Insert 'dbo','SP','u_caixant',@c1, 'u_caixantstamp'