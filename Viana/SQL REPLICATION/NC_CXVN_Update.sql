USE [VIANA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbocx]    Script Date: 27/01/2023 16:43:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_CXVN_Update]
		@c1 char(25),
		@c2 varchar(20),
		@c3 varchar(20),
		@c4 numeric(3,0),
		@c5 numeric(10,0),
		@c6 numeric(4,0),
		@c7 varchar(30),
		@c8 numeric(6,0),
		@c9 datetime,
		@c10 varchar(8),
		@c11 varchar(30),
		@c12 numeric(6,0),
		@c13 datetime,
		@c14 varchar(8),
		@c15 bit,
		@c16 text,
		@c17 varchar(30),
		@c18 datetime,
		@c19 varchar(8),
		@c20 varchar(30),
		@c21 datetime,
		@c22 varchar(8),
		@c23 bit,
		@c24 bit,
		@c25 char(25),
		@c26 varchar(20),
		@c27 varchar(20),
		@c28 numeric(3,0),
		@c29 numeric(10,0),
		@c30 numeric(4,0),
		@c31 varchar(30),
		@c32 numeric(6,0),
		@c33 datetime,
		@c34 varchar(8),
		@c35 varchar(30),
		@c36 numeric(6,0),
		@c37 datetime,
		@c38 varchar(8),
		@c39 bit,
		@c40 text,
		@c41 varchar(30),
		@c42 datetime,
		@c43 varchar(8),
		@c44 varchar(30),
		@c45 datetime,
		@c46 varchar(8),
		@c47 bit,
		@c48 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c26 = @c2) or
 not (@c29 = @c5) or
 not (@c30 = @c6)
begin 
update [VN].[cx] set
		[cxstamp] = @c25,
		[site] = @c26,
		[pnome] = @c27,
		[pno] = @c28,
		[cxno] = @c29,
		[cxano] = @c30,
		[ausername] = @c31,
		[auserno] = @c32,
		[dabrir] = @c33,
		[habrir] = @c34,
		[fusername] = @c35,
		[fuserno] = @c36,
		[dfechar] = @c37,
		[hfechar] = @c38,
		[fechada] = @c39,
		[causa] = @c40,
		[ousrinis] = @c41,
		[ousrdata] = @c42,
		[ousrhora] = @c43,
		[usrinis] = @c44,
		[usrdata] = @c45,
		[usrhora] = @c46,
		[marcada] = @c47,
		[exportado] = @c48
	where [site] = @c2
  and [cxno] = @c5
  and [cxano] = @c6
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[site] = ' + convert(nvarchar(100),@c2,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxno] = ' + convert(nvarchar(100),@c5,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxano] = ' + convert(nvarchar(100),@c6,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[cx]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [VN].[cx] set
		[cxstamp] = @c25,
		[pnome] = @c27,
		[pno] = @c28,
		[ausername] = @c31,
		[auserno] = @c32,
		[dabrir] = @c33,
		[habrir] = @c34,
		[fusername] = @c35,
		[fuserno] = @c36,
		[dfechar] = @c37,
		[hfechar] = @c38,
		[fechada] = @c39,
		[causa] = @c40,
		[ousrinis] = @c41,
		[ousrdata] = @c42,
		[ousrhora] = @c43,
		[usrinis] = @c44,
		[usrdata] = @c45,
		[usrhora] = @c46,
		[marcada] = @c47,
		[exportado] = @c48
	where [site] = @c2
  and [cxno] = @c5
  and [cxano] = @c6
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[site] = ' + convert(nvarchar(100),@c2,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxno] = ' + convert(nvarchar(100),@c5,1) + ', '
			set @primarykey_text = @primarykey_text + '[cxano] = ' + convert(nvarchar(100),@c6,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[VN].[cx]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','VN','cx',@c1, 'cxstamp', 'cxid'