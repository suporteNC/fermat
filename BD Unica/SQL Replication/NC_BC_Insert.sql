USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSins_dbobc]    Script Date: 02/02/2023 17:24:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_BC_Insert]
    @c1 char(25),
    @c2 varchar(18),
    @c3 varchar(60),
    @c4 char(40),
    @c5 char(25),
    @c6 bit,
    @c7 varchar(60),
    @c8 numeric(13,3),
    @c9 varchar(30),
    @c10 datetime,
    @c11 varchar(8),
    @c12 varchar(30),
    @c13 datetime,
    @c14 varchar(8),
    @c15 bit,
    @c16 datetime
as
begin  
	insert into [PG].[bc] (
		[bcstamp],
		[ref],
		[design],
		[codigo],
		[ststamp],
		[defeito],
		[emb],
		[qtt],
		[ousrinis],
		[ousrdata],
		[ousrhora],
		[usrinis],
		[usrdata],
		[usrhora],
		[marcada],
		[u_ttssinc]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16	) 

		EXEC dbo.NC_Sinc_Insert 'dbo','PG','bc',@c1, 'bcstamp'

end  
