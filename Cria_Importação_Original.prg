***********************************************************
*** Cria dossier de importa��o, com base na Mercadoria em transito
***
*** Criado por Rui Vale
*** Criado em  13/11/2017
***********************************************************

TEXT TO uSql TEXTMERGE noshow
	Select u_estab
	From e1 (nolock)
	Where estab=0
ENDTEXT

If u_Sqlexec(uSql,"uCurTemE1") And Reccount("uCurTemE1")>0
	uDocEstab = uCurTemE1.u_estab
Else
	Msg("Erro ao ler Estabelecimento!!")
	Return
Endif

TEXT TO uSql TEXTMERGE noshow
	Select ndos,nmdos
	From ts (nolock)
	Where nmdos like '%Importa��o%'
	And u_estab = <<Astr(uDocEstab)>>
ENDTEXT


If u_Sqlexec(uSql,"uCurTemTs") And Reccount("uCurTemTs")>0
	mNdos = uCurTemTs.ndos
	uNmdos = uCurTemTs.nmdos
Else
	Msg("Erro ao ler configura��o do Documento de Importa��o!!")
	Return
Endif
select uCurTemTs
mostrameisto("c_stk")
mostrameisto("uCurTemTs")

Select bostamp From c_stk Group By bostamp Into Cursor uCurOrigem Readwrite

Select uCurOrigem
Go Top
Do While !Eof()
	Tts_CriaDoss(mNdos,uNmdos,uCurOrigem.bostamp)

	Select uCurOrigem
	Skip
Enddo

Msg("Acabei!!")

Function Tts_AlteraDoss
	Lparameters mNdos,uNmdos,uOBostamp

	TEXT TO uSql TEXTMERGE noshow
		Select *
		From bi (nolock)
		Where bostamp = '<<uOBostamp>>'
	ENDTEXT
	If u_Sqlexec(uSql,"uCurTmpBi") And Reccount("uCurTmpBi")>0
		Select uCurTmpBi
		Go Top
		Do While !Eof()
			user_AlteraRegistoBi(uCurTmpBi.bistamp,uOBostamp)
			Select uCurTmpBi
			Skip
		Enddo
	Endif

Endfunc

Function Tts_CriaDoss
	Lparameters mNdos,uNmdos,uOBostamp

	TEXT TO uSql TEXTMERGE noshow
		Select nmdos,obrano,dataobra
		From bo (nolock)
		Where ndos = <<Astr(mNdos)>>
		And u_obostamp = '<<uOBostamp>>'
	ENDTEXT
	If u_Sqlexec(uSql,"uCurValBo") And Reccount("uCurValBo")>0
		Return Tts_AlteraDoss(mNdos,uNmdos,uOBostamp)
	Endif

	Return user_cria_Dossier(mNdos,uNmdos,uOBostamp)

Endfunc

Function user_AlteraRegistoBi
	Lparameters uOBistamp,uOBostamp

	Select c_stk
	Locate For Alltrim(Upper(c_stk.bistamp)) == Alltrim(Upper(uOBistamp))
	If Found()
			
		If uCurTmpBi.ttmoeda <> 0
			uAuxCambio = uCurTmpBi.ettdeb / uCurTmpBi.ttmoeda
		Else
			uAuxCambio = 0
		Endif

		Select uCurTmpBi
		Replace uCurTmpBi.edebito With c_stk.novopult
		Replace uCurTmpBi.ettdeb With c_stk.novopult * uCurTmpBi.qtt
		Replace uCurTmpBi.desconto With 0
		Replace uCurTmpBi.desc2 With 0
		If uAuxCambio <> 0
			Replace uCurTmpBi.vumoeda With c_stk.novopult / uAuxCambio
			Replace uCurTmpBi.ttmoeda With (c_stk.novopult * uCurTmpBi.qtt) / uAuxCambio
		Else
			Replace uCurTmpBi.vumoeda With 0
			Replace uCurTmpBi.ttmoeda With 0
		Endif
		
		TEXT TO uSql TEXTMERGE NOSHOW
			Update bi set
			edebito = <<ALLTRIM(Strtran(STR(uCurTmpBi.edebito),",","."))>>
			,ettdeb = <<ALLTRIM(Strtran(STR(uCurTmpBi.ettdeb),",","."))>>
			,desconto = 0
			,desc2 = 0
			,vumoeda = <<ALLTRIM(Strtran(STR(uCurTmpBi.vumoeda),",","."))>>
			,ttmoeda = <<ALLTRIM(Strtran(STR(uCurTmpBi.ttmoeda),",","."))>>
			From bi 
			inner join bo on bo.bostamp = bi.bostamp
			Where obistamp = '<<uOBistamp>>'
			and bo.u_obostamp = '<<uOBostamp>>'
		ENDTEXT

		If u_Sqlexec(uSql)
		Endif
	Endif
Endfunc


Function user_cria_Dossier
	Lparameters uNdos,uNmdos,uOBostamp

	TEXT TO uSql TEXTMERGE noshow
		Select Isnull(Max(obrano)+1,1) as obrano
		From bo (nolock)
		Where ndos=<<STR(uNdos)>>
		and boano=<<Astr(YEAR(DATE()))>>
	ENDTEXT

	If ! u_Sqlexec(uSql,"uCurNumBo") Or Reccount("uCurNumBo")<=0
		Msg("Erro ao tentar criar dossier!!")
		Return
	Endif

	TEXT TO uSql TEXTMERGE noshow
		Select *
		From bo (nolock)
		Where bostamp = '<<uOBostamp>>'
	ENDTEXT

	If ! u_Sqlexec(uSql,"uCurTmpBo") Or Reccount("uCurTmpBo")<=0
		Msg("Erro ao tentar criar dossier!!")
		Return
	Endif

	uSql = "Begin Transaction"

	If ! u_Sqlexec(uSql)
		Msg("Erro ao tentar criar dossier!!")
		Return
	Endif

	uBoStamp = u_Stamp()
	Select uCurTmpBo
	Replace uCurTmpBo.bostamp With uBoStamp

	Replace uCurTmpBo.ndos With uNdos
	Replace uCurTmpBo.nmdos With uNmdos
	Replace uCurTmpBo.obrano With uCurNumBo.obrano
	Replace uCurTmpBo.dataobra With Date()
	Replace uCurTmpBo.boano With Year(Date())
	Replace uCurTmpBo.dataopen With Date()
	Replace uCurTmpBo.u_obostamp With uOBostamp

	Replace uCurTmpBo.ousrdata With Date()
	Replace uCurTmpBo.ousrhora With Time()
	Replace uCurTmpBo.ousrinis With M_CHINIS
	Replace uCurTmpBo.usrdata With Date()
	Replace uCurTmpBo.usrhora With Time()
	Replace uCurTmpBo.usrinis With M_CHINIS

	If ! Tts_GuardaSql("uCurTmpBo","BO")
		Msg("Erro ao tentar criar documento!!")

		uSql = "RollBack Transaction"

		If u_Sqlexec(uSql)
		Endif
		Return
	Endif

	TEXT TO uSql TEXTMERGE noshow
		Select *
		From bo2 (nolock)
		Where bo2stamp = '<<uOBostamp>>'
	ENDTEXT

	If ! u_Sqlexec(uSql,"uCurTmpBo2")
		Msg("Erro ao tentar criar documento!!")

		uSql = "Rollback Transaction"

		If u_Sqlexec(uSql)
		Endif
		Return
	Endif

	Select uCurTmpBo2
	Replace uCurTmpBo2.bo2stamp With uBoStamp

	Replace uCurTmpBo2.ousrdata With Date()
	Replace uCurTmpBo2.ousrhora With Time()
	Replace uCurTmpBo2.ousrinis With M_CHINIS
	Replace uCurTmpBo2.usrdata With Date()
	Replace uCurTmpBo2.usrhora With Time()
	Replace uCurTmpBo2.usrinis With M_CHINIS

	If ! Tts_GuardaSql("uCurTmpBo2","BO2")
		Msg("Erro ao tentar criar documento!!")

		uSql = "RollBack Transaction"

		If u_Sqlexec(uSql)
		Endif
		Return
	Endif

	TEXT TO uSql TEXTMERGE noshow
		SELECT *
		From bi (nolock)
		Where bostamp = '<<uOBostamp>>'
	ENDTEXT

	If u_Sqlexec(uSql,"uCurTmpBi") And Reccount("uCurTmpBi")>0
		Select uCurTmpBi
		Go Top
		Do While !Eof()

			Select c_stk
			Locate For Alltrim(Upper(c_stk.bistamp)) == Alltrim(Upper(uCurTmpBi.bistamp))
			If Found()
				If ! user_guarda_linha_dossier(uBoStamp,uNdos,uNmdos,uCurTmpBi.bistamp)
					Msg("Erro ao tentar criar documento!!")
					uSql = "RollBack Transaction"

					If u_Sqlexec(uSql)
					Endif
					Return
				Endif
			Endif

			Select uCurTmpBi
			Skip
		Enddo
	Endif

	uSql = "Commit Transaction"

	If ! u_Sqlexec(uSql)
		Msg("Erro ao Guardar!!")

		uSql = "Rollback Transaction"

		If u_Sqlexec(uSql)
		Endif
		Return
	Endif

Endfunc


Function user_guarda_linha_dossier
	Lparameters uBoStamp,uNdos,uNmdos,uOBistamp

	TEXT TO uSql TEXTMERGE noshow
		Select *
		From bi (nolock)
		Where bistamp = '<<uOBistamp>>'
	ENDTEXT

	If !u_Sqlexec(uSql,"uCurTempBi") Or Reccount("uCurTempBi")<=0
		Msg("Erro ao tentar registar!!")
		Return .F.
	Endif

	uBistamp = u_Stamp()
	Select uCurTempBi
	Replace uCurTempBi.bistamp With uBistamp
	Replace uCurTempBi.bostamp With uBoStamp
	Replace uCurTempBi.ndos With uNdos
	Replace uCurTempBi.nmdos With uNmdos
	Replace uCurTempBi.obrano With uCurTmpBo.obrano
	Replace uCurTempBi.dataobra With uCurTmpBo.dataobra
	Replace uCurTempBi.obistamp With uOBistamp
	Replace uCurTempBi.oobistamp With uOBistamp
	Replace uCurTempBi.qtt2 With 0

	If uCurTempBi.ttmoeda <> 0
		uAuxCambio = uCurTempBi.ettdeb / uCurTempBi.ttmoeda
	Else
		uAuxCambio = 0
	Endif

	Select c_stk
	Locate For Alltrim(Upper(c_stk.bistamp)) == Alltrim(Upper(uOBistamp))
	If Found()
		Select uCurTempBi
		Replace uCurTempBi.edebito With c_stk.novopult
		Replace uCurTempBi.ettdeb With c_stk.novopult * uCurTempBi.qtt
		Replace uCurTempBi.desconto With 0
		Replace uCurTempBi.desc2 With 0
		If uAuxCambio <> 0
			Replace uCurTempBi.vumoeda With c_stk.novopult / uAuxCambio
			Replace uCurTempBi.ttmoeda With (c_stk.novopult * uCurTempBi.qtt) / uAuxCambio
		Else
			Replace uCurTempBi.vumoeda With 0
			Replace uCurTempBi.ttmoeda With 0
		Endif
	Endif

	Replace uCurTempBi.ousrdata With Date()
	Replace uCurTempBi.ousrhora With Time()
	Replace uCurTempBi.ousrinis With M_CHINIS
	Replace uCurTempBi.usrdata With Date()
	Replace uCurTempBi.usrhora With Time()
	Replace uCurTempBi.usrinis With M_CHINIS

	Select uCurTempBi

	If ! Tts_GuardaSql("uCurTempBi","bi")
		Return .F.
	Endif

	TEXT TO uSql TEXTMERGE noshow
		SELECT *
		From bi2 (nolock)
		Where bi2stamp = '<<uOBistamp>>'
	ENDTEXT

	If ! u_Sqlexec(uSql,"uCurTempBi2")
		Return .F.
	Endif

	Select uCurTempBi2
	Replace uCurTempBi2.bi2stamp With uBistamp
	Replace uCurTempBi2.bostamp With uBoStamp

	Select uCurTempBi2

	Return Tts_GuardaSql("uCurTempBi2","bi2")

Endfunc

Function Tts_GuardaSql
	Parameter meucursor,uMinhaTabela,uBaseDados

	***************************************************
	** Fun��o que guarda um registo do cursor, numa  **
	** tabela do sql						         **
	***************************************************

	**************** PAR�METROS *******************
	** meucursor   : Nome do cursor a guardar
	** uMinhaTabela: Tabela onde vai guardar
	** uBDados     : Base de Dados onde vai guardar
	***********************************************

	Local uMENCOUTROU,uNREG,uNREG1,uNRTENTATIVAS

	merro=.F.

	Select &meucursor

	gnFieldcount = Afields(gaMyArray)  && Create array
	Clear
	Dimension aCampos  [gnFieldcount]
	Dimension aTipos   [gnFieldcount]
	Dimension aTamanho [gnFieldcount]
	Dimension aCasas   [gnFieldcount]

	For nCount = 1 To gnFieldcount
		aCampos [nCount] = gaMyArray[nCount,1]
		aTipos  [nCount] = gaMyArray[nCount,2]
		aTamanho[nCount] = gaMyArray[nCount,3]
		aCasas  [nCount] = gaMyArray[nCount,4]
	Endfor

	sSQL=""
	cSQL=""

	If !Empty(uBaseDados)
		sSQLString = "select * from "+Alltrim(uBaseDados)+".."+Alltrim(uMinhaTabela)+Chr(13)
	Else
		sSQLString = "select * from "+Alltrim(uMinhaTabela)+Chr(13)
	Endif

	sSQLString =sSQLString +" (nolock) where 1=2"+Chr(13)

	If u_Sqlexec(sSQLString,"uCURTEMP")

		Select uCURTEMP
		gnFieldcount1 = Afields(uCURTEMP)  && Create array
		Clear
		Dimension aCampos1  [gnFieldcount1]
		Dimension aTipos1   [gnFieldcount1]
		Dimension aTamanho1 [gnFieldcount1]
		Dimension aCasas1   [gnFieldcount1]

		For nCount = 1 To gnFieldcount1
			aCampos1 [nCount] = uCURTEMP[nCount,1]
			aTipos1  [nCount] = uCURTEMP[nCount,2]
			aTamanho1[nCount] = uCURTEMP[nCount,3]
			aCasas1  [nCount] = uCURTEMP[nCount,4]
		Endfor

		** verifica quantos registos s�o iguais
		uNREG=0
		For nCount = 1 To gnFieldcount

			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i]
					uNREG=uNREG+1
				Endif

			Endfor
		Endfor

		uNREG1=0
		For nCount = 1 To gnFieldcount

			uMENCOUTROU=.F.
			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i]
					uMENCOUTROU=.T.
				Endif

			Endfor

			If uMENCOUTROU

				uNREG1=uNREG1+1

				Select &meucursor

				cDado = Evaluate(aCampos[nCount])

				cTipo = aTipos[nCount]

				ucCampo = aCampos[nCount]

				*Select uCurCamposQuery
				*Locate For Alltrim(Upper(uCurCamposQuery.campo))==ucCampo
				*If Found()

				If Alltrim(Upper(ucCampo))<>Alltrim(Upper(uMinhaTabela))+'ID' ;
						And Alltrim(Upper(ucCampo))<>'AUTOMATICOID';
						And Alltrim(Upper(ucCampo))<>'U_HV_PHC';
						And Alltrim(Upper(ucCampo))<>'U_HV_INT';
						And Alltrim(Upper(ucCampo))<>'ROWID'

					If !Empty(sSQL)
						sSQL = sSQL + "," + Chr(13)
						cSQL = cSQL + "," + Chr(13)
					Endif

					If aTipos[nCount] = "N" Or aTipos[nCount] = "Y"
						cDado = Alltrim(Str(cDado,aTamanho[nCount],aCasas[nCount]))
					Endif

					If aTipos[nCount] = "I"
						cDado = Alltrim(Str(cDado))
					Endif

					If aTipos[nCount] = "T" Or aTipos[nCount] = "D"
						If Empty(cDado)
							cDado = "19000101"
						Else
							cDado = Alltrim(Dtos(Date(Year(cDado),Month(cDado),Day(cDado))))
						Endif
					Endif

					If aTipos[nCount] = "L"
						If cDado = .T.
							cDado = Alltrim(Str(1))
						Else
							cDado = Alltrim(Str(0))
						Endif
					Endif

					If aTipos[nCount] = "N" Or aTipos[nCount] = "I" Or ;
							aTipos[nCount] = "Y" Or aTipos[nCount] = "B" Or ;
							aTipos[nCount] = "L"
						sSQL = sSQL + Strtran(Alltrim(Astr(cDado)),",",".")
					Else
						sSQL = sSQL + "'" + Strtran(Alltrim(Astr(cDado)),"'","''") + "'"
					Endif

					cSQL = cSQL + Alltrim(ucCampo)

				Endif

				*Endif

			Endif

		Endfor

		If !Empty(uBaseDados)
			sSQLString = "Insert Into "+Alltrim(uBaseDados)+".."+Alltrim(uMinhaTabela)
		Else
			sSQLString = "Insert Into "+Alltrim(uMinhaTabela)
		Endif

		sSQLString =sSQLString +" ("+cSQL
		sSQLString =sSQLString +") Values ("
		sSQLString =sSQLString + sSQL + ")"

		uNRTENTATIVAS=0
		merro=.T.
		Do While uNRTENTATIVAS<10 And merro

			If u_Sqlexec(sSQLString)
				merro=.F.
			Else
				merro=.T.
			Endif

			uNRTENTATIVAS=uNRTENTATIVAS+1

		Enddo

		If merro
			Return .F.
		Endif

	Endif

	Return .T.

Endfunc
