************************************************************
*** Cria campo controlo TotalSincro em todas as tabelas (se n�o existir)
***
*** Criado por : Rui Vale
***  Criado em : 20.02.2020
************************************************************

*uTipo = "Pull"
*uTipo = "Push"
uTipo = "%"

TEXT TO uSql TEXTMERGE noshow
	Select Tabela From(
		select distinct tabela from totalsincro..sincmest Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela2 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela3 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela4 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela5 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela6 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela7 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela8 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela9 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
		union all
		select distinct tabela10 tabela from totalsincro..sincdoc Where tipo Like '<<uTipo>>'
	) a
	Where tabela <> ''
	Order by 1
ENDTEXT
_cliptext=uSql
return
If u_Sqlexec(uSql,"uCurTmpTab") And Reccount("uCurTmpTab")>0
	Select uCurTmpTab
	Go Top
	Do While !Eof()
		uBDOrigem = SQL_DB
		Tts_criacampo(Alltrim(uCurTmpTab.tabela),"u_ttssinc","D","","'19000101'",10,0,uBDOrigem)
		Tts_criadiccampo(Alltrim(uCurTmpTab.tabela),"ttssinc","D","","'19000101'",10,0,uBDOrigem)

		Select uCurTmpTab
		Skip
	Enddo
Endif

Function Tts_verificatabela
	Parameters uTabela,uBaseDados

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	TEXT TO uSql TEXTMERGE noshow
		SELECT table_name
		FROM <<ALLTRIM(uBaseDados)>>.INFORMATION_SCHEMA.TABLES
		Where table_name = '<<uTabela>>'
	ENDTEXT

	If u_Sqlexec( uSql, "uCurTmp") And Reccount("uCurTmp")>0
		Return .T.
	Else
		Return .F.
	Endif
Endproc

Procedure Tts_verifica_tipo_campo
	Parameters uTabela,uCampo,uBaseDados

	uTabela = Alltrim(uTabela)

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	TEXT TO uSql TEXTMERGE noshow
		SELECT convert(char(1),
			Case when data_type='varchar' or data_type='char' then 'C'
			Else
				Case when data_type='numeric' then 'N'
				Else
					Case when data_type='float' then 'F'
					Else
						Case when data_type='bit' then 'L'
						Else
							Case when data_type='datetime' then 'D'
							Else
								Case when data_type='text' then 'M'
								Else
									Case when data_type='int' then 'I'
									Else
										Case when data_type='nvarchar' then 'C'
										Else
											Case when data_type='image' then 'B'
											End
										End
									End
								End
							End
						End
					End
				End
			End, 121) as tipo
			,Case when data_type='varchar' or data_type='char' then CHARACTER_MAXIMUM_LENGTH
			Else
				Case when data_type='numeric' Or data_type='float' then NUMERIC_PRECISION
				Else
					Case when data_type='nvarchar' then CHARACTER_MAXIMUM_LENGTH
					Else 0
					End
				End
			End as comprimento
			,Case when data_type='numeric' then NUMERIC_SCALE
				Else
					Case when data_type='float' then NUMERIC_PRECISION_RADIX
					Else 0
				End
			End as decimais,*
		FROM <<uBaseDados>>.INFORMATION_SCHEMA.COLUMNS
		Where table_name = '<<uTabela>>'
		and column_name = '<<uCampo>>'
	ENDTEXT

	If u_Sqlexec( uSql, "uCurTmpCampo")
	Endif
Endproc

Function Tts_verificacampo
	Parameters uTabela,uCampo,uBaseDados

	uTabela = Alltrim(uTabela)

	*If Inlist(Upper(uTabela),"BO","BI","BO2","BI2") And Upper(uCampo)="OBRANO"
	*	Return .F.
	*Endif

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	TEXT TO uSql TEXTMERGE noshow
		SELECT column_name
		FROM <<ALLTRIM(uBaseDados)>>.INFORMATION_SCHEMA.COLUMNS
		Where table_name = '<<uTabela>>'
		and column_name = '<<uCampo>>'
	ENDTEXT

	If u_Sqlexec( uSql, "uCurTmp") And Reccount("uCurTmp")>0
		Return .T.
	Else
		Return .F.
	Endif
Endproc

Function Tts_criatabela
	Parameters uTabela,uTbdesc,uBaseDados

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	uBaseDados= Alltrim(uBaseDados)
	uTabela = Alltrim(uTabela)

	If ! Tts_verificatabela(uTabela,uBaseDados)
		TEXT TO uSql TEXTMERGE noshow
			Create table <<uBaseDados>>..<<uTabela>>
			(<<ALLTRIM(UPPER(uTabela))>>stamp varchar(25) primary key default left(newid(),25)
			,usrinis varchar(10) not null default ''
			,usrdata datetime not null default CONVERT(datetime,REPLACE(CONVERT(CHAR(10),getdate(),121),'-',''),121)
			,usrhora varchar(8) not null default substring(convert(char(25),getdate(),121),12,8)
			,ousrinis varchar(10) not null default ''
			,ousrdata datetime not null default CONVERT(datetime,REPLACE(CONVERT(CHAR(10),getdate(),121),'-',''),121)
			,ousrhora varchar(8) not null default substring(convert(char(25),getdate(),121),12,8)
			,marcada bit not null default 0)
		ENDTEXT

		If u_Sqlexec(uSql)
			Return .T.
		Else
			Return .F.
		Endif
	Else
		Return .T.
	Endif

Endproc

Function Tts_criadiccampo
	Parameters uTabela,uCampo,uTipo,uDescTabela,uDefault,uComprimento,uDecimais,uBaseDados

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	uBaseDados= Alltrim(uBaseDados)
	uTabela = Alltrim(uTabela)
	uCampo= Alltrim(Strtran(Upper(uCampo),"U_",""))

	TEXT TO uSql TEXTMERGE noshow
		Select *
		From <<uBaseDados>>.dbo.campos
		Where tabela = '<<uTabela>>'
		And nomecampo = '<<uCampo>>'
	ENDTEXT

	If u_Sqlexec(uSql,"uCurDic")
		If Reccount("uCurDic")<=0
			TEXT TO uSql TEXTMERGE noshow
				INSERT INTO <<uBaseDados>>.dbo.campos
				(camposstamp,tabela,nomecampo,titulo,tipo,comprimento,decimais,destino)
				values (LEFT(newid(),25)
				,'<<uTabela>>','<<Proper(uCampo)>>','<<uCampo>>','<<uTipo>>'
				,<<ALLTRIM(STR(uComprimento))>>,<<ALLTRIM(STR(uDecimais))>>,3)
			ENDTEXT

			If !u_Sqlexec(uSql)
				Return .F.
			Endif
		Endif
	Endif

	Return .T.

Endproc

Function Tts_criacampo
	Parameters uTabela,uCampo,uTipo,uDescTabela,uDefault,uComprimento,uDecimais,uBaseDados

	If Empty(uBaseDados)
		uBaseDados	= uBDados
	Endif

	uBaseDados= Alltrim(uBaseDados)
	uTabela = Alltrim(uTabela)
	uCampo= Alltrim(uCampo)

	If Empty(uDecimais)
		uDecimais = 0
	Endif

	** Verifica se a rabela existe, ou se conseguiu criar
	If Tts_criatabela(uTabela,uDescTabela,uBaseDados)

		** Verifica se o campo n�o existe
		If ! Tts_verificacampo(uTabela,uCampo,uBaseDados)

			Do Case
				Case uTipo = "Y"

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						bigint IDENTITY(1,1)
					ENDTEXT

				Case uTipo = "C"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "''"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						varchar(<<Astr(uComprimento)>>)
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "M"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "''"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						text
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "I"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "0"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						int
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "N"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "0"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						numeric(<<Astr(uComprimento)>>,<<Astr(uDecimais)>>)
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "F"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "0"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						float
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "D"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "CONVERT(datetime,REPLACE(CONVERT(CHAR(10),getdate(),121),'-',''),121)"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						datetime
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "L"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "0"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						bit
						not null default <<uDefault>>
					ENDTEXT

				Case uTipo = "B"

					If Empty(uDefault) Or Isnull(uDefault) Or Alltrim(Upper(uDefault)) = "NULL"
						uDefault = "''"
					Endif

					TEXT TO uSql TEXTMERGE noshow
						Alter table <<uBaseDados>>..<<uTabela>>
						add <<uCampo>>
						image
						not null default <<uDefault>>
					ENDTEXT

				Otherwise
					Return .F.
			Endcase

			If u_Sqlexec(uSql)
				Return .T.
			Else
				Return .F.
			Endif

			** Se o campo j� existe
		Else

			Tts_verifica_tipo_campo(uTabela,uCampo,uBaseDados)

			If uCurTmpCampo.tipo <> "M"

				Select uCurTmpCampo
				Go Top

				Do Case
					Case uCurTmpCampo.tipo <> uTipo

						Return Tts_Altera_campo (uTabela,uCampo,uTipo,uDescTabela,uDefault,uComprimento,uDecimais,uBaseDados)

					Case uTipo = "C" And uComprimento <> uCurTmpCampo.comprimento

						Return Tts_Altera_campo (uTabela,uCampo,uTipo,uDescTabela,uDefault,uComprimento,uDecimais,uBaseDados)

					Case (uTipo = "N" Or uTipo = "F") And (uComprimento <> uCurTmpCampo.comprimento Or uDecimais <> uCurTmpCampo.decimais)

						Return Tts_Altera_campo (uTabela,uCampo,uTipo,uDescTabela,uDefault,uComprimento,uDecimais,uBaseDados)

					Otherwise

						Return .T.

				Endcase

			Endif
		Endif
	Else
		Return .F.
	Endif

	Return .T.

Endproc
