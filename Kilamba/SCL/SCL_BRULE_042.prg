Select CL2
IF !EMPTY(CL2.U_NCARTAO)
	TEXT TO MSQL TEXTMERGE NOSHOW
	select NOME 
	from cl (nolock) inner join cl2 (nolock) on cl2.cl2stamp = cl.clstamp
	where cl2.u_ncartao = '<<ALLTRIM(CL2.u_NCARTAO)>>'
		and CL.CLSTAMP <> '<<CL2.CL2STAMP>>'
	ENDTEXT
	IF !U_SQLEXEC(MSQL,'cr_CLCARTAOTMP')
		MSG('ERRO ENCONTRADO')
		MSG(MSQL)
		return
	ENDIF
	select cr_CLCARTAOTMP
	IF reccount("cr_CLCARTAOTMP")>0
		msg("N� de cart�o repetido! Existe outro cliente com o mesmo n�mero de cart�o")
		return .f.
	ENDIF
ENDIF

TEXT TO uSql TEXTMERGE noshow
	Select 1
	Where <<Astr(cl.no)>> between isnull((select e1.u_noclini from e1 (nolock) where estab=0 and e1.u_noclini<>0),0)
		and isnull((select e1.u_noclfim from e1 (nolock) where estab=0 and e1.u_noclfim <>0),9999999999)
ENDTEXT

If u_sqlexec(uSql,"uCurNumCl") And Reccount("uCurNumCl")>0
	Return .T.
Else
	Msg("N�o pode criar/alterar clientes com esse n�")
	Return .F.
Endif

Return .T.
