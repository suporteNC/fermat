** Criado por: Vasco Rocha
** Data cria��o: 08.09.2015
CREATE CURSOR c_obo (bostamp c(25),barcode c(25),obrano n(15))
** cria cursor com os stamps das encomendas de fornecedor 
SELECT bi
** vai buscar os stamps das encomendas existentes na entrada e coloca no cursor c_bo 
SCAN FOR NOT EMPTY(bi.obistamp) and  (empty(bi.lobs3) OR bi.lobs3 = 'N�o Entregue')
	TEXT TO m.msel NOSHOW TEXTMERGE
		select 	bo.bostamp, bo3.barcode, bo.obrano, qtt 
		from 	bi (nolock) 
		inner 	join bo (nolock) on bo.bostamp = bi.bostamp 
		inner 	join bo3 (nolock) on bo.bostamp = bo3.bo3stamp 
		where 	bistamp = '<<ALLTRIM(bi.obistamp)>>'
	ENDTEXT 
	**u_sqlexec("select bo.bostamp, bo3.barcode, bo.obrano, qtt from bi(nolock) inner join bo (nolock) on bo.bostamp = bi.bostamp where bistamp = '"+bi.obistamp+"'","c_tbi")
	IF NOT u_sqlexec(m.msel, 'c_tbi')
		msg('Erro ao consultar encomendas existentes na entrada.')
		RETURN .f.
	ENDIF
	**msg(ALLTRIM(bi.ref) + ' qtt: ' + astr(bi.qtt))
	SELECT  bi
	replace bi.oobostamp WITH  c_tbi.bostamp 
	replace bi.u_qttenc  WITH c_tbi.qtt  
	SELECT c_tbi
	IF RECCOUNT("c_tbi")>0
		SELECT c_obo
		LOCATE FOR  c_obo.bostamp = c_tbi.bostamp
		IF NOT FOUND()
			SELECT  c_obo
			APPEND BLANK
			REPLACE  c_obo.bostamp  WITH  c_tbi.bostamp
			REPLACE  c_obo.barcode WITH  c_tbi.barcode
			REPLACE  c_obo.obrano WITH  c_tbi.obrano 
		ENDIF
	ENDIF
	fecha("c_tbi")
ENDSCAN
IF RECCOUNT("c_obo")= 0
	fecha("c_fposlbo")
	RETURN
ENDIF
** vai buscar as linnhas de cada encomenda de fornecedor 
SELECT  c_obo
SCAN  
	TEXT TO msel NOSHOW TEXTMERGE
		select 	bo.bostamp, ref, u_pdarm.barcode, sum(qtt) qtt, max(u_pdarmstamp) u_pdarmstamp
		from 	u_pdarm (nolock)
		inner 	join bo (nolock) on bo.boano = u_pdarm.boano and bo.obrano = u_pdarm.obrano
		where  	u_pdarm.barcode = '<<c_obo.barcode>>'
				and u_pdarm.u_pdarmstamp not in (select optstamp from bi (nolock) where optstamp <> '')
				and bo.ndos = <<Astr(p_dieencforn)>> 
		group 	by u_pdarm.barcode,bo.bostamp,  ref
	ENDTEXT
	if not u_sqlexec(msel,"c_boem")
		msg(msel)
		return
	endif  
	select c_boem
	IF  reccount("c_boem") = 0 
		*msg("A encomenda a fornecedor n� " +  astr(c_obo.obrano) +  " n�o tem entrada de PDA")
		m.lPergunta = dpergunta(2,1,"","A enc. forn. n� " +  astr(c_obo.obrano) +  " n�o tem ent. PDA"+chr(13)+"Quer manter as quantidades sa encomenda?","","Sim","N�o")
		if m.lPergunta = 1
			return 
		endif
	ELSE  
		SELECT  c_boem
		SCAN 
			SELECT bi 
			LOCATE FOR  bi.ref = c_boem.ref AND bi.oobostamp = c_boem.bostamp  
			IF FOUND()
				SELECT bi
				replace bi.qtt WITH  c_boem.qtt 
				replace bi.u_qttpda WITH  c_boem.qtt
				replace bi.optstamp with c_boem.u_pdarmstamp  && coloca a linha ligacao ao pdarm
				IF  c_boem.qtt > bi.u_qttenc
					replace bi.lobs3 WITH  'QTD +'				
				ENDIF 
				IF  c_boem.qtt = bi.u_qttenc
					replace bi.lobs3 WITH  'QTD ='				
				ENDIF 
				IF  c_boem.qtt < bi.u_qttenc
					replace bi.lobs3 WITH  'QTD -'				
				ENDIF 
				do u_bottdeb with 'BI',.F.
				Do BOTOTS with .T.
			ELSE
				sbo.Pageframe1.Page1.Cont1.Grid1.refresh()
				SELECT bi  
				goto bottom
				Do Boine2in
				replace bi.ref WITH  c_boem.ref 
				Do BOACTREF with '',.t.,'OKPRECOS','BI'
				replace bi.qtt WITH  c_boem.qtt 
				replace bi.u_qttpda WITH  c_boem.qtt
				replace bi.optstamp with c_boem.u_pdarmstamp  && coloca a linha ligacao ao pdarm
				replace bi.lobs3 WITH  'S/Enc.Forn.'
				do u_bottdeb with 'BI',.F.
				Do BOTOTS with .T.
			ENDIF  
			select  c_boem
		ENDSCAN 	
	ENDIF 
ENDSCAN 

SELECT bi  
SCAN for NOT EMPTY(bi.ref) AND  NOT EMPTY(bi.obistamp) AND EMPTY(bi.lobs3) 
	replace bi.qtt  WITH  0 
	replace bi.lobs3 WITH  'N�o Entregue'
	do u_bottdeb with 'BI',.F.
	Do BOTOTS with .T.
ENDSCAN 
msg("Conferido")
