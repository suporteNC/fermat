** JRic
**2009-02-17
** Alterado por Paulo Ricardo Martins em data:25/09/2017 - controlo de pontos


IF NOT  WEXIST("SFT")
	sft.CancelarGravacao(.T.,.T.)
	RETURN ft.bidata
ENDIF
sft.emitere.ENABLED = .T.

IF WEXIST("SFPOS")
	RETURN ft.bidata
ENDIF
*s� actua na nota de credito
IF NOT  INLIST(ft.ndoc,14,40, 64)
	*SFT.Pageframe1.Page2.ENABLED 				= .T.
	*SFT.Pageframe1.Page4.ENABLED 				= .t.
	SFT.Pageframe1.Page1.NOME.ENABLED 			= .T.
	SFT.Pageframe1.Page1.moeda.ENABLED 			= .T.
	SFT.Pageframe1.Page1.Finvm.ENABLED 			= .T.
	SFT.Pageframe1.Page1.Finv.ENABLED 			= .T.
	SFT.Pageframe1.Page1.Fdata .ENABLED 		= .T.
	SFT.Pageframe1.Page1.Cont1.Bocopy.ENABLED 	= .T.
	SFT.Pageframe1.Page1.Cont1.Ftcopy.ENABLED 	= .T.
	SFT.Pageframe1.Page1.Cont1.MENU.ENABLED 	= .T.
	SFT.Pageframe1.Page1.Zona.ENABLED 			= .T.
	SFT.Pageframe1.Page1.Tpdesc1.ENABLED 		= .T.
	SFT.Pageframe1.Page1.Pdata.ENABLED 			= .T.
	SFT.Pageframe1.Page1.Encomenda.ENABLED 		= .T.
	SFT.Pageframe1.Page1.Vendedor1 .ENABLED 	= .T.
	SFT.Pageframe1.Page1.Cont1.Autototal.ENABLED = .T.
	SFT.Pageframe1.Page1.Fin.ENABLED			= .T.
	SFT.Pageframe1.Page1.area.ENABLED = .T.
	RETURN ft.bidata
ENDIF

IF INLIST(ft.ndoc,14,40, 64) OR  SFT.Tmpdoc.DISPLAYVALUE = 'N/C Devolu��o'
	sft.emitere.ENABLED = .F.
	ccolumnexiste = 0
	i = 0
	FOR EACH COLUMNS IN SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS
		i = i + 1
		IF  SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS(i).CONTROLSOURCE == "FI.OPGSTAMP"
			ccolumnexiste = 1
		ENDIF
	NEXT
	*msg(astr(ccolumnexiste))
	IF  ccolumnexiste = 0
		FOR EACH oMyform IN _SCREEN.FORMS
			IF ALLTRIM(UPPER(oMyform.NAME)) = UPPER("SFT")
				WITH oMyform
					.Pageframe1.Page1.Cont1.Grid1.ADDCOLUMN(1)   && Insert column at left.
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).NAME = "Motivo"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).CONTROLSOURCE="FI.OPGSTAMP"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).FONTSIZE=8
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).DYNAMICBACKCOLOR="rgb(255,255,128)"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).FORMAT = "RTKZ"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).READONLY = .T.
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).SPARSE = .F.
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).header1.CAPTION="Motivo"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).header1.FONTSIZE=8
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).text1.FORMAT = "RTKZ"
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).text1.BORDERSTYLE = 0
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).text1.COLORSOURCE = 3
					.Pageframe1.Page1.Cont1.Grid1.COLUMNS(.Pageframe1.Page1.Cont1.Grid1.COLUMNCOUNT).text1.MARGIN = 0
				ENDWITH
			ENDIF
		NEXT oMyform
	ENDIF
ENDIF

**** pede pw
cPass = ''
cPass = getnome('Introduza a sua Password','','','',1,.T.)
IF EMPTY(cPass)
	msg("Password Inv�lida")
	sft.CancelarGravacao(.T.,.T.)
	RETURN ft.bidata
ENDIF
TEXT TO msel NOSHOW TEXTMERGE
	select username from us (nolock) where  pwpos = '<<cpass>>' and  pwpos <> '' and u_devft = 1 
ENDTEXT

IF  NOT u_sqlexec(msel,"c_pass")
	msg(msel)
	sft.CancelarGravacao(.T.,.T.)
	RETURN ft.bidata
ENDIF
IF  RECCOUNT("c_pass") > 1
	msg("Existem v�rios utilizadores para a mesma password.")
	fecha("c_pass")
	sft.CancelarGravacao(.T.,.T.)
	RETURN ft.bidata
ENDIF

IF  RECCOUNT("c_pass") = 0
	msg("Password Errada.")
	fecha("c_pass")
	sft.CancelarGravacao(.T.,.T.)
	RETURN ft.bidata
ENDIF
SELECT  ft
REPLACE ft.FINAL  WITH  c_pass.username
fecha("c_pass")
*** fim pede pw


*** Carregar Parametros dos pontos *******************
m_valpontos=0
m_pontosval=0
m_lminpontos=0

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_valpontos'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	m_valpontos = cr_para1.valor
endif

fecha("CR_PARA1")
	
TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_pontosval'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	m_pontosval = cr_para1.valor
endif

fecha("CR_PARA1")

TEXT TO MSEL TEXTMERGE NOSHOW
select valor
from para1 (nolock)
where descricao='user_lminpontos'
ENDTEXT

IF !u_sqlexec(MSEL,'CR_PARA1')
	msg('Erro Encontrado')
	msg(msel)
	return
ENDIF

select cr_para1
if reccount()>0
	m_lminpontos = cr_para1.valor
endif

fecha("CR_PARA1")

** Fim de carrega Parametros Pontos




DO escondegrid_devolucao
lExpirou = .F. && valida se a venda j� expirou
cref  = ''
nFtano = 0
nFno = 0
*9AALLLNNNNNNC
num_ft = ''

wait window '' TIMEOUT 1

MTIPOCL = 0
MTIPOCL = DPERGUNTA(2 ,1 ,"Tipo de cliente" ,"","","N�O Comissionista" ,"Comissionista") 

IF MTIPOCL = 0 
	Return
ENDIF 

IF MTIPOCL = 1
	cref = getnome("Introduza o C�digo de BARRAS do documento de venda",'')
	nLOJAFT =0
	nFtano  = 0
	nFno  = 0
	IF EMPTY(cref)
		temndoc = 0 
		num_ft = getnome('Introduza o Ano + N� doc da VD (exemplo: 2014.000001)','')
		*************************************************************************************************
		*** pede o n� da V.D.	*************************************************************************
		*************************************************************************************************
		DO WHILE temndoc = 0

			temndoc = 0  &&JR 20131226

			IF  SUBSTR(num_ft,5,1) <> '.' OR  VAL(LEFT(num_ft,4))=0   or empty(num_ft) OR VAL(alltrim(SUBSTR(num_ft,6,20))) =0 
				***** pergunta se utilizador ker repetir o numero ou sair
				rr = pergunta('ERRO ! ! !' + CHR(13) + 'N�MERO INV�LIDO'+ CHR(13)+'DESEJA TENTAR DE NOVO')
				IF rr = .F.
					EXIT
				else  
					num_ft = getnome('Introduza o Ano + N� doc da VD (exemplo: 2014.000001)','')
				ENDIF
			ELSE
				temndoc =  1
			ENDIF
		ENDDO


		nLOJAFT = 0
		nndoc = 0
		IF  temndoc = 1
			nFtano = VAL(LEFT(num_ft,4))
			nFno = VAL(SUBSTR(num_ft,6,10))
		ENDIF


		TEXT TO qrytxt NOSHOW TEXTMERGE
			ftano = replace('<<nFtano>>',',','.') and fno = replace('<<nFno>>',',','.')  and ndoc = 61
		ENDTEXT

		*qrytxt  = "(ftano = "+astr(nFtano)+" and fno = "+astr(nFno) + "  and ft.nmdoc  like  '"+txttipodoc+"' )"
	ELSE
		nLOJAFT = VAL(SUBSTR(RTRIM(cref),4,3))
		qrytxt = " ft3.barcode =  '"+cref+"' and  ft.ndoc = 61 "
	ENDIF

	** valida a data da devolu��o
	TEXT TO m.msel NOSHOW TEXTMERGE
			SELECT TOP 1 0 erro, datediff(dy, fdata,getdate()) data, cobrado, ftstamp, fno, no , ftano , ftstamp
			FROM FT (NOLOCK)
			inner join ft3 (nolock) on  ft.ftstamp =  ft3.ft3stamp
			where <<qrytxt>>
	ENDTEXT

	IF NOT u_sqlexec(m.msel,"c_ft")
		msg(m.msel)
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	cftstamp = c_ft.ftstamp
	SELECT c_ft
	IF RECCOUNT("c_ft") = 0
		IF  NOT  pergunta("O documento n�o existe, quer  continuar",2,"Devolu��o de Excep��o")
			DO escondegrid_devolucao
			RETURN ft.bidata
		ELSE
			lExpirou = .T.
		ENDIF
	ENDIF
	SELECT c_ft
	*IF  lExpirou = .F.
	*	msg("Esta venda a dinheiro n�o pode ser devolvida")
	*	fecha("c_ft")
	*	DO escondegrid_devolucao
	*	RETURN ft.bidata
	*ENDIF
	SELECT   c_ft
	IF  c_ft.DATA > user_pos_devolucao_dias AND  lExpirou = .F.
		IF  NOT  pergunta("A venda tem " +astr(c_ft.DATA) + " dias, quer continuar",2,"Devolu��o de Excep��o")
			sft.CancelarGravacao(.T.,.T.)
			RETURN ft.bidata
		ELSE
			lExpirou = .T.
		ENDIF
	ENDIF
	** pede expcep��o
	IF lExpirou = .T.
		DO abrecampos_devolucao
		** Devolu��o Execp��o

		**** pede pw
		cPass = ''
		cPass = getnome('Introduza a sua Password','','','',1,.T.)
		IF EMPTY(cPass)
			msg("Password Inv�lida")
			sft.CancelarGravacao(.T.,.T.)
			RETURN ft.bidata
		ENDIF
		TEXT TO msel NOSHOW TEXTMERGE
		select username from us (nolock) where  pwpos = '<<cpass>>' and  pwpos <> '' AND u_devex = 1
		ENDTEXT

		IF  NOT u_sqlexec(msel,"c_pass")
			msg(msel)
			RETURN
		ENDIF
		IF  RECCOUNT("c_pass") > 1
			msg("Existem v�rios utilizadores para a mesma password.")
			fecha("c_pass")
			sft.CancelarGravacao(.T.,.T.)
			RETURN ft.bidata
		ENDIF

		IF  RECCOUNT("c_pass") = 0
			msg("N�o existem utilizadores para a password indicada.")
			fecha("c_pass")
			sft.CancelarGravacao(.T.,.T.)
			RETURN ft.bidata
		ENDIF


	ENDIF
	*********** fim devolu��o execp��o

	IF  EMPTY(cftstamp)
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	DO escondegrid_devolucao
	******************************
	*** Valida o n� da V.D.
	******************************
	TEXT TO m.msel NOSHOW TEXTMERGE
				--DECLARE @ftstamp char(25)
				--SET @ftstamp = '<<cftstamp>>'
				 select 0 erro, ft.nmdoc
				 , abs ((isnull(liga.qtt,0)-fi.qtt)) as qttrest
				 ,abs ((isnull(liga.qtt,0)-fi.qtt)) as qttresv
				 , (fi.u_qttent-isnull(liga.u_qttent,0)) u_qttent
				 , convert(numeric(15,3),0) qtd_nent
				  , convert(numeric(15,3),0) qtd_ent
				 ,ft.no,(fi.epv)epv,
				fi.ficcusto,fi.fincusto
				,(fi.desconto)desconto,(fi.desc2)desc2
				,(fi.desc3)desc3,(fi.desc4)desc4,(fi.desc5)desc5,(fi.desc6)desc6,
				fi.ecusto,fi.cpoc,fi.usr1,fi.usr2,fi.usr3,fi.usr4,
				fi.usr5,fi.familia,fi.stns,fi.peso,fi.usalote,fi.texteis,fi.noserie,fi.marcada,
				(fi.ofistamp) ofistamp,ft.nome,fi.ref,fi.design,(fi.fistamp) fistamp
				,ft.morada,ft.local,ft.codpost,ft.ncont,ft.pais,ft.estab,ft.moeda,ft.usrdata,ft.usrinis,ft.usrhora ,ft.introfin
				,ft.ftstamp, ft.fno, fi.ivaincl, fi.tabiva, fi.iva
				,OPGSTAMP motivo
				, fi.unidade, ft.fdata, fi.codigo 
				, ft2.u_ncartao, ft.etotal as total, ft3.u_vcomext as vcomext, ft3.u_ctcomext as ctcomext
				, ft.fin
				, fi.armazem as Armazem
				from  ft(nolock)
					inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
					inner join ft3 (nolock) on ft3.ft3stamp = ft.ftstamp
					inner join fi (nolock) on fi.ftstamp = ft.ftstamp
						left join (
									select fi.ofistamp as stamp,sum(fi.qtt) as Qtt ,sum(fi.u_qttent) u_qttent
									from  ft ftt(nolock)
										inner join fi(nolock) on fi.ftstamp = ftt.ftstamp
									where   ftt.snstamp = '<<cftstamp>>'
											and  fi.ref <> ''
									group by fi.ofistamp
								) as liga on liga.stamp = fi.fistamp
				where ft.ftstamp = '<<cftstamp>>'
					and ref <> ''
					and abs(isnull(liga.qtt,0) - fi.qtt) <> 0

	ENDTEXT
	IF  NOT  u_sqlexec( m.msel,"cr_devol")
		msg(m.msel)
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF


	SELECT cr_devol
	IF RECCOUNT("cr_devol") = 0
		mss = 'Erro!!!' + CHR(13) + 'Venda Dinheiro Inv�lida' + CHR(13) + 'Ou Venda Dinheiro totalmente devolvida'
		msg(mss)
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ELSE
		IF  cr_devol.ERRO <> 0
			mss = 'Erro!!!' + CHR(13) + 'N�o foi possivel efectuar a liga��o � sede'
			msg(mss)
			sft.CancelarGravacao(.T.,.T.)
			RETURN ft.bidata
		ENDIF
	ENDIF

	*************************************************************************************************
	** copia as linhas da V.D. para a Devolu��o
	*************************************************************************************************
	LOCAL var_ftnome,var_ftno,var_ftmorada,var_ftlocal,var_ftcodpost,var_ftncont,var_ftpais,var_ftestab,var_ftmoeda
	m.escolheu = .F.
	****** fim cabe�alho
	*****************************
	**   Browlist
	*****************************
	SELECT cr_devol

	***** Declarar do Array a utilizar no BROWLIST
	i = 11
	DECLARE list_tit(i),list_cam(i),list_pic(i),list_tam(i),list_ronly(i), list_pic(i), list_rot(i), list_valid(i)
	** Agora preenchemos os Arrays
	i = 1
	list_tit(i) = "Sel?"
	list_pic(i) = "BOTAO IMG:c:\phc\source\cs91\bmps\actd.bmp"
	list_cam(i) = "cr_devol.marcada"
	list_rot(i) = "do user_sel"

	i = i +1
	list_tit(i)="Copiar ?"
	list_cam(i)="cr_devol.marcada"
	list_pic(i)="LOGIC"
	list_tam(i)=8*8
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Qtd. Fact."
	list_cam(i)="cr_devol.qttrest"
	list_pic(i)="9,999.999"
	list_tam(i)=8*10
	list_ronly(i)=.t.

	i = i +1
	list_tit(i)="Qtd. Entregue"
	list_cam(i)="cr_devol.u_qttent"
	list_pic(i)="9,999.999"
	list_tam(i)=8*10
	list_ronly(i)=.t.


	i = i +1
	list_tit(i)="Dev. Qtd n�o Entregue"
	list_cam(i)="cr_devol.qtd_nent"
	list_pic(i)="9,999.999"
	list_tam(i)=8*10
	list_ronly(i)=.F.
	list_valid(i)="Valid_n_entregue(123)"

	i = i +1
	list_tit(i)="Dev. Qtd Entregue"
	list_cam(i)="cr_devol.qtd_ent"
	list_pic(i)="9,999.999"
	list_tam(i)=8*10
	list_ronly(i)=.F.
	list_valid(i)="Valid_entregue(123)"


	i = i +1
	list_tit(i)="Referencia"
	list_cam(i)="cr_devol.ref"
	list_pic(i)=""
	list_tam(i)=8*8
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Designa��o"
	list_cam(i)="cr_devol.design"
	list_pic(i)=""
	list_tam(i)=8*30
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Pre�o"
	list_cam(i)="cr_devol.epv"
	list_pic(i)="999,999,999.99"
	list_tam(i)=8*10
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Desc. 1"
	list_cam(i)="cr_devol.desconto"
	list_pic(i)="999.99"
	list_tam(i)=8*10
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="motivo"
	list_cam(i)="cr_devol.motivo"
	list_pic(i)=""
	list_tam(i)=8*10
	list_ronly(i)=.T.


	*** Fim do Array
	****** Lista os campos
	** =CURSORSETPROP('Buffering',5,'cr_devol')

	browlist('Linhas Documento Original','cr_devol','MeuCursordev',.T.,.F.,.F.,.T.,.F.,'',.t.)

	**browlist("Artigos da VD","cr_devol","MEUCURSOR",.T.,.F.,.F.,.T.,.F.)
	******************************** Fim Browlist **************************
	IF  m.escolheu = .F.
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	SELECT cr_devol
	LOCATE FOR cr_devol.marcada = .T.  AND cr_devol.qtd_ent+cr_devol.qtd_nent <> 0
	IF NOT FOUND()
		RETURN ft.bidata
	ENDIF
	*****************************
	****** Preenche os dados do cabe�alo igual a V.D.
	*****************************
	SELECT cr_devol
	GOTO TOP
	SCAN
		var_ftnome 		= cr_devol.NOME
		var_ftno		= cr_devol.no
		var_ftmorada	= cr_devol.morada
		var_ftlocal		= cr_devol.LOCAL
		var_ftcodpost	= cr_devol.codpost
		var_ftncont		= cr_devol.ncont
		var_ftpais		= cr_devol.pais
		var_ftestab		= cr_devol.estab
		var_ftmoeda		= cr_devol.moeda
		var_ftstamp     = cr_devol.ftstamp
		var_fno		    = astr(cr_devol.fno)
		var_ncartao		= cr_devol.u_ncartao
		var_fin			= cr_devol.fin
		EXIT
	ENDSCAN
	SELECT ft
	REPLACE ft.NOME 	WITH var_ftnome
	REPLACE ft.no 		WITH var_ftno
	REPLACE ft.morada 	WITH var_ftmorada
	REPLACE ft.LOCAL 	WITH var_ftlocal
	REPLACE ft.codpost 	WITH var_ftcodpost
	REPLACE ft.ncont 	WITH var_ftncont
	REPLACE ft.pais 	WITH var_ftpais
	REPLACE ft.estab 	WITH var_ftestab
	REPLACE ft.moeda	WITH var_ftmoeda
	REPLACE ft.snstamp	WITH var_ftstamp IN ft
	REPLACE ft.arno		WITH VAL(var_fno) IN ft
	replace ft.fin 		WITH var_fin
	*****************************
	** Escrever uma linha com o numero da V.D.
	*****************************
	*MESS = 'Venda a dinheiro n � ' + var_fno
	MESS = RTRIM(cr_devol.nmdoc) + ' n � ' + var_fno + ' de ' + DTOC(cr_devol.fdata)

	select fi
	delete 

	SELECT fi
	DO ftine2in
	SELECT fi
	REPLACE fi.DESIGN 		WITH MESS
	SELECT cr_devol
	SCAN FOR cr_devol.marcada = .T.  AND cr_devol.qtd_ent+cr_devol.qtd_nent<> 0
		*****************************
		**  Copia as linhas selecionadas na Browlist
		*****************************
		SELECT fi
		DO ftine2in
		SELECT fi
		REPLACE fi.ref 		WITH cr_devol.ref
		REPLACE fi.DESIGN 		WITH cr_devol.DESIGN
		IF (cr_devol.qtd_ent+cr_devol.qtd_nent) <= cr_devol.qttresv
			REPLACE fi.qtt		WITH (cr_devol.qtd_ent+cr_devol.qtd_nent)
		ELSE && <valida  se a quantidade escolhida � maior que a quantidade pendente  
			REPLACE fi.qtt		WITH cr_devol.qttresv
			MESS = 'ATEN��O ! ! !' + CHR(13) + 'A qtd da ref '+ALLTRIM(astr(cr_devol.ref))+' � superior a disponivel'+CHR(13)
			MESS = MESS + 'Vai ser colocada na DEVOLU��O a qtd disponivel'
			msg(MESS)
		ENDIF
		REPLACE fi.ftstamp			WITH ft.ftstamp
		REPLACE fi.epv			WITH cr_devol.epv
		REPLACE fi.u_qttent			WITH cr_devol.qtd_ent
		*replace fi.ficcusto		with cr_devol.ficcusto
		REPLACE fi.fincusto		WITH cr_devol.fincusto
		REPLACE fi.desconto		WITH cr_devol.desconto
		REPLACE fi.desc2		WITH cr_devol.desc2
		REPLACE fi.desc3		WITH cr_devol.desc3
		REPLACE fi.desc4		WITH cr_devol.desc4
		REPLACE fi.desc5		WITH cr_devol.desc5
		REPLACE fi.desc6		WITH cr_devol.desc6
		REPLACE fi.ecusto		WITH cr_devol.ecusto
		REPLACE fi.ivaincl		WITH cr_devol.ivaincl
		*REPLACE fi.nsujpp		WITH cr_devol.nsujpp
		*REPLACE fi.ecomissao	WITH cr_devol.ecomissao
		REPLACE fi.cpoc			WITH cr_devol.cpoc
		REPLACE fi.usr1			WITH cr_devol.usr1
		REPLACE fi.usr2			WITH cr_devol.usr2
		REPLACE fi.usr3			WITH cr_devol.usr3
		REPLACE fi.usr4			WITH cr_devol.usr4
		REPLACE fi.usr5			WITH cr_devol.usr5
		REPLACE fi.familia		WITH cr_devol.familia
		REPLACE fi.stns			WITH cr_devol.stns
		REPLACE fi.peso			WITH cr_devol.peso
		REPLACE fi.usalote		WITH cr_devol.usalote
		REPLACE fi.texteis		WITH cr_devol.texteis
		REPLACE fi.noserie		WITH cr_devol.noserie
		REPLACE fi.ofistamp		WITH cr_devol.fistamp


		**REPLACE fi.iva			WITH 0
		**REPLACE fi.tabiva		WITH 1


		REPLACE fi.iva		WITH cr_devol.iva
		REPLACE fi.tabiva		WITH cr_devol.tabiva


		REPLACE fi.OPGSTAMP		WITH cr_devol.motivo
		REPLACE fi.unidade		WITH cr_devol.unidade
		REPLACE fi.codigo		WITH cr_devol.codigo
		REPLACE Fi.ofistamp 	with cr_devol.fistamp
		replace fi.oftstamp		with cr_devol.ftstamp
		replace fi.armazem		with cr_devol.armazem
		** altera tabela de iva
		IF  fi.iva = 20
			REPLACE tabiva WITH  6
		ENDIF
		IF  fi.iva = 5
			REPLACE tabiva WITH  5
		ENDIF
		IF  fi.iva = 12
			REPLACE tabiva WITH  7
		ENDIF
		** fim altera tabela de iva
		DO u_ftiliq
		SELECT cr_devol
	ENDSCAN

	SELECT cr_devol
	GOTO TOP
	SELECT  cr_devol
	LOCATE FOR NOT EMPTY(cr_devol.motivo)
	IF FOUND()
		SELECT  ft3
		REPLACE ft3.MOTRETIF  WITH  cr_devol.motivo
	ENDIF

	DO fttots WITH .T.
	SFT.Pageframe1.Page1.Basei.VALUE = ft.ettiliq
	SFT.Pageframe1.Page1.Ttiva.VALUE = ft.ettiva
	SFT.Pageframe1.Page1.TOTAL.VALUE = ft.etotal
	DO fttots WITH .T.

ENDIF


*** COMISSIONISTA
IF MTIPOCL = 2

	M_CARTAOCL = GETNOME('Passe Cart�o de Cliente!','','','',1,.T.)


	If Empty(M_CARTAOCL)
		msg('Ac��o cancelada pelo Utilizador')
		fecha('CR_CLTMP')
		Return
	Endif

	If M_CARTAOCL = '0'
		M_CARTAOCL = 'CLVD'
	Endif

	TEXT TO MSEL TEXTMERGE NOSHOW
		SELECT CL.*, CL2.u_CTCLSUP SUPERVISAO, CL2.u_CTCLUPIN USAPIN, CL2.u_CTCLPIN PIN
		FROM CL (NOLOCK)
		INNER JOIN CL2 (NOLOCK) on cl2.cl2stamp = cl.clstamp
		WHERE CL2.u_NCARTAO = '<<ALLTRIM(M_CARTAOCL)>>'
	ENDTEXT

	If !U_sqlexec(MSEL,'CR_CLTMP')
		msg('ERRO ENCONTRADO')
		msg(MSEL)
		Return
	Endif

	Select CR_CLTMP
	If Reccount()=0
		msg('Cart�o de Cliente Invalido')
		Return
	ENDIF

	**** SUPERVISAO Autoriza Cart�o
	Select CR_CLTMP
	IF CR_CLTMP.SUPERVISAO = .t.
			
		Local m_USER, m_FUNCAO, m_CAMPO, m_TABELA, m_STAMP, m_OBSERV

		********************VARIAVEIS*******************************************
		M_TABELA = 'FT'
		M_CAMPO = 'FTSTAMP'

		SELECT &M_TABELA
		M_STAMP = &M_TABELA..&M_CAMPO

		M_FUNCAO = 'DESCONTAR COMISSAO'

		*************************************************************************


		m_PASS = GETNOME('Introduza PASSWORD para efectuar '+astr(M_FUNCAO)+'! ','','','',1,.t.)

		IF EMPTY(m_PASS)
			msg('Ac��o cancelada pelo Utilizador')
			fecha('CR_ACESSO')
			RETURN
		ENDIF


		TEXT TO MSQL TEXTMERGE NOSHOW
			SELECT U_ACESSOS.PASS, US.USERNO, US.USERNAME, US.USRINIS, US.USSTAMP
			FROM U_ACESSOS (NOLOCK)
			INNER JOIN US (NOLOCK) ON US.USSTAMP=U_ACESSOS.USSTAMP
			WHERE US.PWPOS = '<<m_PASS>>'
				and U_ACESSOS.FUNCAO = '<<M_FUNCAO>>'
		ENDTEXT

		If !U_SQLEXEC(MSQL,'CR_ACESSO')
			msg('Erro Encontrado')
			msg(MSQL)
			Return
		ENDIF

		Select CR_ACESSO
		If Reccount() = 0
			msg('O utilizador digitado n�o tem permissao para terminar a ac��o '+astr(M_FUNCAO)+'! Por favor contacte o supervisor!')
			fecha('CR_ACESSO')	
			FECHA("CR_CLTMP")
			return
		ENDIF

		Select CR_ACESSO

		M_OBSERV = 'CART�O DE CLIENTE AUTORIZADO - opera��o permitida pelo utilizador: '+ALLTRIM(CR_ACESSO.USERNAME) 

		
		TEXT TO MINS TEXTMERGE NOSHOW
		INSERT INTO u_HISTALT
			   ([u_histaltstamp]
	           ,[userno]
	           ,[username]
	           ,[usstamp]
	           ,[otabela]
	           ,[oregstamp]
	           ,[observ]
	           ,[ousrinis]
	           ,[ousrdata]
	           ,[ousrhora]
	           ,[usrinis]
	           ,[usrdata]
	           ,[usrhora]
	           ,[marcada])
	   	SELECT '<<u_STAMP()>>',<<CR_ACESSO.USERNO>>,'<<CR_ACESSO.USERNAME>>','<<CR_ACESSO.USSTAMP>>','<<M_TABELA>>','<<M_STAMP>>','<<M_OBSERV>>',
	     		'<<CR_ACESSO.USRINIS>>',getdate(),convert(char(10),getdate(),108),
	     		'<<CR_ACESSO.USRINIS>>',getdate(),convert(char(10),getdate(),108),0
		ENDTEXT
		IF !U_SQLEXEC(MINS)
			msg('ERRO ENCONTRADO')
			msg(MINS)
			FECHA("CR_CLTMP")
			RETURN
		ENDIF

		fecha('CR_ACESSO')

	ENDIF
	******************************************


	**** SUPERVISAO Autoriza Cart�o
	Select CR_CLTMP
	IF CR_CLTMP.USAPIN = .t.

		m_PASS = GETNOME('Introduza PIN do cart�o ','','','',1,.t.)

		IF EMPTY(m_PASS)
			msg('Ac��o cancelada pelo Utilizador')
			fecha('CR_ACESSO')
			RETURN
		ENDIF
		
		IF ALLTRIM(UPPER(m_PASS)) <> ALLTRIM(UPPER(STR(CR_CLTMP.PIN,6)))
			MSG('PIN INCORRECTO - Venda N�O autorizada para o cliente '+ASTR(CR_CLTMP.NOME)+'')
			return
		ENDIF
		
		MSG('Venda Autorizada para o cliente '+ASTR(CR_CLTMP.NOME)+'')
			
	ENDIF	

	Select CL
	v_clstamp = CR_CLTMP.CLSTAMP
	U_requery('CL')

	Select FT
	Replace FT.no With CL.no
	Replace FT.estab With CL.estab

	FTCLACT("MAIN","FT","FI")

	LOCAL var_ftnome,var_ftno,var_ftmorada,var_ftlocal,var_ftcodpost,var_ftncont,var_ftpais,var_ftestab,var_ftmoeda

	SELECT FT
	var_ftnome 		= ft.nome
	var_ftno		= ft.no
	var_ftmorada	= ft.morada
	var_ftlocal		= ft.LOCAL
	var_ftcodpost	= ft.codpost
	var_ftncont		= ft.ncont
	var_ftpais		= ft.pais
	var_ftestab		= ft.estab
	var_ftmoeda		= ft.moeda
	var_ftstamp     = ft.ftstamp
	var_fno		    = ft.fno
	var_ncartao		= M_CARTAOCL

	TEXT TO m.msel NOSHOW TEXTMERGE
		select 0 erro
			 , 'NSDESCVALE'
			 , CC.CMDESC
			 , cc.cm
			 , 1 as qtt
			 , CC.no
			 , (CC.CRED-CC.CREDF) epv
 			 , (CC.CRED-CC.CREDF) as total
 			 , CC.marcada
		from  cc(nolock)
	    where cc.no = <<ft.no>> and cc.estab = <<ft.estab>>
			and cc.cm = 122
			and (CC.CRED-CC.CREDF) > 0
	ENDTEXT
	IF  NOT  u_sqlexec( m.msel,"cr_devol")
		msg(m.msel)
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	
	IF RECCOUNT("cr_devol") = 0
		msg("N�o foram encontrada comiss�es disponiveis para devolu��o")
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	
	m.escolheu = .F.
	****** fim cabe�alho
	*****************************
	**   Browlist
	*****************************
	SELECT cr_devol

	***** Declarar do Array a utilizar no BROWLIST
	i = 5
	DECLARE list_tit(i),list_cam(i),list_pic(i),list_tam(i),list_ronly(i), list_pic(i), list_rot(i), list_valid(i)
	** Agora preenchemos os Arrays
	i = 1
	list_tit(i) = "Sel?"
	list_pic(i) = "BOTAO IMG:c:\phc\source\cs91\bmps\actd.bmp"
	list_cam(i) = "cr_devol.marcada"
	list_rot(i) = "do user_sel2"

	i = i +1
	list_tit(i)="Devolver ?"
	list_cam(i)="cr_devol.marcada"
	list_pic(i)="LOGIC"
	list_tam(i)=8*8
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Designa��o"
	list_cam(i)="cr_devol.cmdesc"
	list_pic(i)=""
	list_tam(i)=8*30
	list_ronly(i)=.T.

	i = i +1
	list_tit(i)="Qtd."
	list_cam(i)="cr_devol.qtt"
	list_pic(i)="9,999.999"
	list_tam(i)=8*10
	list_ronly(i)=.t.

	i = i +1
	list_tit(i)="Valor"
	list_cam(i)="cr_devol.epv"
	list_pic(i)="999,999,999.99"
	list_tam(i)=8*10
	list_ronly(i)=.T.


	*** Fim do Array
	****** Lista os campos
	** =CURSORSETPROP('Buffering',5,'cr_devol')

	browlist('Comiss�es Disponiveis','cr_devol','MeuCursordev',.T.,.F.,.F.,.T.,.F.,'',.t.)

	******************************** Fim Browlist **************************
	IF  m.escolheu = .F.
		sft.CancelarGravacao(.T.,.T.)
		RETURN ft.bidata
	ENDIF
	SELECT cr_devol
	LOCATE FOR cr_devol.marcada = .T.
	IF NOT FOUND()
		RETURN ft.bidata
	ENDIF


	select fi
	delete 

	SELECT fi
	DO ftine2in
	SELECT fi
	REPLACE fi.DESIGN 	WITH 'Devolu��o de Comiss�es'
	SELECT cr_devol
	SCAN FOR cr_devol.marcada = .T.  AND cr_devol.qtt<>0
		*****************************
		**  Copia as linhas selecionadas na Browlist
		*****************************
		SELECT fi
		DO ftine2in
		SELECT fi
		REPLACE fi.ref 		WITH 'NSDESCVALE'
		DO FTACTREF
		REPLACE fi.DESIGN 		WITH cr_devol.CMDESC
		REPLACE fi.qtt		WITH cr_devol.qtt
		REPLACE fi.ftstamp			WITH ft.ftstamp
		REPLACE fi.epv			WITH cr_devol.epv

		DO u_ftiliq
		SELECT cr_devol
	ENDSCAN

	DO fttots WITH .T.
	SFT.Pageframe1.Page1.Basei.VALUE = ft.ettiliq
	SFT.Pageframe1.Page1.Ttiva.VALUE = ft.ettiva
	SFT.Pageframe1.Page1.TOTAL.VALUE = ft.etotal
	DO fttots WITH .T.	
	
ENDIF





**** PONTOS  *****************

SELECT FT

TEXT TO MSQL TEXTMERGE NOSHOW
SELECT CLIVD 
FROM CL (NOLOCK)
WHERE CL.NO=<<FT.NO>>
ENDTEXT
IF !U_SQLEXEC(MSQL,'CR_CLVDTMP')
	MSG('ERRO ENCONTRADO')
	MSG(MSQL)
	RETURN
ENDIF
SELECT CR_CLVDTMP

IF CR_CLVDTMP.CLIVD = .f.

select ft2
replace ft2.u_ncartao with var_ncartao
replace ft2.u_pontos WITH round((ft.etotal/val(m_valpontos)),0)

TEXT TO MSQL TEXTMERGE NOSHOW
SELECT u_PONTOS
FROM CL (NOLOCK)
WHERE CL.NO = <<ASTR(ft.NO)>>
ENDTEXT
IF !U_SQLEXEC(MSQL,'CR_PONTOS')
	MSG('Erro Encontrado')
	MSG(MSQL)
	RETURN
ENDIF

select CR_PONTOS
MPONTOS	= CR_PONTOS.U_PONTOS
fecha("CR_PONTOS")

select ft2

MPTS = MPONTOS + ft2.u_pontos	

REPLACE ft2.u_PTSACUM WITH (MPTS)

** REPLACE ft2.u_PTSACUM WITH ((MPTS/VAL(m_LMINPONTOS))-INT((MPTS/VAL(m_LMINPONTOS))))*VAL(m_LMINPONTOS)

IF MPONTOS > 0
	REPLACE ft2.u_VALACUM WITH INT((MPONTOS/VAL(m_LMINPONTOS)))*VAL(m_pontosval)		  
ELSE
	REPLACE ft2.u_VALACUM WITH 0		  	
ENDIF

ENDIF
FECHA("CR_CLVDTMP")

**** Fim Pontos *************


**JR deixou de fazer os decontos do rebate 2014-06-23
**Do user_valida_rebate


fecha("cr_devol")
fecha("c_ft")
SELECT fi
GOTO TOP
RETURN ft.bidata




****************************************************
**** 	FUNC�ES
*****************************************************


**************************************************************************************************************************************
********* Enconde os campos a excp��o da designa��o
**************************************************************************************************************************************
PROCEDURE  escondegrid_devolucao
*SFT.Pageframe1.Page2.ENABLED 				= .F.
*SFT.Pageframe1.Page4.ENABLED 				= .F.
SFT.Pageframe1.Page1.NOME.ENABLED 			= .F.
SFT.Pageframe1.Page1.moeda.ENABLED 			= .F.
SFT.Pageframe1.Page1.Finvm.ENABLED 			= .F.
SFT.Pageframe1.Page1.Finv.ENABLED 			= .F.
SFT.Pageframe1.Page1.Fdata .ENABLED 		= .F.
SFT.Pageframe1.Page1.Cont1.Bocopy.ENABLED 	= .F.
SFT.Pageframe1.Page1.Cont1.Ftcopy.ENABLED 	= .F.
SFT.Pageframe1.Page1.Cont1.MENU.ENABLED 	= .F.
SFT.Pageframe1.Page1.Zona.ENABLED 			= .F.
SFT.Pageframe1.Page1.Tpdesc1.ENABLED 		= .F.
SFT.Pageframe1.Page1.Pdata.ENABLED 			= .F.
SFT.Pageframe1.Page1.Encomenda.ENABLED 		= .F.
SFT.Pageframe1.Page1.Vendedor1 .ENABLED 	= .F.
SFT.Pageframe1.Page1.Cont1.Autototal.ENABLED = .F.
SFT.Pageframe1.Page1.Fin.ENABLED			= .F.
SFT.Pageframe1.Page1.area.ENABLED = .F.
************************ Grid
LOCAL i,texto
i = 0
texto = ''
FOR EACH COLUMNS IN SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS
	i = i + 1
	SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS(i).ENABLED = .F.
	DO CASE
	CASE SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS(i).header1.CAPTION =  'Designa��o'
		SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS(i).ENABLED = .T.
	OTHERWISE
	ENDCASE
NEXT
************************* Fim Grid
ENDPROC
**************************************************************************************************************************************
********* FIM Enconde os campos a excp��o da designa��o
**************************************************************************************************************************************
**************************************************************************************************************************************
********* Procedure que Reabre os campos
**************************************************************************************************************************************
PROCEDURE abrecampos_devolucao
SFT.Pageframe1.Page2.ENABLED 				= .T.
*SFT.Pageframe1.Page4.ENABLED 				= .T.
SFT.Pageframe1.Page1.NOME.ENABLED 			= .T.
SFT.Pageframe1.Page1.moeda.ENABLED 			= .T.
SFT.Pageframe1.Page1.Finvm.ENABLED 			= .T.
SFT.Pageframe1.Page1.Finv.ENABLED 			= .T.
SFT.Pageframe1.Page1.Fdata .ENABLED 		= .T.
SFT.Pageframe1.Page1.Cont1.Bocopy.ENABLED 	= .T.
SFT.Pageframe1.Page1.Cont1.Ftcopy.ENABLED 	= .T.
SFT.Pageframe1.Page1.Cont1.MENU.ENABLED 	= .T.
SFT.Pageframe1.Page1.Zona.ENABLED 			= .T.
SFT.Pageframe1.Page1.Tpdesc1.ENABLED 		= .T.
SFT.Pageframe1.Page1.Pdata.ENABLED 			= .T.
SFT.Pageframe1.Page1.Encomenda.ENABLED 		= .T.
SFT.Pageframe1.Page1.Vendedor1 .ENABLED 	= .T.
SFT.Pageframe1.Page1.Cont1.Autototal.ENABLED = .T.
SFT.Pageframe1.Page1.Fin.ENABLED			= .T.
************************ Grid
LOCAL i,texto
i = 0
texto = ''
FOR EACH COLUMNS IN SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS
	i = i + 1
	SFT.Pageframe1.Page1.Cont1.Grid1.COLUMNS(i).ENABLED = .T.
NEXT
************************ Fim Grig
ENDPROC


PROCEDURE user_sel

cmotivo = ''
SELECT cr_devol
crefori = cr_devol.ref

IF cr_devol.marcada = .T.
	REPLACE cr_devol.marcada WITH .F.
	REPLACE cr_devol.motivo WITH ''
ELSE
	SELECT  cr_devol
	nreco = RECNO()
	CREATE CURSOR c_tempdev2 (ref c(25), motivo c(50))
	cmotivo = ''
	SELECT  cr_devol
	SCAN  FOR NOT  EMPTY(cr_devol.motivo)
		SELECT c_tempdev2
		LOCATE FOR ALLTRIM(c_tempdev2.ref) == ALLTRIM(cr_devol.ref)
		IF  NOT FOUND()
			SELECT c_tempdev2
			APPEND  BLANK
			REPLACE   c_tempdev2.ref WITH cr_devol.ref
			REPLACE   c_tempdev2.motivo WITH cr_devol.motivo
		ENDIF
	ENDSCAN

	SELECT cr_devol
	GOTO nreco

	SELECT  c_tempdev2
	LOCATE FOR ALLTRIM(c_tempdev2.ref) == ALLTRIM(crefori)
	IF FOUND()
		cmotivo = c_tempdev2.motivo
	ELSE
		cmotivo  = ''
	ENDIF
	fecha("c_tmpdev2")
	** escolhe o motivo da nota de credito

	SELECT cr_devol
	GOTO nreco

	DO WHILE  EMPTY(cmotivo)
		******* Definir dados do array com lojas para transferir
		TEXT TO sqle1 NOSHOW TEXTMERGE
					select campo
					from dytable (nolock)
					where  entityname  = 'NC_MOTIVOS' order by dytable.campo
		ENDTEXT
		IF NOT u_sqlexec(sqle1, "c_temparr")
			msg(sqle1)
			RETURN
		ENDIF
		IF RECCOUNT("c_temparr")>0
			DECLARE var_array(RECCOUNT("c_temparr"))
			SELECT c_temparr
			var_contador = 1
			SCAN
				var_array(var_contador) = c_temparr.campo
				var_contador = var_contador + 1
			ENDSCAN
			fecha("c_temparr")
		ELSE
			mensagem("N�o h� registos!!!","DIRECTA")
		ENDIF
		******* Fim de Definir dados do array
		cmotivo = getnome('Introduza o motivo do c�dito',"","","",1,.F.,"var_array")
		SELECT  cr_devol
		MREG = RECNO()
		go top
		scan
			REPLACE cr_devol.motivo WITH  cmotivo
		ENDSCAN
		SELECT CR_DEVOL
		GOTO MREG IN "CR_DEVOL"
	ENDDO
	SELECT  cr_devol
	REPLACE cr_devol.motivo WITH  cmotivo
	cmotivo = ''

	***	fim escolhe o motivo da nota de credito	
	REPLACE cr_devol.marcada WITH .T.

ENDIF
ENDPROC


PROCEDURE user_sel2
	
	IF cr_devol.marcada = .T.
		REPLACE cr_devol.marcada WITH .F.
	ELSE
		REPLACE cr_devol.marcada WITH .T.
	ENDIF

ENDPROC



PROCEDURE user_valida_rebate
ref_reb = 'REBATE'
IF NOT  (ft.ndoc = 62 AND ft.arno > 0)
	RETURN ft.bidata
ENDIF
LOCAL nVenda, nValorUnit, nBonusTotal, nSaldoCrt
nVenda = ft.arno
SET MESSAGE TO "Estou a verificar O documento de venda. Aguarde um momento..."
TEXT TO msel NOSHOW TEXTMERGE
	select ftstamp, u_valorcrt,no, ft2.epaga6, etotal from ft (nolock)
	inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
	where ft.ftstamp = '<<c_ft.ftstamp>>'
	-- fno = '<<astr(nVenda)>>'
	--and ft.ndoc between 7 and 10
ENDTEXT
u_sqlexec(msel,"_ft")
SET MESSAGE TO
SELECT _ft
IF RECCOUNT() =0
	MESSAGEBOX("O documento de venda introduzido, n�o existe!",16)
	fecha("_ft")
	RETURN ft.bidata
ENDIF
IF _ft.no <> ft.no
	MESSAGEBOX("O documento de venda introduzido, n�o pertence a este Cliente!",16)
	fecha("_ft")
	RETURN ft.bidata
ENDIF
msel="select no, u_clcli from cl (nolock) where no = '"+astr(ft.no)+"'"
u_sqlexec(msel,"_cl")
SELECT _cl
IF !u_clcli
	fecha("_ft")
	fecha("_cl")
	RETURN ft.bidata
ENDIF
IF _ft.epaga6 = 0
	fecha("_ft")
	RETURN ft.bidata
ENDIF
ndevol = 0 && total de devolu�es
SELECT fi
SCAN FOR UPPER(fi.ref) <>  ref_reb
	ndevol = ndevol  + fi.etiliquido
ENDSCAN
nrebate = ((ndevol /_ft.etotal) * _ft.epaga6)
SELECT  ft
REPLACE ft.u_valorcrt WITH nrebate
IF nrebate <> 0
	SELECT fi
	GOTO BOTTOM
	DO ftine2in
	SELECT fi
	REPLACE fi.ref WITH ref_reb
	DO Ftactref
	SELECT fi
	REPLACE  fi.epv WITH nrebate
	DO u_ftiliq
	DO fttots WITH .T.
ENDIF
fecha("_ft")
fecha("_crt")
ENDPROC


FUNCTION valid_n_entregue()
	LPARAMETERS oHandleGrid,nColumnNumber,oColunmValue, xxx
	if  cr_devol.qtd_nent > iif(cr_devol.qttresv-cr_devol.u_qttent >=0, cr_devol.qttresv-cr_devol.u_qttent, 0) 
		msg("A quantidade a devolver n�o entregue n�o pode ser superior � quantidade n�o entregue.")
		RETURN .f. 
	endif  
	
	IF  (cr_devol.qtd_ent+cr_devol.qtd_nent) >  cr_devol.qttresv
		msg("A quantidade a devolver entregue e n�o entregue n�o pode ser superior � qtd faturada.")
		RETURN .f. 
	endif  
	RETURN .t.  
ENDFUNC


FUNCTION valid_entregue()
	LPARAMETERS oHandleGrid,nColumnNumber,oColunmValue, xxx
	if  cr_devol.qtd_ent > cr_devol.u_qttent
		msg("A quantidade a devolver entregue n�o pode ser superior � quantidade entregue.")
		RETURN .f. 
	endif  
	IF  (cr_devol.qtd_ent+cr_devol.qtd_nent) >  cr_devol.qttresv
		msg("A quantidade a devolver entregue e n�o entregue n�o pode ser superior � qtd faturada.")
		RETURN .f. 
	endif  
	RETURN .t.  
ENDFUNC


