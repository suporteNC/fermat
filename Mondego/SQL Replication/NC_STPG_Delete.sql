USE [Mondego_testes]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbost]    Script Date: 24/01/2023 11:23:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STPG_Delete]
		@c1 char(25),
		@c2 char(18),
		@c3 char(60),
		@c4 varchar(18),
		@c5 char(20),
		@c6 varchar(55),
		@c7 varchar(4),
		@c8 numeric(10,0),
		@c9 numeric(3,0),
		@c10 varchar(68),
		@c11 char(40),
		@c12 varchar(4),
		@c13 numeric(15,7),
		@c14 varchar(120),
		@c15 numeric(6,0),
		@c16 varchar(15),
		@c17 varchar(15),
		@c18 numeric(6,2),
		@c19 numeric(6,2),
		@c20 numeric(3,0),
		@c21 numeric(5,2),
		@c22 bit,
		@c23 bit,
		@c24 datetime,
		@c25 varchar(60),
		@c26 bit,
		@c27 varchar(60),
		@c28 varchar(100),
		@c29 numeric(1,0),
		@c30 varchar(20),
		@c31 char(20),
		@c32 char(60),
		@c33 char(20),
		@c34 char(60),
		@c35 char(20),
		@c36 char(60),
		@c37 char(20),
		@c38 char(60),
		@c39 char(20),
		@c40 char(60),
		@c41 bit,
		@c42 varchar(1),
		@c43 bit,
		@c44 varchar(30),
		@c45 datetime,
		@c46 varchar(8),
		@c47 varchar(30),
		@c48 datetime,
		@c49 varchar(8),
		@c50 bit,
		@c51 bit,
		@c52 char(18),
		@c53 char(60),
		@c54 numeric(6,2),
		@c55 numeric(6,2),
		@c56 char(25),
		@c57 varchar(4),
		@c58 varchar(3),
		@c59 bit,
		@c60 varchar(25),
		@c61 bit,
		@c62 bit,
		@c63 bit,
		@c64 bit,
		@c65 bit,
		@c66 varchar(10),
		@c67 varchar(30),
		@c68 varchar(10),
		@c69 varchar(30),
		@c70 varchar(60),
		@c71 bit,
		@c72 varchar(20),
		@c73 varchar(20),
		@c74 varchar(20),
		@c75 varchar(20),
		@c76 varchar(20),
		@c77 varchar(60),
		@c78 varchar(20),
		@c79 varchar(20),
		@c80 varchar(15),
		@c81 varchar(55),
		@c82 numeric(10,0),
		@c83 varchar(20),
		@c84 datetime,
		@c85 varchar(20),
		@c86 datetime,
		@c87 varchar(20),
		@c88 varchar(20),
		@c89 datetime,
		@c90 datetime,
		@c91 bit,
		@c92 bit,
		@c93 bit,
		@c94 numeric(10,2),
		@c95 numeric(10,2),
		@c96 bit,
		@c97 datetime,
		@c98 datetime,
		@c99 numeric(10,2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[st] 
	where [ref] = @c2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ref] = ' + convert(nvarchar(100),@c2,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[st]', @param2=@primarykey_text, @param3=13234
		End
end  

 
delete from st where ststamp=@c1