USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbostfami]    Script Date: 24/01/2023 17:57:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_STFAMI_Delete]
		@c1 char(25),
		@c2 varchar(18),
		@c3 varchar(60),
		@c4 varchar(55),
		@c5 varchar(20),
		@c6 varchar(20),
		@c7 numeric(1,0),
		@c8 numeric(10,0),
		@c9 numeric(3,0),
		@c10 numeric(6,0),
		@c11 varchar(4),
		@c12 varchar(4),
		@c13 numeric(15,7),
		@c14 varchar(15),
		@c15 varchar(15),
		@c16 numeric(5,2),
		@c17 numeric(5,2),
		@c18 numeric(3,0),
		@c19 numeric(16,3),
		@c20 numeric(16,3),
		@c21 numeric(16,3),
		@c22 numeric(16,3),
		@c23 numeric(16,3),
		@c24 bit,
		@c25 bit,
		@c26 bit,
		@c27 varchar(35),
		@c28 varchar(20),
		@c29 varchar(120),
		@c30 varchar(20),
		@c31 bit,
		@c32 bit,
		@c33 bit,
		@c34 bit,
		@c35 bit,
		@c36 bit,
		@c37 numeric(18,5),
		@c38 numeric(19,6),
		@c39 numeric(6,2),
		@c40 varchar(15),
		@c41 varchar(4),
		@c42 bit,
		@c43 varchar(2),
		@c44 varchar(50),
		@c45 varchar(18),
		@c46 varchar(60),
		@c47 numeric(1,0),
		@c48 varchar(20),
		@c49 bit,
		@c50 varchar(30),
		@c51 varchar(80),
		@c52 varchar(30),
		@c53 datetime,
		@c54 varchar(8),
		@c55 varchar(30),
		@c56 datetime,
		@c57 varchar(8),
		@c58 bit,
		@c59 bit,
		@c60 numeric(20,0),
		@c61 bit,
		@c62 numeric(20,0),
		@c63 bit,
		@c64 numeric(20,0),
		@c65 bit,
		@c66 numeric(20,0),
		@c67 bit,
		@c68 varchar(50),
		@c69 numeric(2,0),
		@c70 bit,
		@c71 bit,
		@c72 varchar(1),
		@c73 varchar(3),
		@c74 varchar(100),
		@c75 datetime,
		@c76 varchar(10),
		@c77 varchar(150)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[stfami] 
	where [ref] = @c2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ref] = ' + convert(nvarchar(100),@c2,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stfami]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from stfami where stfamistamp=@c1