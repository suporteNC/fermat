USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbou_stfp]    Script Date: 07/02/2023 19:20:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_U_STFP_Update]
		@c1 char(25),
		@c2 char(25),
		@c3 varchar(50),
		@c4 text,
		@c5 varchar(4),
		@c6 varchar(50),
		@c7 bit,
		@c8 varchar(254),
		@c9 text,
		@c10 varchar(50),
		@c11 varchar(30),
		@c12 datetime,
		@c13 varchar(8),
		@c14 varchar(30),
		@c15 datetime,
		@c16 varchar(8),
		@c17 bit,
		@c18 char(25),
		@c19 char(25),
		@c20 varchar(50),
		@c21 text,
		@c22 varchar(4),
		@c23 varchar(50),
		@c24 bit,
		@c25 varchar(254),
		@c26 text,
		@c27 varchar(50),
		@c28 varchar(30),
		@c29 datetime,
		@c30 varchar(8),
		@c31 varchar(30),
		@c32 datetime,
		@c33 varchar(8),
		@c34 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c18 = @c1)
begin 
update [PG].[u_stfp] set
		[u_stfpstamp] = @c18,
		[ststamp] = @c19,
		[nomef] = @c20,
		[dados] = @c21,
		[exti] = @c22,
		[descricao] = @c23,
		[ntratado] = @c24,
		[caminho] = @c25,
		[foto] = @c26,
		[nomei] = @c27,
		[ousrinis] = @c28,
		[ousrdata] = @c29,
		[ousrhora] = @c30,
		[usrinis] = @c31,
		[usrdata] = @c32,
		[usrhora] = @c33,
		[marcada] = @c34
	where [u_stfpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_stfpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_stfp]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[u_stfp] set
		[ststamp] = @c19,
		[nomef] = @c20,
		[dados] = @c21,
		[exti] = @c22,
		[descricao] = @c23,
		[ntratado] = @c24,
		[caminho] = @c25,
		[foto] = @c26,
		[nomei] = @c27,
		[ousrinis] = @c28,
		[ousrdata] = @c29,
		[ousrhora] = @c30,
		[usrinis] = @c31,
		[usrdata] = @c32,
		[usrhora] = @c33,
		[marcada] = @c34
	where [u_stfpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_stfpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_stfp]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','U_STFP',@c1, 'u_stfpstamp', ''