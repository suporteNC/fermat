** Criado por: Vasco Rocha
** Data Cria��o: 06.10.2015
** Marcar rece��es
SELECT pdarm
IF EMPTY(pdarm.bostamp)
	msg('Desculpe, mas tem de seleccionar uma linha da rece��o a marcar.')
	return
ENDIF
IF pergunta('Tem a certeza que pretende marcar a rece��o n� ' + astr(pdarm.obrano) + ' do cliente ' + ALLTRIM(pdarm.nome))
	TEXT TO m.msel NOSHOW TEXTMERGE
		update	u_pdarm
		set		marcada = 1
		where 	bostamp = '<<ALLTRIM(pdarm.bostamp)>>'
	ENDTEXT
	IF NOT u_sqlexec(m.msel)
		msg('N�o foi poss�vel marcar a rece��o.' + CHR(13) + 'Contacte o Administrador de Sistema.')
		RETURN .f. 
	ENDIF
	
	PDU_2TI0N75I1.Actualizarsopag.Nossobutton1.click()
	msg('Rece��o Marcada!')
ELSE
	msg('Opera��o cancelada.')
	RETURN .f.
ENDIF