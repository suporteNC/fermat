USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbostil]    Script Date: 30/03/2023 15:01:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STIL_Update]
		@c1 char(25),
		@c2 varchar(18),
		@c3 varchar(60),
		@c4 datetime,
		@c5 numeric(14,3),
		@c6 char(25),
		@c7 char(25),
		@c8 numeric(5,0),
		@c9 varchar(30),
		@c10 varchar(25),
		@c11 varchar(25),
		@c12 bit,
		@c13 bit,
		@c14 numeric(6,0),
		@c15 varchar(20),
		@c16 varchar(20),
		@c17 numeric(18,5),
		@c18 numeric(19,6),
		@c19 varchar(30),
		@c20 datetime,
		@c21 varchar(8),
		@c22 varchar(30),
		@c23 datetime,
		@c24 varchar(8),
		@c25 bit,
		@c26 varchar(25),
		@c27 varchar(30),
		@c28 varchar(25),
		@c29 varchar(55),
		@c30 numeric(18,5),
		@c31 numeric(19,6),
		@c32 numeric(10,0),
		@c33 varchar(4),
		@c34 bit,
		@c35 bit,
		@c36 numeric(3,0),
		@c37 varchar(50),
		@c38 char(25),
		@c39 varchar(18),
		@c40 varchar(60),
		@c41 datetime,
		@c42 numeric(14,3),
		@c43 char(25),
		@c44 char(25),
		@c45 numeric(5,0),
		@c46 varchar(30),
		@c47 varchar(25),
		@c48 varchar(25),
		@c49 bit,
		@c50 bit,
		@c51 numeric(6,0),
		@c52 varchar(20),
		@c53 varchar(20),
		@c54 numeric(18,5),
		@c55 numeric(19,6),
		@c56 varchar(30),
		@c57 datetime,
		@c58 varchar(8),
		@c59 varchar(30),
		@c60 datetime,
		@c61 varchar(8),
		@c62 bit,
		@c63 varchar(25),
		@c64 varchar(30),
		@c65 varchar(25),
		@c66 varchar(55),
		@c67 numeric(18,5),
		@c68 numeric(19,6),
		@c69 numeric(10,0),
		@c70 varchar(4),
		@c71 bit,
		@c72 bit,
		@c73 numeric(3,0),
		@c74 varchar(50)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c38 = @c1)
begin 
update [PG].[stil] set
		[stilstamp] = @c38,
		[ref] = @c39,
		[design] = @c40,
		[data] = @c41,
		[stock] = @c42,
		[stamp] = @c43,
		[sticstamp] = @c44,
		[armazem] = @c45,
		[lote] = @c46,
		[cor] = @c47,
		[tam] = @c48,
		[usalote] = @c49,
		[texteis] = @c50,
		[cpoc] = @c51,
		[ccusto] = @c52,
		[local] = @c53,
		[valstock] = @c54,
		[evalstock] = @c55,
		[ousrinis] = @c56,
		[ousrdata] = @c57,
		[ousrhora] = @c58,
		[usrinis] = @c59,
		[usrdata] = @c60,
		[usrhora] = @c61,
		[marcada] = @c62,
		[szzstamp] = @c63,
		[zona] = @c64,
		[alvstamp] = @c65,
		[identificacao] = @c66,
		[pcpond] = @c67,
		[epcpond] = @c68,
		[lordem] = @c69,
		[unidade] = @c70,
		[zera] = @c71,
		[u_contado] = @c72,
		[u_conta2] = @c73,
		[u_obs] = @c74
	where [stilstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[stilstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stil]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[stil] set
		[ref] = @c39,
		[design] = @c40,
		[data] = @c41,
		[stock] = @c42,
		[stamp] = @c43,
		[sticstamp] = @c44,
		[armazem] = @c45,
		[lote] = @c46,
		[cor] = @c47,
		[tam] = @c48,
		[usalote] = @c49,
		[texteis] = @c50,
		[cpoc] = @c51,
		[ccusto] = @c52,
		[local] = @c53,
		[valstock] = @c54,
		[evalstock] = @c55,
		[ousrinis] = @c56,
		[ousrdata] = @c57,
		[ousrhora] = @c58,
		[usrinis] = @c59,
		[usrdata] = @c60,
		[usrhora] = @c61,
		[marcada] = @c62,
		[szzstamp] = @c63,
		[zona] = @c64,
		[alvstamp] = @c65,
		[identificacao] = @c66,
		[pcpond] = @c67,
		[epcpond] = @c68,
		[lordem] = @c69,
		[unidade] = @c70,
		[zera] = @c71,
		[u_contado] = @c72,
		[u_conta2] = @c73,
		[u_obs] = @c74
	where [stilstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[stilstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stil]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','stil',@c1, 'stilstamp', ''