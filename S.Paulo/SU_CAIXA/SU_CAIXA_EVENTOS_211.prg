** julio Ricardo
** 2013-02-16
If  u_caixa.fdia = .T. Or  p_estab  = 0 and 0 = 1 &&desabilidado por JR
	Select u_caixal
	Scan For Upper(u_caixal.tipo) = 'DEP'
		TEXT TO msel NOSHOW TEXTMERGE
			SELECT  oxstamp, evalor  etotal, contado  FROM ox(nolock) WHERE  u_usrt1 = '<<u_caixa.u_caixastamp>>'	 AND  u_usrt2 = '<<u_caixal.u_caixalstamp>>'
		ENDTEXT
		If Not  u_sqlexec(msel,"c_temp")
			msg(msel)
			Return
		Endif
		TEXT TO msel NOSHOW TEXTMERGE
			SELECT  TOP 1 contado, ollocal  FROM u_paracx (nolock) WHERE  codigo = '<<u_caixal.item>>'
		ENDTEXT
		If Not  u_sqlexec(msel,"c_temppcx")
			msg(msel)
			Return
		Endif
		Select  c_temp
		If  Reccount("c_temp") = 0 Or c_temp.etotal <>   u_caixal.valor  Or c_temp.contado <>   c_temppcx.contado
			TEXT TO msel NOSHOW TEXTMERGE
				DELETE  ox  WHERE  u_usrt1 = '<<u_caixa.u_caixastamp>>'	 AND  u_usrt2 = '<<u_caixal.u_caixalstamp>>'
				insert into ox
				(oxstamp, data,dvalor,evalor, valor, contado, ollocal, enumer, numer,u_usrt1 ,u_usrt2, u_estab, olorigem, moeda, moeda1, oxdesc)
				values
				('<<u_stamp()>>', '<<DTOS(u_caixa.data)>>', '<<DTOS(u_caixa.data)>>', replace('<<u_caixal.valor>>',',','.')
				,replace('<<u_caixal.valor*200.482>>',',','.'), replace('<<c_temppcx.contado>>',',','.') , '<<c_temppcx.ollocal>>'
				, replace('<<u_caixal.valor>>',',','.') ,replace('<<u_caixal.valor*200.482>>',',','.')
				, '<<u_caixa.u_caixastamp>>' , '<<u_caixal.u_caixalstamp>>',  '<<p_estab>>', 'Caixa', 'PTE ou EURO','PTE ou EURO', 'Fecho de Caixa - ' + '<<upper(u_caixal.item)>>'
				 )
			ENDTEXT
			If Not  u_sqlexec(msel)
				msg(msel)
				Return
			Endif
		Endif
	Endscan
	Return
Endif
TEXT TO msel NOSHOW TEXTMERGE
		Update fcx Set
		fcx.efundocx = u_caixa.maneio
		, fcx.evdinheiro = u_caixa.totnum
		, fcx.echtotal = u_caixa.cheques
		, fcx.epaga1 = u_caixa.paga1
		, fcx.epaga2 = u_caixa.paga2
		, fcx.epaga3 = u_caixa.paga3
		, fcx.epaga4 = u_caixa.paga4
		, fcx.epaga5 = u_caixa.paga5
		, fcx.epaga6 = u_caixa.paga6
		, fcx.fundocx = u_caixa.maneio*200.482
		, fcx.vdinheiro = u_caixa.totnum*200.482
		, fcx.chtotal = u_caixa.cheques*200.482
		, fcx.paga1 = u_caixa.paga1*200.482
		, fcx.paga2 = u_caixa.paga2*200.482
		, fcx.paga3 = u_caixa.paga3*200.482
		, fcx.paga4 = u_caixa.paga4*200.482
		, fcx.paga5 = u_caixa.paga5*200.482
		, fcx.paga6 = u_caixa.paga6*200.482
		From fcx (nolock)
		inner Join u_caixa (nolock) On fcx.cxstamp = u_caixa.cxstamp And  operacao = 'F'
		Where u_caixa.u_caixastamp  = '<<u_caixa.u_caixastamp>>'
ENDTEXT
*If Not  u_sqlexec(msel)
*	msg(msel)
*	Return
*Endif
TEXT TO msel NOSHOW TEXTMERGE
	delete from u_caixal where tipo like 'vd%' and u_caixastamp = '<<u_caixa.u_caixastamp>>'
	delete from u_caixal where tipo like 'iv%' and u_caixastamp = '<<u_caixa.u_caixastamp>>'
	delete from u_caixal where tipo like 'RD%' and u_caixastamp = '<<u_caixa.u_caixastamp>>'
	delete from u_caixal where tipo like 'FTRD%' and u_caixastamp = '<<u_caixa.u_caixastamp>>'
ENDTEXT
If Not u_sqlexec(msel)
	msg(msel)
	Return
Endif
**** calcula linhas com vendas e iva
Select u_caixa
nIV = 0
nVD = 0
TEXT TO  msel NOSHOW TEXTMERGE
	SELECT  ISNULL(SUM(valor),0) valor
	from u_caixal (nolock)
	where u_caixal.u_caixalstamp  = '<<u_caixa.u_caixastamp>>'
	and tipo like  'VD%'
ENDTEXT
If Not u_sqlexec(msel,"c_tempvd")
	msg(msel)
	Return
Endif
nVD = c_tempvd.valor
TEXT TO msel NOSHOW TEXTMERGE
	SELECT  ISNULL(SUM(valor),0) valor
	from u_caixal (nolock)
	where u_caixal.u_caixalstamp  = '<<u_caixa.u_caixastamp>>'
	and tipo like  'IV%'
ENDTEXT
If Not u_sqlexec(msel,"c_tempIV")
	msg(msel)
	Return
Endif
nIV = c_tempIV.valor
fecha("c_tempVD")
fecha("c_tempIV")
If  Round(nVD,0) <> Round(u_caixa.vendiva,2) Or Round(nIV,2) <> Round(u_caixa.iva,2)
	i = 1
	Do While i < 10
		TEXT TO msel NOSHOW TEXTMERGE
		select isnull (sum(ft.eivain<<i>>),0) as Base_Inc
		,isnull (sum(ft.eivav<<i>>),0) as IVA
		from ft (nolock)
		where ft.cxstamp = '<<u_caixa.cxstamp>>'
			  and ft.tipodoc not in (4,5)
		ENDTEXT
		If Not u_sqlexec(msel,[tempsel])
			msg(msel)
			Return
		Endif
		If tempsel.base_inc <> 0
			TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora
				)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.base_inc>>',',','.')
				,'VD<<i>>'
				,'VD', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
			ENDTEXT
			If Not u_sqlexec(msel)
				msg(msel)
				Return .F.
			Endif
		Endif
		If tempsel.iva <> 0
			TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.iva>>',',','.')
				,'IV<<i>>'
				,'IVA', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
			ENDTEXT
			If Not u_sqlexec(msel)
				msg(msel)
				Return .F.
			Endif
		Endif
		i = i + 1
	Enddo
Endif
*** sinais
i = 1
Do While i < 10
	TEXT TO msel NOSHOW TEXTMERGE
		SELECT
		isnull (sum(RD.EBASE),0) as 	'RDBASE'
		,isnull (sum(RD.EIVAV),0) as 	'RDIVA'
		from RD (nolock)
		where RD.cxstamp = '<<u_caixa.cxstamp>>'
		and  tabiva = <<i>>
	ENDTEXT
	If Not u_sqlexec(msel,[tempsel])
		msg(msel)
		Return
	Endif
	If tempsel.rdbase <> 0
		TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.RDBASE>>',',','.')
				,'RDBASE<<i>>'
				,'RDBASE', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
		ENDTEXT
		If Not u_sqlexec(msel)
			msg(msel)
			Return .F.
		Endif
	Endif
	If tempsel.RDIVA <> 0
		TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.RDIVA>>',',','.')
				,'RDIVA<<i>>'
				,'RDIVA', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
		ENDTEXT
		If Not u_sqlexec(msel)
			msg(msel)
			Return .F.
		Endif
	Endif
	i = i + 1
Enddo
***  regularização de sinais
TEXT TO msel NOSHOW TEXTMERGE
		select
			isnull(sum(evreg),0) erec
			from ftrd (nolock)
			inner join ft (nolock) on ft.ftstamp = ftrd.ftstamp and  cdesc  <> 'vale'
			where ft.cxstamp  = '<<u_caixa.u_caixastamp>>'
ENDTEXT
If Not u_sqlexec(msel,[tempsel])
	msg(msel)
	Return
Endif
If tempsel.erec <> 0
	TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.erec>>',',','.')
				,'FTRDRD'
				,'FTRDRD', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
	ENDTEXT
	If Not u_sqlexec(msel)
		msg(msel)
		Return .F.
	Endif
Endif
i = 1
Do While i < 10
	TEXT TO msel NOSHOW TEXTMERGE
		select
			isnull(sum(ftrd.eivav<<i>>),0) ivav
			from ftrd (nolock)
			inner join ft (nolock) on ft.ftstamp = ftrd.ftstamp and  cdesc  <> 'vale'
			where ft.cxstamp  = '<<u_caixa.u_caixastamp>>'
	ENDTEXT
	If Not u_sqlexec(msel,[tempsel])
		msg(msel)
		Return
	Endif
	If tempsel.ivav <> 0
		TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.ivav>><<i>>',',','.')
				,'FTRDRDIVA<<i>>'
				,'FTRDRDIVA', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
		ENDTEXT
		If Not u_sqlexec(msel)
			msg(msel)
			Return .F.
		Endif
	Endif
	i = i + 1
Enddo
***  regularização de VALES
TEXT TO msel NOSHOW TEXTMERGE
		select
			isnull(sum(evreg),0) erec
			from ftrd (nolock)
			inner join ft (nolock) on ft.ftstamp = ftrd.ftstamp and  cdesc  = 'vale'
			where ft.cxstamp  = '<<u_caixa.u_caixastamp>>'
ENDTEXT
If Not u_sqlexec(msel,[tempsel])
	msg(msel)
	Return
Endif
If tempsel.erec <> 0
	TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.erec>>',',','.')
				,'FTRDFT'
				,'FTRDFT', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
	ENDTEXT
	If Not u_sqlexec(msel)
		msg(msel)
		Return .F.
	Endif
Endif
i = 1
Do While i < 10
	TEXT TO msel NOSHOW TEXTMERGE
		select
			isnull(sum(ftrd.eivav<<i>>),0) ivav
			from ftrd (nolock)
			inner join ft (nolock) on ft.ftstamp = ftrd.ftstamp and  cdesc  = 'vale'
			where ft.cxstamp  = '<<u_caixa.u_caixastamp>>'
	ENDTEXT
	If Not u_sqlexec(msel,[tempsel])
		msg(msel)
		Return
	Endif
	If tempsel.ivav <> 0
		TEXT TO msel NOSHOW TEXTMERGE
				insert into u_caixal (u_caixalstamp, u_caixastamp, valor, item, tipo,estab, site, pno, pnome, CXUSERNAME, multibanco
				, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
				values ('<<u_stamp()>>'
				,'<<u_caixa.u_caixastamp>>'
				,replace('<<tempsel.ivav>><<i>>',',','.')
				,'FTRDFTIVA<<i>>'
				,'FTRDFTIVA', '<<p_estab>>'
				,'<<u_caixa.site>>'
				,'<<u_caixa.pno>>'
				,'<<u_caixa.pnome>>'
				,'<<u_caixa.cxusername>>'
				,'<<u_caixa.multibanco>>'
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				,'<<u_caixa.cxusername>>'
				,convert(char(8),getdate(),112)
				,convert(char(10), getdate(), 108)
				)
		ENDTEXT
		If Not u_sqlexec(msel)
			msg(msel)
			Return .F.
		Endif
	Endif
	i = i + 1
Enddo
*** fim
su_caixa.doactualizar
Return
*!*	TEXT TO msel NOSHOW TEXTMERGE
*!*			UPDATE u_caixa SET
*!*				u_caixa.paga1r = ftt.paga1
*!*				, u_caixa.paga2r = ftt.paga2
*!*				, u_caixa.paga3r = ftt.paga3
*!*				, u_caixa.paga4r = ftt.paga4
*!*				, u_caixa.paga5r = ftt.paga5
*!*				, u_caixa.paga6r = ftt.paga6
*!*				,  u_caixa.totreal = ftt.dinheiro
*!*				,  u_caixa.cheqreal = ftt.cheque
*!*				, fechada  = 1
*!*			from u_caixa
*!*			inner join
*!*			(	select ft.cxstamp
*!*				, isnull(sum(evdinheiro-etroco),0) dinheiro
*!*				,isnull(sum(echtotal),0) cheque
*!*				,isnull(sum(epaga1),0)  paga1
*!*				,isnull(sum(epaga2),0)  paga2
*!*				,isnull(sum(epaga3),0)  paga3
*!*				,isnull(sum(epaga4),0)  paga4
*!*				,isnull(sum(epaga5),0)  paga5
*!*				,isnull(sum(epaga6),0) paga6
*!*				from ft (nolock)
*!*				left join ft2  (nolock) on ft.ftstamp = ft2.ft2stamp
*!*				where ft.cxstamp  = '<<u_caixa.u_caixastamp>>'
*!*				group by ft.cxstamp  ) ftt on ftt.cxstamp = u_caixa.u_caixastamp
*!*			where u_caixa.u_caixastamp = '<<u_caixa.u_caixastamp>>'
*!*			update u_caixa set
*!*			sobras  = case when  (totreal+cheqreal+paga1r+paga2r+paga3r+paga4r+paga5r+paga6r) -(maneio + totnum+ cheques + paga1 + paga2 + paga3 + paga4 + paga5 + paga6-despesas) > 0
*!*						then  	 (totreal+cheqreal+paga1r+paga2r+paga3r+paga4r+paga5r+paga6r) -(maneio + totnum+ cheques + paga1 + paga2 + paga3 + paga4 + paga5 + paga6)
*!*						else  0  end
*!*			,faltas  = case when  (totreal+cheqreal+paga1r+paga2r+paga3r+paga4r+paga5r+paga6r) -(maneio+ totnum+ cheques + paga1 + paga2 + paga3 + paga4 + paga5 + paga6-despesas) < 0
*!*						then  	 abs((totreal+cheqreal+paga1r+paga2r+paga3r+paga4r+paga5r+paga6r) -(maneio + totnum+ cheques + paga1 + paga2 + paga3 + paga4 + paga5 + paga6))
*!*						else  0  end
*!*			from u_caixa
*!*			where  u_caixa.u_caixastamp = '<<u_caixa.u_caixastamp>>'
*!*	ENDTEXT
*!*	If Not  u_sqlexec(msel)
*!*		msg(msel)
*!*		Return
*!*	Endif
