*************************************
*** Importação inicial de artigos
*** Criado em 13/07/2017
*** Criado por Rui Vale
*************************************


Msg("Atenção, a estrutura do ficheiro tem de ser :" + Chr(13) + Chr(10);
	+ "Referência C(18), Designação C(60), Cria Mondego L (Sim,Não), Cria Import L (Sim,Não), Cria Fermat L (Sim,Não), Tipo Artigo C (20)" + Chr(13) + Chr(10) ;
	+ ", Família C(20), Sub Família C(20), Marca C(20), Modelo C(20), Inactivo L (Sim,Não), Unidade C(4), Unid. Alt. C(4)" + Chr(13) + Chr(10) ;
	+ ", Fator Conversão N(16,6), Cod Barras C(20), Nº Fornecedor N(10), Fornecedor C(55), Ref.Fornecedor C(18), Cod Pautal C(20)" + Chr(13) + Chr(10) ;
	+ ", Preço de custo N(16,2), Preço de tabela N(16,2), Margem 1 N(10,3), PV1 N(16,2), Iva 1 Incl C(3),PV5 N(16,2), Iva 5 Incl C(3), Stock min N(16,3), Stock maxN(16,3)" + Chr(13) + Chr(10) ;
	+ ", Tabela iva N(2), Circuito c(20), Natureza C(20), Pais do Artigo C(15), Ref Forn Secund C(20), Nome Forn Secund C(55), Nr Forn Secund N(10)" + Chr(13) + Chr(10) ;
	+ ", Designação AO C(60), Bloqueado para compras L (Sim,Não), Acondicionamento N (16,2), Não entra na PEA L (Sim,Não)")

Create Cursor uCurErros ( Erro C(254) )
Create Cursor uDadosSt (Ref C(18), Design C(60), Mondego C(3), Import C(3), Fermat C(3), TipoArtigo C (20), Familia C(20), Sub_Familia C(20) ;
	, Marca C(20), Modelo C(20), Inactivo C(3), Unidade C(4), Unid_Alt C(4), Conversao N(16,6), Codigo C(20), Fornec N(10), Fornecedor C(55) ;
	, RefFor C(18), CodigoPautal C(20), epcult N(16,3), epcusto N(16,3), Margem1 N(16,3), epv1 N(16,3), iva1incl C(3), epv5 N(16,3), iva5incl C(3), Stmin N(16,3), Stmax N(16,3), Tabiva N(2) ;
	, Circuito C(20), Natureza C(20), Pais C(15), Reffornsecund C(20), NomeFornsecund C(55), Nrfornsecund N(10), DesignAO C(60), BLQCMP C(3), eoq N(16,3), u_pea C(3))

** Vou pedir o ficheiro para importar
uNomeFicheiroZip = Getfile("","Ficheiro a abrir","Abrir",2,"Abrir")

If Empty(uNomeFicheiroZip)
	Msg("Cancelado pelo utilizador!!!")
	Return
Endif

uFolhas = ""

uExtFile = Upper(Justext(uNomeFicheiroZip))

If !Alltrim(Upper(uExtFile))=="XLS" And !Alltrim(Upper(uExtFile))=="XLSX"
	Msg("Tipo de ficheiro inválido!!!")
	Return
Endif

** Se é um XLSX, transformo em XLS
If uExtFile = "XLSX"

	u_nficheiro = Justpath(uNomeFicheiroZip)+"\"+Strtran(Justfname(uNomeFicheiroZip),".XLSX","_XLS.XLS")

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.Visible = .T.
	oExcel.DisplayAlerts = .F.
	oExcel.ActiveWorkbook.SaveAs(u_nficheiro, 39)
	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

	uNomeFicheiroZip = u_nficheiro
Else

	oExcel = Createobject("Excel.Application")
	oExcel.Workbooks.Open(uNomeFicheiroZip)

	For Each uFolha In oExcel.Worksheets
		uFolhas = uFolhas + "," + Alltrim(uFolha.Name)
	Next

	oExcel.ActiveWorkbook.Close()
	oExcel.Quit()

Endif
**************

Create Cursor xVars ( No N(5), tipo C(1), Nome C(40), Pict C(100), lOrdem N(10), nValor N(18,5), cValor C(250), lValor l, dValor d, tbval M )

Select xVars
Append Blank
Replace xVars.No With 1
Replace xVars.tipo With "T"
Replace xVars.Nome With "Folha"
Replace xVars.Pict With ""
Replace xVars.lOrdem With 1
Replace xVars.tbval With uFolhas

m.mCaption = "Qual a Folha a importar"
m.escolheu=.F.
docomando("do form usqlvar with 'xvars',m.mCaption")

If ! m.escolheu
	Msg("Cancelado pelo utilizador")
	Return
Else
	Select xVars
	Locate For No=1
	uNomeFolha = xVars.cValor
Endif

uFechar = .F.

uNomeFicheiroZip = ["] + uNomeFicheiroZip + ["]
uNomeFolha = ["] + Alltrim(uNomeFolha) + ["]

Try
	Select uDadosSt
	Append From &uNomeFicheiroZip Type Xl5 Sheet &uNomeFolha
Catch
	Msg("Os dados a importar não estão de acordo com o esperado!!!")
	uFechar = .T.
Endtry

Select uDadosSt
Delete For Empty(uDadosSt.Design)

mostrameisto("uDadosSt")

If ! Pergunta("Quer mesmo importar")
	uFechar = .T.
Endif

If uFechar
	Return
Endif

Select uDadosSt
ntotal=Reccount()

regua(0,ntotal,"A importar os artigos!!!")

Select uDadosSt
Go Top
Skip
Do While !Eof()

	regua[1,recno("uDadosSt"),"A importar o artigo Referência : "+Alltrim(uDadosSt.Ref)]

	If Empty(uDadosSt.Ref)
		uCriar=.T.
	Else
		TEXT TO uSql TEXTMERGE noshow
			SELECT *
			From st (nolock)
			Where ref = '<<uDadosSt.ref>>'
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			If Reccount("uCurArtigos")<=0
				uCriar=.T.
			Else
				uCriar=.F.
			Endif
		Endif
	Endif

	If uCriar
		Fecha("uCurArtigos")

		TEXT TO uSql TEXTMERGE noshow
			Select *
			From st (nolock)
			Where 1=2
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			Select uCurArtigos
			Append Blank
			Replace uCurArtigos.ststamp With u_Stamp()
			If Empty(uDadosSt.Ref)
				TEXT TO uSql TEXTMERGE noshow
					select isnull(max(cast(ref as numeric(14)))+1,1) as ref
					from st (nolock)
					where ref<>''
					and isnumeric(ref)=1
				ENDTEXT
				uRef  = ''
				If u_Sqlexec(uSql,"uCurRefArtigos") And Reccount("uCurRefArtigos")>0
					uRef = astr(uCurRefArtigos.Ref)
				Endif
			Else
				uRef = uDadosSt.Ref
			Endif
			Replace uCurArtigos.Ref With uRef
			Replace uCurArtigos.Design With uDadosSt.Design
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.u_ctpart With Alltrim(uDadosSt.TipoArtigo)
			**Replace uCurArtigos.usr5 With Alltrim(uCurArtigos.u_ctpar)
			Replace uCurArtigos.u_csubfam With Alltrim(uDadosSt.Sub_Familia)
			Replace uCurArtigos.u_mondego With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Inactivo))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.F.,.T.)
			Replace uCurArtigos.u_impor With Iif(Alltrim(Upper(uDadosSt.Import))=="SIM",.T.,.F.)
			Replace uCurArtigos.u_fermat With Iif(Alltrim(Upper(uDadosSt.Fermat))=="SIM",.T.,.F.)
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.usr1 With Alltrim(uDadosSt.Marca)
			Replace uCurArtigos.usr2 With Alltrim(uDadosSt.Modelo)
			Replace uCurArtigos.Unidade With Alltrim(uDadosSt.Unidade)
			Replace uCurArtigos.Uni2 With Alltrim(uDadosSt.Unid_Alt)
			Replace uCurArtigos.Conversao With uDadosSt.Conversao
			Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
			Replace uCurArtigos.Fornec With uDadosSt.Fornec
			Replace uCurArtigos.cpoc With 1
			Replace uCurArtigos.containv With '321'
			Replace uCurArtigos.contacev With '611'
			Replace uCurArtigos.Fornecedor With Alltrim(uDadosSt.Fornecedor)
			Replace uCurArtigos.ForRef With Alltrim(uDadosSt.RefFor)
			Replace uCurArtigos.u_cpautal With Strtran(uDadosSt.CodigoPautal,".","")
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva


			Replace uCurArtigos.U_CIRCUITO With uDadosSt.Circuito
			Replace uCurArtigos.U_NATUREZA With uDadosSt.Natureza
			Replace uCurArtigos.U_PAIS With uDadosSt.Pais
			Replace uCurArtigos.U_RFORNEC1 With uDadosSt.Reffornsecund
			Replace uCurArtigos.U_NFORNEC1 With uDadosSt.NomeFornsecund
			Replace uCurArtigos.U_FORNEC1 With uDadosSt.Nrfornsecund
			Replace uCurArtigos.U_DESIGN With uDadosSt.DesignAO
			Replace uCurArtigos.FOBLOQ With Iif(Alltrim(Upper(uDadosSt.BLQCMP ))=="SIM",.T.,.F.)

			Replace uCurArtigos.iva1incl With Iif(Alltrim(Upper(uDadosSt.iva1incl))=="SIM",.T.,.F.)

			Replace uCurArtigos.eoq With uDadosSt.eoq



			Select uCurArtigos
			If !Empty(uDadosSt.Codigo)
				uCodEan13 = Ean13ckd(Left(Alltrim(uDadosSt.Codigo),12))
				If uCodEan13 == Alltrim(uDadosSt.Codigo)
					Select uCurArtigos
					Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
				Else
					Select uCurErros
					Append Blank
					Replace uCurErros.Erro With "O EAN " + Alltrim(uDadosSt.Codigo) + " do artigo com referência " + Alltrim(uDadosSt.Ref) + ", é inválido !!"
				Endif
			Endif

			Select uCurArtigos
			Replace uCurArtigos.Marg1 With uDadosSt.Margem1
			Replace uCurArtigos.epcpond With uDadosSt.epcult
			Replace uCurArtigos.epcusto With uDadosSt.epcusto
			Replace uCurArtigos.epcult With uDadosSt.epcult
			Replace uCurArtigos.epv1 With uDadosSt.epv1
            Replace uCurArtigos.epv5 With uDadosSt.epv5
            Replace uCurArtigos.iva5incl With Iif(Alltrim(Upper(uDadosSt.iva5incl))=="SIM",.T.,.F.)
			Replace uCurArtigos.Stmin With uDadosSt.Stmin
			Replace uCurArtigos.Stmax With uDadosSt.Stmax
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

            ** Calcular EPV3
            if uCurArtigos.iva1incl=.t.
                if  U_SQLEXEC("SELECT codigo,taxa from taxasiva where codigo =?uDadosSt.tabiva","xtxiva") = .T. And Reccount("xtxiva")>0
                    replace uCurArtigos.epv3 with uDadosSt.EPV1/(1+(xtxiva.taxa/100))
                endif
                Else
                replace uCurArtigos.epv3 with uDadosSt.epv1
            endif



			** Valida e lê familia
			TEXT TO uSql TEXTMERGE noshow
				SELECT nome
				From stfami (nolock)
				Where ref = '<<Alltrim(uDadosSt.Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurFami") And Reccount("uCurFami")>0
				Select uCurArtigos
				Replace uCurArtigos.faminome With uCurFami.Nome
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A familia " + Left(Alltrim(uDadosSt.Sub_Familia),3) + ", não existe !!"
			Endif

			** Valida Tipo Artigo
			TEXT TO uSql TEXTMERGE noshow
				SELECT ctpart,tpart
				From u_sttpart (nolock)
				Where ctpart = '<<Alltrim(uDadosSt.TipoArtigo)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurTpArt") And Reccount("uCurTpArt")>0
				Select uCurArtigos
				Replace uCurArtigos.u_tpart With uCurTpArt.tpart
				Replace uCurArtigos.usr5 With uCurTpArt.tpart
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "O Tipo Artigo " + Left(Alltrim(uDadosSt.Sub_Familia),1) + ", não existe !!"
			Endif

			** Valida a Sub-família
			TEXT TO uSql TEXTMERGE noshow
				SELECT csubfam,subfam
				From u_stsubfam (nolock)
				Where csubfam = '<<Alltrim(uDadosSt.Sub_Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCursubfam") And Reccount("uCursubfam")>0
				Select uCurArtigos
				Replace uCurArtigos.u_subfam With uCursubfam.subfam
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A Sub-Família " + Alltrim(uDadosSt.Sub_Familia) + ", não existe !!"
			Endif

			Select uCurArtigos
			Replace uCurArtigos.ousrdata With Date()
			Replace uCurArtigos.ousrhora With Time()
			Replace uCurArtigos.ousrinis With m_chinis
			Replace uCurArtigos.usrdata With Date()
			Replace uCurArtigos.usrhora With Time()
			Replace uCurArtigos.usrinis With m_chinis
			cref  =  uCurArtigos.Ref



			If !Tts_GuardaSql("uCurArtigos","ST")
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "Não foi possível guardar o artigo com referência " + Alltrim(uDadosSt.Ref) + " !!"
			Endif

			TEXT TO uSql TEXTMERGE noshow
				SELECT *
				From stobs (nolock)
				Where ref = '<<cref>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurStobs")
				If Reccount("uCurStobs")<=0

					Select uCurStobs
					Append Blank
					Replace uCurStobs.stobsstamp With u_Stamp()
					Replace uCurStobs.Ref With cref
					Replace uCurStobs.tipoprod With "M"

					Replace uCurStobs.u_pea With Iif(Alltrim(Upper(uDadosSt.u_pea ))=="SIM",.T.,.F.)

					Replace uCurStobs.ousrdata With Date()
					Replace uCurStobs.ousrhora With Time()
					Replace uCurStobs.ousrinis With m_chinis
					Replace uCurStobs.usrdata With Date()
					Replace uCurStobs.usrhora With Time()
					Replace uCurStobs.usrinis With m_chinis

					If !Tts_GuardaSql("uCurStobs","STOBS")
						Select uCurErros
						Append Blank
						Replace uCurErros.Erro With "Não foi possível guardar o artigo com referência " + Alltrim(uDadosSt.Ref) + " !!"
					Endif
				Endif
			Endif
		Endif
	Else
		Fecha("uCurArtigos")

		TEXT TO uSql TEXTMERGE noshow
			Select REF,Familia,u_ctpart ,u_csubfam ,u_mondego ,Inactivo ,u_impor ,u_fermat ,usr1 ,usr2,Unidade ,Uni2 ,Conversao ,Codigo
				,Fornecedor ,Fornec ,ForRef ,u_cpautal ,Tabiva ,Codigo,epcpond , epcusto ,epcult ,epv1 ,epv5,Stmin ,Stmax ,Marg1,faminome ,u_tpart
				,u_subfam ,u_circuito, u_natureza , U_PAIS ,U_RFORNEC1 ,U_NFORNEC1 ,U_FORNEC1, U_DESIGN ,FOBLOQ,iva1incl,iva5incl, EOQ, usrdata ,usrhora ,usrinis
			From st (nolock)
			Where ref = '<<uDadosSt.Ref>>'
		ENDTEXT

		If u_Sqlexec(uSql,"uCurArtigos")
			Select uCurArtigos
			Replace uCurArtigos.Familia With Alltrim(uDadosSt.Familia)
			Replace uCurArtigos.u_ctpart With Alltrim(uDadosSt.TipoArtigo)
			Replace uCurArtigos.u_csubfam With Alltrim(uDadosSt.Sub_Familia)
			Replace uCurArtigos.u_mondego With Iif(Alltrim(Upper(uDadosSt.Mondego))=="SIM",.T.,.F.)
			Replace uCurArtigos.Inactivo With Iif(Alltrim(Upper(uDadosSt.Inactivo))=="SIM",.T.,.F.)
			Replace uCurArtigos.u_impor With Iif(Alltrim(Upper(uDadosSt.Import))=="SIM",.T.,.F.)
			Replace uCurArtigos.u_fermat With Iif(Alltrim(Upper(uDadosSt.Fermat))=="SIM",.T.,.F.)
			Replace uCurArtigos.usr1 With Alltrim(uDadosSt.Marca)
			Replace uCurArtigos.usr2 With Alltrim(uDadosSt.Modelo)
			Replace uCurArtigos.Unidade With Alltrim(uDadosSt.Unidade)
			Replace uCurArtigos.Uni2 With Alltrim(uDadosSt.Unid_Alt)
			Replace uCurArtigos.Conversao With uDadosSt.Conversao
			Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
			Replace uCurArtigos.Fornec With uDadosSt.Fornec
			Replace uCurArtigos.Fornecedor With Alltrim(uDadosSt.Fornecedor)
			Replace uCurArtigos.ForRef With Alltrim(uDadosSt.RefFor)
			Replace uCurArtigos.u_cpautal With Strtran(uDadosSt.CodigoPautal,".","")
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva


			Replace uCurArtigos.U_CIRCUITO With uDadosSt.Circuito
			Replace uCurArtigos.U_NATUREZA With uDadosSt.Natureza
			Replace uCurArtigos.U_PAIS With uDadosSt.Pais
			Replace uCurArtigos.U_RFORNEC1 With uDadosSt.Reffornsecund
			Replace uCurArtigos.U_NFORNEC1 With uDadosSt.NomeFornsecund
			Replace uCurArtigos.U_FORNEC1 With uDadosSt.Nrfornsecund
			Replace uCurArtigos.U_DESIGN With uDadosSt.DesignAO
			Replace uCurArtigos.FOBLOQ With Iif(Alltrim(Upper(uDadosSt.BLQCMP ))=="SIM",.T.,.F.)
			Replace uCurArtigos.iva1incl With Iif(Alltrim(Upper(uDadosSt.iva1incl ))=="SIM",.T.,.F.)

			Replace uCurArtigos.eoq With uDadosSt.eoq


			Select uCurArtigos
			If !Empty(uDadosSt.Codigo)
				uCodEan13 = Ean13ckd(Left(Alltrim(uDadosSt.Codigo),12))
				If uCodEan13 == Alltrim(uDadosSt.Codigo)
					Select uCurArtigos
					Replace uCurArtigos.Codigo With Alltrim(uDadosSt.Codigo)
				Else
					Select uCurErros
					Append Blank
					Replace uCurErros.Erro With "O EAN " + Alltrim(uDadosSt.Codigo) + " do artigo com referência " + Alltrim(uDadosSt.Ref) + ", é inválido !!"
				Endif
			Endif

			Select uCurArtigos
			Replace uCurArtigos.Marg1 With uDadosSt.Margem1
			Replace uCurArtigos.epcpond With uDadosSt.epcult
			Replace uCurArtigos.epcusto With uDadosSt.epcusto
			Replace uCurArtigos.epcult With uDadosSt.epcult
			Replace uCurArtigos.epv1 With uDadosSt.epv1
            Replace uCurArtigos.epv5 With uDadosSt.epv5
            Replace uCurArtigos.iva5incl With Iif(Alltrim(Upper(uDadosSt.iva5incl))=="SIM",.T.,.F.)
			Replace uCurArtigos.Stmin With uDadosSt.Stmin
			Replace uCurArtigos.Stmax With uDadosSt.Stmax
			Replace uCurArtigos.Tabiva With uDadosSt.Tabiva

            ** Calcular EPV3
            if uCurArtigos.iva1incl=.t.
                if  U_SQLEXEC("SELECT codigo,taxa from taxasiva where codigo =?uDadosSt.tabiva","xtxiva") = .T. And Reccount("xtxiva")>0
                    replace uCurArtigos.epv3 with uDadosSt.EPV1/(1+(xtxiva.taxa/100))
                endif
                Else
                replace uCurArtigos.epv3 with uDadosSt.epv1
            endif


			** Valida e lê familia
			TEXT TO uSql TEXTMERGE noshow
				SELECT nome
				From stfami (nolock)
				Where ref = '<<Alltrim(uDadosSt.Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurFami") And Reccount("uCurFami")>0
				Select uCurArtigos
				Replace uCurArtigos.faminome With uCurFami.Nome
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A familia " + Left(Alltrim(uDadosSt.Sub_Familia),3) + ", não existe !!"
			Endif

			** Valida Tipo Artigo
			TEXT TO uSql TEXTMERGE noshow
				SELECT ctpart,tpart
				From u_sttpart (nolock)
				Where ctpart = '<<Alltrim(uDadosSt.TipoArtigo)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurTpArt") And Reccount("uCurTpArt")>0
				Select uCurArtigos
				Replace uCurArtigos.u_tpart With uCurTpArt.tpart
				**Replace uCurArtigos.usr5  With uCurTpArt.tpart
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "O Tipo Artigo " + Left(Alltrim(uDadosSt.Sub_Familia),1) + ", não existe !!"
			Endif

			** Valida a Sub-família
			TEXT TO uSql TEXTMERGE noshow
				SELECT csubfam,subfam
				From u_stsubfam (nolock)
				Where csubfam = '<<Alltrim(uDadosSt.Sub_Familia)>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCursubfam") And Reccount("uCursubfam")>0
				Select uCurArtigos
				Replace uCurArtigos.u_subfam With uCursubfam.subfam
			Else
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "A Sub-Família " + Alltrim(uDadosSt.Sub_Familia) + ", não existe !!"
			Endif

			Select uCurArtigos
			Replace uCurArtigos.usrdata With Date()
			Replace uCurArtigos.usrhora With Time()
			Replace uCurArtigos.usrinis With m_chinis

			If !Tts_AlteraReg("uCurArtigos","ST","st.ref='"+uDadosSt.Ref+"'")
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "Não foi possível guardar o artigo com referência " + Alltrim(uDadosSt.Ref) + " !!"
			Endif


			TEXT TO uSql TEXTMERGE noshow
				SELECT U_PEA,ref
				From stobs (nolock)
				Where ref = '<<uCurArtigos.Ref>>'
			ENDTEXT

			If u_Sqlexec(uSql,"uCurStobs")
					Select uCurStobs
					Replace uCurStobs.u_pea With Iif(Alltrim(Upper(uDadosSt.u_pea ))=="SIM",.T.,.F.)			
			Endif

			If !Tts_AlteraReg("uCurStobs","STOBS","stobs.ref='"+uDadosSt.Ref+"'")
				Select uCurErros
				Append Blank
				Replace uCurErros.Erro With "Não foi possível guardar o artigo com referência " + Alltrim(uDadosSt.Ref) + " !!"
			Endif

		Endif


	Endif

	Select uDadosSt
	Skip
Enddo

regua(2)

If Reccount("uCurErros")>0
	mostrameisto("uCurErros")
Endif



Function Tts_GuardaSql
	Parameter meucursor,uMinhaTabela
	************************************************
	** Função para guardar cursor numa tabela SQL **
	************************************************

	Local uMENCOUTROU,uNREG,uNREG1,uNRTENTATIVAS

	merro=.F.

	Select &meucursor

	gnFieldcount = Afields(gaMyArray)  && Create array
	Clear
	Dimension aCampos  [gnFieldcount]
	Dimension aTipos   [gnFieldcount]
	Dimension aTamanho [gnFieldcount]
	Dimension aCasas   [gnFieldcount]

	For nCount = 1 To gnFieldcount
		aCampos [nCount] = gaMyArray[nCount,1]
		aTipos  [nCount] = gaMyArray[nCount,2]
		aTamanho[nCount] = gaMyArray[nCount,3]
		aCasas  [nCount] = gaMyArray[nCount,4]
	Endfor

	sSQL=""
	cSQL=""
	inicSQL=""

	sSQLString = "select * from "+Alltrim(uMinhaTabela)+Chr(13)
	sSQLString =sSQLString +" (nolock) where 1=2"+Chr(13)

	If u_Sqlexec(sSQLString,"uCURTEMP")

		Select uCURTEMP

		gnFieldcount1 = Afields(uCURTEMP)  && Create array
		Clear
		Dimension aCampos1  [gnFieldcount1]
		Dimension aTipos1   [gnFieldcount1]
		Dimension aTamanho1 [gnFieldcount1]
		Dimension aCasas1   [gnFieldcount1]

		For nCount = 1 To gnFieldcount1
			aCampos1 [nCount] = uCURTEMP[nCount,1]
			aTipos1  [nCount] = uCURTEMP[nCount,2]
			aTamanho1[nCount] = uCURTEMP[nCount,3]
			aCasas1  [nCount] = uCURTEMP[nCount,4]
		Endfor


		** verifica quantos registos são iguais
		uNREG=0
		For nCount = 1 To gnFieldcount

			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uNREG=uNREG+1
				Endif

			Endfor
		Endfor

		uNREG1=0
		For nCount = 1 To gnFieldcount

			uMENCOUTROU=.F.
			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uMENCOUTROU=.T.
				Endif

			Endfor

			If uMENCOUTROU And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))

				uNREG1=uNREG1+1

				Select &meucursor

				cDado = Evaluate(aCampos[nCount])

				cTipo = aTipos[nCount]

				ucCampo = aCampos[nCount]


				If aTipos[nCount] = "N" Or aTipos[nCount] = "Y"
					cDado = Alltrim(Str(cDado,aTamanho[nCount],aCasas[nCount]))
				Endif

				If aTipos[nCount] = "I"
					cDado = Alltrim(Str(cDado))
				Endif

				If aTipos[nCount] = "T" Or aTipos[nCount] = "D"
					If Empty(cDado)
						cDado = "1900-01-01"
					Else
						cData = Alltrim(Str(Year(cDado)))+"-"+Padl(Alltrim(Str(Month(cDado))),2,"0")+"-"+Padl(Alltrim(Str(Day(cDado))),2,"0")
						cDado = cData
					Endif
				Endif

				If aTipos[nCount] = "L"
					If cDado = .T.
						cDado = Alltrim(Str(1))
					Else
						cDado = Alltrim(Str(0))
					Endif
				Endif

				If aTipos[nCount] = "N" Or aTipos[nCount] = "I" Or ;
						aTipos[nCount] = "Y" Or aTipos[nCount] = "B" Or ;
						aTipos[nCount] = "L"
					sSQL = sSQL + Strtran(Alltrim(cDado),",",".")
				Else
					sSQL = sSQL + "'" + Strtran(Alltrim(cDado),"'","'+char(39)+'")+ "'"
				Endif

				cSQL = cSQL + Alltrim(ucCampo)

				If uNREG1 < uNREG
					sSQL = sSQL + "," +Chr(13)
					cSQL = cSQL + ","+Chr(13)
				Endif

			Endif

		Endfor

		sSQLString = "Insert Into "+Alltrim(uMinhaTabela)+Chr(13)
		sSQLString =sSQLString +" ("+cSQL
		sSQLString =sSQLString +") Values ("
		sSQLString =sSQLString + sSQL + ")"
		If !Empty(inicSQL)
			sSQLString = sSQLString + inicSQL+Chr(13)
		Endif

		uNRTENTATIVAS=0
		merro=.T.
		Do While uNRTENTATIVAS<10 And merro

			If u_Sqlexec(sSQLString,"")
				merro=.F.
			Else
				merro=.T.
			Endif

			uNRTENTATIVAS=uNRTENTATIVAS+1

			Wait Window "Estou a Guardar!!" Nowait Timeout 100

		Enddo

		If merro
			_Cliptext=sSQLString
			= Aerror(aErrorArray)
			msgerro=aErrorArray(2)
			erros=.T.
			*!*				msg="O Registo Não foi gravado!!!"
		Endif

	Endif

	If Used(meucursor)
		Fecha(meucursor)
	Endif

	Return !merro
Endfunc

Function Tts_AlteraReg
	Lparameter meucursor,uMinhaTabela,uMinhaCondicao,uBaseDados,uServer

	***************************************************
	** Função que altera um registo da Base de dados **
	***************************************************

	**************** PARÂMETROS *******************
	** meucursor     : Nome do cursor a guardar
	** uMinhaTabela  : Nome da tabela para alterar
	** uMinhaCondicao: Condição para alterar
	** uBDados       : Base de Dados onde vai guardar
	***********************************************

	Local uMENCOUTROU,uNREG,uNREG1,uNRTENTATIVAS

	If Empty(uBaseDados)
		uBaseDados = ""
	Endif
	If Empty(uServer)
		uServer = ""
	Endif

	merro=.F.

	Select &meucursor

	gnFieldcount = Afields(gaMyArray)  && Create array
	Clear
	Dimension aCampos  [gnFieldcount]
	Dimension aTipos   [gnFieldcount]
	Dimension aTamanho [gnFieldcount]
	Dimension aCasas   [gnFieldcount]

	For nCount = 1 To gnFieldcount
		aCampos [nCount] = gaMyArray[nCount,1]
		aTipos  [nCount] = gaMyArray[nCount,2]
		aTamanho[nCount] = gaMyArray[nCount,3]
		aCasas  [nCount] = gaMyArray[nCount,4]
	Endfor

	sSQL=""
	cSQL=""

	If Empty(uBaseDados)
		sSQLString = "select * from " + Alltrim(uMinhaTabela) + Chr(13)
	Else
		If Empty(uServer)
			sSQLString = "select * from " + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + Chr(13)
		Else
			sSQLString = "select * from [" + Alltrim(uServer) + "]." + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + Chr(13)
		Endif
	Endif
	sSQLString = sSQLString + Alltrim(uMinhaTabela) + " (nolock) where 1=2"+Chr(13)

	If u_Sqlexec(sSQLString,"uCURTEMP")

		Select uCURTEMP
		gnFieldcount1 = Afields(uCURTEMP)  && Create array
		Clear
		Dimension aCampos1  [gnFieldcount1]
		Dimension aTipos1   [gnFieldcount1]
		Dimension aTamanho1 [gnFieldcount1]
		Dimension aCasas1   [gnFieldcount1]

		For nCount = 1 To gnFieldcount1
			aCampos1 [nCount] = uCURTEMP[nCount,1]
			aTipos1  [nCount] = uCURTEMP[nCount,2]
			aTamanho1[nCount] = uCURTEMP[nCount,3]
			aCasas1  [nCount] = uCURTEMP[nCount,4]
		Endfor

		** verifica quantos registos são iguais
		uNREG=0
		For nCount = 1 To gnFieldcount

			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uNREG=uNREG+1
				Endif

			Endfor
		Endfor

		uNREG1=0
		For nCount = 1 To gnFieldcount

			uMENCOUTROU=.F.
			For i=1 To gnFieldcount1

				If aCampos[nCount]==aCampos1[i] And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))
					uMENCOUTROU=.T.
				Endif

			Endfor

			If uMENCOUTROU And !Like(Alltrim(uMinhaTabela)+"ID",Alltrim(Upper(aCampos[nCount])))

				uNREG1=uNREG1+1

				Select &meucursor

				cDado = Evaluate(aCampos[nCount])

				cTipo = aTipos[nCount]

				ucCampo = aCampos[nCount]

				If Alltrim(Upper(ucCampo))<>'USTRID' And Alltrim(Upper(ucCampo))<>'USTRID'

					If aTipos[nCount] = "N" Or aTipos[nCount] = "Y"
						cDado = Alltrim(Str(cDado,aTamanho[nCount],aCasas[nCount]))
					Endif

					If aTipos[nCount] = "I"
						cDado = Alltrim(Str(cDado))
					Endif

					If aTipos[nCount] = "T" Or aTipos[nCount] = "D"
						If Empty(cDado)
							cDado = "19000101"
						Else
							cData = Alltrim(Dtos(cDado))
							cDado = cData
						Endif
					Endif

					If aTipos[nCount] = "L"
						If cDado = .T.
							cDado = Alltrim(Str(1))
						Else
							cDado = Alltrim(Str(0))
						Endif
					Endif

					If aTipos[nCount] = "N" Or aTipos[nCount] = "I" Or ;
							aTipos[nCount] = "Y" Or aTipos[nCount] = "B" Or ;
							aTipos[nCount] = "L"
						sSQL = sSQL + aCampos[nCount] + "=" + Strtran(Alltrim(cDado),",",".")
					Else
						sSQL = sSQL + aCampos[nCount] + "=" + "'" +  Strtran(Alltrim(cDado),"'","''") + "'"
					Endif

					cSQL = cSQL + Alltrim(ucCampo)

				Endif
				If uNREG1 < uNREG
					sSQL = sSQL + "," +Chr(13)
					cSQL = cSQL + ","+Chr(13)
				Endif

			Endif

		Endfor

		If Empty(uBaseDados)
			sSQLString = "update " + Alltrim(uMinhaTabela) + " set "
		Else
			If Empty(uServer)
				sSQLString = "update " + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + " set "
			Else
				sSQLString = "update [" + Alltrim(uServer) + "]." + Alltrim(uBaseDados) + ".." + Alltrim(uMinhaTabela) + " set "
			Endif
		Endif

		sSQLString = sSQLString + sSQL
		sSQLString = sSQLString + " where " + Alltrim(uMinhaCondicao)

		uNRTENTATIVAS=0
		merro=.T.
		Do While uNRTENTATIVAS<100 And merro

			If u_Sqlexec(sSQLString)
				merro=.F.
			Else
				merro=.T.
			Endif

			uNRTENTATIVAS=uNRTENTATIVAS+1

			Wait Window "Estou a Guardar!!" Nowait Timeout 100

		Enddo

		If merro
			= Aerror(aErrorArray)
			_Cliptext=sSQLString
			Msg(sSQLString)
			*MostraErros(aErrorArray(1),aErrorArray(2),aErrorArray(3),aErrorArray(4))

			*mostraerros()

			*MostraErros(aErrorArray(1),aErrorArray(2),aErrorArray(3),aErrorArray(4))
			Return .F.
		Endif

	Endif

	*If Used(meucursor)
	*	fecha(meucursor)
	*Endif

	Return .T.

Endfunc
