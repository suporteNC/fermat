USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbostic]    Script Date: 30/03/2023 15:00:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NC_STIC_Delete]
		@c1 char(25),
		@c2 datetime,
		@c3 varchar(60),
		@c4 bit,
		@c5 char(25),
		@c6 varchar(20),
		@c7 varchar(30),
		@c8 datetime,
		@c9 varchar(8),
		@c10 varchar(30),
		@c11 datetime,
		@c12 varchar(8),
		@c13 bit,
		@c14 bit,
		@c15 bit,
		@c16 varchar(60),
		@c17 varchar(8),
		@c18 bit,
		@c19 varchar(30),
		@c20 varchar(30),
		@c21 varchar(20),
		@c22 varchar(40),
		@c23 varchar(6),
		@c24 bit,
		@c25 varchar(40),
		@c26 numeric(1,0),
		@c27 bit,
		@c28 numeric(10,0),
		@c29 numeric(10,0),
		@c30 varchar(55),
		@c31 varchar(16)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[stic] 
	where [sticstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[sticstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[stic]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from stic where sticstamp=@c1