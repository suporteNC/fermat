*** Criado por Nelson Sim�es
*** Data: 23/11/2017


SELECT BO

IF inlist(BO.NDOS,207)

Local m_pass

m_PASS = GETNOME('Introduza PASSWORD! ','','','',1,.t.)

IF EMPTY(m_PASS)
	msg('Ac��o cancelada pelo Utilizador')
	fecha('CR_ACESSO')
	sbo.CancelarGravacao(.T.,.T.)
	RETURN
ENDIF

TEXT TO MSQL TEXTMERGE NOSHOW
	SELECT US.USERNO, US.USERNAME, US.USRINIS, US.USSTAMP, US.VENDEDOR, US.VENDNM
	FROM US (NOLOCK)
	WHERE US.PWPOS = '<<m_PASS>>' and us.u_gerente=1
ENDTEXT

If !U_SQLEXEC(MSQL,'CR_ACESSO')
	msg('Erro Encontrado')
	msg(MSQL)
	sbo.CancelarGravacao(.T.,.T.)	
	Return
ENDIF

Select CR_ACESSO
If Reccount() = 0
	msg('O utilizador digitado n�o tem permissao para terminar a ac��o! Por favor contacte o gerente!')
	fecha('CR_ACESSO')	
	sbo.CancelarGravacao(.T.,.T.)
	return
ENDIF


ENDIF

select BO
return BO.ZONA
