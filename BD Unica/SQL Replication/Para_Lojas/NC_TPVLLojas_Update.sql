USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbotpvl]    Script Date: 09/02/2023 18:22:16 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_TPVLLojas_Update]
		@c1 char(25),
		@c2 varchar(200),
		@c3 numeric(3,0),
		@c4 bit,
		@c5 numeric(1,0),
		@c6 numeric(1,0),
		@c7 numeric(5,2),
		@c8 numeric(18,5),
		@c9 numeric(19,6),
		@c10 numeric(1,0),
		@c11 varchar(18),
		@c12 varchar(18),
		@c13 varchar(18),
		@c14 numeric(18,5),
		@c15 numeric(19,6),
		@c16 bit,
		@c17 numeric(10,0),
		@c18 bit,
		@c19 bit,
		@c20 text,
		@c21 bit,
		@c22 bit,
		@c23 varchar(30),
		@c24 datetime,
		@c25 varchar(8),
		@c26 varchar(30),
		@c27 datetime,
		@c28 varchar(8),
		@c29 bit,
		@c30 bit,
		@c31 char(25),
		@c32 varchar(200),
		@c33 numeric(3,0),
		@c34 bit,
		@c35 numeric(1,0),
		@c36 numeric(1,0),
		@c37 numeric(5,2),
		@c38 numeric(18,5),
		@c39 numeric(19,6),
		@c40 numeric(1,0),
		@c41 varchar(18),
		@c42 varchar(18),
		@c43 varchar(18),
		@c44 numeric(18,5),
		@c45 numeric(19,6),
		@c46 bit,
		@c47 numeric(10,0),
		@c48 bit,
		@c49 bit,
		@c50 text,
		@c51 bit,
		@c52 bit,
		@c53 varchar(30),
		@c54 datetime,
		@c55 varchar(8),
		@c56 varchar(30),
		@c57 datetime,
		@c58 varchar(8),
		@c59 bit,
		@c60 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c33 = @c3)
begin 
update [PG].[tpvl] set
		[tpvlstamp] = @c31,
		[design] = @c32,
		[tpvlno] = @c33,
		[inativo] = @c34,
		[tipo] = @c35,
		[tipodesc] = @c36,
		[descperc] = @c37,
		[descval] = @c38,
		[edescval] = @c39,
		[tipost] = @c40,
		[refft] = @c41,
		[refst] = @c42,
		[familia] = @c43,
		[valmin] = @c44,
		[evalmin] = @c45,
		[obrigacl] = @c46,
		[dias] = @c47,
		[pedechoferno] = @c48,
		[temexpr] = @c49,
		[expressao] = @c50,
		[eprg] = @c51,
		[exprlinha] = @c52,
		[ousrinis] = @c53,
		[ousrdata] = @c54,
		[ousrhora] = @c55,
		[usrinis] = @c56,
		[usrdata] = @c57,
		[usrhora] = @c58,
		[marcada] = @c59,
		[u_cartaocl] = @c60
	where [tpvlno] = @c3
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpvlno] = ' + convert(nvarchar(100),@c3,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tpvl]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[tpvl] set
		[tpvlstamp] = @c31,
		[design] = @c32,
		[inativo] = @c34,
		[tipo] = @c35,
		[tipodesc] = @c36,
		[descperc] = @c37,
		[descval] = @c38,
		[edescval] = @c39,
		[tipost] = @c40,
		[refft] = @c41,
		[refst] = @c42,
		[familia] = @c43,
		[valmin] = @c44,
		[evalmin] = @c45,
		[obrigacl] = @c46,
		[dias] = @c47,
		[pedechoferno] = @c48,
		[temexpr] = @c49,
		[expressao] = @c50,
		[eprg] = @c51,
		[exprlinha] = @c52,
		[ousrinis] = @c53,
		[ousrdata] = @c54,
		[ousrhora] = @c55,
		[usrinis] = @c56,
		[usrdata] = @c57,
		[usrhora] = @c58,
		[marcada] = @c59,
		[u_cartaocl] = @c60
	where [tpvlno] = @c3
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpvlno] = ' + convert(nvarchar(100),@c3,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tpvl]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','tpvl',@c1, 'tpvlstamp', ''