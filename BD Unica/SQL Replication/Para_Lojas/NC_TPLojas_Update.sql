USE [LOJA_TESTE]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSupd_dbotp]    Script Date: 31/01/2023 16:44:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[NC_TPLojas_Update]
		@c1 char(25),
		@c2 numeric(1,0),
		@c3 varchar(55),
		@c4 numeric(3,0),
		@c5 numeric(3,0),
		@c6 numeric(3,0),
		@c7 numeric(6,2),
		@c8 numeric(6,2),
		@c9 numeric(6,2),
		@c10 numeric(6,2),
		@c11 numeric(3,0),
		@c12 numeric(1,0),
		@c13 varchar(20),
		@c14 varchar(30),
		@c15 datetime,
		@c16 varchar(8),
		@c17 varchar(30),
		@c18 datetime,
		@c19 varchar(8),
		@c20 bit,
		@c21 bit,
		@c22 bit,
		@c23 varchar(20),
		@c24 varchar(3),
		@c25 varchar(4),
		@c26 bit,
		@c27 char(25),
		@c28 numeric(1,0),
		@c29 varchar(55),
		@c30 numeric(3,0),
		@c31 numeric(3,0),
		@c32 numeric(3,0),
		@c33 numeric(6,2),
		@c34 numeric(6,2),
		@c35 numeric(6,2),
		@c36 numeric(6,2),
		@c37 numeric(3,0),
		@c38 numeric(1,0),
		@c39 varchar(20),
		@c40 varchar(30),
		@c41 datetime,
		@c42 varchar(8),
		@c43 varchar(30),
		@c44 datetime,
		@c45 varchar(8),
		@c46 bit,
		@c47 bit,
		@c48 bit,
		@c49 varchar(20),
		@c50 varchar(3),
		@c51 varchar(4),
		@c52 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if not (@c27 = @c1)
begin 
update [PG].[tp] set
		[tpstamp] = @c27,
		[tipo] = @c28,
		[descricao] = @c29,
		[dias1f] = @c30,
		[dias2f] = @c31,
		[dias3f] = @c32,
		[dpct1] = @c33,
		[dpct2] = @c34,
		[dpct3] = @c35,
		[dpct4] = @c36,
		[dias] = @c37,
		[vencimento] = @c38,
		[diames] = @c39,
		[ousrinis] = @c40,
		[ousrdata] = @c41,
		[ousrhora] = @c42,
		[usrinis] = @c43,
		[usrdata] = @c44,
		[usrhora] = @c45,
		[marcada] = @c46,
		[cheque] = @c47,
		[cobrancasdd] = @c48,
		[mesesnaopag] = @c49,
		[formapag] = @c50,
		[paymenttermsid] = @c51,
		[inativo] = @c52
	where [tpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tp]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [PG].[tp] set
		[tipo] = @c28,
		[descricao] = @c29,
		[dias1f] = @c30,
		[dias2f] = @c31,
		[dias3f] = @c32,
		[dpct1] = @c33,
		[dpct2] = @c34,
		[dpct3] = @c35,
		[dpct4] = @c36,
		[dias] = @c37,
		[vencimento] = @c38,
		[diames] = @c39,
		[ousrinis] = @c40,
		[ousrdata] = @c41,
		[ousrhora] = @c42,
		[usrinis] = @c43,
		[usrdata] = @c44,
		[usrhora] = @c45,
		[marcada] = @c46,
		[cheque] = @c47,
		[cobrancasdd] = @c48,
		[mesesnaopag] = @c49,
		[formapag] = @c50,
		[paymenttermsid] = @c51,
		[inativo] = @c52
	where [tpstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[tpstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[tp]', @param2=@primarykey_text, @param3=13233
		End
end 
end 
EXEC dbo.NC_Sinc_Update 'dbo','PG','tp',@c1, 'tpstamp', ''