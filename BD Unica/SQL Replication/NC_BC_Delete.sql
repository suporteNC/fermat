USE [FERMAT]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbobc]    Script Date: 02/02/2023 17:23:12 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_BC_Delete]
		@c1 char(25),
		@c2 varchar(18),
		@c3 varchar(60),
		@c4 char(40),
		@c5 char(25),
		@c6 bit,
		@c7 varchar(60),
		@c8 numeric(13,3),
		@c9 varchar(30),
		@c10 datetime,
		@c11 varchar(8),
		@c12 varchar(30),
		@c13 datetime,
		@c14 varchar(8),
		@c15 bit,
		@c16 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[bc] 
	where [bcstamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[bcstamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[bc]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from bc where bcstamp=@c1