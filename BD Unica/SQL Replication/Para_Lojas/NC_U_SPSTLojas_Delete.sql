USE [fermat]
GO
/****** Object:  StoredProcedure [dbo].[sp_MSdel_dbou_spst]    Script Date: 09/02/2023 18:23:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[NC_U_SPSTLojas_Delete]
		@c1 char(25),
		@c2 char(25),
		@c3 varchar(18),
		@c4 varchar(65),
		@c5 varchar(30),
		@c6 datetime,
		@c7 varchar(8),
		@c8 varchar(30),
		@c9 datetime,
		@c10 varchar(8),
		@c11 bit
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [PG].[u_spst] 
	where [u_spststamp] = @c1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[u_spststamp] = ' + convert(nvarchar(100),@c1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[PG].[u_spst]', @param2=@primarykey_text, @param3=13234
		End
end  
delete from u_spst where u_spststamp=@c1