
select u_sc
replace u_sc.respons with  m.m_chnome 
replace u_sc.ano with year(date())
replace u_sc.estab  with  p_estab
replace u_sc.tipo with 'REPARA��O'
replace u_sc.concluido with .f.

su_sc.pageframe1.page1.obj26.visible = .t.  && listagem de nome
su_sc.pageframe1.page1.obj39.visible  = .f.  && nome 
su_sc.pageframe1.page1.vd.enabled  = .t.  && nome 
su_sc.pageframe1.page1.obj33.visible  = .t. && nova linha
su_sc.fechado.caption = 'Aberto' && concluido
m.lPergunta = dpergunta(3,1,"Qual o tipo do Servi�o de Cliente","","","REPARA��O","ALUGUER MAQUINAS","ENTREGA DOMICILIO")
DO CASE 
	CASE m.lPergunta = 1
		replace u_sc.tipo with  "REPARA��O" in u_sc
		su_sc.pageframe1.page1.vd.enabled = .T. 
		su_sc.pageframe1.page1.navvd.enabled = .T.
	CASE m.lPergunta = 2
		replace u_sc.tipo with  "ALUGUER MAQUINAS" in u_sc
		su_sc.pageframe1.page1.vd.enabled = .f. 
		su_sc.pageframe1.page1.navvd.enabled = .f.
	CASE m.lPergunta = 3
		replace u_sc.tipo with  "ENTREGA DOMICILIO" in u_sc
		su_sc.pageframe1.page1.vd.enabled = .T. 
		su_sc.pageframe1.page1.navvd.enabled = .T.
endcase
	
select u_sc
replace u_sc.dtfecho with date(1900,01,01)
return date()

